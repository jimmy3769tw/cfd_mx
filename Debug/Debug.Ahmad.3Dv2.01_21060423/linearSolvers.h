/**
 * @file        linearSolvers.h
 *
 * @project     3D N-S solver rebuild
 * @version     2.0beta
 *
 * @author      Syed Ahmad Raza (sahmadrz@gmail.com)
 * @date        2020-10-27
 *
 * @brief       Linear solvers and functions to solve the pressure Poisson
 *              equation for the Navier-Stokes solver.
 *
 */

#ifndef LINEARSOLVERS_H
#define LINEARSOLVERS_H

void bCGSTAB();
void multiplyMatrixVector(double* prd,
    const double* mat, const double* vec);

#endif
