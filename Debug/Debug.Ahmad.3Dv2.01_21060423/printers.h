/**
 * @file        printers.h
 *
 * @project     3D N-S solver rebuild
 * @version     2.0beta
 *
 * @author      Syed Ahmad Raza (sahmadrz@gmail.com)
 * @date        2020-10-27
 *
 * @brief       Functions for printing the progress of the Navier-Stokes to the
 *              terminal output during execution of the solver.
 *
 */

#ifndef PRINTERS_H
#define PRINTERS_H

#include <ostream>

void printCaseInfo(std::ostream& out);
void printSimulationStatus(std::ostream& out);
void printerProgress();
void printerInfoEnd();

#endif
