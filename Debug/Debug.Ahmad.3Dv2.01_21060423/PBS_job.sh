#!/bin/bash

module purge
module load gcc/9.3.0
module list

cd $PBS_O_WORKDIR

cat $PBS_NODEFILE
echo $PBS_O_WORKDIR
date

./cppNSSolver3D

#qstat -xs