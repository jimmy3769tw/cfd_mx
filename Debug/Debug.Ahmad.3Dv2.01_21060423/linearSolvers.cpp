/**
 * @file        linearSolvers.cpp
 *
 * @project     3D N-S solver rebuild
 * @version     2.0beta
 *
 * @author      Syed Ahmad Raza (sahmadrz@gmail.com)
 * @date        2020-10-27
 *
 * @brief       Linear solvers and functions to solve the pressure Poisson
 *              equation for the Navier-Stokes solver.
 *
 */

#include "linearSolvers.h"
#include "parameters.h"
#include "printers.h"




using Eigen::Matrix, Eigen::MatrixXd, Eigen::VectorXd,
    std::sqrt, std::isnan, std::isinf, std::abs;

double normVector(const double* x) {
    double result = 0.0;
    #pragma omp parallel for default(none) firstprivate(x) reduction(+:result)
    for (int h=0; h<ndim; ++h) {
        result += *(x+h)**(x+h);
    }
    return sqrt(result);
}

double dotProductVectors(const double* x, const double* y) {
    double result = 0.0;
    #pragma omp parallel for default(none) firstprivate(x, y)\
        reduction(+:result)
    for (int h=0; h<ndim; ++h) {
        result += *(x+h)**(y+h);
    }
    return result;
}

void multiplyMatrixVector(double* prd,
    const double* mat, const double* vec) {
    #pragma omp parallel for default(none) firstprivate(prd, vec, mat)
    for (int r=0; r<ndim; r++) {
        double sum = 0.0;
        if (r-nx*ny >= 0)   { sum += *(mat+r+0*ndim)**(vec+r-nx*ny); }
        if (r-nx >= 0)      { sum += *(mat+r+1*ndim)**(vec+r-nx);    }
        if (r-1 >= 0)       { sum += *(mat+r+2*ndim)**(vec+r-1);     }
                              sum += *(mat+r+3*ndim)**(vec+r);
        if (r+1 < ndim)     { sum += *(mat+r+4*ndim)**(vec+r+1);     }
        if (r+nx < ndim)    { sum += *(mat+r+5*ndim)**(vec+r+nx);    }
        if (r+nx*ny < ndim) { sum += *(mat+r+6*ndim)**(vec+r+nx*ny); }
        *(prd+r) = sum;
    }
}


void bCGSTAB() {
    VectorXd
        R0(ndim), R1(ndim), P1(ndim), S1(ndim), AP(ndim), AS(ndim),
        PX1(ndim), PX2(ndim);
    double
        * pa = PA.data(), * pb = PB.data(), * r0 = R0.data(), * r1 = R1.data(),
        * p1 = P1.data(), * s1 = S1.data(), * ap = AP.data(), * as = AS.data(),
        * px = PX.data(), * px1= PX1.data(),* px2= PX2.data();
    double
        r1r0 = 0, r1r0l = 0, a = 0, w = 0, b = 0, norm0 = normVector(pb);

    multiplyMatrixVector(r0, pa, px);
    #pragma omp parallel for default(none)\
        firstprivate(px, px1, px2, r0, pb, r1, p1)
    for (int l = 0; l < ndim; ++l) {
        *(px2+l) = *(px1+l);
        *(px1+l) = *(px+l);
        *(r0+l)  = *(pb+l) - *(r0+l);
        *(r1+l)  = *(r0+l);
        *(p1+l)  = *(r0+l);
    }
    r1r0 = dotProductVectors(r1, r0);
    pIter = 0;
    while (1) {
        multiplyMatrixVector(ap, pa, p1);
        a  = r1r0 / dotProductVectors(ap, r0);

        #pragma omp parallel for default(none) firstprivate(s1, r1, ap, a)
        for (int l=0; l<ndim; ++l) {
            *(s1+l) = *(r1+l) - a**(ap+l);
        }

        multiplyMatrixVector(as, pa, s1);
        w  = dotProductVectors(as, s1) / dotProductVectors(as, as);

        #pragma omp parallel for default(none)\
            firstprivate(px, p1, s1, r1, as, a, w)
        for (int l=0; l<ndim; ++l) {
            *(px+l) += a**(p1+l) + w**(s1+l);
            *(r1+l) = *(s1+l) - w**(as+l);
        }

        // Check for terminating conditions
        if (normVector(r1)/norm0 < pResNormTol) { break; }

        // Check for reset of bCGSTAB
        if (pIter >= pIterMax) {
            int checkReset = 0;
            #pragma omp parallel for default(none) firstprivate(px, px1)\
                reduction(+:checkReset)
            for (int l=0; l<ndim; ++l) {
                if (isnan(*(px+l)) || isinf(*(px+l))) {
                    *(px+l) = *(px1+l), ++checkReset;
                }
            }
            if (checkReset > ndim*0.1) {
                #pragma omp parallel for default(none) firstprivate(px, px2)
                for (int l=0; l<ndim; ++l) {
                    *(px+l) = *(px2+l);
                }
                pResetMembers += ndim, ++pResets;
            } else if (checkReset != 0) {
                pResetMembers += checkReset, ++pResets;
            }
            break;
        }

        r1r0l = r1r0;
        r1r0 = dotProductVectors(r1, r0);
        b  = (a / w) * (r1r0 / r1r0l);

        // Check rho for restart of bCGSTAB
        if (timestep < 10 || abs(r1r0l) > pResNormTol*pRestartFactor) {
            #pragma omp parallel for default(none)\
                firstprivate(p1, r1, ap, b, w)
            for (int l=0; l<ndim; ++l) {
                *(p1+l) = *(r1+l) + b*(*(p1+l) - w**(ap+l)); }
        }
        else {
            #pragma omp parallel for default(none) firstprivate(r0, r1, p1)
            for (int l = 0; l<ndim; ++l) {
                *(r0+l) = *(r1+l);
                *(p1+l) = *(r1+l);
            }
            ++pRestarts;
        }
        ++pIter;
        ++pIterTotal;
    }
}