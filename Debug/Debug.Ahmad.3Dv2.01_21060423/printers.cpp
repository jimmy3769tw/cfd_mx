/**
 * @file        printers.cpp
 *
 * @project     3D N-S solver rebuild
 * @version     2.0beta
 *
 * @author      Syed Ahmad Raza (sahmadrz@gmail.com)
 * @date        2020-10-27
 *
 * @brief       Functions for printing the progress of the Navier-Stokes to the
 *              terminal output during execution of the solver.
 *
 */

#include "printers.h"
#include "parameters.h"
#include "conditions.h"

using std::round, std::cout, std::endl, std::left, std::right, std::setw,
    std::setfill, std::ostream;

void printCaseInfo(ostream& out) {
    int sz = 32;    // Description column size
    out << left
        << "Finite Volume CFD code with direct-forcing IB method\n"
        << "=================================================================\n"
        << '\n'
        << "Case characteristics\n"
        << "-----------------------------------------------------------------\n"
        << '\n'
        << "Grid description\n"
        << "-----------------\n"
        << setw(sz) << "2D or 3D" << is2Dor3DStr.at(is2Dor3D)            << '\n'
        << setw(sz) << "Grid type" << gridStr.at(gridType)               << '\n'
        << '\n'
        << setw(sz) << "x-distance"                     << xDist         << '\n'
        << setw(sz) << "y-distance"                     << yDist         << '\n'
        << setw(sz) << "z-distance"                     << zDist         << '\n'
        << setw(sz) << "x-direction grids"              << nx            << '\n'
        << setw(sz) << "y-direction grids"              << ny            << '\n'
        << setw(sz) << "z-direction grids"              << nz            << '\n'
        << setw(sz) << "x-direction subgrids"           << nxSub         << '\n'
        << setw(sz) << "y-direction subgrids"           << nySub         << '\n'
        << setw(sz) << "z-direction subgrids"           << nzSub         << '\n'
        << '\n'
        << setw(sz) << "Coarse grid x-dist. (initial)"  << xDistLrgI     << '\n'
        << setw(sz) << "Fine grid x-dist."              << xDistSml      << '\n'
        << setw(sz) << "Coarse grid y-dist. (initial)"  << yDistLrgI     << '\n'
        << setw(sz) << "Fine grid y-dist."              << yDistSml      << '\n'
        << setw(sz) << "Coarse grid z-dist. (initial)"  << zDistLrgI     << '\n'
        << setw(sz) << "Fine grid z-dist."              << zDistSml      << '\n'
        << '\n'
        << setw(sz) << "Coarse grids x-dir. (initial)"  << nxLrgI        << '\n'
        << setw(sz) << "Fine grids x-dir."              << nxSml         << '\n'
        << setw(sz) << "Coarse grids y-dir. (initial)"  << nyLrgI        << '\n'
        << setw(sz) << "Fine grids y-dir."              << nySml         << '\n'
        << setw(sz) << "Coarse grids z-dir. (initial)"  << nzLrgI        << '\n'
        << setw(sz) << "Fine grids z-dir."              << nzSml         << '\n'
        << '\n'
        << "Structures description\n"
        << "-----------------------\n";
        printStructures(out);
    out << '\n'
        << "Initial and boundary conditions\n"
        << "--------------------------------\n"
        << setw(sz) << "Initial conditions"
                            << "Pressure:   " << initials[0]            << "\n"
        << setw(sz) << ""   << "x-velocity: " << initials[1]            << "\n"
        << setw(sz) << ""   << "y-velocity: " << initials[2]            << "\n"
        << setw(sz) << ""   << "z-velocity: " << initials[3]            <<"\n\n"

        << setw(sz) << "Pressure boundary conditions"
        << "West:  " << boundTypeStr.at(presBoundaries.conditions.west) << ", "
        << "East:  " << boundTypeStr.at(presBoundaries.conditions.east) << ",\n"
        << setw(sz) << ""
        << "South: " << boundTypeStr.at(presBoundaries.conditions.south)<< ", "
        << "North: " << boundTypeStr.at(presBoundaries.conditions.north)<< ",\n"
        << setw(sz) << ""
        << "Back:  " << boundTypeStr.at(presBoundaries.conditions.back) << ", "
        << "Front: " << boundTypeStr.at(presBoundaries.conditions.front)<<"\n\n"

        << setw(sz) << "x-vel. boundary conditions"
        << "West:  " << boundTypeStr.at(uVelBoundaries.conditions.west) << ", "
        << "East:  " << boundTypeStr.at(uVelBoundaries.conditions.east) << ",\n"
        << setw(sz) << ""
        << "South: " << boundTypeStr.at(uVelBoundaries.conditions.south)<< ", "
        << "North: " << boundTypeStr.at(uVelBoundaries.conditions.north)<< ",\n"
        << setw(sz) << ""
        << "Back:  " << boundTypeStr.at(uVelBoundaries.conditions.back) << ", "
        << "Front: " << boundTypeStr.at(uVelBoundaries.conditions.front)<<"\n\n"

        << setw(sz) << "y-vel. boundary conditions"
        << "West:  " << boundTypeStr.at(vVelBoundaries.conditions.west) << ", "
        << "East:  " << boundTypeStr.at(vVelBoundaries.conditions.east) << ",\n"
        << setw(sz) << ""
        << "South: " << boundTypeStr.at(vVelBoundaries.conditions.south)<< ", "
        << "North: " << boundTypeStr.at(vVelBoundaries.conditions.north)<< ",\n"
        << setw(sz) << ""
        << "Back:  " << boundTypeStr.at(vVelBoundaries.conditions.back) << ", "
        << "Front: " << boundTypeStr.at(vVelBoundaries.conditions.front)<<"\n\n"

        << setw(sz) << "z-vel. boundary conditions"
        << "West:  " << boundTypeStr.at(wVelBoundaries.conditions.west) << ", "
        << "East:  " << boundTypeStr.at(wVelBoundaries.conditions.east) << ",\n"
        << setw(sz) << ""
        << "South: " << boundTypeStr.at(wVelBoundaries.conditions.south)<< ", "
        << "North: " << boundTypeStr.at(wVelBoundaries.conditions.north)<< ",\n"
        << setw(sz) << ""
        << "Back:  " << boundTypeStr.at(wVelBoundaries.conditions.back) << ", "
        << "Front: " << boundTypeStr.at(wVelBoundaries.conditions.front)<<"\n\n"

        << setw(sz) << "Press. Dirichlet values"
        << "West:  "
        << (presBoundaries.conditions.west!=BoundaryType::Dirichlet ? "-" :
                to_string(presBoundaries.values.west).substr(0,1))      << ", "
        << "East:  "
        << (presBoundaries.conditions.east!=BoundaryType::Dirichlet ? "-" :
                to_string(presBoundaries.values.east).substr(0,1))      << ",\n"
        << setw(sz) << ""
        << "South: "
        << (presBoundaries.conditions.south!=BoundaryType::Dirichlet? "-" :
                to_string(presBoundaries.values.south).substr(0,1))     << ", "
        << "North: "
        << (presBoundaries.conditions.north!=BoundaryType::Dirichlet? "-" :
                to_string(presBoundaries.values.north).substr(0,1))     << ",\n"
        << setw(sz) << ""
        << "Back:  "
        << (presBoundaries.conditions.back!=BoundaryType::Dirichlet ? "-" :
                to_string(presBoundaries.values.back).substr(0,1))      << ", "
        << "Front: "
        << (presBoundaries.conditions.front!=BoundaryType::Dirichlet? "-" :
                to_string(presBoundaries.values.front).substr(0,1))     <<"\n\n"

        << setw(sz) << "x-vel. Dirichlet values"
        << "West:  "
        << (uVelBoundaries.conditions.west!=BoundaryType::Dirichlet ? "-" :
                to_string(uVelBoundaries.values.west).substr(0,1))      << ", "
        << "East:  "
        << (uVelBoundaries.conditions.east!=BoundaryType::Dirichlet ? "-" :
                to_string(uVelBoundaries.values.east).substr(0,1))      << ",\n"
        << setw(sz) << ""
        << "South: "
        << (uVelBoundaries.conditions.south!=BoundaryType::Dirichlet? "-" :
                to_string(uVelBoundaries.values.south).substr(0,1))     << ", "
        << "North: "
        << (uVelBoundaries.conditions.north!=BoundaryType::Dirichlet? "-" :
                to_string(uVelBoundaries.values.north).substr(0,1))     << ",\n"
        << setw(sz) << ""
        << "Back:  "
        << (uVelBoundaries.conditions.back!=BoundaryType::Dirichlet ? "-" :
                to_string(uVelBoundaries.values.back).substr(0,1))      << ", "
        << "Front: "
        << (uVelBoundaries.conditions.front!=BoundaryType::Dirichlet? "-" :
                to_string(uVelBoundaries.values.front).substr(0,1))     <<"\n\n"

        << setw(sz) << "y-vel. Dirichlet values"
        << "West:  "
        << (vVelBoundaries.conditions.west!=BoundaryType::Dirichlet ? "-" :
                to_string(vVelBoundaries.values.west).substr(0,1))      << ", "
        << "East:  "
        << (vVelBoundaries.conditions.east!=BoundaryType::Dirichlet ? "-" :
                to_string(vVelBoundaries.values.east).substr(0,1))      << ",\n"
        << setw(sz) << ""
        << "South: "
        << (vVelBoundaries.conditions.south!=BoundaryType::Dirichlet? "-" :
                to_string(vVelBoundaries.values.south).substr(0,1))     << ", "
        << "North: "
        << (vVelBoundaries.conditions.north!=BoundaryType::Dirichlet? "-" :
                to_string(vVelBoundaries.values.north).substr(0,1))     << ",\n"
        << setw(sz) << ""
        << "Back:  "
        << (vVelBoundaries.conditions.back!=BoundaryType::Dirichlet ? "-" :
                to_string(vVelBoundaries.values.back).substr(0,1))      << ", "
        << "Front: "
        << (vVelBoundaries.conditions.front!=BoundaryType::Dirichlet? "-" :
                to_string(vVelBoundaries.values.front).substr(0,1))     <<"\n\n"

        << setw(sz) << "z-vel. Dirichlet values"
        << "West:  "
        << (wVelBoundaries.conditions.west!=BoundaryType::Dirichlet ? "-" :
                to_string(wVelBoundaries.values.west).substr(0,1))      << ", "
        << "East:  "
        << (wVelBoundaries.conditions.east!=BoundaryType::Dirichlet ? "-" :
                to_string(wVelBoundaries.values.east).substr(0,1))      << ",\n"
        << setw(sz) << ""
        << "South: "
        << (wVelBoundaries.conditions.south!=BoundaryType::Dirichlet? "-" :
                to_string(wVelBoundaries.values.south).substr(0,1))     << ", "
        << "North: "
        << (wVelBoundaries.conditions.north!=BoundaryType::Dirichlet? "-" :
                to_string(wVelBoundaries.values.north).substr(0,1))     << ",\n"
        << setw(sz) << ""
        << "Back:  "
        << (wVelBoundaries.conditions.back!=BoundaryType::Dirichlet ? "-" :
                to_string(wVelBoundaries.values.back).substr(0,1))      << ", "
        << "Front: "
        << (wVelBoundaries.conditions.front!=BoundaryType::Dirichlet? "-" :
                to_string(wVelBoundaries.values.front).substr(0,1))     <<"\n\n"
        << '\n'
        << "Simulation description\n"
        << "-----------------------\n"
        << setw(sz) << "Reynolds number"          << reynolds           << '\n'
        << setw(sz) << "VIV: reduced velocity"    << reducedVel         << '\n'
        << setw(sz) << "VIV: mass ratio"          << massRatio          << '\n'
        << setw(sz) << "VIV: structural damping ratio"<< structDamping  << '\n'
        << setw(sz) << "Turbulence: Smagorinsky const." << cS           << '\n'
        << setw(sz) << "Override timestep size"   << dtOverride         << '\n'
        << setw(sz) << "CFL factor"               << cflFactor          << '\n'
        << setw(sz) << "Grid Fo factor"           << gridFoFactor       << '\n'
        << setw(sz) << "Minimum timestep size"    << dtMin              << '\n'
        << setw(sz) <<"u-velocity convergance criteria"<< uChangeCriteria <<'\n'
        << setw(sz) <<"v-velocity convergance criteria"<< vChangeCriteria <<'\n'
        << setw(sz) <<"w-velocity convergance criteria"<< wChangeCriteria <<'\n'
        << setw(sz) << "Pres. residual norm tolerance" << pResNormTol   << '\n'
        << setw(sz) << "Pressure restart factor" << pRestartFactor  << '\n'
        << '\n'
        << setw(sz) << "Number of OMP threads"    << threadsOMPCount    << '\n'
        << setw(sz) << "VIV: Timesteps struct. paused"<< timestepsPaused <<'\n'
        << setw(sz) << "Max. timesteps"           << timestepMax        << '\n'
        << setw(sz) << "Max. pressure iterations" << pIterMax           << '\n'
        << '\n'
        << setw(sz) << "Conv. velocity scheme" << convVelStr.at(convVelScheme)
                                                                        <<'\n'
        << setw(sz) << "Numerical scheme" << numSchemeStr.at(numericalScheme)
                                                                        <<'\n'
        << setw(sz) << "Solution state" << solStateStr.at(steadiness)   <<'\n'
        << setw(sz) << "VIV simulation" << polarQStr.at(simulateVIV)    <<'\n'
        << setw(sz) << "Flow direction" << flowDirStr.at(flowDir)       <<'\n'
        << setw(sz) << "Oscillation direction" << oscilDirStr.at(oscillationDir)
                                                                        <<'\n'
        << setw(sz) << "Turbulence model" <<turbulenceStr.at(turbulenceModel)
        << "\n\n"
        << "Filer description\n"
        << "------------------\n"
        << setw(sz) << "File interval timesteps"  << fileInterv         << '\n'
        << setw(sz) << "Number of animation datasets"<< animationCount  << '\n'
        << setw(sz) << "End simulation after animation"
            << polarQStr.at(endSimAfterAnim)  << '\n';
        if constexpr (animationCount > 0) {
            // Animation filing at frequent intervals ------------------------//
            for (Animation* anim : Animation::getSet()) {
                out << setw(sz) << "Anim. " << anim->getId()
                    << " begin timestep" << anim->m_bgnAnimTimestep << '\n'
                    << setw(sz) << "Anim. " << anim->getId()
                    << " file interval" << anim->m_animFileInterv << '\n'
                    << setw(sz) << "Anim. " << anim->getId()
                    << " number of files" << anim->m_endAnimSolFiles << '\n';
            }
        }
    out << '\n'
        << setw(sz) << "Unique name for solution" << fileUniqueName     << '\n'
        << setw(sz) << "Remarks on solution"      << remarks            << '\n'
        << "_________________________________________________________________\n"
        << endl;
}

void printSimulationStatus(ostream& out) {
    int sz = 32;    // Description column size
    out << "simulation status\n"
        << "-----------------------------------------------------------------\n"
        << '\n'
        << setw(sz) << "Maximum change in u"        << uChangeMax       << '\n'
        << setw(sz) << "Maximum change in v"        << vChangeMax       << '\n'
        << setw(sz) << "Maximum change in w"        << wChangeMax       << '\n'
        << setw(sz) << "Maximum mass residual"      << mChangeMax       << '\n'
        << '\n'
        << setw(sz) << "Maximum change in pressure" << pChangeMax       << '\n'
        << setw(sz) << "Pressure iterations"        << pIter            << '\n'
        << setw(sz) << "Total pr. iterations"       << pIterTotal       << '\n'
        << setw(sz) << "Pressure restarts"          << pRestarts        << '\n'
        << setw(sz) << "Pressure resets"            << pResets          << '\n'
        << setw(sz) << "Pr. reset total members"    << pResetMembers    << '\n'
        << '\n'
        << setw(sz) << "Timestep size"              << dt               << '\n'
        << setw(sz) << "Simulation time"            << simTime          << '\n'
        << setw(sz) << "Timesteps"                  << timestep         << '\n'
        << '\n'
        << setw(sz) << "Computational run time"
                                << runTime                       << " sec. | "
                                << round((runTime/60)*10)/10     << " min. | "
                                << round((runTime/3600)*100)/100 << " hrs.\n"
        << "_________________________________________________________________\n"
        << endl;
}

void printerProgress() {
    // Print column headers
    if (timestep % 24 == 0 || timestep == 1)
    {
        cout<< right
            << setfill('-') << setw(103) << "" << setfill(' ')
            << '\n'
            << "| "
            << setw(10) << "timestep |"
            << setw(13) << "dt |"
            << setw(8)  << "pIter |"
            << setw(14) << "mChangeMax |"
            << setw(14) << "pChangeMax |"
            << setw(14) << "uChangeMax |"
            << setw(14) << "vChangeMax |"
            << setw(14) << "wChangeMax |"
            << '\n'
            << setfill('=') << setw(103) << "" << setfill(' ')
            << endl;
    }
    // Print timestep data
    cout<< "| "
        << setw(8)  << timestep     << " |"
        << setw(11) << dt           << " |"
        << setw(6)  << pIter        << " |"
        << setw(12) << mChangeMax   << " |"
        << setw(12) << pChangeMax   << " |"
        << setw(12) << uChangeMax   << " |"
        << setw(12) << vChangeMax   << " |"
        << setw(12) << wChangeMax   << " |"
        << endl;
}

void printerInfoEnd() {
    // Write heading and column headers for case characteristics
    printCaseInfo(cout);
    printSimulationStatus(cout);
}
