/**
 * @file        updaters.h
 *
 * @project     3D N-S solver rebuild
 * @version     2.0beta
 *
 * @author      Syed Ahmad Raza (sahmadrz@gmail.com)
 * @date        2020-10-27
 *
 * @brief       Update the pressure and velocity values as per the calculations
 *              in the current timestep of the Navier-Stokes solver.
 *
 */
#include <tuple>
#include <vector>
#include "matrix/CSR_sparseMatrix.hpp"

#ifndef UPDATERS_H
#define UPDATERS_H

void massVectorConstructor();
void massVectorConstructor_vector(std::vector<double> &b_mat);

void pressureMatrixConstructor();

// std::tuple<std::vector<int>, std::vector<int>, std::vector<double> >
// pressureMat_CSR();

void pressureMatrixConstructor_CSR( mat::CSR_matrix<double> &mat );

void pressureUpdater();
void velocityUpdater();

#endif
