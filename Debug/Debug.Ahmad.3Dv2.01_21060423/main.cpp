/**
 * @file        main.cpp
 *
 * @project     3D N-S solver
 * @version     2.0beta
 *
 * @author      Syed Ahmad Raza (sahmadrz@gmail.com)
 * @date        2020-10-27
 *
 * @brief       The main solver file outlining the algorithm of the Navier-
 *              Stokes solver.
 *
 * @detail      3D N-S solver with turbulence model. The current version
 *              is a fully working 3D N-S solver for nonuniform grid with
 *              parallelization using openMP, adaptive timestepping,
 *              BiCGSTAB method for pressure Poisson equation and data
 *              structures from the Eigen library. Smagorinsky-Lilly turbulence
 *              model has also been incorporated. A triuniform gridder is also
 *              fully implemented with two modes of operation. Fixed structures
 *              can be modeled as well as structures oscillating transversely
 *              due to vortex induced vibration.
 */

#include "parameters.h"
#include "structures.h"
#include "gridders.h"
#include "conditions.h"
#include "timestepper.h"
#include "velocities.h"
#include "linearSolvers.h"
#include "updaters.h"
#include "filers.h"
#include "printers.h"
#include <dirent.h>             // opendir to check that directory exists
#include <ctime>                // to time the script
#include <chrono>               // to measure and display the time duration

int
    timestep{0}, pIter{0}, pIterTotal{0}, pRestarts{0}, pResets{0},
    pResetMembers{0};
double
    dt{0}, simTime{0}, runTime{0},              // time varuables
    dxMin{0}, dyMin{0}, dzMin{0},               // minimum grid distances
    minConvX{100}, minConvY{100}, minConvZ{100},
    mChangeMax{0}, pChangeMax{0},
    uChangeMax{100}, vChangeMax{100}, wChangeMax{100};
VectorXd
    X(nx+1), Y(ny+1), Z(nz+1),
    DX(nxt), DY(nyt), DZ(nzt), DXM(nxt-1), DYM(nyt-1), DZM(nzt-1),
    DDX(nxt), DDY(nyt), DDZ(nzt), DDXM(nxt-1), DDYM(nyt-1), DDZM(nzt-1),
    PJ(ndim), PB(ndim), PX(ndim),
    Fij(nzt);
MatrixXd
    PA(ndim, ddim), PM(ndim, ddim),
    Fi(nyt, nzt);
TensorFixedSize<double, Sizes<nxt, nyt, nzt>>
    U, V, W, US, VS, WS, DUS1, DVS1, DWS1, DUS2, DVS2, DWS2, P,
    VELSX, VELSY, VELSZ,    // structure velocity array
    UGRD, VGRD, WGRD,       // grid variable arrays
    IVFU, IVBU, IWNU, IWSU, IUFV, IUBV, IWEV, IWWV, IUNW, IUSW, IVEW, IVWW,
        // interpolation factors for cross-interpolation required in turbulence
    ETA, FX, FY, FZ;        // volume of solid and virtual force arrays

// Update runtime
using std::chrono::steady_clock, std::chrono::duration_cast;
inline void updateRunTime(steady_clock::time_point startTime) {
    steady_clock::time_point endTime {steady_clock::now()};
    runTime = duration_cast<std::chrono::seconds>(endTime - startTime).count();
}

int main() {
    // Check the directory and create, if not present ------------------------//
    if (opendir((filePath).c_str()) == NULL) {
        if (opendir(parentPath.c_str()) == NULL) {
            if (system(("mkdir " + parentPath).c_str()) != 0) { return 1; }
        }
        if (system(("mkdir "+ filePath).c_str()) != 0) { return 1; }
    }

    // Set the number of threads for OpenMP ----------------------------------//
    #ifdef _OPENMP
    omp_set_dynamic(0);
    omp_set_num_threads(threadsOMPCount);
    #endif

    // File the case characteristics and note the starting time --------------//
    filerInfoStart();
    if constexpr (steadiness != SolState::none) {
        filerTimestepDataStart();
    }
    steady_clock::time_point tStart {steady_clock::now()};

    // Create the grid -------------------------------------------------------//
    if constexpr (gridType == Grid::triuniform)      { triUniformGridder(); }
    else if constexpr (gridType == Grid::nonuniform) { nonUniformGridder(); }
    else if constexpr (gridType == Grid::uniform)    { uniformGridder(); }

    // Create all the structures ---------------------------------------------//
    constructStructures();

    // Initialize the simulation variables for timestepping ------------------//
    if constexpr (dtOverride > 0) { dt = dtOverride; }
    initialConditions(initials);
    boundaryConditions(presBoundaries,
                        uVelBoundaries, vVelBoundaries, wVelBoundaries);
    pressureMatrixConstructor();                                // PA

    bool velChangeCriteria {true};
    // Main simulation loop --------------------------------------------------//
    while (timestep < timestepMax && (velChangeCriteria || timestep < 10)) {

        if constexpr (dtOverride < 0) { timestepper(); }
        ++timestep, simTime += dt;
        bool timestepFiled {false};

        // Simulate VIV oscillating structures -------------------------------//
        if constexpr (steadiness == SolState::unsteady) {
            if constexpr (simulateVIV == PolarQ::yes) {
                if (timestep > timestepsPaused) { updateStructures(); }
            }
        }

        interimVelocity(mapSelectedScheme.at(convVelScheme));   // US = U
        massVectorConstructor();                                // PB <- US
        bCGSTAB();                                              // PX <- PA, PB
        pressureUpdater();                                      // P = PX
        velocityUpdater();                                      // U <- US,P
        boundaryConditions(presBoundaries,
                            uVelBoundaries, vVelBoundaries, wVelBoundaries);

        //Calculate unsteady flow parameters ---------------------------------//
        if constexpr (steadiness == SolState::unsteady) {
            updateVirtualForce();
            // Calculate VIV oscillating structure variables -----------------//
            if constexpr (simulateVIV == PolarQ::yes) {
                if (timestep > timestepsPaused) {
                    updateVIV(); //Assuming inlet velocity and diameter as 1.
                }
            }
            filerTimestepData();
        }

        // Filing the solution -----------------------------------------------//
        // Animation filing at frequent intervals ----------------------------//
        if constexpr (animationCount > 0) {
            for (Animation* anim : Animation::getSet()) {
                if (timestep >= anim->m_bgnAnimTimestep
                        && anim->m_animFileCount < anim->m_endAnimSolFiles
                        && timestep%(anim->m_animFileInterv!=0?
                            anim->m_animFileInterv : 1) == 0) {
                    if constexpr (steadiness == SolState::steady) {
                        updateVirtualForce();
                        filerTimestepData();
                    }
                    updateRunTime(tStart);
                    filerSolution(timestep);
                    timestepFiled = true;
                    ++anim->m_animFileCount;
                }
            }
            if constexpr (endSimAfterAnim == PolarQ::yes) {
                const Animation* animLast{Animation::getOneP(animationCount-1)};
                if (animLast->m_animFileCount == animLast->m_endAnimSolFiles) {
                    break;
                }
            }
        }
        // Regular filing ----------------------------------------------------//
        if constexpr (fileInterv > 0) {
            if (timestep%fileInterv == 0 && !timestepFiled) {
                if constexpr (steadiness == SolState::steady) {
                    updateVirtualForce();
                    filerTimestepData();
                }
                updateRunTime(tStart);
                filerSolution(timestep);
            }
        }

        // Print variable states for current timestep ------------------------//
        printerProgress();

        // Check for convergence ---------------------------------------------//
        if constexpr (uChangeCriteria>0
                        || vChangeCriteria>0
                        || wChangeCriteria>0) {
            velChangeCriteria = uChangeMax > uChangeCriteria
                                || vChangeMax > vChangeCriteria
                                || wChangeMax > wChangeCriteria;
        }
    }//------------------------- End of timestepping -------------------------//

    // Record the clock time of simulation run -------------------------------//
    updateRunTime(tStart);

    // File the final solution and print the end states of variables ---------//
    if constexpr (steadiness == SolState::steady) {
        updateVirtualForce();
        filerTimestepData();
    }
    filerSolution();
    printerInfoEnd();

    return 0;
}
