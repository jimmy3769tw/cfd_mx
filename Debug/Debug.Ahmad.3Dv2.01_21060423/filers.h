/**
 * @file        filers.h
 *
 * @project     3D N-S solver rebuild
 * @version     2.0beta
 *
 * @author      Syed Ahmad Raza (sahmadrz@gmail.com)
 * @date        2020-10-27
 *
 * @brief       Functions for filing the solution of the Navier-Stokes solver.
 *
 */

#ifndef FILERS_H
#define FILERS_H

#include <string>

using std::string;

void filerSolution(const int timestep = -1);
void filerInfoStart();
void filerInfoEnd();

#endif