/**
 * @file        timestepper.cpp
 *
 * @project     3D N-S solver rebuild
 * @version     2.0beta
 *
 * @author      Syed Ahmad Raza (sahmadrz@gmail.com)
 * @date        2020-10-27
 *
 * @brief       Determine the size of the timestep for the Navier-Stokes solver.
 *
 */

#include "timestepper.h"
#include "parameters.h"

using std::min, std::abs;

void timestepper()
{
    double uMax, vMax, wMax;
    uMax = vMax = wMax = 0.0;

    if (convVelScheme != ConvVel::central)  // convection is considered
    {
        // minConvX minConvY and minConvZ are determined during last timestep
        // in velocityUpdater(); dxMin, dyMin and dzMin are in gridder().

        dt = min({cflFactor*minConvX,
                  cflFactor*minConvY,
                  cflFactor*minConvZ,
                  gridFoFactor*0.5*reynolds*dxMin*dxMin,
                  gridFoFactor*0.5*reynolds*dyMin*dyMin,
                  gridFoFactor*0.5*reynolds*dzMin*dzMin});
        if (dt < dtMin) { dt = dtMin; }
    }
    else                                    // diffusion is overriding
    {
        #pragma omp parallel for default(none) shared(U, V, W)\
            reduction(max:uMax, vMax, wMax)
        for (int k=gc; k<nzt-gc; ++k)
        {
            for (int j=gc; j<nyt-gc; ++j)
            {
                for (int i=gc; i<nxt-gc; ++i)
                {
                    if (abs(U(i,j,k)) > uMax){ uMax = abs(U(i,j,k)); }
                    if (abs(V(i,j,k)) > vMax){ vMax = abs(V(i,j,k)); }
                    if (abs(W(i,j,k)) > wMax){ wMax = abs(W(i,j,k)); }
                }
            }
        }
        dt = min({cflFactor/(reynolds*uMax*uMax),
                  cflFactor/(reynolds*vMax*vMax),
                  cflFactor/(reynolds*wMax*wMax),
                  gridFoFactor*0.5*reynolds*dxMin*dxMin,
                  gridFoFactor*0.5*reynolds*dyMin*dyMin,
                  gridFoFactor*0.5*reynolds*dzMin*dzMin});
        if (dt < dtMin) { dt = dtMin; }
    }
}