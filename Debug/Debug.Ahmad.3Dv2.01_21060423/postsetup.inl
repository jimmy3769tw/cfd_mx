/**
 * @file        postsetup.inl
 *
 * @project     3D N-S solver rebuild
 * @version     2.0beta
 *
 * @author      Syed Ahmad Raza (sahmadrz@gmail.com)
 * @date        2020-10-27
 *
 * @brief       Hidden declarations of key variables; to be included at the end
 *              of "parameters.h".
 *
 */

#ifndef POSTSETUP_INL
#define POSTSETUP_INL

#include "parameters.h"

//----------------------------------------------------------------------------//
// Non-user variables --------------------------------------------------------//
//----------------------------------------------------------------------------//

inline constexpr int
    ndim = nx*ny*nz, ddim = is2Dor3D==3?7:5,
    nxt = nx + gc*2, nyt = ny + gc*2, nzt = nz + gc*2;

inline const unordered_map<int, string> is2Dor3DStr {
    {2, "2D"}, {3, "3D"}
};
inline const unordered_map<PolarQ, string> polarQStr {
    {PolarQ::no, "no"},
    {PolarQ::yes, "yes"}
};
inline const unordered_map<BoundaryType, string> boundTypeStr {
    {BoundaryType::Neumann, "Neumann"},
    {BoundaryType::Dirichlet, "Dirichlet"},
    {BoundaryType::Periodic, "Periodic"},
    {BoundaryType::Symmetry, "Symmetry"}
};
inline const unordered_map<Grid, string> gridStr {
    {Grid::uniform, "uniform"},
    {Grid::nonuniform, "nonuniform"},
    {Grid::triuniform, "tri-uniform"}
};
inline const unordered_map<ConvVel, string> convVelStr {
    {ConvVel::quick, "QUICK scheme"},
    {ConvVel::central, "Central difference scheme"},
    {ConvVel::upwind, "Upwind scheme"}
};
inline const unordered_map<NumScheme, string> numSchemeStr {
    {NumScheme::euler,              "Euler method"},
    {NumScheme::adamsBashforth2,    "Adams-Bashforth 2nd order method"},
    {NumScheme::adamsBashforth3,    "Adams-Bashforth 3rd order method"},
};
inline const unordered_map<SolState, string> solStateStr {
    {SolState::none, "none"},
    {SolState::steady, "steady"},
    {SolState::unsteady, "unsteady"}
};
inline const unordered_map<FlowDir, string> flowDirStr {
    {FlowDir::x, "x-direction"},
    {FlowDir::y, "y-direction"},
    {FlowDir::z, "z-direction"}
};
inline const unordered_map<OscilDir, string> oscilDirStr {
    {OscilDir::x, "x-direction"},
    {OscilDir::y, "y-direction"},
    {OscilDir::z, "z-direction"}
};
inline const unordered_map<TurbModel, string> turbulenceStr {
    {TurbModel::none, "none"},
    {TurbModel::Smagorinsky, "Smagorinsky"}
};
inline const std::unordered_map<Structures::State, std::string>
    mapStructStateToStr {
    {Structures::State::fixed, "fixed"},
    {Structures::State::moving, "moving"},
};
inline const std::unordered_map<Structures::Kind, std::string>
    mapStructKindToStr {
    {Structures::Kind::sphere, "sphere"},
    {Structures::Kind::cylinderX, "cylinderX"},
    {Structures::Kind::cylinderY, "cylinderY"},
    {Structures::Kind::cylinderZ, "cylinderZ"},
};

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-variable"
namespace Structures {
    constexpr int countAll {
        toUType(Count::Sphere)
        + toUType(Count::CylinderX)
        + toUType(Count::CylinderY)
        + toUType(Count::CylinderZ)
    };
}
#pragma GCC diagnostic pop

inline const string
    fileAffix {
                // "_"+to_string(nx)+"x"+to_string(ny)+"x"+to_string(nz)
                + "_Re" + to_string(static_cast<int>(reynolds))
    },
    parentPath {"../data/"},
    filePath   {parentPath + fileUniqueName + fileAffix + '/'},
    fileName   {fileUniqueName + fileAffix};

//----------------------------------------------------------------------------//
// Variable declarations -----------------------------------------------------//
//----------------------------------------------------------------------------//
// Defined in main.cpp

extern int
    timestep, pIter, pIterTotal, pRestarts, pResets, pResetMembers;
extern double
    dt, simTime, runTime,
    // fx, fy,
    vStruct, //disVIV, velVIV, cylCenter,
    // drag, lift, dragSum, liftSum, dragAvg, liftAvg,
    dxMin, dyMin, dzMin, minConvX, minConvY, minConvZ,
    mChangeMax, pChangeMax, uChangeMax, vChangeMax, wChangeMax;

extern VectorXd
    X, Y, Z, DX, DY, DZ, DXM, DYM, DZM, DDX, DDY, DDZ, DDXM, DDYM, DDZM,
    PJ, PB, PX,Test_out, Test_in,
    // FAij, FBij,
    // FAjk, FBjk,
    Fij;//, Fjk;
extern MatrixXd
    PA, PM,
    // FAi, FBi,
    // FAj, FBj,
    // FAk, FBk,
    Fi, Fj;//, Fk;

extern TensorFixedSize<double, Sizes<nxt, nyt, nzt>>
    U, V, W, US, VS, WS, DUS1, DVS1, DWS1, DUS2, DVS2, DWS2, P,
    VELSX, VELSY, VELSZ, UGRD, VGRD, WGRD,
    IVFU, IVBU, IWNU, IWSU, IUFV, IUBV, IWEV, IWWV, IUNW, IUSW, IVEW, IVWW,
    ETA, FX, FY, FZ;

// inline constexpr int    iBgnC=2, iEndC=213,
//                         jBgnC=2, jEndC=142,
//                         kBgnC=2, kEndC=22;
#endif

/*
inline constexpr double
// Segment lengths for virtual force integration; assumes tri-uniform or
// uniform grid because Simpson's method is used. For tri-uniform grid type,
// the solid must be fully inside the finer grid.
    dxIntgr = (nxSml!=0) ? xDistSml/nxSml : xDist/nx,
    dyIntgr = (nySml!=0) ? yDistSml/nySml : yDist/ny,
    dzIntgr = (nzSml!=0) ? zDistSml/nzSml : zDist/nz;
 */
