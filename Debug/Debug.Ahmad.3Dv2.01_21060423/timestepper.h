/**
 * @file        timestepper.h
 *
 * @project     3D N-S solver rebuild
 * @version     2.0beta
 *
 * @author      Syed Ahmad Raza (sahmadrz@gmail.com)
 * @date        2020-10-27
 *
 * @brief       Determine the size of the timestep for the Navier-Stokes solver.
 *
 */

#ifndef TIMESTEPPER_H
#define TIMESTEPPER_H

void timestepper();

#endif