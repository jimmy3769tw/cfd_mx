/**
 * @file        filers.cpp
 *
 * @project     3D N-S solver rebuild
 * @version     2.0beta
 *
 * @author      Syed Ahmad Raza (sahmadrz@gmail.com)
 * @date        2020-10-27
 *
 * @brief       Functions for filing the solution of the Navier-Stokes solver.
 *
 */

#include "filers.h"
#include "parameters.h"
#include "structures.h"
#include "conditions.h"
#include "printers.h"
#include <limits>               // for "numeric_limits<double>::max_digits10"

using std::ofstream, std::endl, std::left, std::setw,
    std::numeric_limits, std::ios_base;

enum class drctn{ i, j, k };

/*
 * Write a tensor in block format.
*/
inline void writeBlockTensor(ofstream& file,
    const Eigen::TensorFixedSize<double, Eigen::Sizes<nxt, nyt, nzt>>& TNSR,
    const int& iBgn, const int& jBgn, const int& kBgn,
    const int& iEnd, const int& jEnd, const int& kEnd) {
    for (int k=kBgn; k<kEnd; ++k) {
        for (int j=jBgn; j<jEnd; ++j) {
            for (int i=iBgn; i<iEnd; ++i) {
                file << TNSR(i,j,k) << " ";
            }
            file << endl;
        }
    }
}

/*
 * Write a vector in block format, with the given direction of vector.
*/
inline void writeBlockVector(ofstream& file,
    const VectorXd& VCTR,
    const int& iBgn, const int& jBgn, const int& kBgn,
    const int& iEnd, const int& jEnd, const int& kEnd,
    const drctn vectorDrctn) {
    switch (vectorDrctn) {
        case drctn::i:
            for (int k=kBgn; k<kEnd; ++k) {
                for (int j=jBgn; j<jEnd; ++j) {
                    for (int i=iBgn; i<iEnd; ++i) {
                        file << VCTR(i) << " ";
                    }
                    file << endl;
                }
            }
            break;
        case drctn::j:
            for (int k=kBgn; k<kEnd; ++k) {
                for (int j=jBgn; j<jEnd; ++j) {
                    for (int i=iBgn; i<iEnd; ++i) {
                        file << VCTR(j) << " ";
                    }
                    file << endl;
                }
            }
            break;
        case drctn::k:
            for (int k=kBgn; k<kEnd; ++k) {
                for (int j=jBgn; j<jEnd; ++j) {
                    for (int i=iBgn; i<iEnd; ++i) {
                        file << VCTR(k) << " ";
                    }
                    file << endl;
                }
            }
            break;
    }
}

/*
 * Write a tensor in block format, averaged for staggered grid in one direction.
*/
inline void writeBlockTensorStgr(ofstream& file,
        const Eigen::TensorFixedSize<double, Eigen::Sizes<nxt, nyt, nzt>>& TNSR,
        const int& iBgn, const int& jBgn, const int& kBgn,
        const int& iEnd, const int& jEnd, const int& kEnd,
        const drctn staggeredDrctn) {
    switch (staggeredDrctn) {
        case drctn::i:
            for (int k=kBgn; k<kEnd; ++k) {
                for (int j=jBgn; j<jEnd; ++j) {
                    for (int i=iBgn; i<iEnd; ++i) {
                        file << 0.5*(TNSR(i,j,k) + TNSR(i-1,j,k)) << " ";
                    }
                    file << endl;
                }
            }
            break;
        case drctn::j:
            for (int k=kBgn; k<kEnd; ++k) {
                for (int j=jBgn; j<jEnd; ++j) {
                    for (int i=iBgn; i<iEnd; ++i) {
                        file << 0.5*(TNSR(i,j,k) + TNSR(i,j-1,k)) << " ";
                    }
                    file << endl;
                }
            }
            break;
        case drctn::k:
            for (int k=kBgn; k<kEnd; ++k) {
                for (int j=jBgn; j<jEnd; ++j) {
                    for (int i=iBgn; i<iEnd; ++i) {
                        file << 0.5*(TNSR(i,j,k) + TNSR(i,j,k-1)) << " ";
                    }
                    file << endl;
                }
            }
            break;
    }
}

/*
 * File the complete solution with coordinates for Tecplot in ASCII block
 * format.
*/
void filerASCIIBlock(const string& fPath, const string& fName) {
    ofstream file;
    file.setf(ios_base::scientific);
    file.precision(numeric_limits<double>::max_digits10);
    file.open(fPath + fName + ".dat");

    // Tecplot file header ---------------------------------------------------//
    file << "TITLE = \"" << fName + ".dat" << "\"\n"
            "FILETYPE = FULL\n"
            "VARIABLES =\n"
            "\n"
            "X, Y, Z, U, V, W, P, ETA, FX, FY, FZ\n"
            "\n";

    // Tecplot zone record header --------------------------------------------//
    file << "ZONE\n"
            "T = " << fName << "\n"
            "SOLUTIONTIME = " << to_string(simTime) << "\n"
            "ZONETYPE = ORDERED,\n"
            "\n"
            "I = " << to_string(nx+1) << ", "
            "J = " << to_string(ny+1) << ", "
            "K = " << to_string(nz+1) << ", \n"
            "\n"
            "DT = (DOUBLE, DOUBLE)\n"
            "\n"
            "DATAPACKING = BLOCK\n"
            "VARLOCATION = ([1-3] = NODAL, [4-11] = CELLCENTERED)\n"
            << endl;

    // Tecplot zone data -----------------------------------------------------//
    writeBlockVector(file, X, 0, 0, 0, nx+1, ny+1, nz+1, drctn::i);
    writeBlockVector(file, Y, 0, 0, 0, nx+1, ny+1, nz+1, drctn::j);
    writeBlockVector(file, Z, 0, 0, 0, nx+1, ny+1, nz+1, drctn::k);
    writeBlockTensorStgr(file, U, gc, gc, gc, nxt-gc, nyt-gc, nzt-gc, drctn::i);
    writeBlockTensorStgr(file, V, gc, gc, gc, nxt-gc, nyt-gc, nzt-gc, drctn::j);
    writeBlockTensorStgr(file, W, gc, gc, gc, nxt-gc, nyt-gc, nzt-gc, drctn::k);
    writeBlockTensor(file, P,   gc, gc, gc, nxt-gc, nyt-gc, nzt-gc);
    writeBlockTensor(file, ETA, gc, gc, gc, nxt-gc, nyt-gc, nzt-gc);
    writeBlockTensorStgr(file,FX, gc, gc, gc, nxt-gc, nyt-gc, nzt-gc, drctn::i);
    writeBlockTensorStgr(file,FY, gc, gc, gc, nxt-gc, nyt-gc, nzt-gc, drctn::j);
    writeBlockTensorStgr(file,FZ, gc, gc, gc, nxt-gc, nyt-gc, nzt-gc, drctn::k);
}

/*
 * Create a file and file characteristics at the start of the simulation.
*/
void filerInfoStart() {
    ofstream f;
    f.open(filePath + fileName + "_characteristics.txt");
    printCaseInfo(f);
    f.close();
}

/*
 * File case information at filing interval.
*/
void filerInfoInterval() {
    ofstream f;
    f.open(filePath + fileName + "_characteristics.txt", ios_base::app);
    f   << left << "Timestep = " << timestep << "; ";
    printSimulationStatus(f);
    f.close();
}

/*
 * File case completion information at the end of the program.
*/
void filerInfoEnd() {
    ofstream f;
    f.open(filePath + fileName + "_characteristics.txt", ios_base::app);
    f   << left << "Final ";
    printSimulationStatus(f);
    f.close();
}

/*
 * File solution files on demand, according to requirement.
*/
void filerSolution(const int timestep) {
    if (timestep != -1) {   // Default argument for timestep = -1; see filers.h
        string timeStr = to_string(timestep);
        filerASCIIBlock(filePath, fileName + "_" + timeStr);
        filerInfoInterval();
    }
    else {
        filerASCIIBlock(filePath, fileName + "_final");
        filerInfoEnd();
    }
}