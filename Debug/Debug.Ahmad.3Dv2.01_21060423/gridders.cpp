/**
 * @file        gridders.cpp
 *
 * @project     3D N-S solver rebuild
 * @version     2.0beta
 *
 * @author      Syed Ahmad Raza (sahmadrz@gmail.com)
 * @date        2020-10-27
 *
 * @brief       Create the grid for Navier-Stokes solver.
 *
 */

#include "gridders.h"
#include "parameters.h"
#include <fstream>
#include <iostream>

using std::abs, std::tan, std::cbrt;
using namespace Structures;

/*
 *          Domain layout for tri-uniform gridder variables below
 *  ----------------------------------------------------------------------------
 *  |  |  |  |  |  |||||||||||||  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
 *  ----------------------------------------------------------------------------
 *  |<--- Lrg1 --->|<-- Sml -->|<------------------- Lrg2 -------------------->|
 *      (first)                                    (second)
 */
void triUniformGridder() {
    double  xSmlCenter=0, dxSml=0, xDistLrg1=0, dxLrg1=0, dxLrg2=0,
            ySmlCenter=0, dySml=0, yDistLrg1=0, dyLrg1=0, dyLrg2=0,
            zSmlCenter=0, dzSml=0, zDistLrg1=0, dzLrg1=0, dzLrg2=0;
    int     nxLrg1=0, nyLrg1=0, nzLrg1=0;

    if constexpr (xDistSml*nxSml > 0 && xDistLrgI*nxLrgI <= 0) {
        // Find smallest and largest structure coordinates in x-direction.
        double  xSmallest=xDist, xLargest=0;
        if constexpr (static_cast<bool>(Total<Sphere>::count)) {
            for (const Sphere* sP : Sphere::getSet()) {
                double dia = sP->getDia();
                InitCenter ic = sP->getInitCenter();
                if (ic.x - dia < xSmallest) { xSmallest = ic.x - dia; }
                if (ic.x + dia > xLargest)  { xLargest  = ic.x + dia; }
            }
        }
        if constexpr (static_cast<bool>(Total<CylinderY>::count)) {
            for (const CylinderY* sP : CylinderY::getSet()) {
                double dia = sP->getDia();
                InitCenter ic = sP->getInitCenter();
                if (ic.x - dia < xSmallest) { xSmallest = ic.x - dia; }
                if (ic.x + dia > xLargest)  { xLargest  = ic.x + dia; }
            }
        }
        if constexpr (static_cast<bool>(Total<CylinderZ>::count)) {
            for (const CylinderZ* sP : CylinderZ::getSet()) {
                double dia = sP->getDia();
                InitCenter ic = sP->getInitCenter();
                if (ic.x - dia < xSmallest) { xSmallest = ic.x - dia; }
                if (ic.x + dia > xLargest)  { xLargest  = ic.x + dia; }
            }
        }
        if constexpr (countAll == Total<CylinderX>::count) {
            xSmallest = 0, xLargest = xDist;
        }
        dxSml = xDistSml/nxSml;
        xSmlCenter = (xSmallest + xLargest)/2.;
        xDistLrg1 = xSmlCenter - xDistSml*0.5;
        nxLrg1 = static_cast<int>((nx - nxSml)*xDistLrg1/(xDist - xDistSml));
        dxLrg1 = xDistLrg1/nxLrg1;
        dxLrg2 = dxLrg1;
    }
    if constexpr (yDistSml*nySml > 0 && yDistLrgI*nyLrgI <= 0) {
        // Find smallest and largest structure coordinates in y-direction.
        double  ySmallest=yDist, yLargest=0;
        if constexpr (static_cast<bool>(Total<Sphere>::count)) {
            for (const Sphere* sP : Sphere::getSet()) {
                double dia = sP->getDia();
                InitCenter ic = sP->getInitCenter();
                if (ic.y - dia < ySmallest) { ySmallest = ic.y - dia; }
                if (ic.y + dia > yLargest)  { yLargest  = ic.y + dia; }
            }
        }
        if constexpr (static_cast<bool>(Total<CylinderX>::count)) {
            for (const CylinderX* sP : CylinderX::getSet()) {
                double dia = sP->getDia();
                InitCenter ic = sP->getInitCenter();
                if (ic.y - dia < ySmallest) { ySmallest = ic.y - dia; }
                if (ic.y + dia > yLargest)  { yLargest  = ic.y + dia; }
            }
        }
        if constexpr (static_cast<bool>(Total<CylinderZ>::count)) {
            for (const CylinderZ* sP : CylinderZ::getSet()) {
                double dia = sP->getDia();
                InitCenter ic = sP->getInitCenter();
                if (ic.y - dia < ySmallest) { ySmallest = ic.y - dia; }
                if (ic.y + dia > yLargest)  { yLargest  = ic.y + dia; }
            }
        }
        if constexpr (countAll == Total<CylinderY>::count) {
            ySmallest = 0, yLargest = yDist;
        }
        dySml = yDistSml/nySml;
        ySmlCenter = (ySmallest + yLargest)/2.;
        yDistLrg1 = ySmlCenter - yDistSml*0.5;
        nyLrg1 = static_cast<int>((ny - nySml)*yDistLrg1/(yDist - yDistSml));
        dyLrg1 = yDistLrg1/nyLrg1;
        dyLrg2 = dyLrg1;
    }
    if constexpr (zDistSml*nzSml > 0 && zDistLrgI*nzLrgI <= 0) {
        // Find smallest and largest structure coordinates in z-direction.
        double  zSmallest=zDist, zLargest=0;
        if constexpr (static_cast<bool>(Total<Sphere>::count)) {
            for (const Sphere* sP : Sphere::getSet()) {
                double dia = sP->getDia();
                InitCenter ic = sP->getInitCenter();
                if (ic.z - dia < zSmallest) { zSmallest = ic.z - dia; }
                if (ic.z + dia > zLargest)  { zLargest  = ic.z + dia; }
            }
        }
        if constexpr (static_cast<bool>(Total<CylinderX>::count)) {
            for (const CylinderX* sP : CylinderX::getSet()) {
                double dia = sP->getDia();
                InitCenter ic = sP->getInitCenter();
                if (ic.z - dia < zSmallest) { zSmallest = ic.z - dia; }
                if (ic.z + dia > zLargest)  { zLargest  = ic.z + dia; }
            }
        }
        if constexpr (static_cast<bool>(Total<CylinderY>::count)) {
            for (const CylinderY* sP : CylinderY::getSet()) {
                double dia = sP->getDia();
                InitCenter ic = sP->getInitCenter();
                if (ic.z - dia < zSmallest) { zSmallest = ic.z - dia; }
                if (ic.z + dia > zLargest)  { zLargest  = ic.z + dia; }
            }
        }
        if constexpr (countAll == Total<CylinderZ>::count) {
                zSmallest = 0, zLargest = zDist;
        }
        dzSml = zDistSml/nzSml;
        zSmlCenter = (zSmallest + zLargest)/2.;
        zDistLrg1 = zSmlCenter - zDistSml*0.5;
        nzLrg1 = static_cast<int>((nz - nzSml)*zDistLrg1/(zDist - zDistSml));
        dzLrg1 = zDistLrg1/nzLrg1;
        dzLrg2 = dzLrg1;
    }
    if constexpr (xDistSml*nxSml > 0 && xDistLrgI*nxLrgI > 0) {
        xDistLrg1 = xDistLrgI;
        nxLrg1 = nxLrgI;
        dxSml = xDistSml/nxSml,
        dxLrg1 = xDistLrg1/nxLrg1;
        dxLrg2 = (xDist - xDistLrg1 - xDistSml) / (nx - nxLrg1 - nxSml);
    }
    if constexpr (yDistSml*nySml > 0 && yDistLrgI*nyLrgI > 0) {
        yDistLrg1 = yDistLrgI;
        nyLrg1 = nyLrgI;
        dySml = yDistSml/nySml,
        dyLrg1 = yDistLrg1/nyLrg1;
        dyLrg2 = (yDist - yDistLrg1 - yDistSml) / (ny - nyLrg1 - nySml);
    }
    if constexpr (zDistSml*nzSml > 0 && zDistLrgI*nzLrgI > 0) {
        zDistLrg1 = zDistLrgI;
        nzLrg1 = nzLrgI;
        dzSml = zDistSml/nzSml,
        dzLrg1 = zDistLrg1/nzLrg1;
        dzLrg2 = (zDist - zDistLrg1 - zDistSml) / (nz - nzLrg1 - nzSml);
    }

    X(0) = 0.0;
    X(nx) = xDist;
    Y(0) = 0.0;
    Y(ny) = yDist;
    Z(0) = 0.0;
    Z(nz) = zDist;

    if constexpr (xDistSml*nxSml > 0) {
        X(nxLrg1) = xDistLrg1;
        X(nxLrg1+nxSml) = xDistLrg1 + xDistSml;
        for (int i=1; i<nxLrg1; ++i)             { X(i) = X(i-1) + dxLrg1; }
        for (int i=nxLrg1+1; i<nxLrg1+nxSml; ++i){ X(i) = X(i-1) + dxSml;  }
        for (int i=nxLrg1+nxSml+1; i<nx; ++i)    { X(i) = X(i-1) + dxLrg2; }
    }
    else {
        double dx = xDist/nx;
        for (int i=1; i<nx; ++i) { X(i) = X(i-1) + dx; }
    }
    if constexpr (yDistSml*nySml > 0) {
        Y(nyLrg1) = yDistLrg1;
        Y(nyLrg1+nySml) = yDistLrg1 + yDistSml;
        for (int j=1; j<nyLrg1; ++j)             { Y(j) = Y(j-1) + dyLrg1; }
        for (int j=nyLrg1+1; j<nyLrg1+nySml; ++j){ Y(j) = Y(j-1) + dySml;  }
        for (int j=nyLrg1+nySml+1; j<ny; ++j)    { Y(j) = Y(j-1) + dyLrg2; }
    }
    else {
        double dy = yDist/ny;
        for (int j=1; j<ny; ++j) { Y(j) = Y(j-1) + dy; }
    }
    if constexpr (zDistSml*nzSml > 0) {
        Z(nzLrg1) = zDistLrg1;
        Z(nzLrg1+nzSml) = zDistLrg1 + zDistSml;
        for (int k=1; k<nzLrg1; ++k)             { Z(k) = Z(k-1) + dzLrg1; }
        for (int k=nzLrg1+1; k<nzLrg1+nzSml; ++k){ Z(k) = Z(k-1) + dzSml;  }
        for (int k=nzLrg1+nzSml+1; k<nz; ++k)    { Z(k) = Z(k-1) + dzLrg2; }
    }
    else {
        double dz = zDist/nz;
        for (int k=1; k<nz; ++k) { Z(k) = Z(k-1) + dz; }
    }

    std::cout
        << "  xSmlCenter = "<< xSmlCenter
        << ", xDistLrg1 = " << xDistLrg1
        << ", nxLrg1 = "    << nxLrg1
        << ", dxSml = "     << dxSml
        << ", dxLrg1 = "    << dxLrg1
        << ", dxLrg2 = "    << dxLrg2 << "\n\n"
        << "  ySmlCenter = "<< ySmlCenter
        << ", yDistLrg1 = " << yDistLrg1
        << ", nyLrg1 = "    << nyLrg1
        << ", dySml = "     << dySml
        << ", dyLrg1 = "    << dyLrg1
        << ", dyLrg2 = "    << dyLrg2 << "\n\n"
        << "  zSmlCenter = "<< zSmlCenter
        << ", zDistLrg1 = " << zDistLrg1
        << ", nzLrg1 = "    << nzLrg1
        << ", dzSml = "     << dzSml
        << ", dzLrg1 = "    << dzLrg1
        << ", dzLrg2 = "    << dzLrg2 << "\n\n";
    gridDistances();
}

void nonUniformGridder() {
    X(0) = 0.0;
    #pragma omp parallel for default(none) shared(X)
    for (int i=1; i<nx+1; ++i) {
        X(i) = 0.5*tan(M_PI*(0.5*(static_cast<double>(i)/nx) - 0.25)) + 0.5;
    }
    Y(0) = 0.0;
    #pragma omp parallel for default(none) shared(Y)
    for (int j=1; j<ny+1; ++j) {
        Y(j) = 0.5*tan(M_PI*(0.5*(static_cast<double>(j)/ny) - 0.25)) + 0.5;
    }
    Z(0) = 0.0;
    #pragma omp parallel for default(none) shared(Z)
    for (int k=1; k<nz+1; ++k) {
        Z(k) = 0.5*tan(M_PI*(0.5*(static_cast<double>(k)/nz) - 0.25)) + 0.5;
    }
    gridDistances();
}

void uniformGridder() {
    const double
        dx = xDist/double(nx), dy = yDist/double(ny), dz = zDist/double(nz);
    #pragma omp parallel for default(none) shared(X) firstprivate(dx)
    for (int i=0; i<nx+1; ++i) { X(i) = i*dx; }

    #pragma omp parallel for default(none) shared(Y) firstprivate(dy)
    for (int j=0; j<ny+1; ++j) { Y(j) = j*dy; }

    #pragma omp parallel for default(none) shared(Z) firstprivate(dz)
    for (int k=0; k<nz+1; ++k) { Z(k) = k*dz; }

    gridDistances();
}

/*
 *  Print X, Y, Z vectors.
 */
void printDistanceVectors() {
    std::cout << "x-coordinates:" << std::endl;
    double* xP = X.data(), * yP = Y.data(), * zP = Z.data();
    for (int i=0; i<nx+1; ++i) { std::cout << *(xP+i) << " "; }
    std::cout << "\n" << std::endl;

    std::cout << "y-coordinates:" << std::endl;
    for (int j=0; j<ny+1; ++j) { std::cout << *(yP+j) << " "; }
    std::cout << "\n" << std::endl;

    std::cout << "z-coordinates:" << std::endl;
    for (int k=0; k<nz+1; ++k) { std::cout << *(zP+k) << " "; }
    std::cout << "\n" << std::endl;
}

/*
 *  Distance interpolation for the cross velocity components in turbulent flow.
 */
inline void crossLinInterpolate(double& lIF,
    const double d1m, const double d2, const double d3) {
    lIF = sqrt(0.25*d1m*d1m + 0.25*d2*d2 + 0.25*d3*d3)
            /sqrt(d1m*d1m + d2*d2 + d3*d3);
}

void gridDistances() {
    // DX, DY, DZ are the grid distances and DXM, DYM, DZM are the staggered
    // grid distances.

    #pragma omp parallel for default(none) shared(X, DX, DDX, DXM, DDXM)
    for (int i=0; i<nx-1; ++i) {
        const int ig   = i+gc;
        DX (ig)  = (X(i+1) - X(i)),         DDX (ig) = 1.0/DX (ig);
        DXM(ig)  = (X(i+2) - X(i))/2.0,     DDXM(ig) = 1.0/DXM(ig);
    }

    #pragma omp parallel for default(none) shared(Y, DY, DYM, DDY, DDYM)
    for (int j=0; j<ny-1; ++j) {
        const int jg   = j+gc;
        DY (jg)  = (Y(j+1) - Y(j)),         DDY (jg) = 1.0/DY (jg);
        DYM(jg)  = (Y(j+2) - Y(j))/2.0,     DDYM(jg) = 1.0/DYM(jg);
    }

    #pragma omp parallel for default(none) shared(Z, DZ, DZM, DDZ, DDZM)
    for (int k=0; k<nz-1; ++k) {
        const int kg   = k+gc;
        DZ (kg)  = (Z(k+1) - Z(k)),         DDZ (kg) = 1.0/DZ (kg);
        DZM(kg)  = (Z(k+2) - Z(k))/2.0,     DDZM(kg) = 1.0/DZM(kg);
    }

    // Leftover grid lengths (due to limitation of single loop for DX and DXM)
    DX(nxt-3) = X(nx) - X(nx-1),            DDX(nxt-3)  = 1.0/DX(nxt-3);
    DY(nyt-3) = Y(ny) - Y(ny-1),            DDY(nyt-3)  = 1.0/DY(nyt-3);
    DZ(nzt-3) = Z(nz) - Z(nz-1),            DDZ(nzt-3)  = 1.0/DZ(nzt-3);

    // Ghost boundary grid lengths
    DX (0)     = DX(2),                     DDX (0)     = 1.0/DX (0);
    DX (1)     = DX(2),                     DDX (1)     = 1.0/DX (1);
    DX (nxt-2) = DX(nxt-3),                 DDX (nxt-2) = 1.0/DX (nxt-2);
    DX (nxt-1) = DX(nxt-3),                 DDX (nxt-1) = 1.0/DX (nxt-1);

    DY (0)     = DY(2),                     DDY (0)     = 1.0/DY (0);
    DY (1)     = DY(2),                     DDY (1)     = 1.0/DY (1);
    DY (nyt-2) = DY(nyt-3),                 DDY (nyt-2) = 1.0/DY (nyt-2);
    DY (nyt-1) = DY(nyt-3),                 DDY (nyt-1) = 1.0/DY (nyt-1);

    DZ (0)     = DZ(2),                     DDZ (0)     = 1.0/DZ (0);
    DZ (1)     = DZ(2),                     DDZ (1)     = 1.0/DZ (1);
    DZ (nzt-2) = DZ(nzt-3),                 DDZ (nzt-2) = 1.0/DZ (nzt-2);
    DZ (nzt-1) = DZ(nzt-3),                 DDZ (nzt-1) = 1.0/DZ (nzt-1);

    // Midpoints of ghost boundary grid lengths
    DXM(0)     = DXM(2),                    DDXM(0)     = 1.0/DXM(0);
    DXM(1)     = DXM(2),                    DDXM(1)     = 1.0/DXM(1);
    DXM(nxt-3) = DXM(nxt-4),                DDXM(nxt-3) = 1.0/DXM(nxt-3);
    DXM(nxt-2) = DXM(nxt-4),                DDXM(nxt-2) = 1.0/DXM(nxt-2);

    DYM(0)     = DYM(2),                    DDYM(0)     = 1.0/DYM(0);
    DYM(1)     = DYM(2),                    DDYM(1)     = 1.0/DYM(1);
    DYM(nyt-3) = DYM(nyt-4),                DDYM(nyt-3) = 1.0/DYM(nyt-3);
    DYM(nyt-2) = DYM(nyt-4),                DDYM(nyt-2) = 1.0/DYM(nyt-2);

    DZM(0)     = DZM(2),                    DDZM(0)     = 1.0/DZM(0);
    DZM(1)     = DZM(2),                    DDZM(1)     = 1.0/DZM(1);
    DZM(nzt-3) = DZM(nzt-4),                DDZM(nzt-3) = 1.0/DZM(nzt-3);
    DZM(nzt-2) = DZM(nzt-4),                DDZM(nzt-2) = 1.0/DZM(nzt-2);

    // Minimum grid distances
    dxMin = DX.minCoeff();
    dyMin = DY.minCoeff();
    dzMin = DZ.minCoeff();

    // Set all structure arrays to zero
    ETA.setZero();
    VELSX.setZero();
    VELSY.setZero();
    VELSZ.setZero();

    // Interpolate additional distances and grid variables for turbulent flow
    if (turbulenceModel != TurbModel::none) {
        #pragma omp parallel for default(none) shared(UGRD, DY, DZ,\
            DXM, DYM, DZM, IVFU, IVBU, IWNU, IWSU) firstprivate(turbulenceModel)
        for (int k=gc; k<nzt-gc; ++k) {
            for (int j=gc; j<nyt-gc; ++j) {
                for (int i=gc; i<nxt-gc-1; ++i) {
                    UGRD(i,j,k) = cbrt(DXM(i)*DY(j)*DZ(k));
                    crossLinInterpolate(IVFU(i,j,k), DXM(i), DY(j), DZM(k)  );
                    crossLinInterpolate(IVBU(i,j,k), DXM(i), DY(j), DZM(k-1));
                    crossLinInterpolate(IWNU(i,j,k), DXM(i), DZ(k), DYM(j)  );
                    crossLinInterpolate(IWSU(i,j,k), DXM(i), DZ(k), DYM(j-1));
                }
            }
        }

        #pragma omp parallel for default(none) shared(VGRD, DX, DZ,\
            DXM, DYM, DZM, IUFV, IUBV, IWEV, IWWV) firstprivate(turbulenceModel)
        for (int k=gc; k<nzt-gc; ++k) {
            for (int j=gc; j<nyt-gc-1; ++j) {
                for (int i=gc; i<nxt-gc; ++i) {
                    VGRD(i,j,k) = cbrt(DX(i)*DYM(j)*DZ(k));
                    crossLinInterpolate(IUFV(i,j,k), DYM(j), DX(i), DZM(k)  );
                    crossLinInterpolate(IUBV(i,j,k), DYM(j), DX(i), DZM(k-1));
                    crossLinInterpolate(IWEV(i,j,k), DYM(j), DZ(k), DXM(i)  );
                    crossLinInterpolate(IWWV(i,j,k), DYM(j), DZ(k), DXM(i-1));
                }
            }
        }

        #pragma omp parallel for default(none) shared(WGRD, DX, DY,\
            DXM, DYM, DZM, IUNW, IUSW, IVEW, IVWW) firstprivate(turbulenceModel)
        for (int k=gc; k<nzt-gc-1; ++k) {
            for (int j=gc; j<nyt-gc; ++j) {
                for (int i=gc; i<nxt-gc; ++i) {
                    WGRD(i,j,k) = cbrt(DX(i)*DY(j)*DZM(k));
                    crossLinInterpolate(IUNW(i,j,k), DZM(k), DX(i), DYM(j)  );
                    crossLinInterpolate(IUSW(i,j,k), DZM(k), DX(i), DYM(j-1));
                    crossLinInterpolate(IVEW(i,j,k), DZM(k), DY(j), DXM(i)  );
                    crossLinInterpolate(IVWW(i,j,k), DZM(k), DY(j), DXM(i-1));
                }
            }
        }
    }
    printDistanceVectors();
}
