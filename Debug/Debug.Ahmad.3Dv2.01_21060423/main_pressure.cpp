/**
 * @file        main.cpp
 *
 * @project     3D N-S solver
 * @version     2.0beta
 *
 * @author      Syed Ahmad Raza (sahmadrz@gmail.com)
 * @date        2020-10-27
 *
 * @brief       The main solver file outlining the algorithm of the Navier-
 *              Stokes solver.
 *
 * @detail      3D N-S solver with turbulence model. The current version
 *              is a fully working 3D N-S solver for nonuniform grid with
 *              parallelization using openMP, adaptive timestepping,
 *              BiCGSTAB method for pressure Poisson equation and data
 *              structures from the Eigen library. Smagorinsky-Lilly turbulence
 *              model has also been incorporated. A triuniform gridder is also
 *              fully implemented with two modes of operation. Fixed structures
 *              can be modeled as well as structures oscillating transversely
 *              due to vortex induced vibration.
 */

#include "parameters.h"
#include "structures.h"
#include "gridders.h"
#include "conditions.h"
#include "timestepper.h"
#include "velocities.h"
#include "linearSolvers.h"
#include "updaters.h"
#include "filers.h"
#include "printers.h"
#include <dirent.h>             // opendir to check that directory exists
#include <ctime>                // to time the script
#include <chrono>               // to measure and display the time duration

int
    timestep{0}, pIter{0}, pIterTotal{0}, pRestarts{0}, pResets{0},
    pResetMembers{0};
double
    dt{0}, simTime{0}, runTime{0},              // time varuables
    dxMin{0}, dyMin{0}, dzMin{0},               // minimum grid distances
    minConvX{100}, minConvY{100}, minConvZ{100},
    mChangeMax{0}, pChangeMax{0},
    uChangeMax{100}, vChangeMax{100}, wChangeMax{100};
VectorXd
    X(nx+1), Y(ny+1), Z(nz+1),
    DX(nxt), DY(nyt), DZ(nzt), DXM(nxt-1), DYM(nyt-1), DZM(nzt-1),
    DDX(nxt), DDY(nyt), DDZ(nzt), DDXM(nxt-1), DDYM(nyt-1), DDZM(nzt-1),
    PJ(ndim), PB(ndim), PX(ndim),
    Fij(nzt);

VectorXd Test_out(ndim), Test_in(ndim);


MatrixXd
    PA(ndim, ddim), PM(ndim, ddim),
    Fi(nyt, nzt);
TensorFixedSize<double, Sizes<nxt, nyt, nzt>>
    U, V, W, US, VS, WS, DUS1, DVS1, DWS1, DUS2, DVS2, DWS2, P,
    VELSX, VELSY, VELSZ,    // structure velocity array
    UGRD, VGRD, WGRD,       // grid variable arrays
    IVFU, IVBU, IWNU, IWSU, IUFV, IUBV, IWEV, IWWV, IUNW, IUSW, IVEW, IVWW,
        // interpolation factors for cross-interpolation required in turbulence
    ETA, FX, FY, FZ;        // volume of solid and virtual force arrays




// Update runtime
using std::chrono::steady_clock, std::chrono::duration_cast;
inline void updateRunTime(steady_clock::time_point startTime) {
    steady_clock::time_point endTime {steady_clock::now()};
    runTime = duration_cast<std::chrono::seconds>(endTime - startTime).count();
}


#if defined(AMGCL_ON)
#include <amgcl/backend/builtin.hpp>
#include <amgcl/adapter/crs_tuple.hpp>
#include <amgcl/make_solver.hpp>
#include <amgcl/amg.hpp>
#include <amgcl/coarsening/smoothed_aggregation.hpp>
#include <amgcl/relaxation/spai0.hpp>
#include <amgcl/relaxation/gauss_seidel.hpp>
#include <amgcl/solver/bicgstab.hpp>
#include <amgcl/profiler.hpp>

  typedef amgcl::make_solver<
             amgcl::amg< 
/*PBackend */ amgcl::backend::builtin<double>,
              amgcl::coarsening::smoothed_aggregation,
              amgcl::relaxation::gauss_seidel>,
/*SBackend */amgcl::solver::bicgstab<amgcl::backend::builtin<double> >
  > SolverBuiltin;

#endif 

int main() {
    // Check the directory and create, if not present ------------------------//
    if (opendir((filePath).c_str()) == NULL) {
        if (opendir(parentPath.c_str()) == NULL) {
            if (system(("mkdir " + parentPath).c_str()) != 0) { return 1; }
        }
        if (system(("mkdir "+ filePath).c_str()) != 0) { return 1; }
    }

    // Set the number of threads for OpenMP ----------------------------------//
    #ifdef _OPENMP
    omp_set_dynamic(0);
    omp_set_num_threads(threadsOMPCount);
    #endif

    // File the case characteristics and note the starting time --------------//
    filerInfoStart();
    if constexpr (steadiness != SolState::none) {
        filerTimestepDataStart();
    }
    steady_clock::time_point tStart {steady_clock::now()};

    // Create the grid -------------------------------------------------------//
    if constexpr (gridType == Grid::triuniform)      { triUniformGridder(); }
    else if constexpr (gridType == Grid::nonuniform) { nonUniformGridder(); }
    else if constexpr (gridType == Grid::uniform)    { uniformGridder(); }

    // Create all the structures ---------------------------------------------//
    constructStructures();

    // Initialize the simulation variables for timestepping ------------------//
    if constexpr (dtOverride > 0) { dt = dtOverride; }
    initialConditions(initials);
    boundaryConditions(presBoundaries,
                        uVelBoundaries, vVelBoundaries, wVelBoundaries);
    pressureMatrixConstructor();                                // PA

        // ---------------
    mat::CSR_matrix<double> matCSR;

    pressureMatrixConstructor_CSR(matCSR);

    // yakutat::SparseMatrixCSR<double> matCSR_Yakuta(ndim);

    // yakutat::bicgstab<yakutat::SparseMatrixCSR<double>> solver_yakuta;

    // matCSRYakuta.resize(ndim);


    auto [ptr, idx, val] = matCSR.get_CSR();

    // ---------------

    // for (size_t row = 0; row < ptr.size() ; ++row)
    // for (size_t j = ptr[row]; j < ptr[row+1];++j)
    // {
    //     matCSR_Yakuta.set(row, idx[j], val[j]);
    // }


    #if defined(ELL_ON)

    // ---------------
    mat::ELL_matrix<double> matELL(ndim, 7);
    matELL.set(matCSR.get_CSR());

    // ---------------
    #endif 
    // ---------------

    // --------------- amgcl
    #if defined(AMGCL_ON)

    // --------------- 

    auto rows = matCSR.row();

    auto matAMG = std::tie(rows, ptr, idx, val );
    // --------------- 
    SolverBuiltin solver( matAMG );
    // --------------- 
    #endif 

        // ---------------
    solver::bicgstabRe2<mat::CSR_matrix<double> > 
      solverCSR(matCSR);
        // ---------------


    // ---------------
    std::vector<double> b_mat(ndim, 0.0), x_result(ndim, 0.0);
    // ---------------

    // ---------------
    std::random_device rd;

    std::vector<double> vector_yIn(ndim), vector_yOut(ndim),
                        vector_myIn(ndim), vector_myOut(ndim);

    for (int i = 0; i < ndim; ++i){
       vector_myIn[i] = vector_yIn[i] =  Test_in(i) = rd();
       vector_myOut[i] = vector_yOut[i] =  Test_out(i) = rd();
    }
    // ---------------



    // --------------- for Ahmad 
    double  * in = Test_in.data(), 
            * out = Test_out.data(), 
            * pa = PA.data();

    multiplyMatrixVector(out, pa, in);
    // --------------- for Ahmad 

    // vector_yOut = matCSR_Yakuta * vector_yIn; 

    vector_myOut = matCSR * vector_myIn;

    // --------------- for ELL
    // cout << "--------matELL--------\n" ;
    // cout << matELL ;
    // --------------- for ELL

    // --------------- for CSR  
    // cout << "--------matCSR--------\n" ;
    // matCSR.Show_CSR();
    // --------------- for CSR  




    double summ ;
    // cout << "\n-----------------------\n" ;
    // summ = 0.;
    // for (int i = 0; i < ndim; ++i){
    //     auto ab = std::abs(Test_out(i) - vector_yOut[i]);
    //     summ += ab;
    // }

    // cout << "\n Between Ahmad and Wei (Sum)" << summ << std::endl;


    cout << "\n-----------------------\n" ;
    summ = 0.;
    for (int i = 0; i < ndim; ++i){
        auto ab = std::abs(Test_out[i] - vector_myOut[i]);
        summ += ab;
    }

    cout << "\n Between Ahmad and my (Sum)" << summ << std::endl;


    // cout << "\nAhmad \n";
    // for (int i = 0; i < ndim; ++i){
    //     cout << Test_out(i) << ", ";
    // }

    // cout << "\nWei \n";
    // for (int i = 0; i < ndim; ++i){
    //     cout << vector_yOut.at(i) << ", ";
    // }
    // cout << "\nMy \n";
    // for (int i = 0; i < ndim; ++i){
    //     cout << vector_myOut.at(i) << ", ";
    // }


    // ---------------








// ----------------------  




    bool velChangeCriteria {true};
    // Main simulation loop --------------------------------------------------//
    while (timestep < timestepMax && (velChangeCriteria || timestep < 10)) {

        if constexpr (dtOverride < 0) { timestepper(); }
        ++timestep, simTime += dt;
        bool timestepFiled {false};

        // Simulate VIV oscillating structures -------------------------------//
        if constexpr (steadiness == SolState::unsteady) {
            if constexpr (simulateVIV == PolarQ::yes) {
                if (timestep > timestepsPaused) { updateStructures(); }
            }
        }

        interimVelocity(mapSelectedScheme.at(convVelScheme));   // US = U
        massVectorConstructor();                                // PB <- US
        // ---------------
        massVectorConstructor_vector(b_mat);
        // ---------------

        bCGSTAB();                                              // PX <- PA, PB


        // ---------------
        // ---------------

        #if defined(AMGCL_ON)
        // ---------------
        auto [iters, error] = solver(b_mat, x_result);
        // --------------
        #else
        // --------------

        auto [iters, error] = solverCSR.solve(b_mat, x_result);

        // --------------
        #endif

        double sum = 0;
        for (size_t i = 0; i < x_result.size(); ++i) {
            auto abs = std::abs(PX(i) - x_result[i]);
            sum += abs;
        }
    
        cout << "\nsum " << sum << " | iters, errer " << iters << ", " << error << std::endl;
        // ---------------

        for (size_t i = 0; i < x_result.size(); ++i) {
            PX(i) = x_result[i];
        }

        pressureUpdater();                                      // P = PX
        velocityUpdater();                                      // U <- US,P
        boundaryConditions(presBoundaries,
                            uVelBoundaries, vVelBoundaries, wVelBoundaries);

        //Calculate unsteady flow parameters ---------------------------------//
        if constexpr (steadiness == SolState::unsteady) {
            updateVirtualForce();
            // Calculate VIV oscillating structure variables -----------------//
            if constexpr (simulateVIV == PolarQ::yes) {
                if (timestep > timestepsPaused) {
                    updateVIV(); //Assuming inlet velocity and diameter as 1.
                }
            }
            filerTimestepData();
        }

        // Filing the solution -----------------------------------------------//
        // Animation filing at frequent intervals ----------------------------//
        if constexpr (animationCount > 0) {
            for (Animation* anim : Animation::getSet()) {
                if (timestep >= anim->m_bgnAnimTimestep
                        && anim->m_animFileCount < anim->m_endAnimSolFiles
                        && timestep%(anim->m_animFileInterv!=0?
                            anim->m_animFileInterv : 1) == 0) {
                    if constexpr (steadiness == SolState::steady) {
                        updateVirtualForce();
                        filerTimestepData();
                    }
                    updateRunTime(tStart);
                    filerSolution(timestep);
                    timestepFiled = true;
                    ++anim->m_animFileCount;
                }
            }
            if constexpr (endSimAfterAnim == PolarQ::yes) {
                const Animation* animLast{Animation::getOneP(animationCount-1)};
                if (animLast->m_animFileCount == animLast->m_endAnimSolFiles) {
                    break;
                }
            }
        }
        // Regular filing ----------------------------------------------------//
        if constexpr (fileInterv > 0) {
            if (timestep%fileInterv == 0 && !timestepFiled) {
                if constexpr (steadiness == SolState::steady) {
                    updateVirtualForce();
                    filerTimestepData();
                }
                updateRunTime(tStart);
                filerSolution(timestep);
            }
        }

        // Print variable states for current timestep ------------------------//
        printerProgress();

        // Check for convergence ---------------------------------------------//
        if constexpr (uChangeCriteria>0
                        || vChangeCriteria>0
                        || wChangeCriteria>0) {
            velChangeCriteria = uChangeMax > uChangeCriteria
                                || vChangeMax > vChangeCriteria
                                || wChangeMax > wChangeCriteria;
        }
    }//------------------------- End of timestepping -------------------------//

    // Record the clock time of simulation run -------------------------------//
    updateRunTime(tStart);

    // File the final solution and print the end states of variables ---------//
    if constexpr (steadiness == SolState::steady) {
        updateVirtualForce();
        filerTimestepData();
    }
    filerSolution();
    printerInfoEnd();

    return 0;
}
