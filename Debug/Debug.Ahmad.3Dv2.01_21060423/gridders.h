/**
 * @file        gridders.h
 *
 * @project     3D N-S solver rebuild
 * @version     2.0beta
 *
 * @author      Syed Ahmad Raza (sahmadrz@gmail.com)
 * @date        2020-10-27
 *
 * @brief       Create the grid for Navier-Stokes solver.
 *
 */

#ifndef GRIDDERS_H
#define GRIDDERS_H

void uniformGridder();
void nonUniformGridder();
void triUniformGridder();
void gridDistances();

#endif