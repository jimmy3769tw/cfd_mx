/**
 * @file        animations.h
 *
 * @project     3D N-S solver rebuild
 * @version     2.0beta
 *
 * @author      Syed Ahmad Raza (sahmadrz@gmail.com)
 * @date        2020-10-27
 *
 * @brief       Functions for filing the animations data files of the
 *              Navier-Stokes solver.
 *
 */

#ifndef ANIMATIONS_H
#define ANIMATIONS_H

#include "presetup.inl"

struct Animation {
    const int         m_id, m_bgnAnimTimestep, m_animFileInterv,
                      m_endAnimSolFiles;
    int               m_animFileCount=0;
    inline static int s_idGenerator{};
    inline static array<Animation*, animationCount> s_animations;

    Animation() = delete;
    Animation(const Animation&) = delete;
    Animation& operator=(const Animation&) = delete;

    Animation(const int bgnAnimTimestep, const int animFileInterv,
                const int endAnimSolFiles)
        : m_id{s_idGenerator++}, m_bgnAnimTimestep{bgnAnimTimestep},
          m_animFileInterv{animFileInterv}, m_endAnimSolFiles{endAnimSolFiles} {
        s_animations[m_id] = this;
    }

    int getId() const { return m_id; }
    static Animation& getOne(const int id) { return *s_animations[id]; }
    static Animation* getOneP(const int id) { return s_animations[id]; }
    static array<Animation*, animationCount>& getSet() {
        return s_animations;
    }
};

#endif