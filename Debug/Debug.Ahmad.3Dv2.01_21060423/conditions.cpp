/**
 * @file        conditions.cpp
 *
 * @project     3D N-S solver rebuild
 * @version     2.0beta
 *
 * @author      Syed Ahmad Raza (sahmadrz@gmail.com)
 * @date        2020-10-27
 *
 * @brief       Generate the initial and boundary conditions for the Navier-
 *              Stokes solver.
 *
 */

#include "conditions.h"
#include "parameters.h"

void initialConditions(const array<double, 4>& initialValues)
{
    double* u  {U.data()},  * v  {V.data()},  * w  {W.data()},
          * us {US.data()}, * vs {VS.data()}, * ws {WS.data()},
          * p  {P.data()};

    #pragma omp parallel for default(none)\
        firstprivate(p, u, us, v, vs, w, ws, initialValues)
    for (int k=0; k<nzt; ++k) {
        for (int j=0; j<nyt; ++j) {
            for (int i=0; i<nxt; ++i) {
                int l = i + j*nxt + k*nxt*nyt;
                *(p+l)  = initialValues[0];
                *(u+l)  = initialValues[1];
                *(v+l)  = initialValues[2];
                *(w+l)  = initialValues[3];

                *(us+l) = *(u+l);
                *(vs+l) = *(v+l);
                *(ws+l) = *(w+l);
            }
        }
    }
}

void boundaryConditions(const Boundaries& pressure, const Boundaries& uVelocity,
        const Boundaries& vVelocity, const Boundaries& wVelocity) {
    double* u  {U.data()},  * v  {V.data()},  * w  {W.data()},
          * us {US.data()}, * vs {VS.data()}, * ws {WS.data()},
          * p  {P.data()};

    #pragma omp parallel for default(none) firstprivate(p, u, us, v, vs, w, ws,\
        pressure, uVelocity, vVelocity, wVelocity, nxt, nyt, nzt)
    for (int k=gc; k<nzt-gc; ++k) {
        for (int i=gc; i<nxt-gc; ++i) {
            // South ---------------------------------------------------------//
            pressure.functions.south(p, i, k, nxt, nyt, nzt,
                                        pressure.values.south);
            uVelocity.functions.south(u, i, k, nxt, nyt, nzt,
                                        uVelocity.values.south);
            vVelocity.functions.south(v, i, k, nxt, nyt, nzt,
                                        vVelocity.values.south);
            wVelocity.functions.south(w, i, k, nxt, nyt, nzt,
                                        wVelocity.values.south);
            int l = i + k*nxt*nyt;

            *(p+l) = *(p+l+nxt);
            *(u+l) = *(u+l+nxt);
            *(v+l) = *(v+l+nxt);
            *(w+l) = *(w+l+nxt);

            *(us+l)     = *(u+l);
            *(us+l+nxt) = *(u+l+nxt);
            *(vs+l)     = *(v+l);
            *(vs+l+nxt) = *(v+l+nxt);
            *(ws+l)     = *(w+l);
            *(ws+l+nxt) = *(w+l+nxt);

            // North ---------------------------------------------------------//
            pressure.functions.north(p, i, k, nxt, nyt, nzt,
                                        pressure.values.north);
            uVelocity.functions.north(u, i, k, nxt, nyt, nzt,
                                        uVelocity.values.north);
            vVelocity.functions.north(v, i, k, nxt, nyt, nzt,
                                        vVelocity.values.north);
            wVelocity.functions.north(w, i, k, nxt, nyt, nzt,
                                        wVelocity.values.north);

            *(p+l+(nyt-1)*nxt) = *(p+l+(nyt-2)*nxt);
            *(u+l+(nyt-1)*nxt) = *(u+l+(nyt-2)*nxt);
            *(v+l+(nyt-2)*nxt) = *(v+l+(nyt-3)*nxt);
            *(v+l+(nyt-1)*nxt) = *(v+l+(nyt-3)*nxt); // omission?
            *(w+l+(nyt-1)*nxt) = *(w+l+(nyt-2)*nxt);

            *(us+l+(nyt-1)*nxt) = *(u+l+(nyt-1)*nxt);
            *(us+l+(nyt-2)*nxt) = *(u+l+(nyt-2)*nxt);
            *(vs+l+(nyt-1)*nxt) = *(v+l+(nyt-1)*nxt); // omission?
            *(vs+l+(nyt-2)*nxt) = *(v+l+(nyt-2)*nxt);
            *(vs+l+(nyt-3)*nxt) = *(v+l+(nyt-3)*nxt);
            *(ws+l+(nyt-1)*nxt) = *(w+l+(nyt-1)*nxt);
            *(ws+l+(nyt-2)*nxt) = *(w+l+(nyt-2)*nxt);
        }
        for (int j=gc; j<nyt-gc; ++j) {
            // West ----------------------------------------------------------//
            pressure.functions.west(p, j, k, nxt, nyt, nzt,
                                        pressure.values.west);
            uVelocity.functions.west(u, j, k, nxt, nyt, nzt,
                                        uVelocity.values.west);
            vVelocity.functions.west(v, j, k, nxt, nyt, nzt,
                                        vVelocity.values.west);
            wVelocity.functions.west(w, j, k, nxt, nyt, nzt,
                                        wVelocity.values.west);
            int l = j*nxt + k*nxt*nyt;

            *(p+l) = *(p+l+1);
            *(u+l) = *(u+l+1);
            *(v+l) = *(v+l+1);
            *(w+l) = *(w+l+1);

            *(us+l)   = *(u+l);
            *(us+l+1) = *(u+l+1);
            *(vs+l)   = *(v+l);
            *(vs+l+1) = *(v+l+1);
            *(ws+l)   = *(w+l);
            *(ws+l+1) = *(w+l+1);

            // East ----------------------------------------------------------//
            pressure.functions.east(p, j, k, nxt, nyt, nzt,
                                        pressure.values.east);
            uVelocity.functions.east(u, j, k, nxt, nyt, nzt,
                                        uVelocity.values.east);
            vVelocity.functions.east(v, j, k, nxt, nyt, nzt,
                                        vVelocity.values.east);
            wVelocity.functions.east(w, j, k, nxt, nyt, nzt,
                                        wVelocity.values.east);

            *(p+l+nxt-1) = *(p+l+nxt-2);
            *(u+l+nxt-2) = *(u+l+nxt-3);
            *(u+l+nxt-1) = *(u+l+nxt-3); // omission?
            *(v+l+nxt-1) = *(v+l+nxt-2);
            *(w+l+nxt-1) = *(w+l+nxt-2);

            *(us+l+nxt-1) = *(u+l+nxt-1); // omission?
            *(us+l+nxt-2) = *(u+l+nxt-2);
            *(us+l+nxt-3) = *(u+l+nxt-3);
            *(vs+l+nxt-1) = *(v+l+nxt-1);
            *(vs+l+nxt-2) = *(v+l+nxt-2);
            *(ws+l+nxt-1) = *(w+l+nxt-1);
            *(ws+l+nxt-2) = *(w+l+nxt-2);
        }
    }
    #pragma omp parallel for default(none) firstprivate(p, u, us, v, vs, w, ws,\
        pressure, uVelocity, vVelocity, wVelocity, nxt, nyt, nzt)
    for (int j=gc; j<nyt-gc; ++j) {
        for (int i=gc; i<nxt-gc; ++i) {
            // Back ----------------------------------------------------------//
            pressure.functions.back(p, i, j, nxt, nyt, nzt,
                                        pressure.values.back);
            uVelocity.functions.back(u, i, j, nxt, nyt, nzt,
                                        uVelocity.values.back);
            vVelocity.functions.back(v, i, j, nxt, nyt, nzt,
                                        vVelocity.values.back);
            wVelocity.functions.back(w, i, j, nxt, nyt, nzt,
                                        wVelocity.values.back);
            int l = i + j*nxt;

            *(p+l) = *(p+l+nxt*nyt);
            *(u+l) = *(u+l+nxt*nyt);
            *(v+l) = *(v+l+nxt*nyt);
            *(w+l) = *(w+l+nxt*nyt);

            *(us+l)         = *(u+l);
            *(us+l+nxt*nyt) = *(u+l+nxt*nyt);
            *(vs+l)         = *(v+l);
            *(vs+l+nxt*nyt) = *(v+l+nxt*nyt);
            *(ws+l)         = *(w+l);
            *(ws+l+nxt*nyt) = *(w+l+nxt*nyt);

            // Front ---------------------------------------------------------//
            pressure.functions.front(p, i, j, nxt, nyt, nzt,
                                        pressure.values.front);
            uVelocity.functions.front(u, i, j, nxt, nyt, nzt,
                                        uVelocity.values.front);
            vVelocity.functions.front(v, i, j, nxt, nyt, nzt,
                                        vVelocity.values.front);
            wVelocity.functions.front(w, i, j, nxt, nyt, nzt,
                                        wVelocity.values.front);

            *(p+l+(nzt-1)*nxt*nyt) = *(p+l+(nzt-2)*nxt*nyt);
            *(u+l+(nzt-1)*nxt*nyt) = *(u+l+(nzt-2)*nxt*nyt);
            *(v+l+(nzt-1)*nxt*nyt) = *(v+l+(nzt-2)*nxt*nyt);
            *(w+l+(nzt-2)*nxt*nyt) = *(w+l+(nzt-3)*nxt*nyt);
            *(w+l+(nzt-1)*nxt*nyt) = *(w+l+(nzt-3)*nxt*nyt); // omission?

            *(us+l+(nzt-1)*nxt*nyt) = *(u+l+(nzt-1)*nxt*nyt);
            *(us+l+(nzt-2)*nxt*nyt) = *(u+l+(nzt-2)*nxt*nyt);
            *(vs+l+(nzt-1)*nxt*nyt) = *(v+l+(nzt-1)*nxt*nyt);
            *(vs+l+(nzt-2)*nxt*nyt) = *(v+l+(nzt-2)*nxt*nyt);
            *(ws+l+(nzt-1)*nxt*nyt) = *(w+l+(nzt-1)*nxt*nyt); // omission?
            *(ws+l+(nzt-2)*nxt*nyt) = *(w+l+(nzt-2)*nxt*nyt);
            *(ws+l+(nzt-3)*nxt*nyt) = *(w+l+(nzt-3)*nxt*nyt);
        }
    }
}
