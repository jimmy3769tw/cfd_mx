/**
 * @file        presetup.inl
 *
 * @project     3D N-S solver rebuild
 * @version     2.0beta
 *
 * @author      Syed Ahmad Raza (sahmadrz@gmail.com)
 * @date        2020-10-27
 *
 * @brief       Hidden declarations of key variables and definitions of
 *              boundary functions; to be included at the beginning of
 *              "parameters.h".
 *
 */

#ifndef PRESETUP_INL
#define PRESETUP_INL

#include <string>
#include <unordered_map>
#include <array>
#include <functional>
#include <cmath>
#include <omp.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <Eigen/CXX11/Tensor>
#include <Eigen/Core>

using Eigen::VectorXd, Eigen::MatrixXd, Eigen::TensorFixedSize, Eigen::Sizes,
    std::function, std::array, std::unordered_map, std::string, std::to_string,
    std::ofstream, std::ifstream;


//----------------------------------------------------------------------------//
// Type alias, enumerations and structures -----------------------------------//
//----------------------------------------------------------------------------//

inline constexpr int gc = 2;    // ghost cells

template<typename E>
constexpr std::underlying_type_t<E> toUType(E enumerator) noexcept {
    return static_cast<std::underlying_type_t<E>>(enumerator);
}

template <class T> void printType(const T&) {
    std::cout << __PRETTY_FUNCTION__ << '\n';
}

using BoundaryFnc = function<void(double*, const int&, const int&,
    const int&, const int&, const int&, const double&)>;
using Schemer = function<void(
    double&, double&, double&, double&, double&, double&,
    const double&, const double&, const double&,
    const double&, const double&, const double&,
    const double*, const double*, const double*,
    const double*, const double*, const double*,
    const double*, const double*, const double*,
    const double*, const double*, const double*,
    const double*, const double*, const double*,
    const int&, const int&, const int&, const int&)>;

/* Polar questions       */ enum class PolarQ { no, yes };

/* Boundary conditions   */ enum class BoundaryType {
                                Neumann, Dirichlet, Periodic, Symmetry
                            };
/* Grid type             */ enum class Grid { uniform, nonuniform, triuniform };
/* Conv. vel. schemes    */ enum class ConvVel { quick, central, upwind };
/* Numerical schemes     */ enum class NumScheme {
                                euler, adamsBashforth2, adamsBashforth3
                            };
/* Solution state        */ enum class SolState { none, steady, unsteady };
/* Flow direction        */ enum class FlowDir { x, y, z };
/* Oscillation direction */ enum class OscilDir { x, y, z };
/* Turbulence model      */ enum class TurbModel { none, Smagorinsky };

struct BoundaryConditions {
    BoundaryType        west, east, south, north, back, front;
};
struct BoundaryValues {
    double              west, east, south, north, back, front;
};
struct BoundaryFunctions {
    BoundaryFnc         west, east, south, north, back, front;
};
struct Boundaries {
    const BoundaryValues      values;
    const BoundaryConditions  conditions;
    const BoundaryFunctions   functions;
};

template<class T> class Structure;
class Sphere;
class CylinderX;
class CylinderY;
class CylinderZ;

namespace Structures {
    enum class Count;
    enum class Kind { sphere, cylinderX, cylinderY, cylinderZ };
    enum class State { fixed, moving };
    struct InitCenter { double x, y, z; };
    struct Center { double x, y, z; };
    struct VForce { double x, y, z; };
    struct Coeff { double drag, lift; };
    template<typename T> struct Total;
    struct LoopLimit {
        int iBgn, iEnd, jBgn, jEnd, kBgn, kEnd;
        LoopLimit& add(int num);
        LoopLimit operator+(const LoopLimit& ll) const;
    };
}

template<typename T> struct Structures::Total {
    inline static constexpr int count{};
};

inline const std::string margin {"\n"+std::string(32, ' ')};

#endif
