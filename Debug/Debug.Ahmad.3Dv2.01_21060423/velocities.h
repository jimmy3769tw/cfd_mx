/**
 * @file        velocities.h
 *
 * @project     3D N-S solver rebuild
 * @version     2.0beta
 *
 * @author      Syed Ahmad Raza (sahmadrz@gmail.com)
 * @date        2020-10-27
 *
 * @brief       Apply the selected velocity scheme for the Navier-Stokes solver.
 *
 */

#ifndef VELOCITIES_H
#define VELOCITIES_H

#include "parameters.h"

void quickConvVelU(
    double& ue, double& uw, double& un, double& us, double& uf, double& ub,
    const double& ueu,  const double& uwu,
    const double& vnu,  const double& vsu,
    const double& wfu,  const double& wbu,
    const double* dx,   const double* dy,   const double* dz,
    const double* dxm,  const double* dym,  const double* dzm,
    const double* ddx,  const double* ddy,  const double* ddz,
    const double* ddxm, const double* ddym, const double* ddzm,
    const double* u,    const double* v,    const double* w,
    const int& i, const int& j, const int& k, const int& l);
void quickConvVelV(
    double& ve, double& vw, double& vn, double& vs, double& vf, double& vb,
    const double& uev,  const double& uwv,
    const double& vnv,  const double& vsv,
    const double& wfv,  const double& wbv,
    const double* dx,   const double* dy,   const double* dz,
    const double* dxm,  const double* dym,  const double* dzm,
    const double* ddx,  const double* ddy,  const double* ddz,
    const double* ddxm, const double* ddym, const double* ddzm,
    const double* u,    const double* v,    const double* w,
    const int& i, const int& j, const int& k, const int& l);
void quickConvVelW(
    double& we, double& ww, double& wn, double& ws, double& wf, double& wb,
    const double& uew,  const double& uww,
    const double& vnw,  const double& vsw,
    const double& wfw,  const double& wbw,
    const double* dx,   const double* dy,   const double* dz,
    const double* dxm,  const double* dym,  const double* dzm,
    const double* ddx,  const double* ddy,  const double* ddz,
    const double* ddxm, const double* ddym, const double* ddzm,
    const double* u,    const double* v,    const double* w,
    const int& i, const int& j, const int& k, const int& l);
void centralConvVelU(    // uniform grid only
    double& ue, double& uw, double& un, double& us, double& uf, double& ub,
    const double& ueu,  const double& uwu,
    const double& vnu,  const double& vsu,
    const double& wfu,  const double& wbu,
    const double* dx,   const double* dy,   const double* dz,
    const double* dxm,  const double* dym,  const double* dzm,
    const double* ddx,  const double* ddy,  const double* ddz,
    const double* ddxm, const double* ddym, const double* ddzm,
    const double* u,    const double* v,    const double* w,
    const int& i, const int& j, const int& k, const int& l);
void centralConvVelV(    // uniform grid only
    double& ve, double& vw, double& vn, double& vs, double& vf, double& vb,
    const double& uev,  const double& uwv,
    const double& vnv,  const double& vsv,
    const double& wfv,  const double& wbv,
    const double* dx,   const double* dy,   const double* dz,
    const double* dxm,  const double* dym,  const double* dzm,
    const double* ddx,  const double* ddy,  const double* ddz,
    const double* ddxm, const double* ddym, const double* ddzm,
    const double* u,    const double* v,    const double* w,
    const int& i, const int& j, const int& k, const int& l);
void centralConvVelW(    // uniform grid only
    double& we, double& ww, double& wn, double& ws, double& wf, double& wb,
    const double& uew,  const double& uww,
    const double& vnw,  const double& vsw,
    const double& wfw,  const double& wbw,
    const double* dx,   const double* dy,   const double* dz,
    const double* dxm,  const double* dym,  const double* dzm,
    const double* ddx,  const double* ddy,  const double* ddz,
    const double* ddxm, const double* ddym, const double* ddzm,
    const double* u,    const double* v,    const double* w,
    const int& i, const int& j, const int& k, const int& l);
void upwindConvVelU(
    double& ue, double& uw, double& un, double& us, double& uf, double& ub,
    const double& ueu,  const double& uwu,
    const double& vnu,  const double& vsu,
    const double& wfu,  const double& wbu,
    const double* dx,   const double* dy,   const double* dz,
    const double* dxm,  const double* dym,  const double* dzm,
    const double* ddx,  const double* ddy,  const double* ddz,
    const double* ddxm, const double* ddym, const double* ddzm,
    const double* u,    const double* v,    const double* w,
    const int& i, const int& j, const int& k, const int& l);
void upwindConvVelV(
    double& ve, double& vw, double& vn, double& vs, double& vf, double& vb,
    const double& uev,  const double& uwv,
    const double& vnv,  const double& vsv,
    const double& wfv,  const double& wbv,
    const double* dx,   const double* dy,   const double* dz,
    const double* dxm,  const double* dym,  const double* dzm,
    const double* ddx,  const double* ddy,  const double* ddz,
    const double* ddxm, const double* ddym, const double* ddzm,
    const double* u,    const double* v,    const double* w,
    const int& i, const int& j, const int& k, const int& l);
void upwindConvVelW(
    double& we, double& ww, double& wn, double& ws, double& wf, double& wb,
    const double& uew,  const double& uww,
    const double& vnw,  const double& vsw,
    const double& wfw,  const double& wbw,
    const double* dx,   const double* dy,   const double* dz,
    const double* dxm,  const double* dym,  const double* dzm,
    const double* ddx,  const double* ddy,  const double* ddz,
    const double* ddxm, const double* ddym, const double* ddzm,
    const double* u,    const double* v,    const double* w,
    const int& i, const int& j, const int& k, const int& l);

struct ConvectiveVelocityScheme{ Schemer uVel, vVel, wVel; };

const ConvectiveVelocityScheme
    centralConvVelScheme {  centralConvVelU,
                            centralConvVelV,
                            centralConvVelW   },
    upwindConvVelScheme  {  upwindConvVelU,
                            upwindConvVelV,
                            upwindConvVelW    },
    quickConvVelScheme   {  quickConvVelU,
                            quickConvVelV,
                            quickConvVelW     };

const unordered_map<ConvVel, ConvectiveVelocityScheme>
    mapSelectedScheme = { {ConvVel::central, centralConvVelScheme},
                          {ConvVel::upwind, upwindConvVelScheme},
                          {ConvVel::quick, quickConvVelScheme} };

void interimVelocity(ConvectiveVelocityScheme selectedScheme);

#endif