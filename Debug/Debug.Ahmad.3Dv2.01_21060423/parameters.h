/**
 * @file        parameters.h
 *
 * @project     3D N-S solver rebuild
 * @version     1.953beta
 *
 * @author      Syed Ahmad Raza (sahmadrz@gmail.com)
 * @date        2020-10-27
 *
 * @brief       Declare the constants and key variables for the case.
 *
 * @command
 g++ -std=c++17 -Wpedantic -Wall -Wextra -O3 -funroll-loops -D_GLIBCXX_PARALLEL -fopenmp -march=native main.cpp gridders.cpp structures.cpp conditions.cpp linearSolvers.cpp timestepper.cpp updaters.cpp velocities.cpp filers.cpp printers.cpp -o testing && ./testing
 *
 */

//---------------------------------- IGNORE ----------------------------------//
#ifndef PARAMETERS_H                                                          //
#define PARAMETERS_H                                                          //
#define NDEBUG                                                                //
#include "presetup.inl"                                                       //
//----------------------------------------------------------------------------//



// --------------------------------
#include "solver/bicgstab.hpp"

#include "solver/bicgstab0.hpp"

#include "solver/bicgstabRe.hpp"

#include "solver/bicgstabRe2.hpp"
// --------------------------------


// --------------------------------
#include "matrix/CSR_sparseMatrix.hpp"

#include "matrix/ELL_sparseMatrix.hpp"
// --------------------------------


// --------------------------------
// #include <yakutat/Algorithms/LinearSolvers/bicgstab.hpp>

#include <yakutat/SparseMatrix/SparseMatrixCSR.hpp>

#include <yakutat/SparseMatrix/SparseMatrixELL.hpp>
// --------------------------------



//----------------------------------------------------------------------------//
//***************************** Grid description *****************************//
//----------------------------------------------------------------------------//

inline constexpr int
/* Domain dimensions             */ is2Dor3D        {3};
inline constexpr Grid
/* Grid type                     */ gridType        {Grid::uniform};
inline constexpr double
/* Computational domain x-length */ xDist           {17},
/* Computational domain y-length */ yDist           {12},
/* Computational domain z-length */ zDist           {3};
inline constexpr int
// /* x-dir. total grids            */ nx              {3},
// /* y-dir. total grids            */ ny              {3},
// /* z-dir. total grids            */ nz              {3},
/* x-dir. total grids            */ nx              {128},
/* y-dir. total grids            */ ny              {61},
/* z-dir. total grids            */ nz              {30},
/* x-dir. no. of struct. subgrids*/ nxSub           {50},
/* y-dir. no. of struct. subgrids*/ nySub           {50},
/* z-dir. no. of struct. subgrids*/ nzSub           {0};
/*
 *          Domain layout for tri-uniform gridder variables below
 *  ----------------------------------------------------------------------------
 *  |  |  |  |  |  |||||||||||||  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
 *  ----------------------------------------------------------------------------
 *  |<--- Lrg1 --->|<-- Sml -->|<------------------- Lrg2 -------------------->|
 *      (first)                                    (second)
 *
 * NOTE:    The following gridder variables will ONLY be used for
 *          gridType::triuniform. Otherwise, they will be ignored.
 */
inline constexpr double
/* Coarse grid x-dist. (first)   */ xDistLrgI       {9},
/* Fine grid x-dist.             */ xDistSml        {5},
/* Coarse grid y-dist. (first)   */ yDistLrgI       {4.5},
/* Fine grid y-dist.             */ yDistSml        {3},
/* Coarse grid z-dist. (first)   */ zDistLrgI       {0},
/* Fine grid z-dist.             */ zDistSml        {0};
inline constexpr int
/* Coarse grids x-dir. (first)   */ nxLrgI          {36},
/* Fine grids x-dir.             */ nxSml           {60},
/* Coarse grids y-dir. (first)   */ nyLrgI          {18},
/* Fine grids y-dir.             */ nySml           {35},
/* Coarse grids z-dir. (first)   */ nzLrgI          {0},
/* Fine grids z-dir.             */ nzSml           {0};

//----------------------------------------------------------------------------//
//************************** Structures description **************************//
//----------------------------------------------------------------------------//

enum class Structures::Count {
/* Total spheres                 */ Sphere          = 0,
/* Total x-axis cylinders        */ CylinderX       = 0,
/* Total y-axis cylinders        */ CylinderY       = 0,
/* Total z-axis cylinders        */ CylinderZ       = 1,
};

//---------------------------------- IGNORE ----------------------------------//
template<> struct Structures::Total<Sphere> {                                 //
    inline static constexpr int count{toUType(Count::Sphere)}; };             //
template<> struct Structures::Total<CylinderX> {                              //
    inline static constexpr int count{toUType(Count::CylinderX)}; };          //
template<> struct Structures::Total<CylinderY> {                              //
    inline static constexpr int count{toUType(Count::CylinderY)}; };          //
template<> struct Structures::Total<CylinderZ> {                              //
    inline static constexpr int count{toUType(Count::CylinderZ)}; };          //
#include "structures.h"                                                       //
//----------------------------------------------------------------------------//


/* SPHERE: Diameter, initial centers (x, y, z), struct. state ----------------*/
// inline const Sphere     s01(1, 6.5, 5, 5, Structures::State::fixed)
    // ,                   s02(1, 5, 5, 7)
//     ;

/* X-AXIS CYLINDER: Diameter, initial centers (y, z), struct. state ----------*/
// inline const CylinderX   cX01(1, 5, 6.5, Structures::State::fixed)
//     ;

/* Y-AXIS CYLINDER: Diameter, initial centers (x, z), struct. state ----------*/
// inline const CylinderY   cY01(2.5, 16, 5)
//     ;

/* Z-AXIS CYLINDER: Diameter, initial centers (x, y), struct. state ----------*/
inline const CylinderZ  cZ00(1, 6.5, 6, Structures::State::fixed)
                        // cZ01(1, 12, 10, Structures::State::moving),
                        // cZ02(1, 12, 13, Structures::State::moving),
                        // cZ03(1, 12, 16, Structures::State::moving),
                        // cZ05(1, 12, 11, Structures::State::moving),
                        // cZ06(1, 12, 15, Structures::State::moving),
                        // cZ07(1, 12, 13, Structures::State::moving)
    ;
/*
 * REMIND:  Update Structures::Count above.
 * NOTE:    Structures::state describes whether the structure is expected to
 *          remain fixed or move during the simulation.
 */

//----------------------------------------------------------------------------//
//********************* Initial and boundary conditions **********************//
//----------------------------------------------------------------------------//

inline const array<double, 4>
/* Order: pressure, u-velocity, v-velocity, w-velocity*/
/* All initial condition values  */ initials        {0, 1, 0, 0};
// inline const BoundaryConditions
//     pressureConditions {
//     /* West,  East      */ BoundaryType::Neumann, BoundaryType::Neumann,
//     /* South, North     */ BoundaryType::Symmetry, BoundaryType::Symmetry,
//     /* Back,  Front     */ BoundaryType::Periodic, BoundaryType::Periodic,
//     },
//     uVelocityConditions {
//     /* West,  East      */ BoundaryType::Dirichlet, BoundaryType::Neumann,
//     /* South, North     */ BoundaryType::Symmetry, BoundaryType::Symmetry,
//     /* Back,  Front     */ BoundaryType::Periodic, BoundaryType::Periodic,
//     },
//     vVelocityConditions {
//     /* West,  East      */ BoundaryType::Dirichlet, BoundaryType::Neumann,
//     /* South, North     */ BoundaryType::Symmetry, BoundaryType::Symmetry,
//     /* Back,  Front     */ BoundaryType::Periodic, BoundaryType::Periodic,
//     },
//     wVelocityConditions {
//     /* West,  East      */ BoundaryType::Dirichlet, BoundaryType::Neumann,
//     /* South, North     */ BoundaryType::Symmetry, BoundaryType::Symmetry,
//     /* Back,  Front     */ BoundaryType::Periodic, BoundaryType::Periodic,
//     };
inline const BoundaryConditions
    pressureConditions {
    /* West,  East      */ BoundaryType::Neumann, BoundaryType::Neumann,
    /* South, North     */ BoundaryType::Neumann, BoundaryType::Neumann,
    /* Back,  Front     */ BoundaryType::Neumann, BoundaryType::Neumann,
    },
    uVelocityConditions {
    /* West,  East      */ BoundaryType::Dirichlet, BoundaryType::Neumann,
    /* South, North     */ BoundaryType::Neumann, BoundaryType::Neumann,
    /* Back,  Front     */ BoundaryType::Neumann, BoundaryType::Neumann,
    },
    vVelocityConditions {
    /* West,  East      */ BoundaryType::Dirichlet, BoundaryType::Neumann,
    /* South, North     */ BoundaryType::Neumann, BoundaryType::Neumann,
    /* Back,  Front     */ BoundaryType::Neumann, BoundaryType::Neumann,
    },
    wVelocityConditions {
    /* West,  East      */ BoundaryType::Dirichlet, BoundaryType::Neumann,
    /* South, North     */ BoundaryType::Neumann, BoundaryType::Neumann,
    /* Back,  Front     */ BoundaryType::Neumann, BoundaryType::Neumann,
    };
inline const BoundaryValues
    pressureValues  /* Order: W, E, S, N, B, F */   {0, 0, 0, 0, 0, 0},
    uVelocityValues /* Order: W, E, S, N, B, F */   {1, 0, 0, 0, 0, 0},
    vVelocityValues /* Order: W, E, S, N, B, F */   {0, 0, 0, 0, 0, 0},
    wVelocityValues /* Order: W, E, S, N, B, F */   {0, 0, 0, 0, 0, 0};
/*
 * NOTE: Boundary values will only be used for Dirichlet boundaries and
 * ignored for all other boundary types, defined in BoundaryConditions.
 */

//----------------------------------------------------------------------------//
//************************** Simulation description **************************//
//----------------------------------------------------------------------------//

inline constexpr double
/* Reynolds number               */ reynolds        {40},
/* VIV: Reduced velocity         */ reducedVel      {4.0},
/* VIV: Mass ratio               */ massRatio       {2.0},
/* VIV: Structural damping ratio */ structDamping   {0.0},
/* Turbulence: Smagorinsky const.*/ cS              {0.1},
/* -ve when using dt adjustment  */ dtOverride      {0.005},
/* Timstep adjustment: CFL       */ cflFactor       {0.05},
/* Timstep adjustment: Grid Fo   */ gridFoFactor    {0.15},
/* Timstep adjustment: Minimum dt*/ dtMin           {1e-12},
/* Convergence criteria for U    */ uChangeCriteria {1.0e-6},//1.0e-6
/* Convergence criteria for V    */ vChangeCriteria {1.0e-6},
/* Convergence criteria for W    */ wChangeCriteria {1.0e-6},
// /* BCGSTAB residual norm tol.    */ pResNormTol     {1e-3},
/* BCGSTAB residual norm tol.    */ pResNormTol     {1e-3},
/* BCGSTAB restart factor        */ pRestartFactor  {1e-7};
inline constexpr int
/* Number of OMP threads         */ threadsOMPCount {4},
/* VIV: Timesteps struct. paused */ timestepsPaused {50000},
/* Max. no. of timesteps         */ timestepMax     {10000000},
/* Max. no. of pressure iter.    */ pIterMax        {5000};
inline constexpr ConvVel
/* Convection velocity scheme    */ convVelScheme   {ConvVel::quick
};
inline constexpr NumScheme
// /* Numerical scheme              */ numericalScheme {NumScheme::adamsBashforth3
/* Numerical scheme              */ numericalScheme {NumScheme::euler
};
inline constexpr SolState
/* Solution steady/unsteady      */ steadiness      {SolState::unsteady
};
inline constexpr PolarQ
/* VIV simulation                */ simulateVIV     {PolarQ::yes
};
// TODO: Devise new terminology to replace steady/unsteady

inline constexpr FlowDir
/* Struct. oscillation direction */ flowDir         {FlowDir::x
};
inline constexpr OscilDir
/* Struct. oscillation direction */ oscillationDir  {OscilDir::y
};
inline constexpr TurbModel
/* Turbulence model              */ turbulenceModel {TurbModel::none
};

//----------------------------------------------------------------------------//
//**************************** Filing description ****************************//
//----------------------------------------------------------------------------//

inline constexpr int
/* Filing timestep interval      */ fileInterv      {1000},
/* Total number of animations    */ animationCount  {6};

inline constexpr PolarQ
/* End simulation after animation*/ endSimAfterAnim {PolarQ::no
};

#include "animations.h"

/* Animation start timestep       : bgnAnimTimeStep */
/* Anim. filing timestep interval : animFileInterv  */
/* Total animation files to end   : endAnimSolFiles */
/* Animation argument order: bgnAnimTimeStep, animFileInterv, endAnimSolFiles */
inline const Animation
    anim00(5000,    100, 120),
    anim01(100000,  100, 360),
    anim02(150000,  100, 360),
    anim03(2000000, 100, 360),
    anim04(2500000, 100, 360),
    anim05(3000000, 100, 360)
;

inline const string
    fileUniqueName {"turb3-1CylSp2_tmp"
    },
    remarks {       "Turbulent case with VIV of three cylinders, preceded by"
            +margin+"a fixed cylinder; space between cylinders = 2."
    };

//----------------------------------------------------------------------------//
// NOTES on usage of additional functionality --------------------------------//
//----------------------------------------------------------------------------//

/**
 * 1. A value < 0 for the following parameters will turn off the related
 *    functionality.
 *      a. dtOverride       : adaptive timestepper: turned off.
 *      b. timestepsPaused  : VIV: structures will oscillate from the start.
 *      c. fileInterv       : solution files: only one will be saved at the end.
 *      d. Convergence crit.: if all three of uChangeCriteria, vChangeCriteria,
 *                            and wChangeCriteria are set as less than zero,
 *                            then the solver will not check for convergence.
 *
 * 2. If dtOverride > 0, then the following will be ignored,
 *      a. cflFactor        : adaptive timestepper: CFL factor.
 *      b. gridFoFactor     : adaptive timestepper: grid Fourier factor.
 *      c. dtMin            : adaptive timestepper: minimum value of dt.
 *
 * 3. If turbulenceModel == TurbModel::none, then the following will be ignored.
 *      a. cs               : turbulence: Smagorinsky constant.
 *
 * 4. If steadiness != SolState::unsteady, then the following will be ignored.
 *      a. reducedVel       : VIV: reduced velocity.
 *      b. massRatio        : VIV: mass ratio.
 *      c. structDamping    : VIV: structural damping ratio.
 *      d. timestepsPaused  : VIV: timesteps structure held fixed.
 */

//---------------------------------- IGNORE ----------------------------------//
#include "postsetup.inl"                                                      //
#endif                                                                        //
//----------------------------------------------------------------------------//
