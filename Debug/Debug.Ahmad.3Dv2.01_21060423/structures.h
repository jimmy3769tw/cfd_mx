/**
 * @file        structures.h
 *
 * @project     3D N-S solver rebuild
 * @version     2.0beta
 *
 * @author      Syed Ahmad Raza (sahmadrz@gmail.com)
 * @date        2020-10-27
 *
 * @brief       Create the structures by defining ETA throughout the grid
 *
 */

#ifndef STRUCTURES_H
#define STRUCTURES_H

#include "presetup.inl"

void constructStructures();
void updateStructures();
void printStructures(std::ostream& out);
void printLoopLimtis();
void updateVirtualForce();
void filerTimestepDataStart();
void filerTimestepData();
void updateVIV();

//----------------------------------------------------------------------------//
//******************* Template Structure Class Definition ********************//
//----------------------------------------------------------------------------//

template<typename T>
class Structure {
  protected:
    const int                       m_id;
    const double                    m_diameter;
    const Structures::InitCenter    m_initCenter;
    const Structures::State         m_state;
    const Structures::Kind          m_kind;
    inline static int s_idGenerator{};
    inline static array<T*, Structures::Total<T>::count> s_structures;

    Structure() = delete;
    Structure(const Structure&) = delete;
    Structure& operator=(const Structure&) = delete;

    Structure(const double diameter,
                const double xInitCenter, const double yInitCenter,
                const double zInitCenter,
                const Structures::State state, const Structures::Kind kind)
        : m_id{s_idGenerator++},
          m_diameter{diameter},
          m_initCenter{xInitCenter, yInitCenter, zInitCenter}, m_state{state},
          m_kind{kind} {
        s_structures[m_id] = static_cast<T*>(this);
    }
  public:
    // Individual structure characteristics
    int getId() const                                   { return m_id; }
    double getDia() const                               { return m_diameter; }
    const Structures::InitCenter& getInitCenter() const { return m_initCenter; }
    const Structures::State& getStructState() const     { return m_state; }
    const Structures::Kind& getStructKind() const       { return m_kind; }

    // Individual structure
    static T& getOne(const int id)  { return *s_structures[id]; }
    static T* getOneP(const int id) { return s_structures[id]; }

    // All structures of a type
    static array<T*, Structures::Total<T>::count>& getSet() {
        return s_structures;
    }
};

//----------------------------------------------------------------------------//
//************* Template Structure Children Classes Definitions **************//
//----------------------------------------------------------------------------//

class Sphere : public Structure<Sphere> {
  public:
    Sphere(const double diameter,
            const double xInitCenter, const double yInitCenter,
            const double zInitCenter,
            const Structures::State state=Structures::State::fixed)
            : Structure(diameter, xInitCenter, yInitCenter, zInitCenter, state,
                Structures::Kind::sphere) {
    }
};

class CylinderX : public Structure<CylinderX> {
  public:
    CylinderX(const double diameter,
            const double yInitCenter, const double zInitCenter,
            const Structures::State state=Structures::State::fixed)
            : Structure(diameter, -1.0, yInitCenter, zInitCenter, state,
               Structures::Kind::cylinderX) {
    }
};

class CylinderY : public Structure<CylinderY> {
  public:
    CylinderY(const double diameter,
            const double xInitCenter, const double zInitCenter,
            const Structures::State state=Structures::State::fixed)
            : Structure(diameter, xInitCenter, -1.0, zInitCenter, state,
               Structures::Kind::cylinderY) {
    }
};

class CylinderZ : public Structure<CylinderZ> {
  public:
    CylinderZ(const double diameter,
            const double xInitCenter, const double yInitCenter,
            const Structures::State state=Structures::State::fixed)
            : Structure(diameter, xInitCenter, yInitCenter, -1.0, state,
               Structures::Kind::cylinderZ) {
    }
};

//----------------------------------------------------------------------------//
//****************** Mutable Structure Classes Declarations ******************//
//----------------------------------------------------------------------------//

class SphereMutable;
class CylinderXMutable;
class CylinderYMutable;
class CylinderZMutable;

#endif