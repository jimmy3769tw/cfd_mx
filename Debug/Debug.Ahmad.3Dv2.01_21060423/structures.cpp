/**
 * @file        structures.cpp
 *
 * @project     3D N-S solver rebuild
 * @version     2.0beta
 *
 * @author      Syed Ahmad Raza (sahmadrz@gmail.com)
 * @date        2020-10-27
 *
 * @brief       Create the structures by defining ETA throughout the grid
 *
 */

#include "parameters.h"
#include "structures.h"
#include <tuple>
#include <fstream>              // file output
#include <iomanip>              // set output width
#include <limits>               // for "numeric_limits<double>::max_digits10"

using std::ostream, std::right, std::left, std::setw, std::numeric_limits,
    std::ios_base, std::endl, std::left, std::setw,
    std::tuple, std::tie, std::get;
using namespace Structures;

//----------------------------------------------------------------------------//
//******************** Structures::LoopLimit Definitions *********************//
//----------------------------------------------------------------------------//

LoopLimit& Structures::LoopLimit::add(int num) {
    iBgn+=num, iEnd+=num, jBgn+=num, jEnd+=num, kBgn+=num, kEnd+=num;
    return *this;
}
LoopLimit Structures::LoopLimit::operator+(const LoopLimit& ll) const {
    return LoopLimit {
        iBgn+ll.iBgn, iEnd+ll.iEnd,
        jBgn+ll.jBgn, jEnd+ll.jEnd,
        kBgn+ll.kBgn, kEnd+ll.kEnd,
    };
}

//----------------------------------------------------------------------------//
//******************** Mutable Structure Classes Helpers *********************//
//----------------------------------------------------------------------------//

inline int defSLoopBgn(const double* coordinateP, const double center,
        const double dia) {
    const double r = 0.5*dia;
    int q=0, qBgn=0, testBgn=0;
    while (!testBgn) {
        if (*(coordinateP+q) > (center - r)) {
            qBgn = q-1;
            ++testBgn;
        }
        ++q;
    }
    return qBgn;
}

inline int defSLoopEnd(const double* coordinateP, const double center,
        const double dia) {
    const double r = 0.5*dia;
    int q=0, qEnd=0, testEnd=0;
    while (!testEnd) {
        if (*(coordinateP+q) >= (center + r)) {
            qEnd = q;
            ++testEnd;
        }
        ++q;
    }
    return qEnd;
}
/**
 * Notes: Two aspects should be remembered while choosing the start and end of
 * the loop limits.
 * 1.   These loop limits will be applied to cell-centered matrices but the
 *      distance matrices (coordinateP) used to determine them are defined at
 *      the cell edges.
 * 2.   The end of the loop limits will be affected by the less than (<) loop
 *      condition, which is used in almost all the loops in this code.
 *      Both of these aspects sort of cancel the effect of each other in
 *      defSLoopEnd.
 * 3.   The reason for using greater than (>) in defSLoopBgn but greater than
 *      and equal to (>=) in defSLoopEnd is to obtain the same outcome w.r.t.
 *      positioning of the loop limits in each of the two cases, either when
 *      the center coincides with the the coordinate or when the center lies
 *      between two coordinates.
 */

//----------------------------------------------------------------------------//
//****************** Mutable Structure Classes Definitions *******************//
//----------------------------------------------------------------------------//

class SphereMutable {
  private:
    const int         m_id;
    const Sphere*     m_sP;
    const double      m_icc;    // Initial center coordinate in moving direction
    double            m_dis;
    double            m_vel;
    Center            m_cn;
    LoopLimit         m_ll;
    VForce            m_vf;
    Coeff             m_coeff;
    inline static int s_idGenerator{};
    inline static array<SphereMutable*, Total<Sphere>::count> s_mutables;

    SphereMutable(const SphereMutable&) = delete;
    SphereMutable& operator=(const SphereMutable&) = delete;

  public:
    explicit SphereMutable()
    : m_id{s_idGenerator++}, m_sP{Sphere::getOneP(m_id)},
      m_icc{oscillationDir == OscilDir::x ? m_sP->getInitCenter().x
                : (oscillationDir == OscilDir::y ? m_sP->getInitCenter().y
                                                 : m_sP->getInitCenter().z)},
      m_dis{0.0}, m_vel{0.0},
      m_cn{m_sP->getInitCenter().x, m_sP->getInitCenter().y,
           m_sP->getInitCenter().z},
      m_ll{defSLoopBgn(X.data(), m_sP->getInitCenter().x, m_sP->getDia()),
           defSLoopEnd(X.data(), m_sP->getInitCenter().x, m_sP->getDia()),
           defSLoopBgn(Y.data(), m_sP->getInitCenter().y, m_sP->getDia()),
           defSLoopEnd(Y.data(), m_sP->getInitCenter().y, m_sP->getDia()),
           defSLoopBgn(Z.data(), m_sP->getInitCenter().z, m_sP->getDia()),
           defSLoopEnd(Z.data(), m_sP->getInitCenter().z, m_sP->getDia())},
      m_vf{0, 0, 0}, m_coeff{0, 0} {
        s_mutables[m_id] = this;
    }
    int getId() const { return m_id; }
    const Sphere* getRootP() const { return m_sP; }
    const tuple<double, double> getVIVMotion() const { return {m_dis, m_vel}; }
    const Center getCenter() const { return m_cn; }
    const LoopLimit getLoopLimit() const { return m_ll; }
    const VForce getVForce() const { return m_vf; }
    const Coeff getCoeff() const { return m_coeff; }
    void setVIVMotionAndCenter(tuple<double, double> valPair) {
        m_dis = get<0>(valPair), m_vel = get<1>(valPair);
        if constexpr      (oscillationDir == OscilDir::x){m_cn.x = m_icc+m_dis;}
        else if constexpr (oscillationDir == OscilDir::y){m_cn.y = m_icc+m_dis;}
        else                                             {m_cn.z = m_icc+m_dis;}
    }
    void updateLoopLimits() {
        if constexpr (oscillationDir == OscilDir::x) {
            m_ll.iBgn = defSLoopBgn(X.data(), m_cn.x, m_sP->getDia());
            m_ll.iEnd = defSLoopEnd(X.data(), m_cn.x, m_sP->getDia());
        }
        else if constexpr (oscillationDir == OscilDir::y) {
            m_ll.jBgn = defSLoopBgn(Y.data(), m_cn.y, m_sP->getDia());
            m_ll.jEnd = defSLoopEnd(Y.data(), m_cn.y, m_sP->getDia());
        }
        else {
            m_ll.kBgn = defSLoopBgn(Z.data(), m_cn.z, m_sP->getDia());
            m_ll.kEnd = defSLoopEnd(Z.data(), m_cn.z, m_sP->getDia());
        }
    }
    void setVForces(double vF1, double vF2) {
        if constexpr (flowDir != FlowDir::x && oscillationDir != OscilDir::x) {
            m_vf.y = vF1, m_vf.z = vF2;   }
        if constexpr (flowDir != FlowDir::y && oscillationDir != OscilDir::y) {
            m_vf.x = vF1, m_vf.z = vF2;   }
        if constexpr (flowDir != FlowDir::z && oscillationDir != OscilDir::z) {
            m_vf.x = vF1, m_vf.y = vF2;   }
        // Assuming an inlet velocity = 1, fluid density = 1 and diameter = 1.
        if constexpr (flowDir == FlowDir::x) { m_coeff.drag = -8.0*m_vf.x/M_PI;}
        if constexpr (flowDir == FlowDir::y) { m_coeff.drag = -8.0*m_vf.y/M_PI;}
        if constexpr (flowDir == FlowDir::z) { m_coeff.drag = -8.0*m_vf.z/M_PI;}
        if constexpr (oscillationDir == OscilDir::x)
                                            { m_coeff.lift = -8.0*m_vf.x/M_PI;}
        if constexpr (oscillationDir == OscilDir::y)
                                            { m_coeff.lift = -8.0*m_vf.y/M_PI;}
        if constexpr (oscillationDir == OscilDir::z)
                                            { m_coeff.lift = -8.0*m_vf.z/M_PI;}
    }
    static SphereMutable* getOneP(const int id) { return s_mutables[id]; }
    static array<SphereMutable*, Total<Sphere>::count>& getSet() {
        return s_mutables;
    }
};

class CylinderXMutable {
  private:
    const int         m_id;
    const CylinderX*  m_sP;
    const double      m_icc;    // Initial center coordinate in moving direction
    double            m_dis;
    double            m_vel;
    Center            m_cn;
    LoopLimit         m_ll;
    VForce            m_vf;
    Coeff             m_coeff;
    inline static int s_idGenerator{};
    inline static array<CylinderXMutable*, Total<CylinderX>::count> s_mutables;

    CylinderXMutable(const CylinderXMutable&) = delete;
    CylinderXMutable& operator=(const CylinderXMutable&) = delete;

  public:
    explicit CylinderXMutable()
    : m_id{s_idGenerator++}, m_sP{CylinderX::getOneP(m_id)},
      m_icc{oscillationDir == OscilDir::y ? m_sP->getInitCenter().y
                                          : m_sP->getInitCenter().z},
      m_dis{0.0}, m_vel{0.0},
      m_cn{-1.0, m_sP->getInitCenter().y, m_sP->getInitCenter().z},
      m_ll{0, nx,
           defSLoopBgn(Y.data(), m_sP->getInitCenter().y, m_sP->getDia()),
           defSLoopEnd(Y.data(), m_sP->getInitCenter().y, m_sP->getDia()),
           defSLoopBgn(Z.data(), m_sP->getInitCenter().z, m_sP->getDia()),
           defSLoopEnd(Z.data(), m_sP->getInitCenter().z, m_sP->getDia())},
      m_vf{0, 0, 0}, m_coeff{0, 0} {
        s_mutables[m_id] = this;
    }
    int getId() const { return m_id; }
    const CylinderX* getRootP() const { return m_sP; }
    const tuple<double, double> getVIVMotion() const { return {m_dis, m_vel}; }
    const Center getCenter() const { return m_cn; }
    const LoopLimit getLoopLimit() const { return m_ll; }
    const VForce getVForce() const { return m_vf; }
    const Coeff getCoeff() const { return m_coeff; }
    void setVIVMotionAndCenter(tuple<double, double> valPair) {
        m_dis = get<0>(valPair), m_vel = get<1>(valPair);
        if constexpr (oscillationDir == OscilDir::y) { m_cn.y = m_icc + m_dis; }
        else                                         { m_cn.z = m_icc + m_dis; }
    }
    void updateLoopLimits() {
        if constexpr (oscillationDir == OscilDir::y) {
            m_ll.jBgn = defSLoopBgn(Y.data(), m_cn.y, m_sP->getDia());
            m_ll.jEnd = defSLoopEnd(Y.data(), m_cn.y, m_sP->getDia());
        }
        else {
            m_ll.kBgn = defSLoopBgn(Z.data(), m_cn.z, m_sP->getDia());
            m_ll.kEnd = defSLoopEnd(Z.data(), m_cn.z, m_sP->getDia());
        }
    }
    void setVForces(double yVF, double zVF) {
        m_vf.y = yVF, m_vf.z = zVF;
        // Assuming an inlet velocity = 1 and diameter = 1.
        if constexpr (flowDir == FlowDir::y) {
            m_coeff.drag = -2.0*getVForce().y/xDist;
            m_coeff.lift = -2.0*getVForce().z/xDist;
        }
        else {
            m_coeff.drag = -2.0*getVForce().z/xDist;
            m_coeff.lift = -2.0*getVForce().y/xDist;
        }
    }
    static CylinderXMutable* getOneP(const int id) { return s_mutables[id]; }
    static array<CylinderXMutable*, Total<CylinderX>::count>& getSet() {
        return s_mutables;
    }
};

class CylinderYMutable {
  private:
    const int         m_id;
    const CylinderY*  m_sP;
    const double      m_icc;    // Initial center coordinate in moving direction
    double            m_dis;
    double            m_vel;
    Center            m_cn;
    LoopLimit         m_ll;
    VForce            m_vf;
    Coeff             m_coeff;
    inline static int s_idGenerator{};
    inline static array<CylinderYMutable*, Total<CylinderY>::count> s_mutables;

    CylinderYMutable(const CylinderYMutable&) = delete;
    CylinderYMutable& operator=(const CylinderYMutable&) = delete;

  public:
    explicit CylinderYMutable()
    : m_id{s_idGenerator++}, m_sP{CylinderY::getOneP(m_id)},
      m_icc{oscillationDir == OscilDir::x ? m_sP->getInitCenter().x
                                          : m_sP->getInitCenter().z},
      m_dis{0.0}, m_vel{0.0},
      m_cn{m_sP->getInitCenter().x, -1.0, m_sP->getInitCenter().z},
      m_ll{defSLoopBgn(X.data(), m_sP->getInitCenter().x, m_sP->getDia()),
           defSLoopEnd(X.data(), m_sP->getInitCenter().x, m_sP->getDia()),
           0, ny,
           defSLoopBgn(Z.data(), m_sP->getInitCenter().z, m_sP->getDia()),
           defSLoopEnd(Z.data(), m_sP->getInitCenter().z, m_sP->getDia())},
      m_vf{0, 0, 0}, m_coeff{0, 0} {
        s_mutables[m_id] = this;
    }
    int getId() const { return m_id; }
    const CylinderY* getRootP() const { return m_sP; }
    const tuple<double, double> getVIVMotion() const { return {m_dis, m_vel}; }
    const Center getCenter() const { return m_cn; }
    const LoopLimit getLoopLimit() const { return m_ll; }
    const VForce getVForce() const { return m_vf; }
    const Coeff getCoeff() const { return m_coeff; }
    void setVIVMotionAndCenter(tuple<double, double> valPair) {
        m_dis = get<0>(valPair), m_vel = get<1>(valPair);
        if constexpr (oscillationDir == OscilDir::x) { m_cn.x = m_icc + m_dis; }
        else                                         { m_cn.z = m_icc + m_dis; }
    }
    void updateLoopLimits() {
        if constexpr (oscillationDir == OscilDir::x) {
            m_ll.iBgn = defSLoopBgn(X.data(), m_cn.x, m_sP->getDia());
            m_ll.iEnd = defSLoopEnd(X.data(), m_cn.x, m_sP->getDia());
        }
        else {
            m_ll.kBgn = defSLoopBgn(Z.data(), m_cn.z, m_sP->getDia());
            m_ll.kEnd = defSLoopEnd(Z.data(), m_cn.z, m_sP->getDia());
        }
    }
    void setVForces(double xVF, double zVF) {
        m_vf.x = xVF, m_vf.z = zVF;
        // Assuming an inlet velocity = 1 and diameter = 1.
        if constexpr (flowDir == FlowDir::x) {
            m_coeff.drag = -2.0*getVForce().x/yDist;
            m_coeff.lift = -2.0*getVForce().z/yDist;
        }
        else {
            m_coeff.drag = -2.0*getVForce().z/yDist;
            m_coeff.lift = -2.0*getVForce().x/yDist;
        }
    }
    static CylinderYMutable* getOneP(const int id) { return s_mutables[id]; }
    static array<CylinderYMutable*, Total<CylinderY>::count>& getSet() {
        return s_mutables;
    }
};

class CylinderZMutable {
  private:
    const int         m_id;
    const CylinderZ*  m_sP;
    const double      m_icc;    // Initial center coordinate in moving direction
    double            m_dis;
    double            m_vel;
    Center            m_cn;
    LoopLimit         m_ll;
    VForce            m_vf;
    Coeff             m_coeff;
    inline static int s_idGenerator{};
    inline static array<CylinderZMutable*, Total<CylinderZ>::count> s_mutables;

    CylinderZMutable(const CylinderZMutable&) = delete;
    CylinderZMutable& operator=(const CylinderZMutable&) = delete;

  public:
    explicit CylinderZMutable()
    : m_id{s_idGenerator++}, m_sP{CylinderZ::getOneP(m_id)},
      m_icc{oscillationDir == OscilDir::x ? m_sP->getInitCenter().x
                                          : m_sP->getInitCenter().y},
      m_dis{0.0}, m_vel{0.0},
      m_cn{m_sP->getInitCenter().x, m_sP->getInitCenter().y, -1.0},
      m_ll{defSLoopBgn(X.data(), m_sP->getInitCenter().x, m_sP->getDia()),
           defSLoopEnd(X.data(), m_sP->getInitCenter().x, m_sP->getDia()),
           defSLoopBgn(Y.data(), m_sP->getInitCenter().y, m_sP->getDia()),
           defSLoopEnd(Y.data(), m_sP->getInitCenter().y, m_sP->getDia()),
           0, nz},
      m_vf{0, 0, 0}, m_coeff{0, 0} {
        s_mutables[m_id] = this;
    }
    int getId() const { return m_id; }
    const CylinderZ* getRootP() const { return m_sP; }
    const tuple<double, double> getVIVMotion() const { return {m_dis, m_vel}; }
    const Center getCenter() const { return m_cn; }
    const LoopLimit getLoopLimit() const { return m_ll; }
    const VForce getVForce() const { return m_vf; }
    const Coeff getCoeff() const { return m_coeff; }
    void setVIVMotionAndCenter(tuple<double, double> valPair) {
        m_dis = get<0>(valPair), m_vel = get<1>(valPair);
        if constexpr (oscillationDir == OscilDir::x) { m_cn.x = m_icc + m_dis; }
        else                                         { m_cn.y = m_icc + m_dis; }
    }
    void updateLoopLimits() {
        if constexpr (oscillationDir == OscilDir::x) {
            m_ll.iBgn = defSLoopBgn(X.data(), m_cn.x, m_sP->getDia());
            m_ll.iEnd = defSLoopEnd(X.data(), m_cn.x, m_sP->getDia());
        }
        else {
            m_ll.jBgn = defSLoopBgn(Y.data(), m_cn.y, m_sP->getDia());
            m_ll.jEnd = defSLoopEnd(Y.data(), m_cn.y, m_sP->getDia());
        }
    }
    void setVForces(double xVF, double yVF) {
        m_vf.x = xVF, m_vf.y = yVF;
        // Assuming an inlet velocity = 1 and diameter = 1.
        if constexpr (flowDir == FlowDir::x) {
            m_coeff.drag = -2.0*m_vf.x/zDist;
            m_coeff.lift = -2.0*m_vf.y/zDist;
        }
        else {
            m_coeff.drag = -2.0*m_vf.y/zDist;
            m_coeff.lift = -2.0*m_vf.x/zDist;
        }
    }
    static CylinderZMutable* getOneP(const int id) { return s_mutables[id]; }
    static array<CylinderZMutable*, Total<CylinderZ>::count>& getSet() {
        return s_mutables;
    }
};

//----------------------------------------------------------------------------//
//********************* Structure Construction Functions *********************//
//----------------------------------------------------------------------------//

void constructSpheres(double* etaP, const double dia, const LoopLimit ll,
        const int nxSubS, const int nySubS, const int nzSubS,
        const double xCenter, const double yCenter, const double zCenter,
        const double* const xP, const double* const dxP,
        const double* const yP, const double* const dyP,
        const double* const zP, const double* const dzP) {
    const double r = 0.5*dia;
    /*
     * xmid, ymid, zmid are midpoints of cells
     * dxs, dys, dzs are subgrid cell widths
     * ysmid0, zsmid0 are midpoints of 1st subgrid cell
     * xsmid, ysmid, zsmid are midpoints of subgrid cells
     */
    #pragma omp parallel for default(none)\
        firstprivate(etaP, r, ll, nxSubS, nySubS, nzSubS,\
            xCenter, yCenter, zCenter, xP, dxP, yP, dyP, zP, dzP)
    for (int i=ll.iBgn; i<ll.iEnd; ++i) {
        double x = *(xP+i), dx = *(dxP+i+gc), xmid = x + (dx/2.0);
        for (int j=ll.jBgn; j<ll.jEnd; ++j) {
            double y = *(yP+j), dy = *(dyP+j+gc), ymid = y + (dy/2.0);
            for (int k=ll.kBgn; k<ll.kEnd; ++k) {
                double z = *(zP+k), dz = *(dzP+k+gc), zmid = z + (dz/2.0);
                double eta {0.0};

                // Simple volume of solid for sphere
                double sphereCenterToCellCenter
                    = sqrt(  (xmid - xCenter)*(xmid - xCenter)
                           + (ymid - yCenter)*(ymid - yCenter)
                           + (zmid - zCenter)*(zmid - zCenter));
                if (sphereCenterToCellCenter <= r) { eta = 1.0; }

                // Subgrid method for sphere
                double cellDiagonalHalf
                    = sqrt(dx*dx + dy*dy + dz*dz)/2.0;
                if (abs(sphereCenterToCellCenter - r) <= cellDiagonalHalf) {
                    double dxs    = dx/static_cast<double>(nxSubS),
                           dys    = dy/static_cast<double>(nySubS),
                           dzs    = dz/static_cast<double>(nzSubS),
                           xsmid  = x + dxs/2.0,
                           ysmid0 = y + dys/2.0,
                           zsmid0 = z + dzs/2.0;

                    // Summation of xi for all subgrids in the main cell
                    double xi = 0.0;
                    for (int l = 0; l < nxSubS; ++l) {
                        double ysmid = ysmid0;
                        for (int m = 0; m < nySubS; ++m) {
                            double zsmid = zsmid0;
                            for (int n = 0; n < nzSubS; ++n) {
                                double sphereCenterToSubgridCellCenter
                                    = sqrt(  (xsmid - xCenter)*(xsmid - xCenter)
                                           + (ysmid - yCenter)*(ysmid - yCenter)
                                           + (zsmid - zCenter)*(zsmid - zCenter)
                                           );

                                if (sphereCenterToSubgridCellCenter <= r) {
                                    xi += 1;
                                }
                                zsmid += dzs;
                            }
                            ysmid += dys;
                        }
                        xsmid += dxs;
                    }
                    eta = xi / static_cast<double>(nxSubS*nySubS*nzSubS);
                }
                *(etaP+(i+gc)+(j+gc)*nxt+(k+gc)*nxt*nyt) = eta;
            }
        }
    }
}

void constructCylinders(double* etaP, const double dia, const Kind kind,
        const int aBgn, const int aEnd,
        const int bBgn, const int bEnd,
        const int cBgn, const int cEnd,
        const int nbSub, const int ncSub,
        const double bCenter, const double cCenter,
        const double* const bP, const double* const cP,
        const double* const dbP, const double* const dcP) {
    const double r = 0.5*dia;
    /*
     * bmid, cmid are midpoints of cells
     * dbs, dcs are subgrid cell widths
     * csmid0 is midpoint of 1st subgrid cell
     * bsmid, csmid are midpoints of subgrid cells
     */
    #pragma omp parallel for default(none)\
        firstprivate(etaP, r, kind, aBgn, aEnd, bBgn, bEnd, cBgn, cEnd,\
            nbSub, ncSub, bCenter, cCenter, bP, cP, dbP, dcP)
    for (int bInd=bBgn; bInd<bEnd; ++bInd) {
        double b = *(bP+bInd), db = *(dbP+bInd+gc), bmid = b + (db/2.0);
        for (int cInd=cBgn; cInd<cEnd; ++cInd) {
            double c = *(cP+cInd), dc = *(dcP+cInd+gc), cmid = c + (dc/2.0);

            // Default value of volume of solid for fluid region
            double eta = 0.0;

            // Simple volume of solid for cylinder
            double cylCenterToCellCenter
                = sqrt(  (bmid - bCenter)*(bmid - bCenter)
                       + (cmid - cCenter)*(cmid - cCenter) );
            if (cylCenterToCellCenter <= r) { eta = 1.0; }

            // Subgrid method for cylinder
            double cellDiagonalHalf = sqrt(db*db + dc*dc)/2.0;
            if (abs(cylCenterToCellCenter - r) <= cellDiagonalHalf) {
                double dbs    = db/static_cast<double>(nbSub),
                       dcs    = dc/static_cast<double>(ncSub),
                       bsmid  = b + dbs/2.0,
                       csmid0 = c + dcs/2.0;

                // Summation of xi for all subgrids in the main cell
                double xi = 0.0;
                for (int m=0; m<nbSub; ++m) {
                    double csmid = csmid0;
                    for (int n=0; n<ncSub; ++n) {
                        double cylCenterToSubgridCellCenter
                            = sqrt(  (bsmid - bCenter)*(bsmid - bCenter)
                                   + (csmid - cCenter)*(csmid - cCenter) );

                        if (cylCenterToSubgridCellCenter <= r) {
                            xi += 1;
                        }
                        csmid += dcs;
                    }
                    bsmid += dbs;
                }
                eta = xi / static_cast<double>(nbSub*ncSub);
            }
            if (kind == Kind::cylinderX) {
                for (int aInd = aBgn; aInd < aEnd; ++aInd) {
                    *(etaP+(aInd+gc)+(bInd+gc)*nxt+(cInd+gc)*nxt*nyt) = eta;
                }
            }
            else if (kind == Kind::cylinderY) {
                for (int aInd = aBgn; aInd < aEnd; ++aInd) {
                    *(etaP+(bInd+gc)+(aInd+gc)*nxt+(cInd+gc)*nxt*nyt) = eta;
                }
            }
            else if (kind == Kind::cylinderZ) {
                for (int aInd = aBgn; aInd < aEnd; ++aInd) {
                    *(etaP+(bInd+gc)+(cInd+gc)*nxt+(aInd+gc)*nxt*nyt) = eta;
                }
            }
        }
    }
}

void constructStructures() {
    double  * etaP   {ETA.data()},   * velSXP {VELSX.data()},
            * velSYP {VELSY.data()}, * velSZP {VELSZ.data()};

    #pragma omp parallel for default(none) firstprivate(etaP,\
        velSXP, velSYP, velSZP)
    for (int k=0; k<nzt; ++k) {
        for (int j=0; j<nyt; ++j) {
            for (int i=0; i<nxt; ++i) {
                *(etaP   + i + j*nxt + k*nxt*nyt) = 0.0;
                *(velSXP + i + j*nxt + k*nxt*nyt) = 0.0;
                *(velSYP + i + j*nxt + k*nxt*nyt) = 0.0;
                *(velSZP + i + j*nxt + k*nxt*nyt) = 0.0;
            }
        }
    }
    if constexpr (static_cast<bool>(Total<Sphere>::count)) {
        for (const Sphere* sP : Sphere::getSet()) {
            SphereMutable* sMP = new SphereMutable;
            constructSpheres(ETA.data(),
                sP->getDia(), sMP->getLoopLimit(), nxSub, nySub, nzSub,
                sP->getInitCenter().x, sP->getInitCenter().y,
                sP->getInitCenter().z,
                X.data(), DX.data(), Y.data(), DY.data(), Z.data(), DZ.data());
        }
    }
    if constexpr (static_cast<bool>(Total<CylinderX>::count)) {
        for (const CylinderX* sP : CylinderX::getSet()) {
            CylinderXMutable* sMP = new CylinderXMutable;
            constructCylinders(ETA.data(),
                sP->getDia(), sP->getStructKind(),
                sMP->getLoopLimit().iBgn, sMP->getLoopLimit().iEnd,
                sMP->getLoopLimit().jBgn, sMP->getLoopLimit().jEnd,
                sMP->getLoopLimit().kBgn, sMP->getLoopLimit().kEnd,
                nySub, nzSub, sP->getInitCenter().y, sP->getInitCenter().z,
                Y.data(), Z.data(), DY.data(), DZ.data());
        }
    }
    if constexpr (static_cast<bool>(Total<CylinderY>::count)) {
        for (const CylinderY* sP : CylinderY::getSet()) {
            CylinderYMutable* sMP = new CylinderYMutable;
            constructCylinders(ETA.data(),
                sP->getDia(), sP->getStructKind(),
                sMP->getLoopLimit().jBgn, sMP->getLoopLimit().jEnd,
                sMP->getLoopLimit().iBgn, sMP->getLoopLimit().iEnd,
                sMP->getLoopLimit().kBgn, sMP->getLoopLimit().kEnd,
                nxSub, nzSub, sP->getInitCenter().x, sP->getInitCenter().z,
                X.data(), Z.data(), DX.data(), DZ.data());
        }
    }
    if constexpr (static_cast<bool>(Total<CylinderZ>::count)) {
        for (const CylinderZ* sP : CylinderZ::getSet()) {
            CylinderZMutable* sMP = new CylinderZMutable;
            constructCylinders(ETA.data(),
                sP->getDia(), sP->getStructKind(),
                sMP->getLoopLimit().kBgn, sMP->getLoopLimit().kEnd,
                sMP->getLoopLimit().iBgn, sMP->getLoopLimit().iEnd,
                sMP->getLoopLimit().jBgn, sMP->getLoopLimit().jEnd,
                nxSub, nySub, sP->getInitCenter().x, sP->getInitCenter().y,
                X.data(), Y.data(), DX.data(), DY.data());
        }
    }
    // printLoopLimtis();
}

//----------------------------------------------------------------------------//
//************************ Structure Update Functions ************************//
//----------------------------------------------------------------------------//

void clearStructure(double* etaP, LoopLimit ll) {
    #pragma omp parallel for default(none) firstprivate(ll, etaP)
    for (int k=ll.kBgn; k<ll.kEnd; ++k) {
        for (int j=ll.jBgn; j<ll.jEnd; ++j) {
            for (int i=ll.iBgn; i<ll.iEnd; ++i) {
                *(etaP + i + j*nxt + k*nxt*nyt) = 0.0;
            }
        }
    }
}

void clearStructVel(double* velSP, const LoopLimit ll) {
    #pragma omp parallel for default(none) firstprivate(ll, velSP)
    for (int k=ll.kBgn; k<ll.kEnd; ++k) {
        for (int j=ll.jBgn; j<ll.jEnd; ++j) {
            for (int i=ll.iBgn; i<ll.iEnd; ++i) {
                *(velSP + i + j*nxt + k*nxt*nyt) = 0.0;
            }
        }
    }
}

void setStructVel(double* velSP, const LoopLimit ll, double velNew) {
    #pragma omp parallel for default(none) firstprivate(ll, velNew, velSP)
    for (int k=ll.kBgn; k<ll.kEnd; ++k) {
        for (int j=ll.jBgn; j<ll.jEnd; ++j) {
            for (int i=ll.iBgn; i<ll.iEnd; ++i) {
                *(velSP + i + j*nxt + k*nxt*nyt) = velNew;
            }
        }
    }
}

void updateStructures() {
    /**
     * Based on the oscillation direction, assign the correct structure velocity
     * array to the structure velocity array pointer and extend loop limits in
     * the corresponding staggered direction account for the staggered grid.
     */
    double* etaP {ETA.data()}, * velSP {nullptr};
    LoopLimit llo { 0, 0, 0, 0, 0, 0 }, ll0 { llo }; // offset loop limits
    llo.add(gc), ll0.add(gc);
    if constexpr (oscillationDir==OscilDir::x) {
        velSP = VELSX.data(), --llo.iBgn, ++llo.iEnd;
    }
    if constexpr (oscillationDir==OscilDir::y) {
        velSP = VELSY.data(), --llo.jBgn, ++llo.jEnd;
    }
    if constexpr (oscillationDir==OscilDir::z) {
        velSP = VELSZ.data(), --llo.kBgn, ++llo.kEnd;
    }

    if constexpr (static_cast<bool>(Total<Sphere>::count)) {
        for (SphereMutable* sMP : SphereMutable::getSet()) {
            const Sphere* sP = sMP->getRootP();
            if (sP->getStructState() == State::moving) {
                clearStructure(etaP, sMP->getLoopLimit()+ll0);
                clearStructVel(velSP, sMP->getLoopLimit()+llo);
                sMP->updateLoopLimits();
                constructSpheres(etaP,
                    sP->getDia(), sMP->getLoopLimit(), nxSub, nySub, nzSub,
                    sMP->getCenter().x, sMP->getCenter().y, sMP->getCenter().z,
                    X.data(), DX.data(),
                    Y.data(), DY.data(),
                    Z.data(), DZ.data()
                );
                setStructVel(velSP, sMP->getLoopLimit()+llo,
                    std::get<1>(sMP->getVIVMotion()));
            }
        }
    }
    if constexpr (static_cast<bool>(Total<CylinderX>::count)) {
        for (CylinderXMutable* sMP : CylinderXMutable::getSet()) {
            const CylinderX* sP = sMP->getRootP();
            if (sP->getStructState() == State::moving) {
                clearStructure(etaP, sMP->getLoopLimit()+ll0);
                clearStructVel(velSP, sMP->getLoopLimit()+llo);
                sMP->updateLoopLimits();
                constructCylinders(etaP,
                    sP->getDia(), sP->getStructKind(),
                    sMP->getLoopLimit().iBgn, sMP->getLoopLimit().iEnd,
                    sMP->getLoopLimit().jBgn, sMP->getLoopLimit().jEnd,
                    sMP->getLoopLimit().kBgn, sMP->getLoopLimit().kEnd,
                    nySub, nzSub, sMP->getCenter().y, sMP->getCenter().z,
                    Y.data(), Z.data(), DY.data(), DZ.data()
                );
                setStructVel(velSP, sMP->getLoopLimit()+llo,
                    std::get<1>(sMP->getVIVMotion()));
            }
        }
    }
    if constexpr (static_cast<bool>(Total<CylinderY>::count)) {
        for (CylinderYMutable* sMP : CylinderYMutable::getSet()) {
            const CylinderY* sP = sMP->getRootP();
            if (sP->getStructState() == State::moving) {
                clearStructure(etaP, sMP->getLoopLimit()+ll0);
                clearStructVel(velSP, sMP->getLoopLimit()+llo);
                sMP->updateLoopLimits();
                constructCylinders(etaP,
                    sP->getDia(), sP->getStructKind(),
                    sMP->getLoopLimit().jBgn, sMP->getLoopLimit().jEnd,
                    sMP->getLoopLimit().iBgn, sMP->getLoopLimit().iEnd,
                    sMP->getLoopLimit().kBgn, sMP->getLoopLimit().kEnd,
                    nxSub, nzSub, sMP->getCenter().x, sMP->getCenter().z,
                    X.data(), Z.data(), DX.data(), DZ.data()
                );
                setStructVel(velSP, sMP->getLoopLimit()+llo,
                    std::get<1>(sMP->getVIVMotion()));
            }
        }
    }
    if constexpr (static_cast<bool>(Total<CylinderZ>::count)) {
        for (CylinderZMutable* sMP : CylinderZMutable::getSet()) {
            const CylinderZ* sP = sMP->getRootP();
            if (sP->getStructState() == State::moving) {
                clearStructure(etaP, sMP->getLoopLimit()+ll0);
                clearStructVel(velSP, sMP->getLoopLimit()+llo);
                sMP->updateLoopLimits();
                constructCylinders(etaP,
                    sP->getDia(), sP->getStructKind(),
                    sMP->getLoopLimit().kBgn, sMP->getLoopLimit().kEnd,
                    sMP->getLoopLimit().iBgn, sMP->getLoopLimit().iEnd,
                    sMP->getLoopLimit().jBgn, sMP->getLoopLimit().jEnd,
                    nxSub, nySub, sMP->getCenter().x, sMP->getCenter().y,
                    X.data(), Y.data(), DX.data(), DY.data()
                );
                setStructVel(velSP, sMP->getLoopLimit()+llo,
                    std::get<1>(sMP->getVIVMotion()));
            }
        }
    }
}

double integrateVirtualForce(const LoopLimit ll, const double* f,
        const double* dxm, const double* dym, const double* dzm,
        double* fi, double* fij) {
    // Integrating components in x-direction
    #pragma omp parallel for default(none)\
        firstprivate(ll, f, fi, dxm)
    for (int k=ll.kBgn; k<ll.kEnd; ++k) {
        for (int j=ll.jBgn; j<ll.jEnd; ++j) {
            int m = j + k*nyt;
            *(fi+m) = 0.0;
            for (int i=ll.iBgn; i<ll.iEnd; ++i) {
                int n = i + j*nxt + k*nxt*nyt;
                *(fi+m) += *(f+n)**(dxm+i);
            }
        }
    }
    // Integrating components in y-direction
    #pragma omp parallel for default(none)\
        firstprivate(ll, fi, fij, dym)
    for (int k=ll.kBgn; k<ll.kEnd; ++k) {
        *(fij+k) = 0.0;
        for (int j=ll.jBgn; j<ll.jEnd; ++j) {
            int m = j + k*nyt;
            *(fij+k) += *(fi+m)**(dym+j);
        }
    }
    // Integrating components in z-direction
    double fijk = 0.0;
    #pragma omp parallel for default(none)\
        firstprivate(ll, fij, dzm) reduction(+: fijk)
    for (int k=ll.kBgn; k<ll.kEnd; ++k) {
        fijk += *(fij+k)**(dzm+k);
    }
    return fijk;
}

void updateVirtualForce() {
    /**
     * Loop limits are obtained by traversing the grid points whose index
     * occurs once cell earlier. The loop limits are extended in the direction
     * of the force where the grid is staggered, so as to account for the
     * averaging of eta across cells that takes place when the velocity
     * is updated and the force is determined.
     */
    LoopLimit lloFx { -1, 1, 0, 0, 0, 0 };
    LoopLimit lloFy { 0, 0, -1, 1, 0, 0 };
    LoopLimit lloFz { 0, 0, 0, 0, -1, 1 };
    lloFx.add(gc), lloFy.add(gc), lloFz.add(gc);

    if constexpr (static_cast<bool>(Total<Sphere>::count)) {
        if constexpr (flowDir!=FlowDir::x && oscillationDir!=OscilDir::x) {
            for (SphereMutable* sMP : SphereMutable::getSet()) {
                sMP->setVForces(
                    integrateVirtualForce(sMP->getLoopLimit()+lloFy.add(gc),
                        FY.data(), DXM.data(), DYM.data(), DZM.data(),
                        Fi.data(), Fij.data()),
                    integrateVirtualForce(sMP->getLoopLimit()+lloFz.add(gc),
                        FZ.data(), DXM.data(), DYM.data(), DZM.data(),
                        Fi.data(), Fij.data()));
            }
        }
        else if constexpr (flowDir!=FlowDir::y && oscillationDir!=OscilDir::y) {
            for (SphereMutable* sMP : SphereMutable::getSet()) {
                sMP->setVForces(
                    integrateVirtualForce(sMP->getLoopLimit()+lloFx.add(gc),
                        FX.data(), DXM.data(), DYM.data(), DZM.data(),
                        Fi.data(), Fij.data()),
                    integrateVirtualForce(sMP->getLoopLimit()+lloFz.add(gc),
                        FZ.data(), DXM.data(), DYM.data(), DZM.data(),
                        Fi.data(), Fij.data()));
            }
        }
        else if constexpr (flowDir!=FlowDir::z && oscillationDir!=OscilDir::z) {
            for (SphereMutable* sMP : SphereMutable::getSet()) {
                sMP->setVForces(
                    integrateVirtualForce(sMP->getLoopLimit()+lloFx.add(gc),
                        FX.data(), DXM.data(), DYM.data(), DZM.data(),
                        Fi.data(), Fij.data()),
                    integrateVirtualForce(sMP->getLoopLimit()+lloFy.add(gc),
                        FY.data(), DXM.data(), DYM.data(), DZM.data(),
                        Fi.data(), Fij.data()));
            }
        }
    }
    if constexpr (static_cast<bool>(Total<CylinderX>::count)) {
        for (CylinderXMutable* sMP : CylinderXMutable::getSet()) {
            sMP->setVForces(
                integrateVirtualForce(sMP->getLoopLimit()+lloFy.add(gc),
                    FY.data(), DXM.data(), DYM.data(), DZM.data(),
                    Fi.data(), Fij.data()),
                integrateVirtualForce(sMP->getLoopLimit()+lloFz.add(gc),
                    FZ.data(), DXM.data(), DYM.data(), DZM.data(),
                    Fi.data(), Fij.data()));
        }
    }
    if constexpr (static_cast<bool>(Total<CylinderY>::count)) {
        for (CylinderYMutable* sMP : CylinderYMutable::getSet()) {
            sMP->setVForces(
                integrateVirtualForce(sMP->getLoopLimit()+lloFx.add(gc),
                    FX.data(), DXM.data(), DYM.data(), DZM.data(),
                    Fi.data(), Fij.data()),
                integrateVirtualForce(sMP->getLoopLimit()+lloFz.add(gc),
                    FZ.data(), DXM.data(), DYM.data(), DZM.data(),
                    Fi.data(), Fij.data()));
        }
    }
    if constexpr (static_cast<bool>(Total<CylinderZ>::count)) {
        for (CylinderZMutable* sMP : CylinderZMutable::getSet()) {
            sMP->setVForces(
                integrateVirtualForce(sMP->getLoopLimit()+lloFx,
                    FX.data(), DXM.data(), DYM.data(), DZM.data(),
                    Fi.data(), Fij.data()),
                integrateVirtualForce(sMP->getLoopLimit()+lloFy ,
                    FY.data(), DXM.data(), DYM.data(), DZM.data(),
                    Fi.data(), Fij.data()));
        }
    }
}

double disVIVSphere(const double structDis, const double structVel,
        const double structLift, const double reducedVelocity,
        const double mass, const double damping) {
    return  (  3.0*structLift/(4.0*mass)
             - 4.0*M_PI*structVel*damping/reducedVelocity
             - (2.0*M_PI/reducedVelocity)*(2.0*M_PI/reducedVelocity)*structDis);
}

double disVIVCylinder(const double structDis, const double structVel,
        const double structLift, const double reducedVelocity,
        const double mass, const double damping) {
    return  (  2.0*structLift/(M_PI*mass)
             - 4.0*M_PI*structVel*damping/reducedVelocity
             - (2.0*M_PI/reducedVelocity)*(2.0*M_PI/reducedVelocity)*structDis);
}

tuple<double, double> VIVCylinder(const double liftOld,
        const tuple<double, double> motionVIV) {
    double dis1, dis2, dis3, dis4, vel1, vel2, vel3, vel4, disOld, velOld;
    tie(disOld, velOld) = motionVIV;

    dis1 = disVIVCylinder(disOld, velOld,
                            liftOld, reducedVel, massRatio, structDamping);
    vel1 = velOld;

    dis2 = disVIVCylinder((disOld + 0.5*dt*vel1), (velOld + 0.5*dt*dis1),
                            liftOld, reducedVel, massRatio, structDamping);
    vel2 = velOld + 0.5*dt*dis1;

    dis3 = disVIVCylinder((disOld + 0.5*dt*vel2), (velOld + 0.5*dt*dis2),
                            liftOld, reducedVel, massRatio, structDamping);
    vel3 = velOld + 0.5*dt*dis2;

    dis4 = disVIVCylinder((disOld + dt*vel3), (velOld + dt*dis3),
                            liftOld, reducedVel, massRatio, structDamping);
    vel4 = velOld + dt*dis3;

    double disNew = disOld + (dt/6.0*(vel1 + 2.0*vel2 + 2.0*vel3 + vel4));
    double velNew = velOld + (dt/6.0*(dis1 + 2.0*dis2 + 2.0*dis3 + dis4));

    return {disNew, velNew};
}

tuple<double, double> VIVSphere(const double liftOld,
        const tuple<double, double> motionVIV) {

    /* int iBgn = llOld.iBgn + llog.iBgn, iEnd = llOld.iEnd + llog.iEnd,
        jBgn = llOld.jBgn + llog.jBgn, jEnd = llOld.jEnd + llog.jEnd,
        kBgn = llOld.kBgn + llog.kBgn, kEnd = llOld.kEnd + llog.kEnd;

    #pragma omp parallel for default(none)\
        firstprivate(velSP, iBgn, iEnd, jBgn, jEnd, kBgn, kEnd)
    for (int k=kBgn; k<kEnd; ++k) {
        for (int j=jBgn; j<jEnd; ++j) {
            for (int i=iBgn; i<iEnd; ++i) {
                *(velSP + i + j*nxt + k*nxt*nyt) = 0.0;
            }
        }
    } */

    double dis1, dis2, dis3, dis4, vel1, vel2, vel3, vel4, disOld, velOld;
    tie(disOld, velOld) = motionVIV;

    dis1 = disVIVSphere(disOld, velOld,
                        liftOld, reducedVel, massRatio, structDamping);
    vel1 = velOld;

    dis2 = disVIVSphere((disOld + 0.5*dt*vel1), (velOld + 0.5*dt*dis1),
                        liftOld, reducedVel, massRatio, structDamping);
    vel2 = velOld + 0.5*dt*dis1;

    dis3 = disVIVSphere((disOld + 0.5*dt*vel2), (velOld + 0.5*dt*dis2),
                        liftOld, reducedVel, massRatio, structDamping);
    vel3 = velOld + 0.5*dt*dis2;

    dis4 = disVIVSphere((disOld + dt*vel3), (velOld + dt*dis3),
                        liftOld, reducedVel, massRatio, structDamping);
    vel4 = velOld + dt*dis3;

    double disNew = disOld + (dt/6*(vel1 + 2*vel2 + 2*vel3 + vel4));
    double velNew = velOld + (dt/6*(dis1 + 2*dis2 + 2*dis3 + dis4));

    /* iBgn = ll.iBgn + llog.iBgn, iEnd = ll.iEnd + llog.iEnd,
    jBgn = ll.jBgn + llog.jBgn, jEnd = ll.jEnd + llog.jEnd,
    kBgn = ll.kBgn + llog.kBgn, kEnd = ll.kEnd + llog.kEnd;

    #pragma omp parallel for default(none)\
        firstprivate(velNew, velSP, iBgn, iEnd, jBgn, jEnd, kBgn, kEnd)
    for (int k=kBgn; k<kEnd; ++k) {
        for (int j=jBgn; j<jEnd; ++j) {
            for (int i=iBgn; i<iEnd; ++i) {
                *(velSP + i + j*nxt + k*nxt*nyt) = velNew;
            }
        }
    } */
    return {disNew, velNew};
}

void updateVIV() {
    if constexpr (static_cast<bool>(Total<Sphere>::count)) {
        for (SphereMutable* sMP : SphereMutable::getSet()) {
            if (sMP->getRootP()->getStructState() == State::moving) {
                sMP->setVIVMotionAndCenter(VIVSphere(
                    sMP->getCoeff().lift, sMP->getVIVMotion()));
            }
        }
    }
    if constexpr (static_cast<bool>(Total<CylinderX>::count)) {
        for (CylinderXMutable* sMP : CylinderXMutable::getSet()) {
            if (sMP->getRootP()->getStructState() == State::moving) {
                sMP->setVIVMotionAndCenter(VIVCylinder(
                    sMP->getCoeff().lift, sMP->getVIVMotion()));
            }
        }
    }
    if constexpr (static_cast<bool>(Total<CylinderY>::count)) {
        for (CylinderYMutable* sMP : CylinderYMutable::getSet()) {
            if (sMP->getRootP()->getStructState() == State::moving) {
                sMP->setVIVMotionAndCenter(VIVCylinder(
                    sMP->getCoeff().lift, sMP->getVIVMotion()));
            }
        }
    }
    if constexpr (static_cast<bool>(Total<CylinderZ>::count)) {
        for (CylinderZMutable* sMP : CylinderZMutable::getSet()) {
            if (sMP->getRootP()->getStructState() == State::moving) {
                sMP->setVIVMotionAndCenter(VIVCylinder(
                    sMP->getCoeff().lift, sMP->getVIVMotion()));
            }
        }
    }
}

//----------------------------------------------------------------------------//
//*********************** Structure Printing Functions ***********************//
//----------------------------------------------------------------------------//

/*
 * Create a file for timestep data at the start of the simulation.
 */

void filerTimestepDataStart() {
    ofstream f {filePath + fileName + "_timestepData.csv"};
    f   << left
        << "timestep" << ','
        << "simTime" << ',';
    if constexpr (static_cast<bool>(Total<Sphere>::count)) {
        for (const Sphere* sP : Sphere::getSet()) {
            f
                // << "FxSphere"           << sP->getId()  << ','
                // << "FySphere"           << sP->getId()  << ','
                // << "FzSphere"           << sP->getId()  << ','
                << "dragSphere"         << sP->getId()  << ','
                << "liftSphere"         << sP->getId()  << ',';
            if (sP->getStructState() == Structures::State::moving) {
                f << "displaceSphere"       << sP->getId()  << ',';
            }
        }
    }
    if constexpr (static_cast<bool>(Total<CylinderX>::count)) {
        for (const CylinderX* sP : CylinderX::getSet()) {
            f
                // << "FyCylinderX"        << sP->getId()  << ','
                // << "FzCylinderX"        << sP->getId()  << ','
                << "dragCylinderX"      << sP->getId()  << ','
                << "liftCylinderX"      << sP->getId()  << ',';
            if (sP->getStructState() == Structures::State::moving) {
                f << "displaceCylinderX"    << sP->getId()  << ',';
            }
        }
    }
    if constexpr (static_cast<bool>(Total<CylinderY>::count)) {
        for (const CylinderY* sP : CylinderY::getSet()) {
            f
                // << "FxCylinderY"        << sP->getId()  << ','
                // << "FzCylinderY"        << sP->getId()  << ','
                << "dragCylinderY"      << sP->getId()  << ','
                << "liftCylinderY"      << sP->getId()  << ',';
            if (sP->getStructState() == Structures::State::moving) {
                f << "displaceCylinderY"    << sP->getId()  << ',';
            }
        }
    }
    if constexpr (static_cast<bool>(Total<CylinderZ>::count)) {
        for (const CylinderZ* sP : CylinderZ::getSet()) {
            f
                // << "FxCylinderZ"        << sP->getId()  << ','
                // << "FyCylinderZ"        << sP->getId()  << ','
                << "dragCylinderZ"      << sP->getId()  << ','
                << "liftCylinderZ"      << sP->getId()  << ',';
            if (sP->getStructState() == Structures::State::moving) {
                f << "displaceCylinderZ"    << sP->getId()  << ',';
            }
        }
    }
    f << std::endl;
    f.close();
}


void filerTimestepData() {
    ofstream f {filePath + fileName + "_timestepData.csv", ios_base::app};
    f.setf(ios_base::scientific);
    f.precision(numeric_limits<double>::max_digits10);
    f   << left
        << timestep << ','
        << simTime << ',';
    if constexpr (static_cast<bool>(Total<Sphere>::count)) {
        for (const SphereMutable* sMP : SphereMutable::getSet()) {
            f
                // << sMP->getVForce().x   << ','
                // << sMP->getVForce().y   << ','
                // << sMP->getVForce().z   << ','
                << sMP->getCoeff().drag << ','
                << sMP->getCoeff().lift << ',';
            if (sMP->getRootP()->getStructState() == Structures::State::moving){
                f << get<0>(sMP->getVIVMotion())  << ',';
            }
        }
    }
    if constexpr (static_cast<bool>(Total<CylinderX>::count)) {
        for (const CylinderXMutable* sMP : CylinderXMutable::getSet()) {
            f
                // << sMP->getVForce().y   << ','
                // << sMP->getVForce().z   << ','
                << sMP->getCoeff().drag << ','
                << sMP->getCoeff().lift << ',';
            if (sMP->getRootP()->getStructState() == Structures::State::moving){
                f << get<0>(sMP->getVIVMotion())  << ',';
            }
        }
    }
    if constexpr (static_cast<bool>(Total<CylinderY>::count)) {
        for (const CylinderYMutable* sMP : CylinderYMutable::getSet()) {
            f
                // << sMP->getVForce().x   << ','
                // << sMP->getVForce().z   << ','
                << sMP->getCoeff().drag << ','
                << sMP->getCoeff().lift << ',';
            if (sMP->getRootP()->getStructState() == Structures::State::moving){
                f << get<0>(sMP->getVIVMotion())  << ',';
            }
        }
    }
    if constexpr (static_cast<bool>(Total<CylinderZ>::count)) {
        for (const CylinderZMutable* sMP : CylinderZMutable::getSet()) {
            f
                // << sMP->getVForce().x   << ','
                // << sMP->getVForce().y   << ','
                << sMP->getCoeff().drag << ','
                << sMP->getCoeff().lift << ',';
            if (sMP->getRootP()->getStructState() == Structures::State::moving){
                f << get<0>(sMP->getVIVMotion())  << ',';
            }
        }
    }
    f << std::endl;
    f.close();
}

//----------------------------------------------------------------------------//

ostream& operator<<(ostream& out, const Sphere& structure) {
    out << right
        << setw(2) << structure.getId() << ". Sphere:" << '\n'
        << left
        << setw(30) << "      Diameter" << structure.getDia() << '\n'
        << setw(30) << "      Initial center(x, y, z)"
            << structure.getInitCenter().x << ", "
            << structure.getInitCenter().y << ", "
            << structure.getInitCenter().z << '\n'
        << setw(30) << "      Struct. state"
            << mapStructStateToStr.at(structure.getStructState()) << '\n';
    return out;
}

ostream& operator<<(ostream& out, const CylinderX& structure) {
    out << right
        << setw(2) << structure.getId() << ". CylinderX:" << '\n'
        << left
        << setw(30) << "      Diameter" << structure.getDia() << '\n'
        << setw(30) << "      Initial center(y, z)"
            << structure.getInitCenter().y << ", "
            << structure.getInitCenter().z << '\n'
        << setw(30) << "      Struct. state"
            << mapStructStateToStr.at(structure.getStructState()) << '\n';
    return out;
}

ostream& operator<<(ostream& out, const CylinderY& structure) {
    out << right
        << setw(2) << structure.getId() << ". CylinderY:" << '\n'
        << left
        << setw(30) << "      Diameter" << structure.getDia() << '\n'
        << setw(30) << "      Initial center(x, z)"
            << structure.getInitCenter().x << ", "
            << structure.getInitCenter().z << '\n'
        << setw(30) << "      Struct. state"
            << mapStructStateToStr.at(structure.getStructState()) << '\n';
    return out;
}

ostream& operator<<(ostream& out, const CylinderZ& structure) {
    out << right
        << setw(2) << structure.getId() << ". CylinderZ:" << '\n'
        << left
        << setw(30) << "      Diameter" << structure.getDia() << '\n'
        << setw(30) << "      Initial center(x, y)"
            << structure.getInitCenter().x << ", "
            << structure.getInitCenter().y << '\n'
        << setw(30) << "      Struct. state"
            << mapStructStateToStr.at(structure.getStructState()) << '\n';
    return out;
}

void printStructures(ostream& out) {
    out << setw(30) << "Number of total structures" << countAll
        << "\n\n";
    if constexpr (toUType(Count::Sphere)>0) {
        for (const Sphere* sP : Sphere::getSet()) {
            out << *sP;
        }
    }
    if constexpr (toUType(Count::CylinderX)>0) {
        for (const CylinderX* sP : CylinderX::getSet()) {
            out << *sP;
        }
    }
    if constexpr (toUType(Count::CylinderY)>0) {
        for (const CylinderY* sP : CylinderY::getSet()) {
            out << *sP;
        }
    }
    if constexpr (toUType(Count::CylinderZ)>0) {
        for (const CylinderZ* sP : CylinderZ::getSet()) {
            out << *sP;
        }
    }
}

void printLoopLimtis() {
    if constexpr (static_cast<bool>(Total<Sphere>::count)) {
        for (int q=0; q<Total<Sphere>::count; ++q) {
            SphereMutable* sMutableP = SphereMutable::getOneP(q);
            std::cout
                << "Loop limits order: iBgn, iEnd, jBgn, jEnd, kBgn, kEnd\n"
                << sMutableP->getLoopLimit().iBgn << " "
                << sMutableP->getLoopLimit().iEnd << " "
                << sMutableP->getLoopLimit().jBgn << " "
                << sMutableP->getLoopLimit().jEnd << " "
                << sMutableP->getLoopLimit().kBgn << " "
                << sMutableP->getLoopLimit().kEnd << " "
                << "\n";
        }
    }
    if constexpr (static_cast<bool>(Total<CylinderX>::count)) {
        for (int q=0; q<Total<CylinderX>::count; ++q) {
            CylinderXMutable* sMutableP = CylinderXMutable::getOneP(q);
            std::cout
                << "Loop limits order: iBgn, iEnd, jBgn, jEnd, kBgn, kEnd\n"
                << sMutableP->getLoopLimit().iBgn << " "
                << sMutableP->getLoopLimit().iEnd << " "
                << sMutableP->getLoopLimit().jBgn << " "
                << sMutableP->getLoopLimit().jEnd << " "
                << sMutableP->getLoopLimit().kBgn << " "
                << sMutableP->getLoopLimit().kEnd << " "
                << "\n";
        }
    }
    if constexpr (static_cast<bool>(Total<CylinderY>::count)) {
        for (int q=0; q<Total<CylinderY>::count; ++q) {
            CylinderYMutable* sMutableP = CylinderYMutable::getOneP(q);
            std::cout
                << "Loop limits order: iBgn, iEnd, jBgn, jEnd, kBgn, kEnd\n"
                << sMutableP->getLoopLimit().iBgn << " "
                << sMutableP->getLoopLimit().iEnd << " "
                << sMutableP->getLoopLimit().jBgn << " "
                << sMutableP->getLoopLimit().jEnd << " "
                << sMutableP->getLoopLimit().kBgn << " "
                << sMutableP->getLoopLimit().kEnd << " "
                << "\n";
        }
    }
    if constexpr (static_cast<bool>(Total<CylinderZ>::count)) {
        for (int q=0; q<Total<CylinderZ>::count; ++q) {
            CylinderZMutable* sMutableP = CylinderZMutable::getOneP(q);
            std::cout
                << "Loop limits order: iBgn, iEnd, jBgn, jEnd, kBgn, kEnd\n"
                << sMutableP->getLoopLimit().iBgn << " "
                << sMutableP->getLoopLimit().iEnd << " "
                << sMutableP->getLoopLimit().jBgn << " "
                << sMutableP->getLoopLimit().jEnd << " "
                << sMutableP->getLoopLimit().kBgn << " "
                << sMutableP->getLoopLimit().kEnd << " "
                << "\n";
        }
    }
}
