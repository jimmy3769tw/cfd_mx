/**
 * @file        velocities.cpp
 *
 * @project     3D N-S solver rebuild
 * @version     2.0beta
 *
 * @author      Syed Ahmad Raza (sahmadrz@gmail.com)
 * @date        2020-10-27
 *
 * @brief       Apply the selected velocity scheme for the Navier-Stokes solver.
 *
 */

#include "velocities.h"
#include "parameters.h"

using std::sqrt;

/*
 * Definition of staggered grid terms for the QUICK scheme formula
 *------------------------------------------------------------------
 *                                  |
 * Postive velocity:                |
 *__________________________________|___________________________________________
 *          |       |       |       |       |
 *          |       |       |       |       |
 *     -->  |  -->  |  -->  |  -->  |  -->  |  -->     -->     -->
 *          |       |       |       |       |
 *         uB2             uB      uf      uF
 *          |<--- dxfb ---->|<---- dxf ---->|
 *                  |<---- dxB ---->|
 *----------------------------------|-------------------------------------------
 *                                  |
 * Negative velocity:               |
 *__________________________________|___________________________________________
 *                          |       |       |       |       |
 *                          |       |       |       |       |
 *     <--     <--     <--  |  <--  |  <--  |  <--  |  <--  |  <--
 *                          |       |       |       |       |
 *                         uF      uf      uB              uB2
 *                          |<---- dxf ---->|<--- dxfb ---->|
 *                                  |<---- dxB ---->|
 *----------------------------------|-------------------------------------------
*/

inline void quick(double& uf,
        const double* uF, const double* uB, const double* uB2,
        const double* dxf, const double* ddxf, const double* ddxfb,
        const double* ddxB) {
    uf = 0.5*(*uB + *uF)
        - 0.125**dxf**dxf**ddxB*( (*uF - *uB)**ddxf - (*uB - *uB2)**ddxfb );
}

inline void central(double& uf, const double* u1, const double* u2,
        const double* dx, const double* ddx) {
    double lIF = 0.5**dx**ddx;      // lIF: linear interpolation factor
    uf = *u2*lIF + *u1*(1 - lIF);
}

inline void centralCross(double& uf, const double* u1, const double* u2,
        const double* lIF) {
    uf = *u2**lIF + *u1*(1 - *lIF);     // lIF: linear interpolation factor
}

void interimVelocity(ConvectiveVelocityScheme selectedScheme) {
    const double nu{1.0/reynolds}, a{23.0/12.0}, b{16.0/12.0}, c{5.0/12.0};
    const double* u   {U.data()},    * v   {V.data()},    * w   {W.data()},
                * dx  {DX.data()},   * dy  {DY.data()},   * dz  {DZ.data()},
                * dxm {DXM.data()},  * dym {DYM.data()},  * dzm {DZM.data()},
                * ddx {DDX.data()},  * ddy {DDY.data()},  * ddz {DDZ.data()},
                * ddxm{DDXM.data()}, * ddym{DDYM.data()}, * ddzm{DDZM.data()},
                * ugrd{UGRD.data()}, * vgrd{VGRD.data()}, * wgrd{WGRD.data()},
                * ivfu{IVFU.data()}, * ivbu{IVBU.data()}, * iwnu{IWNU.data()},
                * iwsu{IWSU.data()}, * iufv{IUFV.data()}, * iubv{IUBV.data()},
                * iwev{IWEV.data()}, * iwwv{IWWV.data()}, * iunw{IUNW.data()},
                * iusw{IUSW.data()}, * ivew{IVEW.data()}, * ivww{IVWW.data()};

    double      * ust {US.data()},   * dust1{DUS1.data()},* dust2{DUS2.data()},
                * vst {VS.data()},   * dvst1{DVS1.data()},* dvst2{DVS2.data()},
                * wst {WS.data()},   * dwst1{DWS1.data()},* dwst2{DWS2.data()};

    //------------------------------------------------------------------------//
    //************************* x-direction velocity *************************//
    //------------------------------------------------------------------------//
    #pragma omp parallel for default(none)\
        firstprivate(dt, timestep, u, v, w, ust, dust1, dust2,\
        dx, dy, dz, dxm, dym, dzm, ddx, ddy, ddz, ddxm, ddym, ddzm, ugrd,\
        ivfu, ivbu, iwnu, iwsu, selectedScheme, turbulenceModel)\
        shared(U, V, W, DX, DXM, DY, DYM, DZ, DZM)
    for (int k=gc; k<nzt-gc; ++k) {
        for (int j=gc; j<nyt-gc; ++j) {
            for (int i=gc; i<nxt-gc-1; ++i) {
                const int l = i + j*nxt + k*nxt*nyt;
                double ue, uw, un, us, uf, ub, ueu, uwu, vnu, vsu, wfu, wbu;
                ue = uw = un = us = uf = ub = ueu = uwu = vnu = vsu = wfu = wbu
                    = 0.0;

                // Calculate convective components ---------------------------//

                central(ueu, u+l, u+l+1, dx+i+1, ddx+i+1);
                central(uwu, u+l-1, u+l, dx+i, ddx+i);
                central(vnu, v+l, v+l+1, dx+i, ddxm+i);
                central(vsu, v+l-nxt, v+l+1-nxt, dx+i, ddxm+i);
                central(wfu, w+l, w+l+1, dx+i, ddxm+i);
                central(wbu, w+l-nxt*nyt, w+l+1-nxt*nyt, dx+i, ddxm+i);

                selectedScheme.uVel(
                    ue, uw, un, us, uf, ub, ueu, uwu, vnu, vsu, wfu, wbu,
                    dx, dy, dz, dxm, dym, dzm, ddx, ddy, ddz, ddxm, ddym, ddzm,
                    u, v, w, i, j, k, l);

                // Calculate diffusive components ----------------------------//

                double
                    dudxe{*(ddx+i+1) *(*(u+l+1)       - *(u+l))},
                    dudxw{*(ddx+i)   *(*(u+l)         - *(u+l-1))},
                    dudyn{*(ddym+j)  *(*(u+l+nxt)     - *(u+l))},
                    dudys{*(ddym+j-1)*(*(u+l)         - *(u+l-nxt))},
                    dudzf{*(ddzm+k)  *(*(u+l+nxt*nyt) - *(u+l))},
                    dudzb{*(ddzm+k-1)*(*(u+l)         - *(u+l-nxt*nyt))};

                double nuT = 0.0;
                if (turbulenceModel != TurbModel::none) {
                    // Calculate additional convective components ------------//

                    double unu, usu, ufu, ubu,
                        veu, vwu, weu, wwu, vfu, vbu, wnu, wsu;
                    central(unu, u+l, u+l+nxt, dy+j, ddym+j),
                    central(usu, u+l-nxt, u+l, dy+j-1, ddym+j-1),
                    central(ufu, u+l, u+l+nxt*nyt, dz+k, ddzm+k),
                    central(ubu, u+l-nxt*nyt, u+l, dz+k-1, ddzm+k-1),
                    central(veu, v+l+1-nxt, v+l+1, dy+j, ddy+j),
                    central(vwu, v+l-nxt, v+l, dy+j, ddy+j),
                    central(weu, w+l+1-nxt*nyt, w+l+1, dz+k, ddz+k),
                    central(wwu, w+l-nxt*nyt, w+l, dz+k, ddz+k),
                    centralCross(vfu, v+l, v+l+1-nxt+nxt*nyt, ivfu+l),
                    centralCross(vbu, v+l, v+l+1-nxt-nxt*nyt, ivbu+l),
                    centralCross(wnu, w+l, w+l+1+nxt-nxt*nyt, iwnu+l),
                    centralCross(wsu, w+l, w+l+1-nxt-nxt*nyt, iwsu+l);

                    // Calculate strain-rate tensor components ---------------//

                    double
                        uu{*(ddxm+i)*(ueu - uwu)},
                        vv{*(ddy+j) *(vnu - vsu)},
                        ww{*(ddz+k) *(wfu - wbu)},
                        uv{*(ddy+j) *(unu - usu) + *(ddxm+i)*(veu - vwu)},
                        uw{*(ddz+k) *(ufu - ubu) + *(ddxm+i)*(weu - wwu)},
                        vw{*(ddz+k) *(vfu - vbu) + *(ddy+j) *(wnu - wsu)},

                        strainRateTensor = uu*uu + vv*vv + ww*ww
                                            + 0.5*uv*uv + 0.5*uw*uw + 0.5*vw*vw;

                    nuT = cS*cS**(ugrd+l)**(ugrd+l)
                            *sqrt(2*strainRateTensor);
                }

                // Calculate intermediate velocity ---------------------------//

                double dust = dt*(- (ue*ue  - uw*uw )**(ddxm+i)
                                  - (un*vnu - us*vsu)**(ddy+j)
                                  - (uf*wfu - ub*wbu)**(ddz+k)
                                  + (nu + nuT)*(  (dudxe - dudxw)**(ddxm+i)
                                                + (dudyn - dudys)**(ddy+j)
                                                + (dudzf - dudzb)**(ddz+k)  ));

                //---------------- Apply time-marching scheme ----------------//
                if constexpr (numericalScheme==NumScheme::euler) {
                    *(ust+l) = *(u+l) + dust;
                }
                else if constexpr (numericalScheme==NumScheme::adamsBashforth2){
                    if (timestep > 1) {
                        *(ust+l) = *(u+l) + 1.5*dust - 0.5**(dust1+l);
                        *(dust1+l) = dust;
                    } else if (timestep == 1) {
                        *(ust+l) = *(u+l) + dust;
                    }
                }
                else if constexpr (numericalScheme==NumScheme::adamsBashforth3){
                    if (timestep > 2) {
                        *(ust+l)= *(u+l) + a*dust - b**(dust1+l) + c**(dust2+l);
                        *(dust2+l) = *(dust1+l);
                        *(dust1+l) = dust;
                    } else if (timestep == 2) {
                        *(ust+l) = *(u+l) + 1.5*dust - 0.5**(dust1+l);
                        *(dust1+l) = dust;
                    } else if (timestep == 1) {
                        *(ust+l) = *(u+l) + dust;
                    }
                }
            }
        }
    }
    //------------------------------------------------------------------------//
    //************************* y-direction velocity *************************//
    //------------------------------------------------------------------------//
    #pragma omp parallel for default(none)\
        firstprivate(dt, timestep, u, v, w, vst, dvst1, dvst2,\
        dx, dy, dz, dxm, dym, dzm, ddx, ddy, ddz, ddxm, ddym, ddzm, vgrd,\
        iufv, iubv, iwev, iwwv, selectedScheme, turbulenceModel)\
        shared(U, V, W, DX, DXM, DY, DYM, DZ, DZM)
    for (int k=gc; k<nzt-gc; ++k) {
        for (int j=gc; j<nyt-gc-1; ++j) {
            for (int i=gc; i<nxt-gc; ++i) {
                const int l = i + j*nxt + k*nxt*nyt;
                double ve, vw, vn, vs, vf, vb, uev, uwv, vnv, vsv, wfv, wbv;
                ve = vw = vn = vs = vf = vb = uev = uwv = vnv = vsv = wfv = wbv
                    = 0.0;

                // Calculate convective components ---------------------------//

                central(vnv, v+l, v+l+nxt, dy+j+1, ddy+j+1);
                central(vsv, v+l, v+l-nxt, dy+j, ddy+j);
                central(uev, u+l, u+l+nxt, dy+j, ddym+j);
                central(uwv, u+l-1, u+l-1+nxt, dy+j, ddym+j);
                central(wfv, w+l, w+l+nxt, dy+j, ddym+j);
                central(wbv, w+l-nxt*nyt, w+l+nxt-nxt*nyt, dy+j, ddym+j);

                selectedScheme.vVel(
                    ve, vw, vn, vs, vf, vb, uev, uwv, vnv, vsv, wfv, wbv,
                    dx, dy, dz, dxm, dym, dzm, ddx, ddy, ddz, ddxm, ddym, ddzm,
                    u, v, w, i, j, k, l);

                // Calculate diffusive components ----------------------------//

                double
                    dvdxe{*(ddxm+i)  *(*(v+l+1)       - *(v+l))},
                    dvdxw{*(ddxm+i-1)*(*(v+l)         - *(v+l-1))},
                    dvdyn{*(ddy+j+1) *(*(v+l+nxt)     - *(v+l))},
                    dvdys{*(ddy+j)   *(*(v+l)         - *(v+l-nxt))},
                    dvdzf{*(ddzm+k)  *(*(v+l+nxt*nyt) - *(v+l))},
                    dvdzb{*(ddzm+k-1)*(*(v+l)         - *(v+l-nxt*nyt))};

                double nuT = 0.0;
                if (turbulenceModel != TurbModel::none) {
                    // Calculate additional convective components ------------//

                    double vev, vwv, vfv, vbv,
                        unv, usv, wnv, wsv, ufv, ubv, wev, wwv;
                    central(vev, v+l, v+l+1, dx+i, ddxm+i),
                    central(vwv, v+l-1, v+l, dx+i-1, ddxm+i-1),
                    central(vfv, v+l, v+l+nxt*nyt, dz+k, ddzm+k),
                    central(vbv, v+l-nxt*nyt, v+l, dz+k-1, ddzm+k-1),
                    central(unv, u+l-1+nxt, u+l+nxt, dx+i, ddx+i),
                    central(usv, u+l-1, u+l, dx+i, ddx+i),
                    central(wnv, w+l+nxt-nxt*nyt, w+l+nxt, dz+k, ddz+k),
                    central(wsv, w+l-nxt*nyt, w+l, dz+k, ddz+k),
                    centralCross(ufv, u+l, u+l-1+nxt+nxt*nyt, iufv+l),
                    centralCross(ubv, u+l, u+l-1+nxt-nxt*nyt, iubv+l),
                    centralCross(wev, w+l, w+l+1+nxt-nxt*nyt, iwev+l),
                    centralCross(wwv, w+l, w+l-1+nxt-nxt*nyt, iwwv+l);

                    // Calculate strain-rate tensor components ---------------//

                    double
                        uu{*(ddx+i) *(uev - uwv)},
                        vv{*(ddym+j)*(vnv - vsv)},
                        ww{*(ddz+k) *(wfv - wbv)},
                        uv{*(ddym+j)*(unv - usv) + *(ddx+i) *(vev - vwv)},
                        uw{*(ddz+k) *(ufv - ubv) + *(ddx+i) *(wev - wwv)},
                        vw{*(ddz+k) *(vfv - vbv) + *(ddym+j)*(wnv - wsv)},

                        strainRateTensor = uu*uu + vv*vv + ww*ww
                                            + 0.5*uv*uv + 0.5*uw*uw + 0.5*vw*vw;

                    nuT = cS*cS**(vgrd+l)**(vgrd+l)
                            *sqrt(2*strainRateTensor);
                }

                // Calculate intermediate velocity ---------------------------//

                double dvst = dt*(- (ve*uev - vw*uwv)**(ddx+i)
                                  - (vn*vn  - vs*vs )**(ddym+j)
                                  - (vf*wfv - vb*wbv)**(ddz+k)
                                  + (nu + nuT)*(  (dvdxe - dvdxw)**(ddx+i)
                                                + (dvdyn - dvdys)**(ddym+j)
                                                + (dvdzf - dvdzb)**(ddz+k)  ));

                //---------------- Apply time-marching scheme ----------------//
                if constexpr (numericalScheme==NumScheme::euler) {
                    *(vst+l) = *(v+l) + dvst;
                }
                else if constexpr (numericalScheme==NumScheme::adamsBashforth2){
                    if (timestep > 1) {
                        *(vst+l) = *(v+l) + 1.5*dvst - 0.5**(dvst1+l);
                        *(dvst1+l) = dvst;
                    } else if (timestep == 1) {
                        *(vst+l) = *(v+l) + dvst;
                    }
                }
                else if constexpr (numericalScheme==NumScheme::adamsBashforth3){
                    if (timestep > 2) {
                        *(vst+l)= *(v+l) + a*dvst - b**(dvst1+l) + c**(dvst2+l);
                        *(dvst2+l) = *(dvst1+l);
                        *(dvst1+l) = dvst;
                    } else if (timestep == 2) {
                        *(vst+l) = *(v+l) + 1.5*dvst - 0.5**(dvst1+l);
                        *(dvst1+l) = dvst;
                    } else if (timestep == 1) {
                        *(vst+l) = *(v+l) + dvst;
                    }
                }
            }
        }
    }
    //------------------------------------------------------------------------//
    //************************* z-direction velocity *************************//
    //------------------------------------------------------------------------//
    #pragma omp parallel for default(none)\
        firstprivate(dt, timestep, u, v, w, wst, dwst1, dwst2,\
        dx, dy, dz, dxm, dym, dzm, ddx, ddy, ddz, ddxm, ddym, ddzm, wgrd,\
        iunw, iusw, ivew, ivww, selectedScheme, turbulenceModel)\
        shared(U, V, W, DX, DXM, DY, DYM, DZ, DZM)
    for (int k=gc; k<nzt-gc-1; ++k) {
        for (int j=gc; j<nyt-gc; ++j) {
            for (int i=gc; i<nxt-gc; ++i) {
                const int l = i + j*nxt + k*nxt*nyt;
                double we, ww, wn, ws, wf, wb, uew, uww, vnw, vsw, wfw, wbw;
                we = ww = wn = ws = wf = wb = uew = uww = vnw = vsw = wfw = wbw
                    = 0.0;

                // Calculate convective components ---------------------------//

                central(wfw, w+l, w+l+nxt*nyt, dz+k+1, ddz+k+1);
                central(wbw, w+l-nxt*nyt, w+l, dz+k, ddz+k);
                central(uew, u+l, u+l+nxt*nyt, dz+k, ddzm+k);
                central(uww, u+l-1, u+l-1+nxt*nyt, dz+k, ddzm+k);
                central(vnw, v+l, v+l+nxt*nyt, dz+k, ddzm+k);
                central(vsw, v+l-nxt, v+l-nxt+nxt*nyt, dz+k, ddzm+k);

                selectedScheme.wVel(
                    we, ww, wn, ws, wf, wb, uew, uww, vnw, vsw, wfw, wbw,
                    dx, dy, dz, dxm, dym, dzm, ddx, ddy, ddz, ddxm, ddym, ddzm,
                    u, v, w, i, j, k, l);


                // Calculate diffusive components ----------------------------//

                double
                    dwdxe{*(ddxm+i)  *(*(w+l+1)       - *(w+l))},
                    dwdxw{*(ddxm+i-1)*(*(w+l)         - *(w+l-1))},
                    dwdyn{*(ddym+j)  *(*(w+l+nxt)     - *(w+l))},
                    dwdys{*(ddym+j-1)*(*(w+l)         - *(w+l-nxt))},
                    dwdzf{*(ddz+k+1) *(*(w+l+nxt*nyt) - *(w+l))},
                    dwdzb{*(ddz+k)   *(*(w+l)         - *(w+l-nxt*nyt))};

                double nuT = 0.0;
                if (turbulenceModel != TurbModel::none) {
                    // Calculate additional convective components ------------//

                    double wew, www, wnw, wsw,
                        ufw, ubw, vfw, vbw, unw, usw, vew, vww;
                    central(wew, w+l, w+l+1, dx+i, ddxm+i),
                    central(www, w+l-1, w+l, dx+i-1, ddxm+i-1),
                    central(wnw, w+l, w+l+nxt, dy+j, ddym+j),
                    central(wsw, w+l-nxt, w+l, dy+j-1, ddym+j-1),
                    central(ufw, u+l-1+nxt*nyt, u+l+nxt*nyt, dx+i, ddx+i),
                    central(ubw, u+l-1, u+l, dx+i, ddx+i),
                    central(vfw, v+l-nxt+nxt*nyt, v+l+nxt*nyt, dy+j, ddy+j),
                    central(vbw, v+l-nxt, v+l, dy+j, ddy+j),
                    centralCross(unw, u+l, u+l-1+nxt+nxt*nyt, iunw+l),
                    centralCross(usw, u+l, u+l-1-nxt+nxt*nyt, iusw+l),
                    centralCross(vew, v+l, v+l+1-nxt+nxt*nyt, ivew+l),
                    centralCross(vww, v+l, v+l-1-nxt+nxt*nyt, ivww+l);

                    // Calculate strain-rate tensor components ---------------//

                    double
                        uu{*(ddx+i) *(uew - uww)},
                        vv{*(ddy+j) *(vnw - vsw)},
                        ww{*(ddzm+k)*(wfw - wbw)},
                        uv{*(ddy+j) *(unw - usw) + *(ddx+i)*(vew - vww)},
                        uw{*(ddzm+k)*(ufw - ubw) + *(ddx+i)*(wew - www)},
                        vw{*(ddzm+k)*(vfw - vbw) + *(ddy+j)*(wnw - wsw)},

                        strainRateTensor = uu*uu + vv*vv + ww*ww
                                            + 0.5*uv*uv + 0.5*uw*uw + 0.5*vw*vw;

                    nuT = cS*cS**(wgrd+l)**(wgrd+l)
                            *sqrt(2*strainRateTensor);
                }

                // Calculate intermediate velocity ---------------------------//

                double dwst = dt*(- (we*uew - ww*uww)**(ddx+i)
                                  - (wn*vnw - ws*vsw)**(ddy+j)
                                  - (wf*wf  - wb*wb )**(ddzm+k)
                                  + (nu + nuT)*(  (dwdxe - dwdxw)**(ddx+i)
                                                + (dwdyn - dwdys)**(ddy+j)
                                                + (dwdzf - dwdzb)**(ddzm+k)  ));

                //---------------- Apply time-marching scheme ----------------//
                if constexpr (numericalScheme==NumScheme::euler) {
                    *(wst+l) = *(w+l) + dwst;
                }
                else if constexpr (numericalScheme==NumScheme::adamsBashforth2){
                    if (timestep > 1) {
                        *(wst+l) = *(w+l) + 1.5*dwst - 0.5**(dwst1+l);
                        *(dwst1+l) = dwst;
                    } else if (timestep == 1) {
                        *(wst+l) = *(w+l) + dwst;
                    }
                }
                else if constexpr (numericalScheme==NumScheme::adamsBashforth3){
                    if (timestep > 2) {
                        *(wst+l)= *(w+l) + a*dwst - b**(dwst1+l) + c**(dwst2+l);
                        *(dwst2+l) = *(dwst1+l);
                        *(dwst1+l) = dwst;
                    } else if (timestep == 2) {
                        *(wst+l) = *(w+l) + 1.5*dwst - 0.5**(dwst1+l);
                        *(dwst1+l) = dwst;
                    } else if (timestep == 1) {
                        *(wst+l) = *(w+l) + dwst;
                    }
                }
            }
        }
    }
}

void quickConvVelU(
        double& ue, double& uw, double& un, double& us, double& uf, double& ub,
        const double& ueu,  const double& uwu,
        const double& vnu,  const double& vsu,
        const double& wfu,  const double& wbu,
        const double* dx, __attribute__((unused)) const double* dy,
            __attribute__((unused)) const double* dz,
        __attribute__((unused)) const double* dxm,
            const double* dym,  const double* dzm,
        const double* ddx,  const double* ddy,  const double* ddz,
        const double* ddxm, const double* ddym, const double* ddzm,
        const double* u, __attribute__((unused)) const double* v,
            __attribute__((unused)) const double* w,
        const int& i, const int& j, const int& k, const int& l) {
    // uf, uF, uB, uB2, dxf, ddxf, ddxfb, ddxB
    if (ueu > 0) {
        quick(ue, u+l+1, u+l, u+l-1,
            dx+i+1, ddx+i+1, ddx+i, ddxm+i);
    }
    else {
        quick(ue, u+l, u+l+1, u+l+2,
            dx+i+1, ddx+i+1, ddx+i+2, ddxm+i+1);
    }
    if (uwu > 0) {
        quick(uw, u+l, u+l-1, u+l-2,
            dx+i, ddx+i, ddx+i-1, ddxm+i-1);
    }
    else {
        quick(uw, u+l-1, u+l, u+l+1,
            dx+i, ddx+i, ddx+i+1, ddxm+i);
    }
    // uf, uF, uB, uB2, dyf, ddyf, ddyfb, ddyB
    if (vnu > 0) {
        quick(un, u+l+nxt, u+l, u+l-nxt,
            dym+j, ddym+j, ddym+j-1, ddy+j);
    }
    else {
        quick(un, u+l, u+l+nxt, u+l+nxt*2,
            dym+j, ddym+j, ddym+j+1, ddy+j+1);
    }
    if (vsu > 0) {
        quick(us, u+l, u+l-nxt, u+l-nxt*2,
            dym+j-1, ddym+j-1, ddym+j-2, ddy+j-1);
    }
    else {
        quick(us, u+l-nxt, u+l, u+l+nxt,
            dym+j-1, ddym+j-1, ddym+j, ddy+j);
    }
    // uf, uF, uB, uB2, dzf, ddzf, ddzfb, ddzB
    if (wfu > 0) {
        quick(uf, u+l+nxt*nyt, u+l, u+l-nxt*nyt,
            dzm+k, ddzm+k, ddzm+k-1, ddz+k);
    }
    else {
        quick(uf, u+l, u+l+nxt*nyt, u+l+nxt*nyt*2,
            dzm+k, ddzm+k, ddzm+k+1, ddz+k+1);
    }
    if (wbu > 0) {
        quick(ub, u+l, u+l-nxt*nyt, u+l-nxt*nyt*2,
            dzm+k-1, ddzm+k-1, ddzm+k-2, ddz+k-1);
    }
    else {
        quick(ub, u+l-nxt*nyt, u+l, u+l+nxt*nyt,
            dzm+k-1, ddzm+k-1, ddzm+k, ddz+k);
    }
}

void quickConvVelV(
        double& ve, double& vw, double& vn, double& vs, double& vf, double& vb,
        const double& uev,  const double& uwv,
        const double& vnv,  const double& vsv,
        const double& wfv,  const double& wbv,
        __attribute__((unused)) const double* dx, const double* dy,
            __attribute__((unused)) const double* dz,
        const double* dxm, __attribute__((unused)) const double* dym,
            const double* dzm,
        const double* ddx,  const double* ddy,  const double* ddz,
        const double* ddxm, const double* ddym, const double* ddzm,
        __attribute__((unused)) const double* u, const double* v,
            __attribute__((unused)) const double* w,
        const int& i, const int& j, const int& k, const int& l) {
    // vf, vF, vB, vB2, dxf, ddxf, ddxfb, ddxB
    if (uev > 0) {
        quick(ve, v+l+1, v+l, v+l-1,
            dxm+i, ddxm+i, ddxm+i-1, ddx+i);
    }
    else {
        quick(ve, v+l, v+l+1, v+l+2,
            dxm+i, ddxm+i, ddxm+i+1, ddx+i+1);
    }
    if (uwv > 0) {
        quick(vw, v+l, v+l-1, v+l-2,
            dxm+i-1, ddxm+i-1, ddxm+i-2, ddx+i-1);
    }
    else {
        quick(vw, v+l-1, v+l, v+l+1,
            dxm+i-1, ddxm+i-1, ddxm+i, ddx+i);
    }
    // vf, vF, vB, vB2, dyf, ddyf, ddyfb, ddyB
    if (vnv > 0) {
        quick(vn, v+l+nxt, v+l, v+l-nxt,
            dy+j+1, ddy+j+1, ddy+j, ddym+j);
    }
    else {
        quick(vn, v+l, v+l+nxt, v+l+nxt*2,
            dy+j+1, ddy+j+1, ddy+j+2, ddym+j+1);
    }
    if (vsv > 0) {
        quick(vs, v+l, v+l-nxt, v+l-nxt*2,
        dy+j, ddy+j, ddy+j-1, ddym+j-1);
    }
    else {
        quick(vs, v+l-nxt, v+l, v+l+nxt,
            dy+j, ddy+j, ddy+j+1, ddym+j);
    }
    // vf, vF, vB, vB2, dzf, ddzf, ddzfb, ddzB
    if (wfv > 0) {
        quick(vf, v+l+nxt*nyt, v+l, v+l-nxt*nyt,
            dzm+k, ddzm+k, ddzm+k-1, ddz+k);
    }
    else {
        quick(vf, v+l, v+l+nxt*nyt, v+l+nxt*nyt*2,
            dzm+k, ddzm+k, ddzm+k+1, ddz+k+1);
    }
    if (wbv > 0) {
        quick(vb, v+l, v+l-nxt*nyt, v+l-nxt*nyt*2,
            dzm+k-1, ddzm+k-1, ddzm+k-2, ddz+k-1);
    }
    else {
        quick(vb, v+l-nxt*nyt, v+l, v+l+nxt*nyt,
            dzm+k-1, ddzm+k-1, ddzm+k, ddz+k);
    }
}

void quickConvVelW(
        double& we, double& ww, double& wn, double& ws, double& wf, double& wb,
        const double& uew,  const double& uww,
        const double& vnw,  const double& vsw,
        const double& wfw,  const double& wbw,
        __attribute__((unused)) const double* dx,
            __attribute__((unused)) const double* dy, const double* dz,
        const double* dxm,  const double* dym,
            __attribute__((unused)) const double* dzm,
        const double* ddx,  const double* ddy,  const double* ddz,
        const double* ddxm, const double* ddym, const double* ddzm,
        __attribute__((unused)) const double* u,
            __attribute__((unused)) const double* v, const double* w,
        const int& i, const int& j, const int& k, const int& l) {
    // wf, wF, wB, wB2, dxf, ddxf, ddxfb, ddxB
    if (uew > 0) {
        quick(we, w+l+1, w+l, w+l-1,
            dxm+i, ddxm+i, ddxm+i-1, ddx+i);
    }
    else {
        quick(we, w+l, w+l+1, w+l+2,
            dxm+i, ddxm+i, ddxm+i+1, ddx+i+1);
    }
    if (uww > 0) {
        quick(ww, w+l, w+l-1, w+l-2,
            dxm+i-1, ddxm+i-1, ddxm+i-2, ddx+i-1);
    }
    else {
        quick(ww, w+l-1, w+l, w+l+1,
            dxm+i-1, ddxm+i-1, ddxm+i, ddx+i);
    }
    // wf, wF, wB, wB2, dyf, ddyf, ddyfb, ddyB
    if (vnw > 0) {
        quick(wn, w+l+nxt, w+l, w+l-nxt,
            dym+j, ddym+j, ddym+j-1, ddy+j);
    }
    else {
        quick(wn, w+l, w+l+nxt, w+l+nxt*2,
            dym+j, ddym+j, ddym+j+1, ddy+j+1);
    }
    if (vsw > 0) {
        quick(ws, w+l, w+l-nxt, w+l-nxt*2,
            dym+j-1, ddym+j-1, ddym+j-2, ddy+j-1);
    }
    else {
        quick(ws, w+l-nxt, w+l, w+l+nxt,
            dym+j-1, ddym+j-1, ddym+j, ddy+j);
    }
    // wf, wF, wB, wB2, dzf, ddzf, ddzfb, ddzB
    if (wfw > 0) {
        quick(wf, w+l+nxt*nyt, w+l, w+l-nxt*nyt,
            dz+k+1, ddz+k+1, ddz+k, ddzm+k);
    }
    else {
        quick(wf, w+l, w+l+nxt*nyt, w+l+nxt*nyt*2,
            dz+k+1, ddz+k+1, ddz+k+2, ddzm+k+1);
    }
    if (wbw > 0) {
        quick(wb, w+l, w+l-nxt*nyt, w+l-nxt*nyt*2,
            dz+k, ddz+k, ddz+k-1, ddzm+k-1);
    }
    else {
        quick(wb, w+l-nxt*nyt, w+l, w+l+nxt*nyt,
            dz+k, ddz+k, ddz+k+1, ddzm+k);
    }
}

void centralConvVelU(
        double& ue, double& uw, double& un, double& us, double& uf, double& ub,
        const double& ueu, __attribute__((unused)) const double& uwu,
        __attribute__((unused)) const double& vnu,
            __attribute__((unused)) const double& vsu,
        __attribute__((unused)) const double& wfu,
            __attribute__((unused)) const double& wbu,
        __attribute__((unused)) const double* dx, const double* dy,
            const double* dz,
        __attribute__((unused)) const double* dxm,
            __attribute__((unused)) const double* dym,
            __attribute__((unused)) const double* dzm,
        __attribute__((unused)) const double* ddx,
            __attribute__((unused)) const double* ddy,
            __attribute__((unused)) const double* ddz,
        __attribute__((unused)) const double* ddxm, const double* ddym,
            const double* ddzm,
        const double* u, __attribute__((unused)) const double* v,
            __attribute__((unused)) const double* w,
        __attribute__((unused)) const int& i, const int& j, const int& k,
        const int& l) {
    ue  = ueu;
    uw  = ueu;
    central(un, u+l, u+l+nxt, dy+j, ddym+j);
    central(us, u+l-nxt, u+l, dy+j-1, ddym+j-1);
    central(uf, u+l, u+l+nxt*nyt, dz+k, ddzm+k);
    central(ub, u+l-nxt*nyt, u+l, dz+k-1, ddzm+k-1);
}

void centralConvVelV(
        double& ve, double& vw, double& vn, double& vs, double& vf, double& vb,
        __attribute__((unused)) const double& uev,
            __attribute__((unused)) const double& uwv,
        const double& vnv,  const double& vsv,
        __attribute__((unused)) const double& wfv,
            __attribute__((unused)) const double& wbv,
        const double* dx, __attribute__((unused)) const double* dy,
            const double* dz,
        __attribute__((unused)) const double* dxm,
            __attribute__((unused)) const double* dym,
            __attribute__((unused)) const double* dzm,
        __attribute__((unused)) const double* ddx,
            __attribute__((unused)) const double* ddy,
            __attribute__((unused)) const double* ddz,
        const double* ddxm, __attribute__((unused)) const double* ddym,
            const double* ddzm,
        __attribute__((unused)) const double* u, const double* v,
            __attribute__((unused)) const double* w,
        const int& i, __attribute__((unused)) const int& j, const int& k,
        const int& l) {
    central(ve, v+l, v+l+1, dx+i, ddxm+i);
    central(vw, v+l-1, v+l, dx+i-1, ddxm+i-1);
    vn  = vnv;
    vs  = vsv;
    central(vf, v+l, v+l+nxt*nyt, dz+k, ddzm+k);
    central(vb, v+l-nxt*nyt, v+l, dz+k-1, ddzm+k-1);
}

void centralConvVelW(
        double& we, double& ww, double& wn, double& ws, double& wf, double& wb,
        __attribute__((unused)) const double& uew,
            __attribute__((unused)) const double& uww,
        __attribute__((unused)) const double& vnw,
            __attribute__((unused)) const double& vsw,
        const double& wfw,  const double& wbw,
        const double* dx,   const double* dy,
            __attribute__((unused)) const double* dz,
        __attribute__((unused)) const double* dxm,
            __attribute__((unused)) const double* dym,
            __attribute__((unused)) const double* dzm,
        __attribute__((unused)) const double* ddx,
            __attribute__((unused)) const double* ddy,
            __attribute__((unused)) const double* ddz,
        const double* ddxm, const double* ddym,
            __attribute__((unused)) const double* ddzm,
        __attribute__((unused)) const double* u,
            __attribute__((unused)) const double* v, const double* w,
        const int& i, const int& j, __attribute__((unused)) const int& k,
        const int& l) {
    central(we, w+l, w+l+1, dx+i, ddxm+i);
    central(ww, w+l-1, w+l, dx+i-1, ddxm+i-1);
    central(wn, w+l, w+l+nxt, dy+j, ddym+j);
    central(ws, w+l-nxt, w+l, dy+j-1, ddym+j-1);
    wf  = wfw;
    wb  = wbw;
}

void upwindConvVelU(
        double& ue, double& uw, double& un, double& us, double& uf, double& ub,
        const double& ueu,  const double& uwu,
        const double& vnu,  const double& vsu,
        const double& wfu,  const double& wbu,
        __attribute__((unused)) const double* dx,
            __attribute__((unused)) const double* dy,
            __attribute__((unused)) const double* dz,
        __attribute__((unused)) const double* dxm,
            __attribute__((unused)) const double* dym,
            __attribute__((unused)) const double* dzm,
        __attribute__((unused)) const double* ddx,
            __attribute__((unused)) const double* ddy,
            __attribute__((unused)) const double* ddz,
        __attribute__((unused)) const double* ddxm,
            __attribute__((unused)) const double* ddym,
            __attribute__((unused)) const double* ddzm,
        const double* u, __attribute__((unused)) const double* v,
            __attribute__((unused)) const double* w,
        __attribute__((unused)) const int& i,
        __attribute__((unused)) const int& j,
        __attribute__((unused)) const int& k, const int& l) {
    if (ueu > 0) { ue = *(u+l);         }
    else         { ue = *(u+l+1);       }

    if (uwu > 0) { uw = *(u+l-1);       }
    else         { uw = *(u+l);         }

    if (vnu > 0) { un = *(u+l);         }
    else         { un = *(u+l+nxt);     }

    if (vsu > 0) { us = *(u+l-nxt);     }
    else         { us = *(u+l);         }

    if (wfu > 0) { uf = *(u+l);         }
    else         { uf = *(u+l+nxt*nyt); }

    if (wbu > 0) { ub = *(u+l-nxt*nyt); }
    else         { ub = *(u+l);         }
}

void upwindConvVelV(
        double& ve, double& vw, double& vn, double& vs, double& vf, double& vb,
        const double& uev,  const double& uwv,
        const double& vnv,  const double& vsv,
        const double& wfv,  const double& wbv,
        __attribute__((unused)) const double* dx,
            __attribute__((unused)) const double* dy,
            __attribute__((unused)) const double* dz,
        __attribute__((unused)) const double* dxm,
            __attribute__((unused)) const double* dym,
            __attribute__((unused)) const double* dzm,
        __attribute__((unused)) const double* ddx,
            __attribute__((unused)) const double* ddy,
            __attribute__((unused)) const double* ddz,
        __attribute__((unused)) const double* ddxm,
            __attribute__((unused)) const double* ddym,
            __attribute__((unused)) const double* ddzm,
        __attribute__((unused)) const double* u, const double* v,
            __attribute__((unused)) const double* w,
        __attribute__((unused)) const int& i,
        __attribute__((unused)) const int& j,
        __attribute__((unused)) const int& k, const int& l) {
    if (uev > 0) { ve = *(v+l);         }
    else         { ve = *(v+l+1);       }

    if (uwv > 0) { vw = *(v+l-1);       }
    else         { vw = *(v+l);         }

    if (vnv > 0) { vn = *(v+l);         }
    else         { vn = *(v+l+nxt);     }

    if (vsv > 0) { vs = *(v+l-nxt);     }
    else         { vs = *(v+l);         }

    if (wfv > 0) { vf = *(v+l);         }
    else         { vf = *(v+l+nxt*nyt); }

    if (wbv > 0) { vb = *(v+l-nxt*nyt); }
    else         { vb = *(v+l);         }
}

void upwindConvVelW(
        double& we, double& ww, double& wn, double& ws, double& wf, double& wb,
        const double& uew,  const double& uww,
        const double& vnw,  const double& vsw,
        const double& wfw,  const double& wbw,
        __attribute__((unused)) const double* dx,
            __attribute__((unused)) const double* dy,
            __attribute__((unused)) const double* dz,
        __attribute__((unused)) const double* dxm,
            __attribute__((unused)) const double* dym,
            __attribute__((unused)) const double* dzm,
        __attribute__((unused)) const double* ddx,
            __attribute__((unused)) const double* ddy,
            __attribute__((unused)) const double* ddz,
        __attribute__((unused)) const double* ddxm,
            __attribute__((unused)) const double* ddym,
            __attribute__((unused)) const double* ddzm,
        __attribute__((unused)) const double* u,
            __attribute__((unused)) const double* v, const double* w,
        __attribute__((unused)) const int& i,
        __attribute__((unused)) const int& j,
        __attribute__((unused)) const int& k, const int& l) {
    if (uew > 0) { we = *(w+l);         }
    else         { we = *(w+l+1);       }

    if (uww > 0) { ww = *(w+l-1);       }
    else         { ww = *(w+l);         }

    if (vnw > 0) { wn = *(w+l);         }
    else         { wn = *(w+l+nxt);     }

    if (vsw > 0) { ws = *(w+l-nxt);     }
    else         { ws = *(w+l);         }

    if (wfw > 0) { wf = *(w+l);         }
    else         { wf = *(w+l+nxt*nyt); }

    if (wbw > 0) { wb = *(w+l-nxt*nyt); }
    else         { wb = *(w+l);         }
}
