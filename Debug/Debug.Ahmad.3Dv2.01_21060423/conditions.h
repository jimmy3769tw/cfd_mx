/**
 * @file        conditions.h
 *
 * @project     3D N-S solver rebuild
 * @version     2.0beta
 *
 * @author      Syed Ahmad Raza (sahmadrz@gmail.com)
 * @date        2020-10-27
 *
 * @brief       Generate the initial and boundary conditions for the Navier-
 *              Stokes solver.
 *
 */

#ifndef CONDITIONS_H
#define CONDITIONS_H

#include "parameters.h"

void initialConditions(const array<double, 4>& initialValues);
void boundaryConditions(const Boundaries& pressure, const Boundaries& uVelocity,
    const Boundaries& vVelocity, const Boundaries& wVelocity);
void boundaryConditions();

//----------------------------------------------------------------------------//
// Pressure boundary functions -----------------------------------------------//
//----------------------------------------------------------------------------//

inline void pWestNeumann(double* p, const int& j, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(p+1+j*nxt+k*nxt*nyt) = *(p+2+j*nxt+k*nxt*nyt);
}
inline void pWestDirichlet(double* p, const int& j, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        const double& val) {
    *(p+1+j*nxt+k*nxt*nyt) = -*(p+2+j*nxt+k*nxt*nyt) + 2*val;
}
inline void pSouthNeumann(double* p, const int& i, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(p+i+nxt+k*nxt*nyt) = *(p+i+2*nxt+k*nxt*nyt);
}
inline void pSouthDirichlet(double* p, const int& i, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        const double& val) {
    *(p+i+nxt+k*nxt*nyt) = -*(p+i+2*nxt+k*nxt*nyt) + 2*val;
}
inline void pBackNeumann(double* p, const int& i, const int& j,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(p+i+j*nxt+nxt*nyt) = *(p+i+j*nxt+2*nxt*nyt);
}
inline void pBackDirichlet(double* p, const int& i, const int& j,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        const double& val) {
    *(p+i+j*nxt+nxt*nyt) = -*(p+i+j*nxt+2*nxt*nyt) + 2*val;
}
inline void pEastNeumann(double* p, const int& j, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(p+nxt-2+j*nxt+k*nxt*nyt) = *(p+nxt-3+j*nxt+k*nxt*nyt);
}
inline void pEastDirichlet(double* p, const int& j, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        const double& val) {
    *(p+nxt-2+j*nxt+k*nxt*nyt) = -*(p+nxt-3+j*nxt+k*nxt*nyt) + 2*val;
}
inline void pNorthNeumann(double* p, const int& i, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(p+i+(nyt-2)*nxt+k*nxt*nyt) = *(p+i+(nyt-3)*nxt+k*nxt*nyt);
}
inline void pNorthDirichlet(double* p, const int& i, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        const double& val) {
    *(p+i+(nyt-2)*nxt+k*nxt*nyt) = -*(p+i+(nyt-3)*nxt+k*nxt*nyt) + 2*val;
}
inline void pFrontNeumann(double* p, const int& i, const int& j,
        const int& nxt, const int& nyt, const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(p+i+j*nxt+(nzt-2)*nxt*nyt) = *(p+i+j*nxt+(nzt-3)*nxt*nyt);
}
inline void pFrontDirichlet(double* p, const int& i, const int& j,
        const int& nxt, const int& nyt, const int& nzt, const double& val) {
    *(p+i+j*nxt+(nzt-2)*nxt*nyt) = -*(p+i+j*nxt+(nzt-3)*nxt*nyt) + 2*val;
}

inline void pWestPeriodic(double* p, const int& j, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(p+1+j*nxt+k*nxt*nyt) = *(p+nxt-3+j*nxt+k*nxt*nyt);
}
inline void pEastPeriodic(double* p, const int& j, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(p+nxt-2+j*nxt+k*nxt*nyt) = *(p+2+j*nxt+k*nxt*nyt);
}
inline void pSouthPeriodic(double* p, const int& i, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(p+i+nxt+k*nxt*nyt) = *(p+i+(nyt-3)*nxt+k*nxt*nyt);
}
inline void pNorthPeriodic(double* p, const int& i, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(p+i+(nyt-2)*nxt+k*nxt*nyt) = *(p+i+2*nxt+k*nxt*nyt);
}
inline void pBackPeriodic(double* p, const int& i, const int& j,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(p+i+j*nxt+nxt*nyt) = *(p+i+j*nxt+(nzt-3)*nxt*nyt);
}
inline void pFrontPeriodic(double* p, const int& i, const int& j,
        const int& nxt, const int& nyt, const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(p+i+j*nxt+(nzt-2)*nxt*nyt) = *(p+i+j*nxt+2*nxt*nyt);
}

//----------------------------------------------------------------------------//
// u-velocity boundary functions ---------------------------------------------//
//----------------------------------------------------------------------------//

inline void uWestNeumann(double* u, const int& j, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(u+1+j*nxt+k*nxt*nyt) = *(u+2+j*nxt+k*nxt*nyt);
}
inline void uWestDirichlet(double* u, const int& j, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        const double& val) {
    *(u+1+j*nxt+k*nxt*nyt) = val;
}
inline void uSouthNeumann(double* u, const int& i, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(u+i+nxt+k*nxt*nyt) = *(u+i+2*nxt+k*nxt*nyt);
}
inline void uSouthDirichlet(double* u, const int& i, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        const double& val) {
    *(u+i+nxt+k*nxt*nyt) = -*(u+i+2*nxt+k*nxt*nyt) + 2*val;
}
inline void uBackNeumann(double* u, const int& i, const int& j,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(u+i+j*nxt+nxt*nyt) = *(u+i+j*nxt+2*nxt*nyt);
}
inline void uBackDirichlet(double* u, const int& i, const int& j,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        const double& val) {
    *(u+i+j*nxt+nxt*nyt) = -*(u+i+j*nxt+2*nxt*nyt) + 2*val;
}
inline void uEastNeumann(double* u, const int& j, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(u+nxt-3+j*nxt+k*nxt*nyt) = *(u+nxt-4+j*nxt+k*nxt*nyt);
}
inline void uEastDirichlet(double* u, const int& j, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        const double& val) {
    *(u+nxt-3+j*nxt+k*nxt*nyt) = val;
}
inline void uNorthNeumann(double* u, const int& i, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(u+i+(nyt-2)*nxt+k*nxt*nyt) = *(u+i+(nyt-3)*nxt+k*nxt*nyt);
}
inline void uNorthDirichlet(double* u, const int& i, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        const double& val) {
    *(u+i+(nyt-2)*nxt+k*nxt*nyt) = -*(u+i+(nyt-3)*nxt+k*nxt*nyt) + 2*val;
}
inline void uFrontNeumann(double* u, const int& i, const int& j,
        const int& nxt, const int& nyt, const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(u+i+j*nxt+(nzt-2)*nxt*nyt) = *(u+i+j*nxt+(nzt-3)*nxt*nyt);
}
inline void uFrontDirichlet(double* u, const int& i, const int& j,
        const int& nxt, const int& nyt, const int& nzt, const double& val) {
    *(u+i+j*nxt+(nzt-2)*nxt*nyt) = -*(u+i+j*nxt+(nzt-3)*nxt*nyt) + 2*val;
}

//----------------------------------------------------------------------------//
// v-velocity boundary functions ---------------------------------------------//
//----------------------------------------------------------------------------//

inline void vWestNeumann(double* v, const int& j, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(v+1+j*nxt+k*nxt*nyt) = *(v+2+j*nxt+k*nxt*nyt);
}
inline void vWestDirichlet(double* v, const int& j, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        const double& val) {
    *(v+1+j*nxt+k*nxt*nyt) = -*(v+2+j*nxt+k*nxt*nyt) + 2*val;
}
inline void vSouthNeumann(double* v, const int& i, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(v+i+nxt+k*nxt*nyt) = *(v+i+2*nxt+k*nxt*nyt);
}
inline void vSouthDirichlet(double* v, const int& i, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        const double& val) {
    *(v+i+nxt+k*nxt*nyt) = val;
}
inline void vBackNeumann(double* v, const int& i, const int& j,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(v+i+j*nxt+nxt*nyt) = *(v+i+j*nxt+2*nxt*nyt);
}
inline void vBackDirichlet(double* v, const int& i, const int& j,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        const double& val) {
    *(v+i+j*nxt+nxt*nyt) = -*(v+i+j*nxt+2*nxt*nyt) + 2*val;
}
inline void vEastNeumann(double* v, const int& j, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(v+nxt-2+j*nxt+k*nxt*nyt) = *(v+nxt-3+j*nxt+k*nxt*nyt);
}
inline void vEastDirichlet(double* v, const int& j, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        const double& val) {
    *(v+nxt-2+j*nxt+k*nxt*nyt) = -*(v+nxt-3+j*nxt+k*nxt*nyt) + 2*val;
}
inline void vNorthNeumann(double* v, const int& i, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(v+i+(nyt-3)*nxt+k*nxt*nyt) = *(v+i+(nyt-4)*nxt+k*nxt*nyt);
}
inline void vNorthDirichlet(double* v, const int& i, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        const double& val) {
    *(v+i+(nyt-3)*nxt+k*nxt*nyt) = val;
}
inline void vFrontNeumann(double* v, const int& i, const int& j,
        const int& nxt, const int& nyt, const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(v+i+j*nxt+(nzt-2)*nxt*nyt) = *(v+i+j*nxt+(nzt-3)*nxt*nyt);
}
inline void vFrontDirichlet(double* v, const int& i, const int& j,
        const int& nxt, const int& nyt, const int& nzt, const double& val) {
    *(v+i+j*nxt+(nzt-2)*nxt*nyt) = -*(v+i+j*nxt+(nzt-3)*nxt*nyt) + 2*val;
}

//----------------------------------------------------------------------------//
// w-velocity boundary functions ---------------------------------------------//
//----------------------------------------------------------------------------//
inline void wWestNeumann(double* w, const int& j, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(w+1+j*nxt+k*nxt*nyt) = *(w+2+j*nxt+k*nxt*nyt);
}
inline void wWestDirichlet(double* w, const int& j, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        const double& val) {
    *(w+1+j*nxt+k*nxt*nyt) = -*(w+2+j*nxt+k*nxt*nyt) + 2*val;
}
inline void wSouthNeumann(double* w, const int& i, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(w+i+nxt+k*nxt*nyt) = *(w+i+2*nxt+k*nxt*nyt);
}
inline void wSouthDirichlet(double* w, const int& i, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        const double& val) {
    *(w+i+nxt+k*nxt*nyt) = -*(w+i+2*nxt+k*nxt*nyt) + 2*val;
}
inline void wBackNeumann(double* w, const int& i, const int& j,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(w+i+j*nxt+nxt*nyt) = *(w+i+j*nxt+2*nxt*nyt);
}
inline void wBackDirichlet(double* w, const int& i, const int& j,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        const double& val) {
    *(w+i+j*nxt+nxt*nyt) = val;
}
inline void wEastNeumann(double* w, const int& j, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(w+nxt-2+j*nxt+k*nxt*nyt) = *(w+nxt-3+j*nxt+k*nxt*nyt);
}
inline void wEastDirichlet(double* w, const int& j, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        const double& val) {
    *(w+nxt-2+j*nxt+k*nxt*nyt) = -*(w+nxt-3+j*nxt+k*nxt*nyt) + 2*val;
}
inline void wNorthNeumann(double* w, const int& i, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(w+i+(nyt-2)*nxt+k*nxt*nyt) = *(w+i+(nyt-3)*nxt+k*nxt*nyt);
}
inline void wNorthDirichlet(double* w, const int& i, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        const double& val) {
    *(w+i+(nyt-2)*nxt+k*nxt*nyt) = -*(w+i+(nyt-3)*nxt+k*nxt*nyt) + 2*val;
}
inline void wFrontNeumann(double* w, const int& i, const int& j,
        const int& nxt, const int& nyt, const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(w+i+j*nxt+(nzt-3)*nxt*nyt) = *(w+i+j*nxt+(nzt-4)*nxt*nyt);
}
inline void wFrontDirichlet(double* w, const int& i, const int& j,
        const int& nxt, const int& nyt, const int& nzt, const double& val) {
    *(w+i+j*nxt+(nzt-3)*nxt*nyt) = val;
}

//----------------------------------------------------------------------------//
// All velocity periodic boundary functions ----------------------------------//
//----------------------------------------------------------------------------//

inline void uWestPeriodic(double* u, const int& j, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(u+1+j*nxt+k*nxt*nyt) = *(u+nxt-4+j*nxt+k*nxt*nyt);
}
inline void uEastPeriodic(double* u, const int& j, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(u+nxt-3+j*nxt+k*nxt*nyt) = *(u+2+j*nxt+k*nxt*nyt);
}
inline void uSouthPeriodic(double* u, const int& i, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(u+i+nxt+k*nxt*nyt) = *(u+i+(nyt-3)*nxt+k*nxt*nyt);
}
inline void uNorthPeriodic(double* u, const int& i, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(u+i+(nyt-2)*nxt+k*nxt*nyt) = *(u+i+2*nxt+k*nxt*nyt);
}
inline void uBackPeriodic(double* u, const int& i, const int& j,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(u+i+j*nxt+nxt*nyt) = *(u+i+j*nxt+(nzt-3)*nxt*nyt);
}
inline void uFrontPeriodic(double* u, const int& i, const int& j,
        const int& nxt, const int& nyt, const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(u+i+j*nxt+(nzt-2)*nxt*nyt) = *(u+i+j*nxt+2*nxt*nyt);
}
inline void vWestPeriodic(double* v, const int& j, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(v+1+j*nxt+k*nxt*nyt) = *(v+nxt-3+j*nxt+k*nxt*nyt);
}
inline void vEastPeriodic(double* v, const int& j, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(v+nxt-2+j*nxt+k*nxt*nyt) = *(v+2+j*nxt+k*nxt*nyt);
}
inline void vSouthPeriodic(double* v, const int& i, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(v+i+nxt+k*nxt*nyt) = *(v+i+(nyt-4)*nxt+k*nxt*nyt);
}
inline void vNorthPeriodic(double* v, const int& i, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(v+i+(nyt-3)*nxt+k*nxt*nyt) = *(v+i+2*nxt+k*nxt*nyt);
}
inline void vBackPeriodic(double* v, const int& i, const int& j,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(v+i+j*nxt+nxt*nyt) = *(v+i+j*nxt+(nzt-3)*nxt*nyt);
}
inline void vFrontPeriodic(double* v, const int& i, const int& j,
        const int& nxt, const int& nyt, const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(v+i+j*nxt+(nzt-2)*nxt*nyt) = *(v+i+j*nxt+2*nxt*nyt);
}
inline void wWestPeriodic(double* w, const int& j, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(w+1+j*nxt+k*nxt*nyt) = *(w+nxt-3+j*nxt+k*nxt*nyt);
}
inline void wEastPeriodic(double* w, const int& j, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(w+nxt-2+j*nxt+k*nxt*nyt) = *(w+2+j*nxt+k*nxt*nyt);
}
inline void wSouthPeriodic(double* w, const int& i, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(w+i+nxt+k*nxt*nyt) = *(w+i+(nyt-3)*nxt+k*nxt*nyt);
}
inline void wNorthPeriodic(double* w, const int& i, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(w+i+(nyt-2)*nxt+k*nxt*nyt) = *(w+i+2*nxt+k*nxt*nyt);
}
inline void wBackPeriodic(double* w, const int& i, const int& j,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(w+i+j*nxt+nxt*nyt) = *(w+i+j*nxt+(nzt-4)*nxt*nyt);
}
inline void wFrontPeriodic(double* w, const int& i, const int& j,
        const int& nxt, const int& nyt, const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(w+i+j*nxt+(nzt-3)*nxt*nyt) = *(w+i+j*nxt+2*nxt*nyt);
}

//----------------------------------------------------------------------------//
// All velocity symmetry boundary functions ----------------------------------//
//----------------------------------------------------------------------------//

inline void uWestSymmetry(double* u, const int& j, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(u+1+j*nxt+k*nxt*nyt) = 0.0;
}
inline void uEastSymmetry(double* u, const int& j, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(u+nxt-3+j*nxt+k*nxt*nyt) = 0.0;
}
inline void vSouthSymmetry(double* v, const int& i, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(v+i+nxt+k*nxt*nyt) = 0.0;
}
inline void vNorthSymmetry(double* v, const int& i, const int& k,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(v+i+(nyt-3)*nxt+k*nxt*nyt) = 0.0;
}
inline void wBackSymmetry(double* w, const int& i, const int& j,
        const int& nxt, const int& nyt, __attribute__((unused)) const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(w+i+j*nxt+nxt*nyt) = 0.0;
}
inline void wFrontSymmetry(double* w, const int& i, const int& j,
        const int& nxt, const int& nyt, const int& nzt,
        __attribute__((unused)) const double& val=0) {
    *(w+i+j*nxt+(nzt-3)*nxt*nyt) = 0.0;
}

//----------------------------------------------------------------------------//
// Map boundary functions to respective boundary structure members -----------//
//----------------------------------------------------------------------------//

const unordered_map<BoundaryType, BoundaryFnc>
    pWestMapFnc = { {BoundaryType::Neumann,   pWestNeumann},
                    {BoundaryType::Dirichlet, pWestDirichlet},
                    {BoundaryType::Periodic,  pWestPeriodic},
                    {BoundaryType::Symmetry,  pWestNeumann} },
    pEastMapFnc = { {BoundaryType::Neumann,   pEastNeumann},
                    {BoundaryType::Dirichlet, pEastDirichlet},
                    {BoundaryType::Periodic,  pEastPeriodic},
                    {BoundaryType::Symmetry,  pEastNeumann} },
    pSouthMapFnc= { {BoundaryType::Neumann,   pSouthNeumann},
                    {BoundaryType::Dirichlet, pSouthDirichlet},
                    {BoundaryType::Periodic,  pSouthPeriodic},
                    {BoundaryType::Symmetry,  pSouthNeumann} },
    pNorthMapFnc= { {BoundaryType::Neumann,   pNorthNeumann},
                    {BoundaryType::Dirichlet, pNorthDirichlet},
                    {BoundaryType::Periodic,  pNorthPeriodic},
                    {BoundaryType::Symmetry,  pNorthNeumann} },
    pBackMapFnc = { {BoundaryType::Neumann,   pBackNeumann},
                    {BoundaryType::Dirichlet, pBackDirichlet},
                    {BoundaryType::Periodic,  pBackPeriodic},
                    {BoundaryType::Symmetry,  pBackNeumann} },
    pFrontMapFnc= { {BoundaryType::Neumann,   pFrontNeumann},
                    {BoundaryType::Dirichlet, pFrontDirichlet},
                    {BoundaryType::Periodic,  pFrontPeriodic},
                    {BoundaryType::Symmetry,  pFrontNeumann} };
const unordered_map<BoundaryType, BoundaryFnc>
    uWestMapFnc = { {BoundaryType::Neumann,   uWestNeumann},
                    {BoundaryType::Dirichlet, uWestDirichlet},
                    {BoundaryType::Periodic,  uWestPeriodic},
                    {BoundaryType::Symmetry,  uWestSymmetry} },
    uEastMapFnc = { {BoundaryType::Neumann,   uEastNeumann},
                    {BoundaryType::Dirichlet, uEastDirichlet},
                    {BoundaryType::Periodic,  uEastPeriodic},
                    {BoundaryType::Symmetry,  uEastSymmetry} },
    uSouthMapFnc= { {BoundaryType::Neumann,   uSouthNeumann},
                    {BoundaryType::Dirichlet, uSouthDirichlet},
                    {BoundaryType::Periodic,  uSouthPeriodic},
                    {BoundaryType::Symmetry,  uSouthNeumann} },
    uNorthMapFnc= { {BoundaryType::Neumann,   uNorthNeumann},
                    {BoundaryType::Dirichlet, uNorthDirichlet},
                    {BoundaryType::Periodic,  uNorthPeriodic},
                    {BoundaryType::Symmetry,  uNorthNeumann} },
    uBackMapFnc = { {BoundaryType::Neumann,   uBackNeumann},
                    {BoundaryType::Dirichlet, uBackDirichlet},
                    {BoundaryType::Periodic,  uBackPeriodic},
                    {BoundaryType::Symmetry,  uBackNeumann} },
    uFrontMapFnc= { {BoundaryType::Neumann,   uFrontNeumann},
                    {BoundaryType::Dirichlet, uFrontDirichlet},
                    {BoundaryType::Periodic,  uFrontPeriodic},
                    {BoundaryType::Symmetry,  uFrontNeumann} };
const unordered_map<BoundaryType, BoundaryFnc>
    vWestMapFnc = { {BoundaryType::Neumann,   vWestNeumann},
                    {BoundaryType::Dirichlet, vWestDirichlet},
                    {BoundaryType::Periodic,  vWestPeriodic},
                    {BoundaryType::Symmetry,  vWestNeumann} },
    vEastMapFnc = { {BoundaryType::Neumann,   vEastNeumann},
                    {BoundaryType::Dirichlet, vEastDirichlet},
                    {BoundaryType::Periodic,  vEastPeriodic},
                    {BoundaryType::Symmetry,  vEastNeumann} },
    vSouthMapFnc= { {BoundaryType::Neumann,   vSouthNeumann},
                    {BoundaryType::Dirichlet, vSouthDirichlet},
                    {BoundaryType::Periodic,  vSouthPeriodic},
                    {BoundaryType::Symmetry,  vSouthSymmetry} },
    vNorthMapFnc= { {BoundaryType::Neumann,   vNorthNeumann},
                    {BoundaryType::Dirichlet, vNorthDirichlet},
                    {BoundaryType::Periodic,  vNorthPeriodic},
                    {BoundaryType::Symmetry,  vNorthSymmetry} },
    vBackMapFnc = { {BoundaryType::Neumann,   vBackNeumann},
                    {BoundaryType::Dirichlet, vBackDirichlet},
                    {BoundaryType::Periodic,  vBackPeriodic},
                    {BoundaryType::Symmetry,  vBackNeumann} },
    vFrontMapFnc= { {BoundaryType::Neumann,   vFrontNeumann},
                    {BoundaryType::Dirichlet, vFrontDirichlet},
                    {BoundaryType::Periodic,  vFrontPeriodic},
                    {BoundaryType::Symmetry,  vFrontNeumann} };
const unordered_map<BoundaryType, BoundaryFnc>
    wWestMapFnc = { {BoundaryType::Neumann,   wWestNeumann},
                    {BoundaryType::Dirichlet, wWestDirichlet},
                    {BoundaryType::Periodic,  wWestPeriodic},
                    {BoundaryType::Symmetry,  wWestNeumann} },
    wEastMapFnc = { {BoundaryType::Neumann,   wEastNeumann},
                    {BoundaryType::Dirichlet, wEastDirichlet},
                    {BoundaryType::Periodic,  wEastPeriodic},
                    {BoundaryType::Symmetry,  wEastNeumann} },
    wSouthMapFnc= { {BoundaryType::Neumann,   wSouthNeumann},
                    {BoundaryType::Dirichlet, wSouthDirichlet},
                    {BoundaryType::Periodic,  wSouthPeriodic},
                    {BoundaryType::Symmetry,  wSouthNeumann} },
    wNorthMapFnc= { {BoundaryType::Neumann,   wNorthNeumann},
                    {BoundaryType::Dirichlet, wNorthDirichlet},
                    {BoundaryType::Periodic,  wNorthPeriodic},
                    {BoundaryType::Symmetry,  wNorthNeumann} },
    wBackMapFnc = { {BoundaryType::Neumann,   wBackNeumann},
                    {BoundaryType::Dirichlet, wBackDirichlet},
                    {BoundaryType::Periodic,  wBackPeriodic},
                    {BoundaryType::Symmetry,  wBackSymmetry} },
    wFrontMapFnc= { {BoundaryType::Neumann,   wFrontNeumann},
                    {BoundaryType::Dirichlet, wFrontDirichlet},
                    {BoundaryType::Periodic,  wFrontPeriodic},
                    {BoundaryType::Symmetry,  wFrontSymmetry} };

const BoundaryFunctions
    pressureFunctions {
        pWestMapFnc.at (pressureConditions.west),
        pEastMapFnc.at (pressureConditions.east),
        pSouthMapFnc.at(pressureConditions.south),
        pNorthMapFnc.at(pressureConditions.north),
        pBackMapFnc.at (pressureConditions.back),
        pFrontMapFnc.at(pressureConditions.front)
    },
    uVelocityFunctions {
        uWestMapFnc.at (uVelocityConditions.west),
        uEastMapFnc.at (uVelocityConditions.east),
        uSouthMapFnc.at(uVelocityConditions.south),
        uNorthMapFnc.at(uVelocityConditions.north),
        uBackMapFnc.at (uVelocityConditions.back),
        uFrontMapFnc.at(uVelocityConditions.front)
    },
    vVelocityFunctions {
        vWestMapFnc.at (vVelocityConditions.west),
        vEastMapFnc.at (vVelocityConditions.east),
        vSouthMapFnc.at(vVelocityConditions.south),
        vNorthMapFnc.at(vVelocityConditions.north),
        vBackMapFnc.at (vVelocityConditions.back),
        vFrontMapFnc.at(vVelocityConditions.front)
    },
    wVelocityFunctions {
        wWestMapFnc.at (wVelocityConditions.west),
        wEastMapFnc.at (wVelocityConditions.east),
        wSouthMapFnc.at(wVelocityConditions.south),
        wNorthMapFnc.at(wVelocityConditions.north),
        wBackMapFnc.at (wVelocityConditions.back),
        wFrontMapFnc.at(wVelocityConditions.front)
    };

const Boundaries
    presBoundaries { pressureValues, pressureConditions, pressureFunctions },
    uVelBoundaries { uVelocityValues, uVelocityConditions, uVelocityFunctions },
    vVelBoundaries { vVelocityValues, vVelocityConditions, vVelocityFunctions },
    wVelBoundaries { wVelocityValues, wVelocityConditions, wVelocityFunctions };

#endif