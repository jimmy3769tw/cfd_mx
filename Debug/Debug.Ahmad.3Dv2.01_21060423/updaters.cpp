/**
 * @file        updaters.cpp
 *
 * @project     3D N-S solver rebuild
 * @version     2.0beta
 *
 * @author      Syed Ahmad Raza (sahmadrz@gmail.com)
 * @date        2020-10-27
 *
 * @brief       Update the pressure and velocity values as per the calculations
 *              in the current timestep of the Navier-Stokes solver.
 *
 */

#include "updaters.h"
#include "parameters.h"
#include <tuple>


using std::abs, std::tuple, std::tie;
enum class direction{ i, j, k };

/*
 * Construct the matrix A of the equation AX=B.
 */
void pressureMatrixConstructor() {
    #pragma omp parallel for default(none)\
        shared(PA, PJ, DDX, DDXM, DDY, DDYM, DDZ, DDZM)
    for (int k=0; k<nz; ++k) {
        for (int j=0; j<ny; ++j) {
            for (int i=0; i<nx; ++i) {
                const int ig = i+gc, jg = j+gc, kg = k+gc;
                double ip, im, jp, jm, kp, km, ijk;

                ip = DDX(ig)*DDXM(ig);
                im = DDX(ig)*DDXM(ig-1);
                jp = DDY(jg)*DDYM(jg);
                jm = DDY(jg)*DDYM(jg-1);
                kp = DDZ(kg)*DDZM(kg);
                km = DDZ(kg)*DDZM(kg-1);

                ijk = ip + im + jp + jm + kp + km;

                if (i==0)    { ijk -= im; }
                if (i==nx-1) { ijk -= ip; }
                if (j==0)    { ijk -= jm; }
                if (j==ny-1) { ijk -= jp; }
                if (k==0)    { ijk -= km; }
                if (k==nz-1) { ijk -= kp; }

                const int l = i + j*nx + k*nx*ny;
                // PJ(l) = 1.0/ijk;                    // Jacobian preconditioner
                PJ(l) = 1.0; 

                if (k!=0)    { PA(l, 0) = -km*PJ(l); }
                if (j!=0)    { PA(l, 1) = -jm*PJ(l); }
                if (i!=0)    { PA(l, 2) = -im*PJ(l); }
                //             PA(l, 3) = 1.0;    // Jacobian preconditioner
                               PA(l, 3) = ijk;
                if (i!=nx-1) { PA(l, 4) = -ip*PJ(l); }
                if (j!=ny-1) { PA(l, 5) = -jp*PJ(l); }
                if (k!=nz-1) { PA(l, 6) = -kp*PJ(l); }
            }
        }
    }
}





void pressureMatrixConstructor_CSR( mat::CSR_matrix<double> &mat ) {

    mat.init(ndim * 7, ndim + 1);


    for (int k=0; k<nz; ++k) {
        for (int j=0; j<ny; ++j) {
            for (int i=0; i<nx; ++i) {
                const int ig = i+gc, jg = j+gc, kg = k+gc;
                double ip, im, jp, jm, kp, km, ijk;

                ip = DDX(ig)*DDXM(ig);
                im = DDX(ig)*DDXM(ig-1);
                jp = DDY(jg)*DDYM(jg);
                jm = DDY(jg)*DDYM(jg-1);
                kp = DDZ(kg)*DDZM(kg);
                km = DDZ(kg)*DDZM(kg-1);


                ijk = ip + im + jp + jm + kp + km;

                if (i==0)    { ijk -= im; }
                if (i==nx-1) { ijk -= ip; }
                if (j==0)    { ijk -= jm; }
                if (j==ny-1) { ijk -= jp; }
                if (k==0)    { ijk -= km; }
                if (k==nz-1) { ijk -= kp; }
                const int l = i + j*nx + k*nx*ny;
                // PJ(l) = 1.0/ijk;                    // Jacobian preconditioner

                if (k!=0)    { mat.push_back(l-nx*ny, -km*PJ(l)); }
                if (j!=0)    { mat.push_back(l-nx, -jm*PJ(l)); }
                if (i!=0)    { mat.push_back(l-1, -im*PJ(l)); }
                            //    mat.push_back(l, 1);
                               mat.push_back(l, ijk);
                if (i!=nx-1) { mat.push_back(l+1, -ip*PJ(l)); }
                if (j!=ny-1) { mat.push_back(l+nx, -jp*PJ(l)); }
                if (k!=nz-1) { mat.push_back(l+nx*ny,-kp*PJ(l)); }
                mat.finishIdx();
            }
        }
    }
}

// std::tuple<std::vector<int>, std::vector<int>, std::vector<double> >
// pressureMat_CSR(){

//     std::vector<int>  ptr, idx;
//     std::vector<double>  val;
//     ptr.clear(); ptr.reserve(ndim + 1); ptr.push_back(0);
//     idx.clear(); idx.reserve(ndim * 7); // We use 7-point stencil, so the matrix
//     val.clear(); val.reserve(ndim * 7); // will have at most n3 * 7 nonzero elements.
//     // Interior point. Use 7-point finite difference stencil.

//     for (int k=0; k<nz; ++k)
//     {
//         for (int j=0; j<ny; ++j)
//         {
//             for (int i=0; i<nx; ++i)
//             {
//                 const int ig = i+gc, jg = j+gc, kg = k+gc;
//                 double ip, im, jp, jm, kp, km, ijk;

//                 ip = DDX(ig)*DDXM(ig);
//                 im = DDX(ig)*DDXM(ig-1);
//                 jp = DDY(jg)*DDYM(jg);
//                 jm = DDY(jg)*DDYM(jg-1);
//                 kp = DDZ(kg)*DDZM(kg);
//                 km = DDZ(kg)*DDZM(kg-1);
//                 ijk = ip + im + jp + jm + kp + km;

//                 if (i==0)    { ijk -= im; }
//                 if (i==nx-1) { ijk -= ip; }
//                 if (j==0)    { ijk -= jm; }
//                 if (j==ny-1) { ijk -= jp; }
//                 if (k==0)    { ijk -= km; }
//                 if (k==nz-1) { ijk -= kp; }

//                 const int l = i + j*nx + k*nx*ny;

//                 if (k!=0)    { idx.push_back(l-nx*ny);  val.push_back(im*PJ(l)); }
//                 if (j!=0)    { idx.push_back(l-nx);     val.push_back(jm*PJ(l)); }
//                 if (i!=0)    { idx.push_back(l-1);      val.push_back(km*PJ(l)); }

//                             //    idx.push_back(l);        val.push_back(1.0);  // Jacobian preconditioner
//                                idx.push_back(l);        val.push_back(ijk);

//                 if (i!=nx-1) { idx.push_back(l+1);      val.push_back(kp*PJ(l)); }
//                 if (j!=ny-1) { idx.push_back(l+nx);     val.push_back(jp*PJ(l)); }
//                 if (k!=nz-1) { idx.push_back(l+nx*ny);  val.push_back(ip*PJ(l)); }
//                 ptr.push_back( idx.size());
//             }
//         }
//     }

//     return std::make_tuple(ptr, idx, val);
// }






/*
 * Construct the vector PB of the equation AX=PB and initialize the solution
 * vector PX to zero.
 */

void massVectorConstructor() {
    mChangeMax = 0.0;
    double ddt = 1.0/dt;
    double* us  = US.data(),  * vs  = VS.data(),  * ws  = WS.data(),
          * ddx = DDX.data(), * ddy = DDY.data(), * ddz = DDZ.data(),
          * pb  = PB.data(),  * pj  = PJ.data();
    #pragma omp parallel for default(none)\
        firstprivate(pb, pj, us, vs, ws, ddx, ddy, ddz, ddt)\
        reduction(max:mChangeMax)
    for (int k=0; k<nz; ++k) {
        for (int j=0; j<ny; ++j) {
            for (int i=0; i<nx; ++i) {
                const int l = i + j*nx + k*nx*ny;
                const int ig = i+gc, jg = j+gc, kg = k+gc;
                const int lg = ig + jg*nxt + kg*nxt*nyt;

                double mChange =   *(ddx+ig)*(*(us+lg) - *(us+lg-1))
                                 + *(ddy+jg)*(*(vs+lg) - *(vs+lg-nxt))
                                 + *(ddz+kg)*(*(ws+lg) - *(ws+lg-nxt*nyt));

                if (abs(mChange) > mChangeMax && i!= 0 && j!=0 && k!=0
                                    && i!=nx-1 && j!=ny-1 && k!=nz-1) {
                    mChangeMax = abs(mChange);
                }
                *(pb+l) = -ddt*mChange**(pj+l);
                // Needs to be modified for non-zero Dirichlet pressure boundary
            }
        }
    }
}



void massVectorConstructor_vector(std::vector<double> &b_mat) {

    for (size_t i = 0; i < b_mat.size(); i++)
    {
        b_mat[i] = PB(i);
    }
}



void pressureUpdater() {
    pChangeMax = 0.0;
    double* p = P.data(), * px = PX.data();
    #pragma omp parallel for default(none) firstprivate(p, px)\
        reduction(max:pChangeMax)
    for (int k=0; k<nz; ++k) {
        for (int j=0; j<ny; ++j) {
            for (int i=0; i<nx; ++i) {
                const int l = i + j*nx + k*nx*ny;
                const int ig = i+gc, jg = j+gc, kg = k+gc;
                const int lg = ig + jg*nxt + kg*nxt*nyt;

                double pChange = abs(*(px+l) - *(p+lg));
                if (pChange > pChangeMax) { pChangeMax = pChange; }

                *(p+lg) = *(px+l);
            }
        }
    }
}

inline tuple<double, double> velocityUpdateLoop(double* vel, double* vf,
        const double* vels, const double* pres,
        const double* da, const double* dam, const double* ddam,
        const double* sVel, const double* etaP,
        // const double sVel, const double* etaP,
        const direction dir, const int nxtPInd,
        const int iBgn, const int iEnd, const int jBgn, const int jEnd,
        const int kBgn, const int kEnd,
        const int jMult, const int kMult,
        const double deltaT) {
    double velChangeMax = 0, minConv = 10, ddt = 1.0/deltaT;
    #pragma omp parallel for default(none)\
        firstprivate(vel, vf, vels, pres, da, dam, ddam, sVel, dir,\
            nxtPInd, iBgn, iEnd, jBgn, jEnd, kBgn, kEnd,\
            etaP, jMult, kMult, deltaT, ddt)\
        reduction(max:velChangeMax) reduction(min:minConv)
    for (int k=kBgn; k<kEnd; ++k) {
        for (int j=jBgn; j<jEnd; ++j) {
            for (int i=iBgn; i<iEnd; ++i) {
                int q=0;
                switch (dir) {
                    case direction::i: q=i; break;
                    case direction::j: q=j; break;
                    case direction::k: q=k; break;
                }
                int l = i + j*jMult + k*kMult;

                double velFluid = *(vels+l)
                    - deltaT**(ddam+q)*(*(pres+l+nxtPInd) - *(pres+l));

                double etaPAvg =
                    *(etaP+l) + (*(etaP+l+nxtPInd) - *(etaP+l))*0.5**da**ddam;
                double velFinal = etaPAvg**(sVel+l) + (1 - etaPAvg)*velFluid;
                // double velFinal = etaPAvg*sVel + (1 - etaPAvg)*velFluid;

                *(vf+l) = (velFinal - velFluid)*ddt;

                double velChange = abs(velFinal - *(vel+l));
                if (velChange > velChangeMax) { velChangeMax = velChange; }
                *(vel+l) = velFinal;

                if constexpr (dtOverride < 0.0) {
                    double conv = abs(*(dam+q)/ *(vel+l));
                    if (conv < minConv) { minConv = conv; }
                }
            }
        }
    }
    return {velChangeMax, minConv};
}

void velocityUpdater() {
    double* u     = U.data(),    * v     = V.data(),    * w     = W.data(),
          * us    = US.data(),   * vs    = VS.data(),   * ws    = WS.data(),
          * dx    = DX.data(),   * dy    = DY.data(),   * dz    = DZ.data(),
          * dxm   = DXM.data(),  * dym   = DYM.data(),  * dzm   = DZM.data(),
          * ddxm  = DDXM.data(), * ddym  = DDYM.data(), * ddzm  = DDZM.data(),
          * fx    = FX.data(),   * fy    = FY.data(),   * fz    = FZ.data(),
          * velsx = VELSX.data(),* velsy = VELSY.data(),* velsz = VELSZ.data(),
          * p     = P.data(),    * eta   = ETA.data();

    tie(uChangeMax, minConvX) = velocityUpdateLoop(u, fx, us, p, dx, dxm, ddxm,
                                    velsx, eta, direction::i, 1,
                                    // 0, eta, direction::i, 1,
                                    gc, nxt-gc-1, gc, nyt-gc, gc, nzt-gc,
                                    nxt, nxt*nyt, dt);
    tie(vChangeMax, minConvY) = velocityUpdateLoop(v, fy, vs, p, dy, dym, ddym,
                                    velsy, eta, direction::j, nxt,
                                    // vStruct, eta, direction::j, nxt,
                                    gc, nxt-gc, gc, nyt-gc-1, gc, nzt-gc,
                                    nxt, nxt*nyt, dt);
    tie(wChangeMax, minConvZ) = velocityUpdateLoop(w, fz, ws, p, dz, dzm, ddzm,
                                    velsz, eta, direction::k, nxt*nyt,
                                    // 0, eta, direction::k, nxt*nyt,
                                    gc, nxt-gc, gc, nyt-gc, gc, nzt-gc-1,
                                    nxt, nxt*nyt, dt);
}
