#include"../inc/matrix/ELL_sparseMatrix.hpp" //ELL_matrix
	// icpc -std=c++17 ELLcontanier_test_multiMat.cpp -fopenmp -o ELLcontanier_test_multiMat.exe

using namespace std;


int main(void){

    int m[] = {4,4};
    int n[] = {4,4};
    mat::ELL_matrix<double> Amat_ELL(m[0],n[0],4);
    mat::ELL_matrix<double> Bmat_ELL(m[1],n[1],4);


    for(size_t i=0; i<m[0]; ++i)
    {
        for(size_t j=0; j<n[0]; ++j)
        {
            if(i!=j){
                auto valA = i+j + 2*i + j;
                Amat_ELL.set(i,j,valA)  ;
            }

        }
        // auto valB = (i+2) * 12;
        // Amat_ELL.set(i,i,valB)  ;
    }


    for(size_t i=0; i<m[1]; ++i)
    {
        for(size_t j=0; j<n[1]; ++j)
        {
            if(i!=j){
                // auto valA = 1;
                // Bmat_ELL.set(i,j,valA)  ;
            }
        }
        auto valB = i+1;
        Bmat_ELL.set(i,i,valB)  ;
    }

    cout << Amat_ELL;
    cout << Bmat_ELL;

    auto result = Amat_ELL * Bmat_ELL;


    cout << result;
    return 0;

}