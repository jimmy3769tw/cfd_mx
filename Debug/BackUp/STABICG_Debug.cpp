#include"../inc/matrix/ELL_sparseMatrix.hpp"
#include<iostream>
#include<vector>
#include<string>
#include<cmath>
#include<tuple> // cpp -11
#include<algorithm>
#include<iterator>
#include <numeric>
#include <functional>
using namespace std;


auto  npc_bicgstab (
    std::vector <double> &matB,
    mat::ELL_matrix<double> matA
){

    double  value_Alpha,
            value_Omegai,
            value_Rhoi;
        double zeta = 1e-3;

    double nu,mu,sum,scal,norm,norm0;

    int COL = matB.size();
    int ROW = matB.size();

    vector<double> ri(ROW, 0.0L);
    vector<double> xi(ROW, 0.0L); // x
    vector<double> hat_r0(ROW, 0.0L);//  \hat r0

//     //! check norm
    
    norm0 = std::inner_product(hat_r0.begin(), hat_r0.end(), ri.begin(), 0.0L );// norm0 = Bicg_hat_r0 * Bicg_r
    norm0 = std::sqrt(norm0); 

// https://en.wikipedia.org/wiki/Biconjugate_gradient_stabilized_method
// ! npc BiCGSTAB

//     //* 1.r0 = b − Ax0 // I.C. r @ i = 0
    auto temp_1 = matA * xi;
    std::transform(matB.begin(), matB.end(), temp_1.begin(),
                ri.begin(), std::minus<double>());

    // *2. hat r_0 - r_0 
    hat_r0.assign(ri.begin(), ri.end());

    // *3. rho_0 = alpha = omega_0 =0
    value_Rhoi      = 1.0L;    
    value_Alpha     = 1.0L;    
    value_Omegai    = 1.0L; 

    // *4. v_0 = p_0 = 0.
    vector<double> vi(ROW, 0.0L);
    vector<double> pi(ROW, 0.0L);
    vector<double> si(ROW, 0.0L);
    vector<double> ti(ROW, 0.0L);
    vector<double> h(ROW , 0.0L);

    double value_Beta = 0;
    int iterMax = 3000;
    size_t ik;
    for ( ik = 0; ik<iterMax ;++ik ){
        auto ri_pre = ri;
        auto xi_pre = xi;   
        auto value_Omegai_pre = value_Omegai;
        auto value_Rhoi_pre = value_Rhoi;
        vector<double> tempMat(ROW);
        // * [5.1] ------------ ρi = (r̂0, ri−1) ---------------

        value_Rhoi = std::inner_product(hat_r0.begin(),hat_r0.end(),ri.begin(),0.0L);

        // * [5.1] ------------ ρi = (r̂0, ri−1) ---------------


        // * [5.2] ------  β = (ρi/ρi−1)(α/ωi−1) ---------------

        value_Beta = ( value_Rhoi  / value_Rhoi_pre  ) *
                     ( value_Alpha / value_Omegai_pre);

        // * [5.2] ------  β = (ρi/ρi−1)(α/ωi−1) ---------------



        // * [5.3] -------- pi = r_{i−1} + β(p_{i−1} − ω_{i−1} * v_{i−1}) --------
        // ! pi_pre = pi;
        // ! vi_pre = vi;
        // ! value_Omegai_pre = value_Omegai;

        for(size_t i = 0 ; i < ROW ;++i){
            pi[i] = ri_pre[i] + value_Beta * ( pi[i] - value_Omegai * vi[i]);
        }
        // * [5.3] -------- pi = r_{i−1} + β(p_{i−1} − ω_{i−1} * v_{i−1}) --------


        // * [5.4] --------------- v_i = A \cdot p_i  ---------------

        vi = matA * pi;

        // * [5.4] --------------- v_i = A \cdot p_i  ---------------



        // * [5.5] --------------- α = ρi/(r̂0, vi) ---------------

        auto temp5 = std::inner_product(
                hat_r0.begin(), hat_r0.end(), vi.begin(), 0.0L);
        value_Alpha = value_Rhoi / temp5 ;
    
        // * [5.5] --------------- α = ρi/(r̂0, vi) ---------------


        // * [5.6] ---------------  h = x_{i−1} + α pi ---------------

        for(size_t i = 0 ; i < ROW ;++i)
        {
            h[i] = xi_pre[i] - value_Alpha * pi[i];
        }

        // * [5.6] ---------------  h = x_{i−1} + α pi ---------------

        //* [5.7]  If h is accurate enough, then set xi = h and quit
        
        double temp7=0;

        for(size_t i = 0 ; i < ROW ;++i)
        {
            temp7 += std::abs(h[i] - xi[i]);
        }

        if (temp7 < zeta){
            xi.assign(h.begin(), h.end());
            break;
        }
        
        // * [5.7]  If h is accurate enough, then set xi = h and quit


        // * [5.8] ------------------ s = ri−1 − α * vi ------------------

        for(size_t i = 0 ; i < ROW ;++i)
        {
            si[i] = ri_pre.at(i) - value_Alpha * vi.at(i);
        }
        // * [5.8] ------------------ s = ri−1 − α * vi ------------------


        // * [5.9]-----------  t = As -----------
        ti = matA * si;
        // * [5.9]-----------  t = As -----------



        // * [5.10] --------------  ωi = (t, s)/(t, t) -------------- 
        
        double temp_10_1 = std::inner_product(ti.begin(), ti.end(),si.begin(),0.0L );
        double temp_10_2 = std::inner_product(ti.begin(), ti.end(),ti.begin(),0.0L );
        value_Omegai = temp_10_1 / temp_10_2;

        // * 5.10 --------------  ωi = (t, s)/(t, t) -------------- 


        // * 5.11  --------------  xi = h + ωi * s -------------- 

        for(size_t i = 0 ; i < ROW ;++i)
        {
            xi[i] = h[i] + value_Omegai * si[i];
        }

        // * 5.11  --------------  xi = h + ωi * s -------------- 


        // * 12. If xi is accurate enough, then quit

        std::vector<double > TEMP_12(xi_pre.size());
        std::transform(xi_pre.begin(), xi_pre.end(), xi.begin(), std::back_inserter(TEMP_12), 
                        [](double a, double b){return abs(a-b);});

        auto  Accumulate = std::accumulate(TEMP_12.begin(), TEMP_12.end(),0);
        if (Accumulate < zeta){
            break;
        }

        // * 12. If xi is accurate enough, then quit


        // * 13 -------------- ri = s − ω_i t --------------

        for(size_t i = 0 ; i < ROW ;++i)
        {
            ri[i] = si[i] - value_Omegai * ti[i];
        }

        // * 13 -------------- ri = s − ω_i t --------------
    } //  Bicg main loop end


    return xi;
}


int main(void){
    size_t ROW = 3;
    size_t COL = 3;
    mat::ELL_matrix<double> matA(3,3,3);


// | 5 8 -4 |   | 2  |   | -18|
// | 6 9 -5 | * | -3 | = | -20|
// | 4 7 -2 |   | 1  |   | -15|

    double A[ROW][COL] ={ 5,8,-4,
                          6,9,-5,
                          4,7,-2 };

    for (size_t i = 0 ; i < ROW ;++i ){
        for (size_t j = 0 ; j < COL ; ++j){ 
            matA.set(i,j,A[i][j]);
        }
    }
    cout  << "mat1:" << matA << endl;

    std::vector<double> B(ROW); 
    B[0] = -18; B[1] =-20.; B[2] = -15;

    auto temp = npc_bicgstab(B,matA);

    for (auto x : temp ){
        cout << x << ", ";
    }


    return 0;
}