// TODO: Base on EigneSolver

#define ELL_MPI_ON

#include "../inc/matrix/ELL_sparseMatrix.hpp" //ELL_matrix
#include "../inc/solver/mpi/bicgstab_mpi.hpp"
#include "../inc/solver/mpi/bicgstabRe2_mpi.hpp"
#include "../inc/solver/bicgstab.hpp"
#include "../inc/solver/bicgstabRe2.hpp"
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <omp.h>
#include <mpi.h>

// #include "eigen/Eigen/Eigen/"
// #include "stopWatch.hpp"

int main( int argc, char **argv)
{

    using value_type = double;
    using size_type = size_t;

    // typedef Eigen::Triplet<value_type> Triple_data;
    // std::vector<Triple_data> tripletList;

    size_type n = 10;

    // Eigen::SparseMatrix<value_type, Eigen::RowMajor> A(n,n); 
    // Eigen::VectorXd x(n), b(n);

    std::vector<value_type> bV(n,0);

    // ! ----- my matA
    mat::ELL_matrix<double> mat_ell_mpi(n,n,n);
    mat::ELL_matrix<double> mat_ell_omp(n,n,n);

    value_type val;
    value_type valB;


    //Computation
    MPI_Init(&argc, &argv);

    mat_ell_mpi.mpi_init(MPI_COMM_WORLD, n);



    int rank;
    
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    for(size_t i=0; i<n; ++i)
    {
        for(size_t j=0; j<n; ++j)
        {
            if(i!=j){
                val = i+j + 2*i + j;
                mat_ell_mpi.set(i,j,val);
                // A.insert(i,j) = val;
            }
        }
        valB = (i+1)^2;
        val  = (i+2) * 12;
        // b(i) = valB; 
        bV[i] = valB;
        // A.insert(i,i) = val;
        mat_ell_mpi.set(i,i,val);
    }



    if(rank == 0){

        for(size_t i=0; i<n; ++i)
        {
            for(size_t j=0; j<n; ++j)
            {
                if(i!=j){
                    val = i+j + 2*i + j;
                    mat_ell_omp.set(i,j,val);
                }
            }
            val  = (i+2) * 12;
            mat_ell_omp.set(i,i,val);
        }

    }

    solver::bicgstabRe2_mpi<mat::ELL_matrix<double> > solverMPI(mat_ell_mpi);
    solver::bicgstabRe2<mat::ELL_matrix<double> > solverOMP(mat_ell_omp);


    // std::cout << A << std::endl;
    
    // A.makeCompressed();
    
    // std::cout << A << std::endl;

    // std::cout << "\n===solver1===" << std::endl;
    
    // Eigen::BiCGSTAB<Eigen::SparseMatrix<value_type> > solver1;

    // solver1.compute(A);

    // solver1.setTolerance(1e-15);

    // x = solver1.solve(b);
    
    // if(rank == 0){
    //     std::cout << "#iterations:     " << solver1.iterations() << std::endl;
        
    //     std::cout << "estimated error: " << solver1.error()      << std::endl;
    // }


    std::vector<double> x_JM_in_mpi(n, 0);
    std::vector<double> x_JM_in_omp(n, 0);


    auto [iter_mpi, error_mpi] = solverMPI.solve(bV, x_JM_in_mpi);
    auto [iter_omp, error_omp] = solverOMP.solve(bV, x_JM_in_omp);

    // cout << std::endl;

    // MPI_Barrier(MPI_COMM_WORLD);

    if(rank == 0){

    //     std::cout << "===ANS_1===" << std::endl;
    //     for(auto v:x_JM){
    //         cout << v << ", ";
    //     }
    //     std::cout << "\n===ANS_1===" << std::endl;


    //     std::cout << "===ANS_2===" << std::endl;
    //     for(auto v:x){
    //         cout << v << ", ";
    //     }
    //     std::cout << "\n===ANS_2===" << std::endl;


    //     std::cout << "\n===ANS_3===" << std::endl;
    //     cout << error_seq << ", " << iter_seq << std::endl;
    //     for(auto v:x_JM_in){
    //         cout << v << ", ";
    //     }
    //     std::cout << "\n===ANS_3===" << std::endl;

        std::cout << "\n===CONTANIER_omp===" << std::endl;
        cout << error_omp << ", " << iter_omp << std::endl;
        for(auto v:x_JM_in_omp){
            cout << v << ", ";
        }

        std::cout << "\n===CONTANIER_MPI===" << std::endl;
        cout << error_mpi << ", " << iter_mpi << std::endl;
        for(auto v:x_JM_in_mpi){
            cout << v << ", ";
        }
    //     std::cout << "\n===CONTANIER_MPI===" << std::endl;

    //     double dif =0;
    //     for(size_t i = 0; i < n ; i++){
    //         dif += std::abs( x_JM.at(i) - x(i) );
    //     }

    //     cout << "\n dif :" << dif<< endl;

    }



    MPI_Barrier(MPI_COMM_WORLD);
    
    MPI_Finalize();

    return 0;

}