
#ifndef _ARRAY_
#define _ARRAY_

#include "Resolution.h"
#include "Variables.h"
#include <vector>

// using std::vector;
    


    using std::vector;
    
    //Physical matrix
    // ======================================================== //

    double (*p)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4];
    double (*pre)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4];

    //vector<vector<vector<double> > > u( nx+4, vector<vector<double> >(ny+4,vector<double> (nz+4)) );
    double (*u)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4];
    double (*v)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4];
    double (*w)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4];

    double (*u1)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4];
    double (*v1)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4];
    double (*w1)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4];

    double (*u2)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4];
    double (*v2)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4];
    double (*w2)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4];

    double (*u_star)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4];
    double (*v_star)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4];
    double (*w_star)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4];
    double (*ETA)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4];


    double (*FX)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4];
    double (*FY)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4];
    double (*FZ)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4];

    //double (*div1)[ny][nz] = new double[nx][ny][nz];

    
   
    // ======================================================== //

    
    //Grid matrix
    // ======================================================== //

    //Initial grid coordinates for evaluating grid lengths
    double (*X) = new double[nx+5];

    double (*Y) = new double[ny+5];

    double (*Z) = new double[nz+5];



    //Actual grid cooridinates (with adjusted index)
    double (*Xa) = new double[nx+1];

    double (*Ya) = new double[ny+1];

    double (*Za) = new double[nz+1];



    //Grid lengths
    double (*Dx) = new double[nx+4];

    double (*Dxs) = new double[nx+4];

    double (*Dy) = new double[ny+4];

    double (*Dys) = new double[ny+4];

    double (*Dz) = new double[nz+4];

    double (*Dzs) = new double[nz+4];


    //Midpoints of grid coordinate
    double (*Xc) = new double[nx];

    double (*Yc) = new double[ny];
    
    double (*Zc) = new double[nz];


    // ======================================================== //


    // ======================================================== //

    double (*L2norm) = new double[4];
    // ======================================================== //
 


#endif