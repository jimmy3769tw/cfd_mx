#ifndef _INITIALCONDITIONS_
#define _INITIALCONDITIONS_

#include "Resolution.h"

void InitialConditions(
    //vector<vector<vector<double> > > u,
    double (*u)[ny+4][nz+4],
    double (*v)[ny+4][nz+4],
    double (*w)[ny+4][nz+4],
    
    double (*u1)[ny+4][nz+4],
    double (*v1)[ny+4][nz+4],
    double (*w1)[ny+4][nz+4],

    double (*u2)[ny+4][nz+4],
    double (*v2)[ny+4][nz+4],
    double (*w2)[ny+4][nz+4],

    double (*u_star)[ny+4][nz+4],
    double (*v_star)[ny+4][nz+4],
    double (*w_star)[ny+4][nz+4],

    double (*ETA)[ny+4][nz+4],

    double (*p)[ny+4][nz+4]
    );

#endif