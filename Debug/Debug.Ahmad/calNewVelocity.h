#ifndef _CALNEWVELOCITY_INCLUDE_
#define _CALNEWVELOCITY_INCLUDE_



void calNewVelocity
    (
        double dt,

        double (*p)[ny+4][nz+4],
        double (*u_star)[ny+4][nz+4],
        double (*v_star)[ny+4][nz+4],
        double (*w_star)[ny+4][nz+4],
        double (*u1)[ny+4][nz+4],
        double (*v1)[ny+4][nz+4],
        double (*w1)[ny+4][nz+4],
        double (*u2)[ny+4][nz+4],
        double (*v2)[ny+4][nz+4],
        double (*w2)[ny+4][nz+4],
        double (*ETA)[ny+4][nz+4],
        double (*FX)[ny+4][nz+4],
        double (*FY)[ny+4][nz+4],
        double (*FZ)[ny+4][nz+4],

        double (*Dx),
        double (*Dxs),
        double (*Dy),
        double (*Dys),
        double (*Dz),
        double (*Dzs)

    
    );

void updating
    (
        double (*L2norm),
        double (*pre)[ny+4][nz+4],
        double (*p)[ny+4][nz+4],
        double (*u)[ny+4][nz+4],
        double (*v)[ny+4][nz+4],
        double (*w)[ny+4][nz+4],
        double (*u2)[ny+4][nz+4],
        double (*v2)[ny+4][nz+4],
        double (*w2)[ny+4][nz+4]




    );




#endif
