#ifndef _CONVECTIONSCHEME_INCLUDE_
#define _CONVECTIONSCHEME_INCLUDE_



void DiscretisationQUICK
    (
        double dt, double nu,

        double (*u)[ny+4][nz+4],
        double (*v)[ny+4][nz+4],
        double (*w)[ny+4][nz+4],

        double (*u_star)[ny+4][nz+4],
        double (*v_star)[ny+4][nz+4],
        double (*w_star)[ny+4][nz+4],

        double (*Dx),
        double (*Dxs),
        double (*Dy),
        double (*Dys),
        double (*Dz),
        double (*Dzs)

    
    );






#endif