#ifndef _PRESSURESOLVERS_INCLUDE_
#define _PRESSURESOLVERS_INCLUDE_



void SuccessiveOverRelaxation
    (
        double zeta, int itmax, double dt, double omega,
        double (*pre)[ny+4][nz+4],
        double (*p)[ny+4][nz+4],
        double (*u_star)[ny+4][nz+4],
        double (*v_star)[ny+4][nz+4],
        double (*w_star)[ny+4][nz+4],

        double (*Dx),
        double (*Dxs),
        double (*Dy),
        double (*Dys),
        double (*Dz),
        double (*Dzs)
    
    );





#endif