
#ifndef _GRIDDER_INCLUDED_
#define _GRIDDER_INCLUDED_

void gridder(double GridderXc, double GridderYc, double GridderZc, 
            double lxSml, double lySml, double lzSml, double dx,
            double dy, double dz, double dxSml, double dySml, double dzSml,
            char *Gridder,


            double (*X),
            double (*Y),
            double (*Z),

            double (*Dx),
            double (*Dxs),
            double (*Dy),
            double (*Dys),
            double (*Dz),
            double (*Dzs),

            double (*Xa),
            double (*Ya),
            double (*Za),

            double (*Xc),
            double (*Yc),
            double (*Zc)
             );


#endif

