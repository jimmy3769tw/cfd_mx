
#ifndef _READINGDATA_
#define _READINGDATA_

void readingData(double &dx, double &dy, double &dz, 
                 const double lx, const double ly, const double lz, 
                 const double lxSml, const double lySml, const double lzSml,
                 char *Gridder, 
                 const int nx, const int ny, const int nz,
                 const double nxSml, const double nySml, const double nzSml, 
                 double &dxSml, double &dySml, double &dzSml,
                 double Re, double &nu);

#endif
