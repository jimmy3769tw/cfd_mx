	// icpc -std=c++17 ELLcontanier_test.cpp -fopenmp -o ELL_Debug.exe
#include"../inc/matrix/ELL_sparseMatrix.hpp" //ELL_matrix
#include"../inc/0_6_9_CSR_Example.hpp"
// #include"../incELL_Example.hpp"

#include <iostream>
#include <vector>
#include <string>
#include <cmath>
#include <tuple> // cpp -11
#include <algorithm>
#include <iterator>
#include <numeric>
#include <functional>
#include <Eigen/Dense>

using namespace std;
using namespace Eigen;


int main(void){


    int ROW = 20;
    size_t n = ROW;

    std::vector<double> Amat_ve(ROW*ROW,0.0);

    Eigen::MatrixXd Amat_Eigen(ROW,ROW);
    Eigen::VectorXd B_Eigen(ROW);
    mat::csr_matrix<double> Amat_CSR(ROW,ROW);
    mat::ELL_matrix<double> Amat_ELL(ROW,ROW,ROW);
    mat::SparseMatrixELL<double> Amat_ELL_example(ROW,ROW,ROW);

    vector<double> B(ROW);

    
    for(size_t i=0; i<n; ++i)
    {
        for(size_t j=0; j<n; ++j)
        {
            if(i!=j){
                auto valA = i+j + 2*i + j;
                // cout << valA  <<", " ;
                Amat_CSR.set(valA,i,j);
                Amat_ELL.set(i,j,valA)  ;
                Amat_Eigen(i,j) = valA;
                Amat_ELL_example.set(i,j,valA);
                Amat_ve.at(ROW*i+j)=valA;
            }

        }
        B.at(i) = (i+1)^2+3;
        B_Eigen(i) = (i+1)^2+3;

        auto valB = (i+2) * 12;
        // cout <<valB<< ", ";

        Amat_ELL.set(i,i,valB)  ;
        Amat_CSR.set(valB, i, i)  ;
        Amat_ELL_example.set(i,i,valB);
        Amat_Eigen(i,i) = valB;
        Amat_ve.at(ROW*i+i)=valB;
    }


    // cout << "\nAmat_Eigen\n" << Amat_Eigen << endl;
    // cout << "\nAmat_CSR" << Amat_CSR;
    // cout << "\nAmat_ELL" << Amat_ELL<< endl;

    Amat_ELL_example.ELL2CSR();
    auto [prt,idx, val] = Amat_ELL_example.data();
    auto [prt_,idx_, val_] = Amat_ELL.get_CSR();



    // for(size_t i = 0; i < ROW ; i++){
    //     for(size_t j = 0; j < ROW ; j++){
    //         q.at(i) += Amat_ve[ROW*i+j] * B.at(j);
    //     }
    // }

    bool result = std::equal(prt.begin(), prt.end(), prt_.begin());

    if (result){
        cout << "\nprt are equal !!!\n";
    }
    cout << "\nprt.size() " << prt.size() << ", " << prt_.size() ; 

    result = std::equal(idx_.begin(), idx_.end(), idx.begin());
    if (result){
        cout << "\nidx are equal !!!\n";
    }

    cout << "idx.size() " << idx.size() << ", " << idx_.size() ; 

    result = std::equal(val_.begin(), val_.end(), val.begin());
    if (result){
        cout << "\nval are equal !!!\n";
    }

    cout << "\nval.size() " << val.size() << ", " << val_.size() ; 





    std::vector<double> q(ROW,0);


    // Amat_ELL.Show_ELL();
    // Amat_ELL.sort_Idx();
    // Amat_ELL.Show_ELL();
    // Show_ELL
    auto j = Amat_ELL.get_CSR();

    // cout << endl << "---------  B --------- \n";

    // for (auto v:B){
    //     cout << v << ", ";
    // }

    // cout << endl << "---------  Eigen --------- \n";

    // cout << Amat_Eigen * B_Eigen << endl;


    // cout << endl << "---------  Amat_CSR --------- \n";


    auto d = Amat_CSR * B;
    // for (auto v :d){
        // cout << v << ", ";
    // }
// 
    // cout << endl << "---------  Amat_ELL  --------- \n";
// 
    auto c = Amat_ELL * B;

    // for (auto v :c){
        // cout << v << ", ";
    // }

    // cout << endl << "---------  vector --------- \n";
    // for (auto v :q){
        // cout << v << ", ";
    // }



    for (size_t i = 0 ; i < ROW ; i++){
        if ( d.at(i) != c.at(i) ){
            // cout << d.at(i) << ", " << c.at(i);
        }
    }


    cout << endl;
    return 0;
}