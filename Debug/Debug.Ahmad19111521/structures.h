/**
 * @file structures.h
 * 
 * @project     3D N-S solver rebuild
 * @version     1.6
 * 
 * @author      Syed Ahmad Raza (sahmadrz@gmail.com)
 * @date        2019-11-06
 * 
 * @brief       Create the structures by defining ETA throughout the grid
 * 
 */

void volumeOfSolid();