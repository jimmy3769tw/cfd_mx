/**
 * @file        main.cpp
 * 
 * @project     3D N-S solver rebuild
 * @version     1.6
 * 
 * @author      Syed Ahmad Raza (sahmadrz@gmail.com)
 * @date        2019-11-05
 * 
 * @brief       The main solver file outlining the algorithm of the Navier-
 *              Stokes solver.
 * 
 * @detail      Rebuilding the 3D N-S solver is in progress. The current version
 *              is a fully working 3D N-S solver for uniform grid with
 *              parallelization using openMP and adaptive timestepping,
 *              utilizing BCGSTAB method for pressure Poisson equation. Data
 *              structures from the Eigen library are utilized.
 * 
 * @command     g++ -std=c++17 -Wall -O3 -funroll-loops -mfpmath=sse -D_GLIBCXX_PARALLEL -fopenmp -march=native main.cpp gridders.cpp structures.cpp conditions.cpp pressureSolvers.cpp timestepper.cpp updaters.cpp velocitySchemers.cpp filers.cpp printers.cpp -o testing && ./testing
 * 
 */

#include <dirent.h>             // opendir to check that directory exists
#include <iostream>
#include <ctime>                // to time the script
#include <chrono>               // to measure and display the time duration
#include "parameters.h"
#include "gridders.h"
#include "structures.h"
#include "conditions.h"
#include "timestepper.h"
#include "velocitySchemers.h"
#include "pressureSolvers.h"
#include "updaters.h"
#include "filers.h"
#include "printers.h"
#define OMP_ON
#include "controlPanel.hpp"
#include "run/general.hpp"
#include "matrix/CSR_sparseMatrix.hpp"

// ---------------

int
    timestep = 0, pIter = 0, pIterTotal = 0, pRestarts = 0;
double
    dt = 0.0, simTime = 0.0, runTime = 0.0,
    mChangeMax = 0.0, pChangeMax = 0.0,
    uChangeMax = 1.0, vChangeMax = 1.0, wChangeMax = 1.0;
Eigen::Matrix<double, -1, 1>
    X(nx+1), Y(ny+1), Z(nz+1),
    DX(nxt), DY(nyt), DZ(nzt), DXM(nxt-1), DYM(nyt-1), DZM(nzt-1),
    PB(ndim), PX(ndim);
Eigen::Matrix<double, -1, -1>
    PA(ndim, ddim);
Eigen::TensorFixedSize<double, Eigen::Sizes<nxt, nyt, nzt>>
    U, V, W, US, VS, WS, P, ETA, FX, FY, FZ;


Eigen::Matrix<double, -1, 1>
    testout(ndim),  testin(ndim);

int main()
{

// --------------------------quick


    cout << "Debug main_quick scheme \n";

    // ! --------------------
    // * CFD_MX_struct
    clockstruct timer;
    simuClass simu;
    grid gridA;

    // *   parameter  ------------
    simu.set_time_max(1000);

    simu.set_dt(1.0e-3);

    simu.set_Re(100.0);

    simu.init_fileStep(1.0);
    // *   parameter  ------------


    // *   Accuracy  ------------
    gridA.Gridder                   =   "uniform" ;

    simu.Locality = 1;

    //  * struct ---------------------------
    ImmersedBoundary Dfib;

    pressure t1;

    velocity T0 , T1, T3;

    SORcoefficient Sor;

    shareMenory ShareM;

    MxClass Mx;

    //  * struct ---------------------------
    // *         ============================  divid Domain ============================


    calDomain Lo;

    std::vector<int> grid_size{gridA.nx, gridA.ny, gridA.nz};

    std::vector<int> dims{1, 1, 1};

    Lo.initTable(grid_size, dims);

    Lo.initLocal(0, 0, 0);

    // *         ============================  divid Domain ============================


    resize_variable(gridA, simu, t1, T0, T1, T3, Dfib); // ! resize shared memory

#if defined (PC_SEQ)  
    T0.iniU_omp(0.0, 0.0, 0.0);

    T1.iniU_omp(0.0, 0.0, 0.0);

    T3.iniU_omp(0.0, 0.0, 0.0);

#elif defined (PC_OMP)
    T0.iniU_omp(0.0, 0.0, 0.0);

    T1.iniU_omp(0.0, 0.0, 0.0);

    T3.iniU_omp(0.0, 0.0, 0.0);
#endif


  t1.init_p(0.0);


    generateGride(simu, ShareM, Lo, gridA);
    gridA.io_csv();

// * make the inter face 

// * make the inter face 



//-----------------------


    if(opendir("../data") == NULL)
    { if (system("mkdir ../data") != 0){ return 1; } }

    #ifdef _OPENMP
    omp_set_num_threads(numOfThreads);
    #endif

    filerInfoStart();

    std::chrono::steady_clock::time_point tStart
        = std::chrono::steady_clock::now();

    gridder();
    if (solidPos == "fixed") { volumeOfSolid(); }
    // ETA.setZero();
    initialConditions();
    boundaryConditions();
    std::fill(Dfib.eta.begin(), Dfib.eta.end(), 0.0);
    std::fill(Dfib.f.begin(), Dfib.f.end(), 0.0);
    
    //     // * ---------------- Ahmad ----------------
    // pressureMatrixConstructor();        // PA
    //     // * ---------------- Ahmad ----------------

        // * ---------------- csr ----------------
    mat::CSR_matrix<double> mat( pressureMatrixConstructor_CSR() );
        // * ---------------- csr ----------------

    std::vector<double> X_result(ndim, 0.0), rhs(ndim, 0.0), x_in(ndim, 1.), x_out(ndim );

    std::cout << "finished.";

    srand (time(NULL));

    for (int i = 0; i < ndim ; ++i){
        testin(i)  = i*(rand() % 10 +1 );
        if (testin(i) != 0.0 ){
            testin(i)  = i*(rand() % 10 +1 );
        }
        x_in.at(i) = testin(i);
    }

    multiplyMatrixVector(testout, PA, testin);


    auto summm = 0.;

    // auto  multiplyMatrixVector_csr = [&](auto a, auto b, auto c){ mat.multiply(c, a);};  //!bug!
    auto  multiplyMatrixVector_csr = [&](auto &a, auto &b, auto &c){ mat.multiply(c, a);};
    multiplyMatrixVector_csr(x_out, PA, x_in);

    // mat.multiply(x_in, x_out);

    if(x_out.size() != ndim){std::cout << "efjegfoiewjogi";}

    for(int i=0; i<ndim; ++i){
        summm += std::abs( x_out[i] - testout(i) );     
    }

    std::cout << "sum:: " << summm << std::endl;
        std::cout << "x_out" ;



    while(timestep < timestepMax 
                                    // && (uChangeMax > uResidual
                                    //  || vChangeMax > vResidual
                                    //  || wChangeMax > wResidual)
                                     )
    {
        mChangeMax = 0.0;
        pChangeMax = 0.0;
        uChangeMax = 0.0;
        vChangeMax = 0.0;
        wChangeMax = 0.0;

        // timestepper();
        
        dt = 1.0e-3;
        ++timestep;
        simTime += dt;

        for (int k=0; k<nzt; ++k)
        {
            for (int j=0; j<nyt; ++j)
            {
                for (int i=0; i<nxt; ++i)
                {
                    T0.u.at(gridA.icel(i,j,k)) = U(i,j,k);
                    T0.v.at(gridA.icel(i,j,k)) = V(i,j,k);
                    T0.w.at(gridA.icel(i,j,k)) = W(i,j,k);
                }
            }
        }

        // if (velScheme == "quick") { velocitySchemer(quickU, quickV, quickW); }

        ConvectionDifussion(simu, T0, T1, Lo, gridA);

        for (int k=gc; k<nzt-gc; ++k)
        {
            for (int j=gc; j<nyt-gc; ++j)
            {
                for (int i=gc; i<nxt-gc-1; ++i)
                {
                    US(i,j,k) = T1.u.at(gridA.icel(i,j,k));
                }
            }
        }

        for (int k=gc; k<nzt-gc; ++k)
        {
            for (int j=gc; j<nyt-gc-1; ++j)
            {
                for (int i=gc; i<nxt-gc; ++i)
                {
                    VS(i,j,k) = T1.v.at(gridA.icel(i,j,k));
                }
            }
        }


        for (int k=gc; k<nzt-gc-1; ++k)
        {
            for (int j=gc; j<nyt-gc; ++j)
            {
                for (int i=gc; i<nxt-gc; ++i)
                {
                    WS(i,j,k) = T1.w.at(gridA.icel(i,j,k));
                }
            }
        }

        // * ---------------- csr ----------------
        pressureVectorConstructor_vector(rhs); 
        auto [iters, error] = mat.BiCGSTAB(rhs, X_result);
        // bCGSTAB_vector(rhs, X_result);
        // auto [iters, error] = mat.npc_bicgstab(rhs, X_result);
        std::cout << "iter " << iters << "error " << error << std::endl;
        pressureUpdater_vector(X_result);
        // * ---------------- csr ----------------



        // // * ---------------- Ahmad ----------------
        // pressureVectorConstructor();                                // PB <- US
        // bCGSTAB();                                                  // PX
        // pressureUpdater();                                          // P = PX
        // // * ---------------- Ahmad ----------------
        
        // double summ = 0.;

        // for (int i=0; i<nx; ++i)
        // {
        //     for (int j=0; j<ny; ++j)
        //     {
        //         for (int k=0; k<nz; ++k)
        //         {
        //             int l = k + j*nz + i*ny*nz;

        //             double b = std::abs(PX(l) - X_result.at(l));
        //             summ += b;
        //         }
        //     }
        // }
        // std::cout << "summ(X_result) " << summ << std::endl;

        int dd = 0;
        for (int i=dd; i<nxt-dd; ++i)
        {
            for (int j=dd; j<nyt-dd; ++j)
            {
                for (int k=dd; k<nzt-dd; ++k)
                {
                    t1.p[gridA.icel(i,j,k)] = P(i,j,k);
                }
            }
        }

        update_UandF_seq(Dfib, simu, t1, T1, T3, Lo, gridA);


        velocityUpdater();                                          // U = US
        
        double summ = 0.;

        auto d = 2;
        for (int i=d; i<nxt-d-1; ++i)
        {
            for (int j=d; j<nyt-d; ++j)
            {
                for (int k=d; k<nzt-d; ++k)
                {

                    double b = std::abs(T3.u[gridA.icel(i,j,k)] - U(i,j,k));
                    summ += b;
                    if (b != 0.)
                    std::cout << "[i,j,k] = " 
                    << i << "," << j << ", " << k << ": "
                    << b << std::endl;

                }
            }
        }
        std::cout << "sim [u]" << summ << std::endl;

        summ = 0.;
        for (int i=d; i<nxt-d; ++i)
        {
            for (int j=d; j<nyt-d-1; ++j)
            {
                for (int k=d; k<nzt-d; ++k)
                {

                    double b = std::abs(T3.v[gridA.icel(i,j,k)] - V(i,j,k));
                    summ += b;
                }
            }
        }
        std::cout << "sim [v]" << summ << std::endl;


        summ = 0.;
        for (int i=d; i<nxt-d; ++i)
        {
            for (int j=d; j<nyt-d; ++j)
            {
                for (int k=d; k<nzt-d-3; ++k)
                {

                    double b = std::abs(T3.w[gridA.icel(i,j,k)] - W(i,j,k));
                    summ += b;
                }
            }
        }
        std::cout << "sim [w]" << summ << std::endl;


        std::cout << "summ(X_result) " << summ << std::endl;

        boundaryConditions();

        if (timestep % filerSteps == 0) { filerSolution(timestep); }

        printerProgress();
    }

    std::chrono::steady_clock::time_point tEnd
        = std::chrono::steady_clock::now();
    runTime
        = std::chrono::duration_cast<std::chrono::seconds>(tEnd-tStart).count();

    filerSolution();
    filerInfoEnd();
    printerInfoEnd();

    return 0;
}
