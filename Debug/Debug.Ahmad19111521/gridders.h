/**
 * @file        gridders.h
 * 
 * @project     3D N-S solver rebuild
 * @version     1.6
 * 
 * @author      Syed Ahmad Raza (sahmadrz@gmail.com)
 * @date        2019-11-05
 * 
 * @brief       Create the grid for Navier-Stokes solver.
 * 
 */

#ifndef GRIDDERS_H
#define GRIDDERS_H

void gridder();

#endif