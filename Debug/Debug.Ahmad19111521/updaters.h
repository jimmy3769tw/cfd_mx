/**
 * @file        updaters.cpp
 * 
 * @project     3D N-S solver rebuild
 * @version     1.6
 * 
 * @author      Syed Ahmad Raza (sahmadrz@gmail.com)
 * @date        2019-11-05
 * 
 * @brief       Update the pressure and velocity values as per the calculations
 *              in the current timestep of the Navier-Stokes solver.
 * 
 */
#include<vector>

#ifndef UPDATERS_H
#define UPDATERS_H

void pressureUpdater();
void velocityUpdater();
void pressureUpdater_vector(std::vector<double> &X_result);

#endif