/**
 * @file        velocitySchemers.cpp
 * 
 * @project     3D N-S solver rebuild
 * @version     1.6
 * 
 * @author      Syed Ahmad Raza (sahmadrz@gmail.com)
 * @date        2019-11-05
 * 
 * @brief       Apply the selected velocity scheme for the Navier-Stokes solver.
 * 
 */

#include "velocitySchemers.h"
#include "parameters.h"
#include <omp.h>
#include <string>

using Eigen::TensorFixedSize, Eigen::Sizes;

void centralSchemeU(
    double& ue, double& uw, double& un, double& us, double& uf, double& ub,
    double& ute, double& utw,
    double& vtn, double& vts,
    double& wtf, double& wtb,
    TensorFixedSize<double, Sizes<nxt, nyt, nzt>>& U,
    TensorFixedSize<double, Sizes<nxt, nyt, nzt>>& V,
    TensorFixedSize<double, Sizes<nxt, nyt, nzt>>& W,
    int& i, int& j, int& k)
{
    ute = 0.5*( U(i,j,k)   + U(i+1,j,k)   );
    utw = 0.5*( U(i-1,j,k) + U(i,j,k)     );
    vtn = 0.5*( V(i,j,k)   + V(i+1,j,k)   );
    vts = 0.5*( V(i,j-1,k) + V(i+1,j-1,k) );
    wtf = 0.5*( W(i,j,k)   + W(i+1,j,k)   );
    wtb = 0.5*( W(i,j,k-1) + W(i+1,j,k-1) );

    ue  = 0.5*( U(i,j,k)   + U(i+1,j,k)   );
    uw  = 0.5*( U(i-1,j,k) + U(i,j,k)     );
    un  = 0.5*( U(i,j,k)   + U(i,j+1,k)   );
    us  = 0.5*( U(i,j-1,k) + U(i,j,k)     );
    uf  = 0.5*( U(i,j,k)   + U(i,j,k+1)   );
    ub  = 0.5*( U(i,j,k-1) + U(i,j,k)     );
}

void centralSchemeV(
    double& ve, double& vw, double& vn, double& vs, double& vf, double& vb,
    double& ute, double& utw,
    double& vtn, double& vts,
    double& wtf, double& wtb,
    TensorFixedSize<double, Sizes<nxt, nyt, nzt>>& U,
    TensorFixedSize<double, Sizes<nxt, nyt, nzt>>& V,
    TensorFixedSize<double, Sizes<nxt, nyt, nzt>>& W,
    int& i, int& j, int& k)
{
    ute = 0.5*( U(i,j,k)   + U(i,j+1,k)   );
    utw = 0.5*( U(i-1,j,k) + U(i-1,j+1,k) );
    vtn = 0.5*( V(i,j,k)   + V(i,j+1,k)   );
    vts = 0.5*( V(i,j-1,k) + V(i,j,k)     );
    wtf = 0.5*( W(i,j,k)   + W(i,j+1,k)   );
    wtb = 0.5*( W(i,j,k-1) + W(i,j+1,k-1) );

    ve  = 0.5*( V(i,j,k)   + V(i+1,j,k)   );
    vw  = 0.5*( V(i-1,j,k) + V(i,j,k)     );
    vn  = 0.5*( V(i,j,k)   + V(i,j+1,k)   );
    vs  = 0.5*( V(i,j-1,k) + V(i,j,k)     );
    vf  = 0.5*( V(i,j,k)   + V(i,j,k+1)   );
    vb  = 0.5*( V(i,j,k-1) + V(i,j,k)     );
}

void centralSchemeW(
    double& we, double& ww, double& wn, double& ws, double& wf, double& wb,
    double& ute, double& utw,
    double& vtn, double& vts,
    double& wtf, double& wtb,
    TensorFixedSize<double, Sizes<nxt, nyt, nzt>>& U,
    TensorFixedSize<double, Sizes<nxt, nyt, nzt>>& V,
    TensorFixedSize<double, Sizes<nxt, nyt, nzt>>& W,
    int& i, int& j, int& k)
{
    ute = 0.5*( U(i,j,k)   + U(i,j,k+1)   );
    utw = 0.5*( U(i-1,j,k) + U(i-1,j,k+1) );
    vtn = 0.5*( V(i,j,k)   + V(i,j,k+1)   );
    vts = 0.5*( V(i,j-1,k) + V(i,j-1,k+1) );
    wtf = 0.5*( W(i,j,k)   + W(i,j,k+1)   );
    wtb = 0.5*( W(i,j,k-1) + W(i,j,k)     );

    we  = 0.5*( W(i,j,k)   + W(i+1,j,k)   );
    ww  = 0.5*( W(i-1,j,k) + W(i,j,k)     );
    wn  = 0.5*( W(i,j,k)   + W(i,j+1,k)   );
    ws  = 0.5*( W(i,j-1,k) + W(i,j,k)     );
    wf  = 0.5*( W(i,j,k)   + W(i,j,k+1)   );
    wb  = 0.5*( W(i,j,k-1) + W(i,j,k)     );
}

void upwindSchemeU(
    double& ue, double& uw, double& un, double& us, double& uf, double& ub,
    double& ute, double& utw,
    double& vtn, double& vts,
    double& wtf, double& wtb,
    TensorFixedSize<double, Sizes<nxt, nyt, nzt>>& U,
    TensorFixedSize<double, Sizes<nxt, nyt, nzt>>& V,
    TensorFixedSize<double, Sizes<nxt, nyt, nzt>>& W,
    int& i, int& j, int& k)
{
    ute = 0.5*( U(i,j,k)   + U(i+1,j,k)   );
    utw = 0.5*( U(i-1,j,k) + U(i,j,k)     );
    vtn = 0.5*( V(i,j,k)   + V(i+1,j,k)   );
    vts = 0.5*( V(i,j-1,k) + V(i+1,j-1,k) );
    wtf = 0.5*( W(i,j,k)   + W(i+1,j,k)   );
    wtb = 0.5*( W(i,j,k-1) + W(i+1,j,k-1) );

    if (ute >= 0){ ue = U(i,j,k);   }
    else         { ue = U(i+1,j,k); }

    if (utw >= 0){ uw = U(i-1,j,k); }
    else         { uw = U(i,j,k);   }

    if (vtn >= 0){ un = U(i,j,k);   }
    else         { un = U(i,j+1,k); }

    if (vts >= 0){ us = U(i,j-1,k); }
    else         { us = U(i,j,k);   }

    if (wtf >= 0){ uf = U(i,j,k);   }
    else         { uf = U(i,j,k+1); }

    if (wtb >= 0){ ub = U(i,j,k-1); }
    else         { ub = U(i,j,k);   }
}

void upwindSchemeV(
    double& ve, double& vw, double& vn, double& vs, double& vf, double& vb,
    double& ute, double& utw,
    double& vtn, double& vts,
    double& wtf, double& wtb,
    TensorFixedSize<double, Sizes<nxt, nyt, nzt>>& U,
    TensorFixedSize<double, Sizes<nxt, nyt, nzt>>& V,
    TensorFixedSize<double, Sizes<nxt, nyt, nzt>>& W,
    int& i, int& j, int& k)
{
    ute = 0.5*( U(i,j,k)   + U(i,j+1,k)   );
    utw = 0.5*( U(i-1,j,k) + U(i-1,j+1,k) );
    vtn = 0.5*( V(i,j,k)   + V(i,j+1,k)   );
    vts = 0.5*( V(i,j-1,k) + V(i,j,k)     );
    wtf = 0.5*( W(i,j,k)   + W(i,j+1,k)   );
    wtb = 0.5*( W(i,j,k-1) + W(i,j+1,k-1) );

    if (ute >= 0){ ve = V(i,j,k);   }
    else         { ve = V(i+1,j,k); }

    if (utw >= 0){ vw = V(i-1,j,k); }
    else         { vw = V(i,j,k);   }

    if (vtn >= 0){ vn = V(i,j,k);   }
    else         { vn = V(i,j+1,k); }

    if (vts >= 0){ vs = V(i,j-1,k); }
    else         { vs = V(i,j,k);   }

    if (wtf >= 0){ vf = V(i,j,k);   }
    else         { vf = V(i,j,k+1); }

    if (wtb >= 0){ vb = V(i,j,k-1); }
    else         { vb = V(i,j,k);   }
}

void upwindSchemeW(
    double& we, double& ww, double& wn, double& ws, double& wf, double& wb,
    double& ute, double& utw,
    double& vtn, double& vts,
    double& wtf, double& wtb,
    TensorFixedSize<double, Sizes<nxt, nyt, nzt>>& U,
    TensorFixedSize<double, Sizes<nxt, nyt, nzt>>& V,
    TensorFixedSize<double, Sizes<nxt, nyt, nzt>>& W,
    int& i, int& j, int& k)
{
    ute = 0.5*( U(i,j,k)   + U(i,j,k+1)   );
    utw = 0.5*( U(i-1,j,k) + U(i-1,j,k+1) );
    vtn = 0.5*( V(i,j,k)   + V(i,j,k+1)   );
    vts = 0.5*( V(i,j-1,k) + V(i,j-1,k+1) );
    wtf = 0.5*( W(i,j,k)   + W(i,j,k+1)   );
    wtb = 0.5*( W(i,j,k-1) + W(i,j,k)     );

    if (ute >= 0){ we = W(i,j,k);   }
    else         { we = W(i+1,j,k); }

    if (utw >= 0){ ww = W(i-1,j,k); }
    else         { ww = W(i,j,k);   }

    if (vtn >= 0){ wn = W(i,j,k);   }
    else         { wn = W(i,j+1,k); }

    if (vts >= 0){ ws = W(i,j-1,k); }
    else         { ws = W(i,j,k);   }

    if (wtf >= 0){ wf = W(i,j,k);   }
    else         { wf = W(i,j,k+1); }

    if (wtb >= 0){ wb = W(i,j,k-1); }
    else         { wb = W(i,j,k);   }
}

void quickSchemeU(
    double& ue, double& uw, double& un, double& us, double& uf, double& ub,
    double& ute, double& utw,
    double& vtn, double& vts,
    double& wtf, double& wtb,
    TensorFixedSize<double, Sizes<nxt, nyt, nzt>>& U,
    TensorFixedSize<double, Sizes<nxt, nyt, nzt>>& V,
    TensorFixedSize<double, Sizes<nxt, nyt, nzt>>& W,
    int& i, int& j, int& k)
{
    ute = 0.5*( U(i,j,k)   + U(i+1,j,k)   );
    utw = 0.5*( U(i-1,j,k) + U(i,j,k)     );
    vtn = 0.5*( V(i,j,k)   + V(i+1,j,k)   );
    vts = 0.5*( V(i,j-1,k) + V(i+1,j-1,k) );
    wtf = 0.5*( W(i,j,k)   + W(i+1,j,k)   );
    wtb = 0.5*( W(i,j,k-1) + W(i+1,j,k-1) );

    if (ute >= 0){ ue = 0.75*U(i,j,k)   + 0.375*U(i+1,j,k) - 0.125*U(i-1,j,k); }
    else         { ue = 0.75*U(i+1,j,k) + 0.375*U(i,j,k)   - 0.125*U(i+2,j,k); }

    if (utw >= 0){ uw = 0.75*U(i-1,j,k) + 0.375*U(i,j,k)   - 0.125*U(i-2,j,k); }
    else         { uw = 0.75*U(i,j,k)   + 0.375*U(i-1,j,k) - 0.125*U(i+1,j,k); }

    if (vtn >= 0){ un = 0.75*U(i,j,k)   + 0.375*U(i,j+1,k) - 0.125*U(i,j-1,k); }
    else         { un = 0.75*U(i,j+1,k) + 0.375*U(i,j,k)   - 0.125*U(i,j+2,k); }

    if (vts >= 0){ us = 0.75*U(i,j-1,k) + 0.375*U(i,j,k)   - 0.125*U(i,j-2,k); }
    else         { us = 0.75*U(i,j,k)   + 0.375*U(i,j-1,k) - 0.125*U(i,j+1,k); }

    if (wtf >= 0){ uf = 0.75*U(i,j,k)   + 0.375*U(i,j,k+1) - 0.125*U(i,j,k-1); }
    else         { uf = 0.75*U(i,j,k+1) + 0.375*U(i,j,k)   - 0.125*U(i,j,k+2); }

    if (wtb >= 0){ ub = 0.75*U(i,j,k-1) + 0.375*U(i,j,k)   - 0.125*U(i,j,k-2); }
    else         { ub = 0.75*U(i,j,k)   + 0.375*U(i,j,k-1) - 0.125*U(i,j,k+1); }
}

void quickSchemeV(
    double& ve, double& vw, double& vn, double& vs, double& vf, double& vb,
    double& ute, double& utw,
    double& vtn, double& vts,
    double& wtf, double& wtb,
    TensorFixedSize<double, Sizes<nxt, nyt, nzt>>& U,
    TensorFixedSize<double, Sizes<nxt, nyt, nzt>>& V,
    TensorFixedSize<double, Sizes<nxt, nyt, nzt>>& W,
    int& i, int& j, int& k)
{
    ute = 0.5*( U(i,j,k)   + U(i,j+1,k)   );
    utw = 0.5*( U(i-1,j,k) + U(i-1,j+1,k) );
    vtn = 0.5*( V(i,j,k)   + V(i,j+1,k)   );
    vts = 0.5*( V(i,j-1,k) + V(i,j,k)     );
    wtf = 0.5*( W(i,j,k)   + W(i,j+1,k)   );
    wtb = 0.5*( W(i,j,k-1) + W(i,j+1,k-1) );

    if (ute >= 0){ ve = 0.75*V(i,j,k)   + 0.375*V(i+1,j,k) - 0.125*V(i-1,j,k); }
    else         { ve = 0.75*V(i+1,j,k) + 0.375*V(i,j,k)   - 0.125*V(i+2,j,k); }

    if (utw >= 0){ vw = 0.75*V(i-1,j,k) + 0.375*V(i,j,k)   - 0.125*V(i-2,j,k); }
    else         { vw = 0.75*V(i,j,k)   + 0.375*V(i-1,j,k) - 0.125*V(i+1,j,k); }

    if (vtn >= 0){ vn = 0.75*V(i,j,k)   + 0.375*V(i,j+1,k) - 0.125*V(i,j-1,k); }
    else         { vn = 0.75*V(i,j+1,k) + 0.375*V(i,j,k)   - 0.125*V(i,j+2,k); }

    if (vts >= 0){ vs = 0.75*V(i,j-1,k) + 0.375*V(i,j,k)   - 0.125*V(i,j-2,k); }
    else         { vs = 0.75*V(i,j,k)   + 0.375*V(i,j-1,k) - 0.125*V(i,j+1,k); }

    if (wtf >= 0){ vf = 0.75*V(i,j,k)   + 0.375*V(i,j,k+1) - 0.125*V(i,j,k-1); }
    else         { vf = 0.75*V(i,j,k+1) + 0.375*V(i,j,k)   - 0.125*V(i,j,k+2); }

    if (wtb >= 0){ vb = 0.75*V(i,j,k-1) + 0.375*V(i,j,k)   - 0.125*V(i,j,k-2); }
    else         { vb = 0.75*V(i,j,k)   + 0.375*V(i,j,k-1) - 0.125*V(i,j,k+1); }
}

void quickSchemeW(
    double& we, double& ww, double& wn, double& ws, double& wf, double& wb,
    double& ute, double& utw,
    double& vtn, double& vts,
    double& wtf, double& wtb,
    TensorFixedSize<double, Sizes<nxt, nyt, nzt>>& U,
    TensorFixedSize<double, Sizes<nxt, nyt, nzt>>& V,
    TensorFixedSize<double, Sizes<nxt, nyt, nzt>>& W,
    int& i, int& j, int& k)
{
    ute = 0.5*( U(i,j,k)   + U(i,j,k+1)   );
    utw = 0.5*( U(i-1,j,k) + U(i-1,j,k+1) );
    vtn = 0.5*( V(i,j,k)   + V(i,j,k+1)   );
    vts = 0.5*( V(i,j-1,k) + V(i,j-1,k+1) );
    wtf = 0.5*( W(i,j,k)   + W(i,j,k+1)   );
    wtb = 0.5*( W(i,j,k-1) + W(i,j,k)     );

    if (ute >= 0){ we = 0.75*W(i,j,k)   + 0.375*W(i+1,j,k) - 0.125*W(i-1,j,k); }
    else         { we = 0.75*W(i+1,j,k) + 0.375*W(i,j,k)   - 0.125*W(i+2,j,k); }

    if (utw >= 0){ ww = 0.75*W(i-1,j,k) + 0.375*W(i,j,k)   - 0.125*W(i-2,j,k); }
    else         { ww = 0.75*W(i,j,k)   + 0.375*W(i-1,j,k) - 0.125*W(i+1,j,k); }

    if (vtn >= 0){ wn = 0.75*W(i,j,k)   + 0.375*W(i,j+1,k) - 0.125*W(i,j-1,k); }
    else         { wn = 0.75*W(i,j+1,k) + 0.375*W(i,j,k)   - 0.125*W(i,j+2,k); }

    if (vts >= 0){ ws = 0.75*W(i,j-1,k) + 0.375*W(i,j,k)   - 0.125*W(i,j-2,k); }
    else         { ws = 0.75*W(i,j,k)   + 0.375*W(i,j-1,k) - 0.125*W(i,j+1,k); }

    if (wtf >= 0){ wf = 0.75*W(i,j,k)   + 0.375*W(i,j,k+1) - 0.125*W(i,j,k-1); }
    else         { wf = 0.75*W(i,j,k+1) + 0.375*W(i,j,k)   - 0.125*W(i,j,k+2); }

    if (wtb >= 0){ wb = 0.75*W(i,j,k-1) + 0.375*W(i,j,k)   - 0.125*W(i,j,k-2); }
    else         { wb = 0.75*W(i,j,k)   + 0.375*W(i,j,k-1) - 0.125*W(i,j,k+1); }
}

schemer centralU = &centralSchemeU;
schemer centralV = &centralSchemeV;
schemer centralW = &centralSchemeW;
schemer upwindU  = &upwindSchemeU;
schemer upwindV  = &upwindSchemeV;
schemer upwindW  = &upwindSchemeW;
schemer quickU   = &quickSchemeU;
schemer quickV   = &quickSchemeV;
schemer quickW   = &quickSchemeW;

void velocitySchemer(schemer schemeU, schemer schemeV, schemer schemeW)
{
    double nu = 1.0/Re;
    std::string vScheme = velScheme;

    // x-direction velocity
    #pragma omp parallel for default(none)\
            shared(US, U, V, W, schemeU, dt, nu)
    for (int k=gc; k<nzt-gc; ++k)
    {
        for (int j=gc; j<nyt-gc; ++j)
        {
            for (int i=gc; i<nxt-gc-1; ++i)
            {
                double ue, uw, un, us, uf, ub, ute, utw, vtn, vts, wtf, wtb;
                ue = uw = un = us = uf = ub = ute = utw = vtn = vts = wtf = wtb
                = 0.0;

                schemeU(
                    ue, uw, un, us, uf, ub, ute, utw, vtn, vts, wtf, wtb,
                    U, V, W, i, j, k);
                US(i,j,k) = U(i,j,k) +dt
                *(
                    - ( ue*ute - uw*utw )*ddx
                    - ( un*vtn - us*vts )*ddy
                    - ( uf*wtf - ub*wtb )*ddz
                    + nu*( U(i-1,j,k) - 2*U(i,j,k) + U(i+1,j,k) )*ddx*ddx
                    + nu*( U(i,j-1,k) - 2*U(i,j,k) + U(i,j+1,k) )*ddy*ddy
                    + nu*( U(i,j,k-1) - 2*U(i,j,k) + U(i,j,k+1) )*ddz*ddz
                );
            }
        }
    }
    // y-direction velocity
    #pragma omp parallel for default(none)\
            shared(VS, V, U, W, schemeV, dt, nu)
    for (int k=gc; k<nzt-gc; ++k)
    {
        for (int j=gc; j<nyt-gc-1; ++j)
        {
            for (int i=gc; i<nxt-gc; ++i)
            {
                double ve, vw, vn, vs, vf, vb, ute, utw, vtn, vts, wtf, wtb;
                ve = vw = vn = vs = vf = vb = ute = utw = vtn = vts = wtf = wtb
                = 0.0;

                schemeV(
                    ve, vw, vn, vs, vf, vb, ute, utw, vtn, vts, wtf, wtb,
                    U, V, W, i, j, k);

                VS(i,j,k) = V(i,j,k) + dt
                *(
                    - ( ve*ute - vw*utw )*ddx
                    - ( vn*vtn - vs*vts )*ddy
                    - ( vf*wtf - vb*wtb )*ddz
                    + nu*( V(i-1,j,k) - 2*V(i,j,k) + V(i+1,j,k) )*ddx*ddx
                    + nu*( V(i,j-1,k) - 2*V(i,j,k) + V(i,j+1,k) )*ddy*ddy
                    + nu*( V(i,j,k-1) - 2*V(i,j,k) + V(i,j,k+1) )*ddz*ddz
                );
            }
        }
    }
    // z-direction velocity
    #pragma omp parallel for default(none)\
            shared(WS, W, U, V, schemeW, dt, nu)
    for (int k=gc; k<nzt-gc-1; ++k)
    {
        for (int j=gc; j<nyt-gc; ++j)
        {
            for (int i=gc; i<nxt-gc; ++i)
            {
                double we, ww, wn, ws, wf, wb, ute, utw, vtn, vts, wtf, wtb;
                we = ww = wn = ws = wf = wb = ute = utw = vtn = vts = wtf = wtb
                = 0.0;

                schemeW(
                    we, ww, wn, ws, wf, wb, ute, utw, vtn, vts, wtf, wtb,
                    U, V, W, i, j, k);

                WS(i,j,k) = W(i,j,k) + dt
                *(
                    - ( we*ute - ww*utw )*ddx
                    - ( wn*vtn - ws*vts )*ddy
                    - ( wf*wtf - wb*wtb )*ddz
                    + nu*( W(i-1,j,k) - 2*W(i,j,k) + W(i+1,j,k) )*ddx*ddx
                    + nu*( W(i,j-1,k) - 2*W(i,j,k) + W(i,j+1,k) )*ddy*ddy
                    + nu*( W(i,j,k-1) - 2*W(i,j,k) + W(i,j,k+1) )*ddz*ddz
                );
            }
        }
    }
}