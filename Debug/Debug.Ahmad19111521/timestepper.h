/**
 * @file        timestepper.cpp
 * 
 * @project     3D N-S solver rebuild
 * @version     1.6
 * 
 * @author      Syed Ahmad Raza (sahmadrz@gmail.com)
 * @date        2019-11-05
 * 
 * @brief       Determine the size of the timestep for the Navier-Stokes solver.
 * 
 */

#ifndef TIMESTEPPER_H
#define TIMESTEPPER_H

void timestepper();

#endif