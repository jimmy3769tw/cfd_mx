/**
 * @file        printers.cpp
 * 
 * @project     3D N-S solver rebuild
 * @version     1.6
 * 
 * @author      Syed Ahmad Raza (sahmadrz@gmail.com)
 * @date        2019-11-05
 * 
 * @brief       Functions for printing the progress of the Navier-Stokes to the
 *              terminal output during execution of the solver.
 * 
 */

#include "printers.h"
#include "parameters.h"
#include <iostream>
#include <iomanip>

using namespace std;

void printerProgress()
{
    // Print column headers
    if (timestep == 1 || timestep % 50 == 0)
    {
        cout
            << "\n"
            << setw(10) << "timestep"
            << setw(14) << "dt"
            << setw(6)  << "pIter"
            << setw(15) << "mChangeMax"
            << setw(15) << "pChangeMax"
            << setw(15) << "uChangeMax"
            << setw(15) << "vChangeMax"
            << setw(15) << "wChangeMax"
            << "\n\n";
    }
    // Print timestep data
    cout
        << setw(10) << timestep
        << setw(14) << dt
        << setw(6)  << pIter
        << setw(15) << mChangeMax
        << setw(15) << pChangeMax
        << setw(15) << uChangeMax
        << setw(15) << vChangeMax
        << setw(15) << wChangeMax
        << "\n";
}

void printerInfoEnd()
{
    // Write heading and column headers for case characteristics
    cout << left << "\n"
         << "Finite Volume Method by projection method with BiCGSTAB\n"
         << "=======================================================\n"
         << "Case characteristics\n"
         << "-------------------------------------------------------\n"
         << setw(20) << "nx"                    << nx               << "\n"
         << setw(20) << "ny"                    << ny               << "\n"
         << setw(20) << "nz"                    << nz               << "\n"
         << "\n"
         << setw(20) << "Re"                    << Re               << "\n"
         << "\n"
         << setw(20) << "Velocity scheme"       << velScheme        << "\n"
         << "\n"
         << setw(20) << "timestepMax"           << timestepMax      << "\n"
         << setw(20) << "cflFactor"             << cflFactor        << "\n"
         << setw(20) << "gridFoFactor"          << gridFoFactor     << "\n"
         << "\n"
         << setw(20) << "maxPrIters"            << pIterMax         << "\n"
         << setw(20) << "pRestartFactor"        << pRestartFactor   << "\n"
         << setw(20) << "pResidual"             << pResidual        << "\n"
         << "\n"
         << setw(20) << "uResidual"             << uResidual        << "\n"
         << setw(20) << "vResidual"             << vResidual        << "\n"
         << setw(20) << "wResidual"             << wResidual        << "\n"
         << "\n"
         << setw(20) << "numOfThreads"          << numOfThreads     << "\n"
         << "\n"
         << setw(20) << "fileUniqueName"        << fileUniqueName   << "\n"
         << setw(20) << "comments"              << comments         << "\n"
         << "-------------------------------------------------------\n"
         << "Case completion data\n"
         << "-------------------------------------------------------\n"
         << setw(20) << "Tot. pr. iterations"   << pIterTotal       << "\n"
         << setw(20) << "Pr. restarts"          << pRestarts        << "\n"
         << setw(20) << "Max. mass residual"    << mChangeMax       << "\n"
         << setw(20) << "Max. pr. residual"     << pChangeMax       << "\n"
         << "\n"
         << setw(20) << "Max. change in u"      << uChangeMax       << "\n"
         << setw(20) << "Max. change in v"      << vChangeMax       << "\n"
         << setw(20) << "Max. change in w"      << wChangeMax       << "\n"
         << "\n"
         << setw(20) << "Final dt"              << dt               << "\n"
         << setw(20) << "Simulation time"       << simTime          << "\n"
         << setw(20) << "Final timestep"        << timestep         << "\n"
         << "\n"
         << setw(20) << "Computational time"    << runTime          << " sec\n";

}