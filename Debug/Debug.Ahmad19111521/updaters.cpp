/**
 * @file        updaters.cpp
 * 
 * @project     3D N-S solver rebuild
 * @version     1.6
 * 
 * @author      Syed Ahmad Raza (sahmadrz@gmail.com)
 * @date        2019-11-05
 * 
 * @brief       Update the pressure and velocity values as per the calculations
 *              in the current timestep of the Navier-Stokes solver.
 * 
 */

#include "updaters.h"
#include "parameters.h"
#include "printers.h"
#include <vector>

void pressureUpdater()
{
    #pragma omp parallel for default(none) shared(P, PX)\
            reduction(max:pChangeMax)
    for (int i=0; i<nx; ++i)
    {
        for (int j=0; j<ny; ++j)
        {
            for (int k=0; k<nz; ++k)
            {
                double ig = i+gc, jg = j+gc, kg = k+gc;
                int l = k + j*nz + i*ny*nz;

                double pChange = std::abs(PX(l) - P(ig,jg,kg));
                if (pChange > pChangeMax){ pChangeMax = pChange; }

                P(ig,jg,kg) = PX(l);
            }
        }
    }
}

void pressureUpdater_vector(std::vector<double> &X_result)
{
    #pragma omp parallel for default(none) shared(P, X_result)\
            reduction(max:pChangeMax)
    for (int i=0; i<nx; ++i)
    {
        for (int j=0; j<ny; ++j)
        {
            for (int k=0; k<nz; ++k)
            {
                double ig = i+gc, jg = j+gc, kg = k+gc;
                int l = k + j*nz + i*ny*nz;

                double pChange = std::abs(X_result.at(l) - P(ig,jg,kg));
                if (pChange > pChangeMax){ pChangeMax = pChange; }

                P(ig,jg,kg) = X_result.at(l);
            }
        }
    }
}


void velocityUpdater()
{
    // Updating velocity
    #pragma omp parallel for default(none) shared(U, US, P, ETA, FX, dt)\
            reduction(max:uChangeMax)
    for (int k=gc; k<nzt-gc; ++k)
    {
        for (int j=gc; j<nyt-gc; ++j)
        {
            for (int i=gc; i<nxt-gc-1; ++i)
            {
                double uFluid = US(i,j,k) - dt*ddx*(P(i+1,j,k) - P(i,j,k));
                double uFinal = ETA(i,j,k)*uStrct + (1 - ETA(i,j,k))*uFluid;
                FX(i,j,k) = (uFinal - uFluid) / dt;

                double uChange = std::abs(uFinal - U(i,j,k));
                if (uChange > uChangeMax) { uChangeMax = uChange; }

                U(i,j,k) = uFinal;
            }
        }
    }
    #pragma omp parallel for default(none) shared(V, VS, P, ETA, FY, dt)\
            reduction(max:vChangeMax)
    for (int k=gc; k<nzt-gc; ++k)
    {
        for (int j=gc; j<nyt-gc-1; ++j)
        {
            for (int i=gc; i<nxt-gc; ++i)
            {
                double vFluid = VS(i,j,k) - dt*ddy*(P(i,j+1,k) - P(i,j,k));
                double vFinal = ETA(i,j,k)*vStrct + (1 - ETA(i,j,k))*vFluid;
                FY(i,j,k) = (vFinal - vFluid) / dt;

                double vChange = std::abs(vFinal - V(i,j,k));
                if (vChange > vChangeMax) { vChangeMax = vChange; }

                V(i,j,k) = vFluid;
            }
        }
    }
    #pragma omp parallel for default(none) shared(W, WS, P, ETA, FZ, dt)\
            reduction(max:wChangeMax)
    for (int k=gc; k<nzt-gc-1; ++k)
    {
        for (int j=gc; j<nyt-gc; ++j)
        {
            for (int i=gc; i<nxt-gc; ++i)
            {
                double wFluid = WS(i,j,k) - dt*ddz*(P(i,j,k+1) - P(i,j,k));
                double wFinal = ETA(i,j,k)*wStrct + (1 - ETA(i,j,k))*wFluid;
                FZ(i,j,k) = (wFinal - wFluid) / dt;

                double wChange = std::abs(wFinal - W(i,j,k));
                if (wChange > wChangeMax) { wChangeMax = wChange; }

                W(i,j,k) = wFluid;
            }
        }
    }
}