/**
 * @file        main.cpp
 * 
 * @project     3D N-S solver rebuild
 * @version     1.6
 * 
 * @author      Syed Ahmad Raza (sahmadrz@gmail.com)
 * @date        2019-11-05
 * 
 * @brief       The main solver file outlining the algorithm of the Navier-
 *              Stokes solver.
 * 
 * @detail      Rebuilding the 3D N-S solver is in progress. The current version
 *              is a fully working 3D N-S solver for uniform grid with
 *              parallelization using openMP and adaptive timestepping,
 *              utilizing BCGSTAB method for pressure Poisson equation. Data
 *              structures from the Eigen library are utilized.
 * 
 * @command     g++ -std=c++17 -Wall -O3 -funroll-loops -mfpmath=sse -D_GLIBCXX_PARALLEL -fopenmp -march=native main.cpp gridders.cpp structures.cpp conditions.cpp pressureSolvers.cpp timestepper.cpp updaters.cpp velocitySchemers.cpp filers.cpp printers.cpp -o testing && ./testing
 * 
 */

#include <dirent.h>             // opendir to check that directory exists
#include <iostream>
#include <ctime>                // to time the script
#include <chrono>               // to measure and display the time duration
#include "parameters.h"
#include "gridders.h"
#include "structures.h"
#include "conditions.h"
#include "timestepper.h"
#include "velocitySchemers.h"
#include "pressureSolvers.h"
#include "updaters.h"
#include "filers.h"
#include "printers.h"
// -----------------AMGCL




#include <amgcl/backend/builtin.hpp>
#include <amgcl/adapter/crs_tuple.hpp>
#include <amgcl/make_solver.hpp>
#include <amgcl/amg.hpp>
#include <amgcl/coarsening/smoothed_aggregation.hpp>
#include <amgcl/relaxation/spai0.hpp>
#include <amgcl/relaxation/gauss_seidel.hpp>
#include <amgcl/solver/bicgstab.hpp>
#include <amgcl/profiler.hpp>


// ---------------

int
    timestep = 0, pIter = 0, pIterTotal = 0, pRestarts = 0;
double
    dt = 0.0, simTime = 0.0, runTime = 0.0,
    mChangeMax = 0.0, pChangeMax = 0.0,
    uChangeMax = 1.0, vChangeMax = 1.0, wChangeMax = 1.0;
Eigen::Matrix<double, -1, 1>
    X(nx+1), Y(ny+1), Z(nz+1),
    DX(nxt), DY(nyt), DZ(nzt), DXM(nxt-1), DYM(nyt-1), DZM(nzt-1),
    PB(ndim), PX(ndim);
Eigen::Matrix<double, -1, -1>
    PA(ndim, ddim);
Eigen::TensorFixedSize<double, Eigen::Sizes<nxt, nyt, nzt>>
    U, V, W, US, VS, WS, P, ETA, FX, FY, FZ;

int main()
{
    if(opendir("../data") == NULL)
    { if (system("mkdir ../data") != 0){ return 1; } }

    #ifdef _OPENMP
    omp_set_num_threads(numOfThreads);
    #endif

    filerInfoStart();

    std::chrono::steady_clock::time_point tStart
        = std::chrono::steady_clock::now();

    gridder();
    if (solidPos == "fixed") { volumeOfSolid(); }
    // ETA.setZero();
    initialConditions();
    boundaryConditions();
    pressureMatrixConstructor();        // PA




    //=========== Compose the solver type ===========//

    //   the solver backend:
    typedef amgcl::backend::builtin<double> SBackend;
    //   the preconditioner backend:
    typedef amgcl::backend::builtin<double> PBackend;

    //=========== Compose the solver type ===========//

    typedef amgcl::make_solver<
        amgcl::amg<
            PBackend,
            amgcl::coarsening::smoothed_aggregation,
            amgcl::relaxation::gauss_seidel
            >,
        amgcl::solver::bicgstab<SBackend>
        > Solver;


    auto data      =  pressureMatrixConstructor_CSR();
    auto ptr       =  std::get<0>(data);  // size_t
    auto idx   =  std::get<1>(data);  // szie_t
    auto val    =  std::get<2>(data);  // double
    auto amgcl_mat =  std::tie(ndim, ptr, idx, val);
    std::cout << " ndim " << ndim << "idx.size() "<< ptr.size()   << std::endl;
    std::cout << "Solver -----------building";
    Solver solve(amgcl_mat);
    std::cout << "finished.";

    std::vector<double> X_result(ndim, 0.0), rhs(ndim, 0.0);


    std::cout << "finished.";




    while(timestep < timestepMax && (uChangeMax > uResidual
                                     || vChangeMax > vResidual
                                     || wChangeMax > wResidual))
    {
        mChangeMax = 0.0;
        pChangeMax = 0.0;
        uChangeMax = 0.0;
        vChangeMax = 0.0;
        wChangeMax = 0.0;

        // timestepper();
        dt = 1.0e-3;
        ++timestep;
        simTime += dt;

        if (velScheme == "quick") { velocitySchemer(quickU, quickV, quickW); }
                                                                    // US = U


        // * ---------------- amgcl ----------------
        pressureVectorConstructor_vector(rhs);
        auto [iters, error] = solve(rhs, X_result);
        std::cout << "[iter] " << iters << "[error] " << error << std::endl;
        pressureUpdater_vector(X_result);
        // * ---------------- amgcl ----------------

        // * ---------------- Ahmad ----------------
        pressureVectorConstructor();                                // PB <- US
        bCGSTAB();                                                  // PX
        pressureUpdater();                                          // P = PX
        // * ---------------- Ahmad ----------------




        velocityUpdater();                                          // U = US
        boundaryConditions();

        if (timestep % filerSteps == 0) { filerSolution(timestep); }
        printerProgress();
    }

    std::chrono::steady_clock::time_point tEnd
        = std::chrono::steady_clock::now();
    runTime
        = std::chrono::duration_cast<std::chrono::seconds>(tEnd-tStart).count();

    filerSolution();
    filerInfoEnd();
    printerInfoEnd();

    return 0;
}
