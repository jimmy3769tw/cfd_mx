/**
 * @file        filers.cpp
 * 
 * @project     3D N-S solver rebuild
 * @version     1.6
 * 
 * @author      Syed Ahmad Raza (sahmadrz@gmail.com)
 * @date        2019-11-05
 * 
 * @brief       Functions for filing the solution of the Navier-Stokes solver.
 * 
 */

#ifndef FILERS_H
#define FILERS_H

#include "parameters.h"
#include <string>

void filerASCIIBlock(const std::string fileName);
void filerSolution(const int timestep = -1);
void filerInfoStart();
void filerInfoEnd();

#endif