/**
 * @file        pressureSolvers.h
 * 
 * @project     3D N-S solver rebuild
 * @version     1.6
 * 
 * @author      Syed Ahmad Raza (sahmadrz@gmail.com)
 * @date        2019-11-05
 * 
 * @brief       Functions for solving the pressure Poisson equation for the
 *              Navier-Stokes solver.
 * 
 */

#ifndef PRESSURESOLVERS_H
#define PRESSURESOLVERS_H

#include "parameters.h"

std::tuple<std::vector<int>, std::vector<int>, std::vector<double> >  pressureMatrixConstructor_CSR();

void pressureVectorConstructor();
void pressureMatrixConstructor();
void bCGSTAB();

// ------------

void bCGSTAB_vector(std::vector<double> &PB, std::vector<double> &PX);
void multiplyMatrixVector(Eigen::Matrix<double, -1, 1>& PRD,
    Eigen::Matrix<double, -1, -1>& MAT, Eigen::Matrix<double, -1, 1>& VEC);
void pressureVectorConstructor_vector(std::vector<double> &rhs);





#endif