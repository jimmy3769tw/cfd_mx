/**
 * @file        timestepper.cpp
 * 
 * @project     3D N-S solver rebuild
 * @version     1.6
 * 
 * @author      Syed Ahmad Raza (sahmadrz@gmail.com)
 * @date        2019-11-05
 * 
 * @brief       Determine the size of the timestep for the Navier-Stokes solver.
 * 
 */

#include "timestepper.h"
#include "parameters.h"
#include <cmath>


using namespace std;

void timestepper()
{
    // double cflConditionU;
    // double cflConditionV;
    // double cflConditionW;
    // double gridFourierCondition;
    double uMax, vMax, wMax;
    uMax = vMax = wMax = 0.0;

    #pragma omp parallel for default(none) shared(U, V, W)\
            reduction(max:uMax, vMax, wMax)
    for (int k=gc; k<nzt-gc; ++k)
    {
        for (int j=gc; j<nyt-gc; ++j)
        {
            for (int i=gc; i<nxt-gc; ++i)
            {
                if (abs(U(i,j,k)) > uMax){ uMax = abs(U(i,j,k)); }
                if (abs(V(i,j,k)) > vMax){ vMax = abs(V(i,j,k)); }
                if (abs(W(i,j,k)) > wMax){ wMax = abs(W(i,j,k)); }
            }
        }
    }
    if (velScheme != "central")
    {
        dt = min({cflFactor*dx/uMax,
                  cflFactor*dy/vMax,
                  cflFactor*dz/wMax,
                  gridFoFactor*0.5*Re*dx*dx,
                  gridFoFactor*0.5*Re*dy*dy,
                  gridFoFactor*0.5*Re*dz*dz,
                  dtSizeOverride});
    }
    else
    {
        dt = min({cflFactor/(Re*uMax*uMax),
                  cflFactor/(Re*vMax*vMax),
                  cflFactor/(Re*wMax*wMax),
                  gridFoFactor*0.5*Re*dx*dx,
                  gridFoFactor*0.5*Re*dy*dy,
                  gridFoFactor*0.5*Re*dz*dz,
                  dtSizeOverride});
    }
}