/**
 * @file        printers.cpp
 * 
 * @project     3D N-S solver rebuild
 * @version     1.6
 * 
 * @author      Syed Ahmad Raza (sahmadrz@gmail.com)
 * @date        2019-11-05
 * 
 * @brief       Functions for printing the progress of the Navier-Stokes to the
 *              terminal output during execution of the solver.
 * 
 */

#ifndef PRITNERS_H
#define PRINTERS_H

#include "parameters.h"

void printerProgress();
void printerInfoEnd();

#endif