/**
 * @file        main.cpp
 * 
 * @project     3D N-S solver rebuild
 * @version     1.6
 * 
 * @author      Syed Ahmad Raza (sahmadrz@gmail.com)
 * @date        2019-11-05
 * 
 * @brief       The main solver file outlining the algorithm of the Navier-
 *              Stokes solver.
 * 
 * @detail      Rebuilding the 3D N-S solver is in progress. The current version
 *              is a fully working 3D N-S solver for uniform grid with
 *              parallelization using openMP and adaptive timestepping,
 *              utilizing BCGSTAB method for pressure Poisson equation. Data
 *              structures from the Eigen library are utilized.
 * 
 * @command     g++ -std=c++17 -Wall -O3 -funroll-loops -mfpmath=sse -D_GLIBCXX_PARALLEL -fopenmp -march=native main.cpp gridders.cpp structures.cpp conditions.cpp pressureSolvers.cpp timestepper.cpp updaters.cpp velocitySchemers.cpp filers.cpp printers.cpp -o testing && ./testing
 * 
 */

#include <dirent.h>             // opendir to check that directory exists
#include <iostream>
#include <ctime>                // to time the script
#include <chrono>               // to measure and display the time duration
#include "parameters.h"
#include "gridders.h"
#include "structures.h"
#include "conditions.h"
#include "timestepper.h"
#include "velocitySchemers.h"
#include "pressureSolvers.h"
#include "updaters.h"
#include "filers.h"
#include "printers.h"

// my

#include "controlPanel.hpp"
#include "1_Run.hpp"
#include "source/convection_and_difussion.uniformGrid.hpp"

using std::string, 
      std::vector, 
      std::abs, 
      std::pow, 
      std::sqrt, 
      std::cout, 
      std::endl; 

#include "run/1_1_Sequential.hpp"
// #include "run/1_2_Omp.hpp"

#include "run/general.hpp"

// #include "pressure/solver/SorPipeLine.omp.hpp"
// #include "pressure/solver/SorPipeLine.seq.hpp"

// #include "run/genenz-gC-1ral.hpp"

int
    timestep = 0, pIter = 0, pIterTotal = 0, pRestarts = 0;
double
    dt = 0.0, simTime = 0.0, runTime = 0.0,
    mChangeMax = 0.0, pChangeMax = 0.0,
    uChangeMax = 1.0, vChangeMax = 1.0, wChangeMax = 1.0;
Eigen::Matrix<double, -1, 1>
    X(nx+1), Y(ny+1), Z(nz+1),
    DX(nxt), DY(nyt), DZ(nzt), DXM(nxt-1), DYM(nyt-1), DZM(nzt-1),
    PB(ndim), PX(ndim);
Eigen::Matrix<double, -1, -1>
    PA(ndim, ddim);
Eigen::TensorFixedSize<double, Eigen::Sizes<nxt, nyt, nzt>>
    U, V, W, US, VS, WS, P, ETA, FX, FY, FZ;

int main()
{

    cout << "Debug quick scheme \n";

    // ! --------------------
    // * CFD_MX_struct
    clockstruct timer;
    simuClass simu;
    grid gridA;

    // *   parameter  ------------
    simu.set_time_max(1000);
    simu.set_dt(1.0e-3);
    simu.set_Re(100.0);
    simu.init_fileStep(1.0);

    // *   parameter  ------------


    // *   Accuracy  ------------
    gridA.Gridder                   =   "uniform" ;

    simu.Locality = 1;

  //  * struct ---------------------------
  ImmersedBoundary Dfib;

  pressure t1;

  velocity T0 , T1, T3;

  SORcoefficient Sor;

  shareMenory ShareM;

  MxClass Mx;

  //  * struct ---------------------------
  // *         ============================  divid Domain ============================


  calDomain Lo;

  std::vector<int> grid_size{gridA.nx, gridA.ny, gridA.nz};

  std::vector<int> dims{1, 1, 1};

  Lo.initTable(grid_size, dims);

  Lo.initLocal(0, 0, 0);

  // *         ============================  divid Domain ============================


  resize_variable(gridA, simu, t1, T0, T1, T3, Dfib); // ! resize shared memory

#if defined (PC_SEQ)  
  T0.iniU_omp(0.0, 0.0, 0.0);
  
  T1.iniU_omp(0.0, 0.0, 0.0);
  
  T3.iniU_omp(0.0, 0.0, 0.0);

#elif defined (PC_OMP)
  T0.iniU_omp(0.0, 0.0, 0.0);
  
  T1.iniU_omp(0.0, 0.0, 0.0);
  
  T3.iniU_omp(0.0, 0.0, 0.0);
#endif

  t1.init_p(0.0);


  generateGride(simu, ShareM, Lo, gridA);

  gridCheck_csv(simu, ShareM, Lo, gridA);


    std::random_device rd;

    for(size_t i=0; i<nxt; ++i)
    for(size_t j=0; j<nyt; ++j)
    for(size_t k=0; k<nzt; ++k)
    {
      T0.u[gridA.icel(i,j,k)] = U(i,j,k) = rd();
      T0.v[gridA.icel(i,j,k)] = V(i,j,k) = rd();
      T0.w[gridA.icel(i,j,k)] = W(i,j,k) = rd();
      t1.p[gridA.icel(i,j,k)] = P(i,j,k) = rd();
    }

    for(size_t i=0; i<nxt; ++i)
    for(size_t j=0; j<nyt; ++j)
    for(size_t k=0; k<nzt; ++k)
    {
      T1.u[gridA.icel(i,j,k)] = US(i,j,k) = rd();
      T1.v[gridA.icel(i,j,k)] = VS(i,j,k) = rd();
      T1.w[gridA.icel(i,j,k)] = WS(i,j,k) = rd();
    }


    if (velScheme == "quick") { velocitySchemer(quickU, quickV, quickW); }
    
    simu.dt = dt;

    // ConvectionDifussion_UniformG(simu, T0, T1, Lo, gridA);
    ConvectionDifussion(simu, T0, T1, Lo, gridA);
    double 
            critical = 0.0001,
            sum =0;
    

    sum = 0.0;
    for(int i=0; i<nxt; ++i)
    for(int j=0; j<nyt; ++j)
    for(int k=0; k<nzt; ++k)
    {
        auto a = US(i,j,k);
        auto b = T1.u[gridA.icel(i,j,k)];
        auto ab = std::abs(a-b);
        sum += ab;

        if (ab > critical){
          cout  << i << ", "
                << j << ", "
                << k << ": "
                << a << ", " << b << std::endl;
        }
    }

    cout << "\nus " << sum;

    sum = 0.0;
    for(size_t i=0; i<nxt; ++i)
    for(size_t j=0; j<nyt; ++j)
    for(size_t k=0; k<nzt; ++k)
    {
        auto a = VS(i,j,k);
        auto b = T1.v[gridA.icel(i,j,k)];
        auto ab =  std::abs(a-b);
        sum +=ab;

        // if (ab > critical){
        //   cout << a << ", " << "b" << b << std::endl;
        // }
    }

    cout << "\nvs " << sum;

    sum = 0.0;
    for(size_t i=0; i<nxt; ++i)
    for(size_t j=0; j<nyt; ++j)
    for(size_t k=0; k<nzt; ++k)
    {
        auto a = WS(i,j,k);
        auto b = T1.w[gridA.icel(i,j,k)];
        auto ab =  std::abs(a-b);
        sum +=ab;

        // if (ab > critical){
        //   cout << a << ", " << "b" << b << std::endl;
        // }
    }

    cout << "\nws " << sum;

    sum = 0.0;
    for(size_t i=0; i<nxt; ++i)
    for(size_t j=0; j<nyt; ++j)
    for(size_t k=0; k<nzt; ++k)
    {
        auto a = P(i,j,k);
        auto b = t1.p[gridA.icel(i,j,k)];
        auto ab =  std::abs(a-b);
        sum +=ab;

        // if (ab > critical){
        //   cout << a << ", " << "b" << b << std::endl;
        // }
    }

    cout << "\np " << sum;


    return 0;



}
