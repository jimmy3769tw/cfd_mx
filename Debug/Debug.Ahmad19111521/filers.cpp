/**
 * @file        filers.cpp
 * 
 * @project     3D N-S solver rebuild
 * @version     1.6
 * 
 * @author      Syed Ahmad Raza (sahmadrz@gmail.com)
 * @date        2019-11-05
 * 
 * @brief       Functions for filing the solution of the Navier-Stokes solver.
 * 
 */

#include "filers.h"
#include "parameters.h"
#include <iomanip>              // set output width
#include <fstream>              // file output
#include <limits>               // for "numeric_limits<double>::digits10"

using namespace std;

void filerASCIIBlock(const string fileName)
/*
    File the complete solution with coordinates for Tecplot in ASCII block
    format.
*/
{
    ofstream fileQ;
    fileQ.precision(numeric_limits<double>::digits10 + 2);
    fileQ.open(filePath + fileName + ".dat");

    // Tecplot file header ---------------------------------------------------//
    fileQ << "TITLE = \"3D N-S solver for " << fileUniqueName <<  "\"\n"
             "FILETYPE = FULL\n"
             "VARIABLES =\n"
             "\n"
             "X, Y, Z, P, U, V, W, ETA, FX, FY, FZ\n"
             "\n";

    // Tecplot zone record header --------------------------------------------//
    fileQ << "ZONE\n"
             "T = " << fileUniqueName << "\n"
             "ZONETYPE = ORDERED,\n"
             "\n"
             "I = " << to_string(nx+1) << ", "
             "J = " << to_string(ny+1) << ", "
             "K = " << to_string(nz+1) << ", \n"
             "\n"
             "DT = (DOUBLE, DOUBLE)\n"
             "\n"
             "DATAPACKING = BLOCK\n"
             "VARLOCATION = ([1-3] = NODAL, [4-11] = CELLCENTERED)\n"
          << endl;

    // Tecplot zone data -----------------------------------------------------//
    for (int k=0; k<nz+1; ++k)
    {
        for (int j=0; j<ny+1; ++j)
        {
            for (int i=0; i<nx+1; ++i)
            {
                fileQ << X(i) << " ";
            }
            fileQ << endl;
        }
    }
    for (int k=0; k<nz+1; ++k)
    {
        for (int j=0; j<ny+1; ++j)
        {
            for (int i=0; i<nx+1; ++i)
            {
                fileQ << Y(j) << " ";
            }
            fileQ << endl;
        }
    }
    for (int k=0; k<nz+1; ++k)
    {
        for (int j=0; j<ny+1; ++j)
        {
            for (int i=0; i<nx+1; ++i)
            {
                fileQ << Z(k) << " ";
            }
            fileQ << endl;
        }
    }
    for (int k=gc; k<nzt-gc; ++k)
    {
        for (int j=gc; j<nyt-gc; ++j)
        {
            for (int i=gc; i<nxt-gc; ++i)
            {
                fileQ << P(i,j,k) << " ";
            }
            fileQ << endl;
        }
    }
    for (int k=gc; k<nzt-gc; ++k)
    {
        for (int j=gc; j<nyt-gc; ++j)
        {
            for (int i=gc; i<nxt-gc; ++i)
            {
                fileQ << 0.5*(U(i,j,k) + U(i-1,j,k)) << " ";
            }
            fileQ << endl;
        }
    }
    for (int k=gc; k<nzt-gc; ++k)
    {
        for (int j=gc; j<nyt-gc; ++j)
        {
            for (int i=gc; i<nxt-gc; ++i)
            {
                fileQ << 0.5*(V(i,j,k) + V(i,j-1,k)) << " ";
            }
            fileQ << endl;
        }
    }
    for (int k=gc; k<nzt-gc; ++k)
    {
        for (int j=gc; j<nyt-gc; ++j)
        {
            for (int i=gc; i<nxt-gc; ++i)
            {
                fileQ << 0.5*(W(i,j,k) + W(i,j,k-1)) << " ";
            }
            fileQ << endl;
        }
    }
    for (int k=gc; k<nzt-gc; ++k)
    {
        for (int j=gc; j<nyt-gc; ++j)
        {
            for (int i=gc; i<nxt-gc; ++i)
            {
                fileQ << ETA(i,j,k) << " ";
            }
            fileQ << endl;
        }
    }
    for (int k=gc; k<nzt-gc; ++k)
    {
        for (int j=gc; j<nyt-gc; ++j)
        {
            for (int i=gc; i<nxt-gc; ++i)
            {
                fileQ << FX(i,j,k) << " ";
            }
            fileQ << endl;
        }
    }
    for (int k=gc; k<nzt-gc; ++k)
    {
        for (int j=gc; j<nyt-gc; ++j)
        {
            for (int i=gc; i<nxt-gc; ++i)
            {
                fileQ << FY(i,j,k) << " ";
            }
            fileQ << endl;
        }
    }
    for (int k=gc; k<nzt-gc; ++k)
    {
        for (int j=gc; j<nyt-gc; ++j)
        {
            for (int i=gc; i<nxt-gc; ++i)
            {
                fileQ << FZ(i,j,k) << " ";
            }
            fileQ << endl;
        }
    }
}

/*
File solution files on demand, according to requirement.
*/
void filerSolution(const int timestep)
{
    // default argument for timestep = -1; defined in filers.h
    if (timestep != -1)
    {
        string timeStr = to_string(timestep);
        filerASCIIBlock(fileUniqueName + "_" + timeStr);
    }
    else
    {
        filerASCIIBlock(fileUniqueName + "_final");
    }
}

void filerInfoStart()
{
    ofstream f;
    f.open(filePath + fileUniqueName + "_characteristics.txt");

    // Write heading and case characteristics
    f   << left
        << "Finite Volume Method by projection method with BiCGSTAB\n"
        << "=======================================================\n"
        << "Case characteristics\n"
        << "-------------------------------------------------------\n"
        << setw(20) << "nx"                     << nx               << "\n"
        << setw(20) << "ny"                     << ny               << "\n"
        << setw(20) << "nz"                     << nz               << "\n"
        << "\n"
        << setw(20) << "Re"                     << Re               << "\n"
        << "\n"
        << setw(20) << "Velocity scheme"        << velScheme        << "\n"
        << "\n"
        << setw(20) << "timestepMax"            << timestepMax      << "\n"
        << setw(20) << "cflFactor"              << cflFactor        << "\n"
        << setw(20) << "gridFoFactor"           << gridFoFactor     << "\n"
        << "\n"
        << setw(20) << "maxPrIters"             << pIterMax         << "\n"
        << setw(20) << "pRestartFactor"         << pRestartFactor   << "\n"
        << setw(20) << "pResidual"              << pResidual        << "\n"
        << "\n"
        << setw(20) << "uResidual"              << uResidual        << "\n"
        << setw(20) << "vResidual"              << vResidual        << "\n"
        << setw(20) << "wResidual"              << wResidual        << "\n"
        << "\n"
        << setw(20) << "numOfThreads"           << numOfThreads     << "\n"
        << "\n"
        << setw(20) << "fileUniqueName"         << fileUniqueName   << "\n"
        << setw(20) << "comments"               << comments         << endl;

    f.close();
}

/*
File information at the end of the program.
*/
void filerInfoEnd()
{
    ofstream f;
    f.open(filePath + fileUniqueName
           + "_characteristics.txt", ios_base::app);    // open for editing

    // Write case completion data
    f   << left
        << "-------------------------------------------------------\n"
        << "Case completion data\n"
        << "-------------------------------------------------------\n"
        << setw(20) << "Tot. pr. iterations"    << pIterTotal       << "\n"
        << setw(20) << "Pr. restarts"           << pRestarts        << "\n"
        << setw(20) << "Max. mass residual"     << mChangeMax       << "\n"
        << setw(20) << "Max. pr. residual"      << pChangeMax       << "\n"
        << "\n"
        << setw(20) << "Max. change in u"       << uChangeMax       << "\n"
        << setw(20) << "Max. change in v"       << vChangeMax       << "\n"
        << setw(20) << "Max. change in w"       << wChangeMax       << "\n"
        << "\n"
        << setw(20) << "Final dt"               << dt               << "\n"
        << setw(20) << "Simulation time"        << simTime          << "\n"
        << setw(20) << "Final timestep"         << timestep         << "\n"
        << "\n"
        << setw(20) << "Computational time"     << runTime          << " sec\n"
        << endl;

    f.close();
}