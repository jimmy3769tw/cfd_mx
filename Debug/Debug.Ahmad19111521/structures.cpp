/**
 * @file        structures.cpp
 * 
 * @project     3D N-S solver rebuild
 * @version     1.6
 * 
 * @author      Syed Ahmad Raza (sahmadrz@gmail.com)
 * @date        2019-11-06
 * 
 * @brief       Create the structures by defining ETA throughout the grid
 * 
 */

#include "structures.h"
#include "parameters.h"

// void volumeOfSolid()
// {
//     #pragma omp parallel for default(none) shared(ETA, X, Y, Z, DX, DY, DZ)
//     for (int i = iBgnStrct; i < iEndStrct; ++i)
//     {
//         /**
//          * xmid, ymid, zmid are midpoints of cells
//          * dxs, dys, dzs are subgrid cell widths
//          * xsmid0, ysmid0, zsmid0 are midpoints of 1st subgrid cell
//          * xsmid, ysmid, zsmid are midpoints of subgrid cells
//          */
//         double eta = 0.0, r = 0.5*d;

//         int ig = i+gc;
//         double x = X(i), dx = DX(ig), xmid = x + (dx/2.0);
//         for (int j = jBgnStrct; j < jEndStrct; ++j)
//         {
//             int jg = j+gc;
//             double y = Y(j), dy = DY(jg), ymid = y + (dy/2.0);
//             for (int k = kBgnStrct; k < kEndStrct; ++k)
//             {
//                 int kg = k+gc;
//                 double z = Z(k), dz = DZ(kg), zmid = z + (dz/2.0);

//                 // Default value of volume of solid for fluid region
//                 eta = 0.0;
                
//                 // Simple volume of solid for sphere
//                 double sphereCenterToCellCenter
//                     = sqrt(  (xmid - xc)*(xmid - xc)
//                            + (ymid - yc)*(ymid - yc)
//                            + (zmid - zc)*(zmid - zc));

//                 if (sphereCenterToCellCenter <= r)
//                 {   eta = 1.0;    }

//                 // Subgrid method for sphere                
//                 double cellDiagonalHalf
//                     = sqrt(dx*dx + dy*dy + dz*dz)/2.0;
//                 if (abs(sphereCenterToCellCenter - r) < cellDiagonalHalf)
//                 {
//                     double dxs    = dx/nSubx,
//                            dys    = dy/nSuby,
//                            dzs    = dz/nSubz,
//                            xsmid0 = x + (dxs/2.0),
//                            ysmid0 = y + (dys/2.0),
//                            zsmid0 = z + (dzs/2.0);

//                     // Summation of xi for all subgrids in the main cell
//                     double xi = 0.0;
//                     double xsmid = xsmid0;
//                     for (int l = 0; l < nSubx; ++l)
//                     {
//                         double ysmid = ysmid0;
//                         for (int m = 0; m < nSuby; ++m)
//                         {
//                             double zsmid = zsmid0;
//                             for (int n = 0; n < nSubz; ++n)
//                             {
//                                 double sphereCenterToSubgridCellCenter
//                                     = sqrt(  (xsmid - xc)*(xsmid - xc)
//                                            + (ysmid - yc)*(ysmid - yc)
//                                            + (zsmid - zc)*(zsmid - zc));

//                                 if (sphereCenterToSubgridCellCenter <= r)
//                                 {   xi += 1;    }

//                                 zsmid += dzs;
//                             }
//                             ysmid += dys;
//                         }
//                         xsmid += dxs;
//                     }
//                     eta = xi / static_cast<double>(nSubTotal);
//                 }
//                 ETA(ig,jg,kg) = eta;
//             }
//         }
//     }
// }




void volumeOfSolid()
{
    #pragma omp parallel for default(none) shared(ETA, X, Y, Z, DX, DY, DZ)\



    for (int i = iBgnStrct; i < iBgnStrct+1; ++i)
    {
        /**
         * xmid, ymid, zmid are midpoints of cells
         * dxs, dys, dzs are subgrid cell widths
         * xsmid0, ysmid0, zsmid0 are midpoints of 1st subgrid cell
         * xsmid, ysmid, zsmid are midpoints of subgrid cells
         */
        double eta = 0.0, r = 0.5*d;

        int ig = i+gc;
        // double x = X(i), dx = DX(ig), xmid = x + (dx/2.0);
        for (int j = jBgnStrct; j < jEndStrct; ++j)
        {
            int jg = j+gc;
            double y = Y(j), dy = DY(jg), ymid = y + (dy/2.0);
            for (int k = kBgnStrct; k < kEndStrct; ++k)
            {
                int kg = k+gc;
                double z = Z(k), dz = DZ(kg), zmid = z + (dz/2.0);

                // Default value of volume of solid for fluid region
                eta = 0.0;
                
                // Simple volume of solid for sphere
                double sphereCenterToCellCenter
                    = sqrt(  //(xmid - xc)*(xmid - xc)+
                            (ymid - yc)*(ymid - yc)
                           + (zmid - zc)*(zmid - zc));

                if (sphereCenterToCellCenter <= r)
                {   eta = 1.0;    }

                // Subgrid method for sphere                
                double cellDiagonalHalf
                    = sqrt(//dx*dx +
                     dy*dy + dz*dz)/2.0;
                if (abs(sphereCenterToCellCenter - r) < cellDiagonalHalf)
                {
                    double //dxs    = dx/nSubx,
                           dys    = dy/nSuby,
                           dzs    = dz/nSubz,
                        //    xsmid0 = x + (dxs/2.0),
                           ysmid0 = y + (dys/2.0),
                           zsmid0 = z + (dzs/2.0);

                    // Summation of xi for all subgrids in the main cell
                    double xi = 0.0;
                    // double xsmid = xsmid0;
                    // for (int l = 0; l < nSubx; ++l)
                    // {
                        double ysmid = ysmid0;
                        for (int m = 0; m < nSuby; ++m)
                        {
                            double zsmid = zsmid0;
                            for (int n = 0; n < nSubz; ++n)
                            {
                                double sphereCenterToSubgridCellCenter
                                    = sqrt( // (xsmid - xc)*(xsmid - xc) + 
                                             (ysmid - yc)*(ysmid - yc)
                                           + (zsmid - zc)*(zsmid - zc));

                                if (sphereCenterToSubgridCellCenter <= r)
                                {   xi += 1;    }

                                zsmid += dzs;
                            }
                            ysmid += dys;
                        }
                        // xsmid += dxs;
                    // }
                    eta = xi / static_cast<double>(nSubTotal)*nSubx;
                }
                ETA(ig,jg,kg) = eta;
            }
        }
    }





    for (int i = iBgnStrct+1; i < iEndStrct; ++i)
    for (int j = jBgnStrct  ; j < jEndStrct; ++j)
    for (int k = kBgnStrct  ; k < kEndStrct; ++k)
    {
        int jg = j+gc, ig = i+gc , kg = k+gc;
        ETA(ig,jg,kg) = ETA(iBgnStrct,jg,kg) ;
    }

}

