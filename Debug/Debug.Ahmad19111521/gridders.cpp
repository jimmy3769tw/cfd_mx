/**
 * @file        gridders.cpp
 * 
 * @project     3D N-S solver rebuild
 * @version     1.6
 * 
 * @author      Syed Ahmad Raza (sahmadrz@gmail.com)
 * @date        2019-11-05
 * 
 * @brief       Create the grid for Navier-Stokes solver.
 * 
 */

#include "gridders.h"
#include "parameters.h"
#include <iostream>

void gridder()
{
    /*------------------------ Unequal grid intervals ------------------------*/
    // // const double
    // //     dx = xDist/double(nx), dy = yDist/double(ny), dz = zDist/double(nz);
    // #pragma omp parallel for default(none) shared(X)
    // for (int i=0; i<nx+1; ++i)
    // {
    //     X(i) = std::pow(i,2)*dx;
    // }
    // #pragma omp parallel for default(none) shared(Y)
    // for (int j=0; j<ny+1; ++j)
    // {
    //     Y(j) = std::pow(j,2)*dy;
    // }
    // #pragma omp parallel for default(none) shared(Z)
    // for (int k=0; k<nz+1; ++k)
    // {
    //     Z(k) = std::pow(k,2)*dz;
    // }
    /*------------------------------------------------------------------------*/

    /*------------------------- Equal grid intervals -------------------------*/
    // const double
    //     dx = xDist/double(nx), dy = yDist/double(ny), dz = zDist/double(nz);
    #pragma omp parallel for default(none) shared(X)
    for (int i=0; i<nx+1; ++i)
    {   X(i) = i*dx;    }

    #pragma omp parallel for default(none) shared(Y)
    for (int j=0; j<ny+1; ++j)
    {   Y(j) = j*dy;    }

    #pragma omp parallel for default(none) shared(Z)
    for (int k=0; k<nz+1; ++k)
    {   Z(k) = k*dz;    }
    /*------------------------------------------------------------------------*/

    // Define each of the directional grid lengths
    #pragma omp parallel for default(none) shared(DX, DXM, X)
    for (int i=0; i<nx-1; ++i)
    {
        int ig  = i+gc;
        DX (ig) = (X(i+1) - X(i))      ;
        DXM(ig) = (X(i+2) - X(i)) / 2.0;
    }
    #pragma omp parallel for default(none) shared(DY, DYM, Y)
    for (int j=0; j<ny-1; ++j)
    {
        int jg  = j+gc;
        DY (jg) = (Y(j+1) - Y(j))      ;
        DYM(jg) = (Y(j+2) - Y(j)) / 2.0;
    }
    #pragma omp parallel for default(none) shared(DZ, DZM, Z)
    for (int k=0; k<nz-1; ++k)
    {
        int kg  = k+gc;
        DZ (kg) = (Z(k+1) - Z(k))      ;
        DZM(kg) = (Z(k+2) - Z(k)) / 2.0;
    }

    // Leftover grid lengths (due to limitation of using single loop for DX and 
    // DXM at once, etc.).
    DX(nxt-3) = X(nx) - X(nx-1);
    DY(nyt-3) = Y(ny) - Y(ny-1);
    DZ(nzt-3) = Z(nz) - Z(nz-1);

    // Ghost boundary grid lengths
    DX (0)     = DX(2);
    DX (1)     = DX(2);
    DX (nxt-2) = DX(nxt-3);
    DX (nxt-1) = DX(nxt-3);

    DY (0)     = DY(2);
    DY (1)     = DY(2);
    DY (nyt-2) = DY(nyt-3);
    DY (nyt-1) = DY(nyt-3);

    DZ (0)     = DZ(2);
    DZ (1)     = DZ(2);
    DZ (nzt-2) = DZ(nzt-3);
    DZ (nzt-1) = DZ(nzt-3);

    DXM(0)     = DXM(2);
    DXM(1)     = DXM(2);
    DXM(nxt-3) = DXM(nxt-4);
    DXM(nxt-2) = DXM(nxt-4);

    DYM(0)     = DYM(2);
    DYM(1)     = DYM(2);
    DYM(nyt-3) = DYM(nyt-4);
    DYM(nyt-2) = DYM(nyt-4);

    DZM(0)     = DZM(2);
    DZM(1)     = DZM(2);
    DZM(nzt-3) = DZM(nzt-4);
    DZM(nzt-2) = DZM(nzt-4);

    // std::cout << "X =\n" << X << std::endl;
    // std::cout << "Y =\n" << Y << std::endl;
    // std::cout << "Z =\n" << Z << std::endl;

    // std::cout << "DX =\n" << DX << std::endl;
    // std::cout << "DY =\n" << DY << std::endl;
    // std::cout << "DZ =\n" << DZ << std::endl;

    // std::cout << "DXM =\n" << DXM << std::endl;
    // std::cout << "DYM =\n" << DYM << std::endl;
    // std::cout << "DZM =\n" << DZM << std::endl;
}