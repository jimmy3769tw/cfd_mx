/**
 * @file        parameters.h
 *
 * @project     3D N-S solver rebuild
 * @version     1.6
 *
 * @author      Syed Ahmad Raza (sahmadrz@gmail.com)
 * @date        2019-11-05
 *
 * @brief       Declare the constants and key variables for the case.
 *
 */

#ifndef PARAMETERS_H
#define PARAMETERS_H
// #define NDEBUG
// #define EIGEN_NO_DEBUG
// #define EIGEN_DONT_PARALLELIZE

#include <string>
#include <Eigen/CXX11/Tensor>
#include <Eigen/Core>




// Domain description --------------------------------------------------------//
constexpr double
    xDist           = 17.0,
    yDist           = 12.0,
    zDist           = 3.0,
    d               = 0.5,              // diameter of sphere
    xc              = 10.0,             // x-coordinate of sphere center
    yc              = 6.0,              // y-coordinate of sphere center
    zc              = 3.0,              // z-coordinate of sphere center
    u0              = 1.0,              // x-direction fluid velocity
    p0              = 0.0,              // Dirichlet pressure
    uStrct          = 0.0,              // x-direction sphere velocity
    vStrct          = 0.0,              // y-direction sphere velocity
    wStrct          = 0.0;              // z-direction sphere velocity

// Grid and structures description -------------------------------------------//
constexpr int
    nx              = 128,
    ny              = 60,
    nz              = 40,
    nSubx           = 50,
    nSuby           = 50,
    nSubz           = 50,
    is3Dor2D        = 3,

    ndim = nx*ny*nz,        ddim = is3Dor2D==3?7:5, gc = 2,
    nxt = nx + gc*2,        nyt = ny + gc*2,        nzt = nz + gc*2,
    iBgnStrct = 0,          jBgnStrct = 0 ,         kBgnStrct = 0,
    iEndStrct = nx,         jEndStrct = ny,         kEndStrct = nz,
    nSubTotal = nSubx*nSuby*nSubz;
constexpr double
    dx = xDist/double(nx), dy = yDist/double(ny), dz = zDist/double(nz),
    ddx = 1.0/dx, ddy = 1.0/dy, ddz = 1.0/dz;

// Simulation description ----------------------------------------------------//
const std::string
    velScheme       = "quick",
    solidPos        = "fixed",           // "fixed" or "moving"
    steadiness      = "none";           // "steady" or "unsteady"
constexpr int
    numOfThreads    = 4,
    timestepMax     = 2e5,
    pIterMax        = 5e3;
constexpr double
    Re              = 100.0,
    cflFactor       = 0.25,
    gridFoFactor    = 0.30,
    dtSizeOverride  = 1,
    pResidual       = 1.0e-10,          // residual criteria for P
    uResidual       = 1.0e-10,          // residual criteria for U
    vResidual       = 1.0e-10,          // residual criteria for V
    wResidual       = 1.0e-10,          // residual criteria for W
    pRestartFactor  = 0.6*pResidual;    // BiCGSTAB restart limit

// Filer description ---------------------------------------------------------//
constexpr int
    filerSteps      = 1;
const std::string
    filePath        = "../data/",
    fileUniqueName  = "cavityTest_"
                      + std::to_string(nx) + "x" + std::to_string(ny) + "x"
                      + std::to_string(nz) + "_" + velScheme
                      + "_Re" + std::to_string(int(Re)),
    comments        = "Testing cavity flow.";

// Variable definitions ------------------------------------------------------//
// Defined in main.cpp
extern int
    timestep, pIter, pIterTotal, pRestarts;
extern double
    dt, simTime, runTime,
    mChangeMax, pChangeMax, uChangeMax, vChangeMax, wChangeMax;
extern Eigen::Matrix<double, -1, 1>
    X, Y, Z, DX, DY, DZ, DXM, DYM, DZM, PB, PX;
extern Eigen::Matrix<double, -1, -1>
    PA;
extern Eigen::TensorFixedSize<double, Eigen::Sizes<nxt, nyt, nzt>>
    U, V, W, US, VS, WS, P, ETA, FX, FY, FZ;



#endif
    // Eigen::Tensor<double, 3, 0, std::ptrdiff_t>& U,