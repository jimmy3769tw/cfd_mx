/**
 * @file        velocitySchemers.cpp
 * 
 * @project     3D N-S solver rebuild
 * @version     1.6
 * 
 * @author      Syed Ahmad Raza (sahmadrz@gmail.com)
 * @date        2019-11-05
 * 
 * @brief       Apply the selected velocity scheme for the Navier-Stokes solver.
 * 
 */

#ifndef VELOCITYSCHEMERS_H
#define VELOCITYSCHEMERS_H

#include "parameters.h"
#include <functional>

using schemer = std::function<void(
    double&, double&, double&, double&, double&, double&,
    double&, double&, double&, double&, double&, double&,
    Eigen::TensorFixedSize<double, Eigen::Sizes<nxt, nyt, nzt>>&,
    Eigen::TensorFixedSize<double, Eigen::Sizes<nxt, nyt, nzt>>&,
    Eigen::TensorFixedSize<double, Eigen::Sizes<nxt, nyt, nzt>>&,
    int&, int&, int&)>;

extern schemer centralU;
extern schemer centralV;
extern schemer centralW;
extern schemer upwindU;
extern schemer upwindV;
extern schemer upwindW;
extern schemer quickU;
extern schemer quickV;
extern schemer quickW;

void velocitySchemer(schemer schemeU, schemer schemeV, schemer schemeW);

#endif