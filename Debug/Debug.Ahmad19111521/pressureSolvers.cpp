/**
 * @file        pressureSolvers.cpp
 * 
 * @project     3D N-S solver rebuild
 * @version     1.6
 * 
 * @author      Syed Ahmad Raza (sahmadrz@gmail.com)
 * @date        2019-11-05
 * 
 * @brief       Functions for solving the pressure Poisson equation for the
 *              Navier-Stokes solver.
 * 
 */

#include "pressureSolvers.h"
#include "parameters.h"
#include "printers.h"
#include <iostream>
#include <cmath>
#include <stdexcept>
#include <omp.h>

using namespace std;
using Eigen::Matrix;

void pressureVectorConstructor()
/*
    Construct the vector PB of the equation AX=PB and initialize the solution
    vector PX to zero.
*/
{
    double ddt = 1.0/dt;
    #pragma omp parallel for default(none) shared(PB, US, VS, WS, ddt)\
            reduction(max:mChangeMax)
    for (int i=0; i<nx; ++i)
    {
        for (int j=0; j<ny; ++j)
        {
            for (int k=0; k<nz; ++k)
            {
                double ig = i+gc, jg = j+gc, kg = k+gc;
                double mChange = (  (US(ig,jg,kg) - US(ig-1,jg,kg))*ddx
                                  + (VS(ig,jg,kg) - VS(ig,jg-1,kg))*ddy
                                  + (WS(ig,jg,kg) - WS(ig,jg,kg-1))*ddz );

                if (abs(mChange) > mChangeMax&&
                     i!=nx-1 && j!=ny-1 && k!=nz-1 && i!= 0 && j!=0 && k!=0)
                { mChangeMax = abs(mChange);}

                int l = k + j*nz + i*ny*nz;
                PB(l) = -ddt*mChange;
            }
        }
    }
}


void pressureVectorConstructor_vector(std::vector<double>  &rhs)
/*
    Construct the vector PB of the equation AX=PB and initialize the solution
    vector PX to zero.
*/
{
    double ddt = 1.0/dt;

    #pragma omp parallel for default(none) shared(rhs, US, VS, WS, ddt)\
            reduction(max:mChangeMax)
    for (int i=0; i<nx; ++i)
    {
        for (int j=0; j<ny; ++j)
        {
            for (int k=0; k<nz; ++k)
            {
                double ig = i+gc, jg = j+gc, kg = k+gc;
                double mChange = (  (US(ig,jg,kg) - US(ig-1,jg,kg))*ddx
                                  + (VS(ig,jg,kg) - VS(ig,jg-1,kg))*ddy
                                  + (WS(ig,jg,kg) - WS(ig,jg,kg-1))*ddz );

                if (abs(mChange) > mChangeMax&&
                     i!=nx-1 && j!=ny-1 && k!=nz-1 && i!= 0 && j!=0 && k!=0)
                { mChangeMax = abs(mChange);}

                int l = k + j*nz + i*ny*nz;
                rhs.at(l) = -ddt*mChange;
            }
        }
    }
}

void pressureMatrixConstructor()
/*
    Construct the matrix A of the equation AX=B.
*/
{
    #pragma omp parallel for default(none) shared(PA)
    for (int i=0; i<nx; ++i)
    {
        for (int j=0; j<ny; ++j)
        {
            for (int k=0; k<nz; ++k)
            {
                int l = k + j*nz + i*nz*ny;
                double ip, im, jp, jm, kp, km, ijk;

                ijk = 2.0/dx/dx + 2.0/dy/dy + 2.0/dz/dz;
                im = -1.0/dx/dx;
                ip = -1.0/dx/dx;
                jm = -1.0/dy/dy;
                jp = -1.0/dy/dy;
                km = -1.0/dz/dz;
                kp = -1.0/dz/dz;

                if (i==0)    { ijk -= 1.0/dx/dx; }
                if (i==nx-1) { ijk -= 1.0/dx/dx; }
                if (j==0)    { ijk -= 1.0/dy/dy; }
                if (j==ny-1) { ijk -= 1.0/dy/dy; }
                if (k==0)    { ijk -= 1.0/dz/dz; }
                if (k==nz-1) { ijk -= 1.0/dz/dz; }

                if (i!=0)    { PA(l, 0) = im; }
                if (j!=0)    { PA(l, 1) = jm; }
                if (k!=0)    { PA(l, 2) = km; }
                               PA(l, 3) = ijk;
                if (k!=nz-1) { PA(l, 4) = kp; }
                if (j!=ny-1) { PA(l, 5) = jp; }
                if (i!=nx-1) { PA(l, 6) = ip; }
            }
        }
    }
}



double normVector(Matrix<double, -1, 1>& X)
{
    double result = 0.0;
    #pragma omp parallel for default(none) shared(X) reduction(+:result)
    for (int h=0; h<ndim; ++h)
    {
        result += X(h)*X(h);
    }
    return sqrt(result);
}



double normVector(std::vector<double> & X)
{

    double result = 0.0;
    #pragma omp parallel for default(none) shared(X) reduction(+:result)
    for (int h=0; h<ndim; ++h)
    {
        result += X.at(h)*X.at(h);
    }
    return sqrt(result);
}





std::tuple<std::vector<int>, std::vector<int>, std::vector<double> >
pressureMatrixConstructor_CSR()
/*
    Construct the matrix A of the equation AX=B.
*/
{
    std::vector<int>  ptr, idx;
    std::vector<double>  val;
    ptr.clear(); ptr.reserve(ndim + 1); ptr.push_back(0);
    idx.clear(); idx.reserve(ndim * 7); // We use 7-point stencil, so the matrix
    val.clear(); val.reserve(ndim * 7); // will have at most n3 * 7 nonzero elements.
// Interior point. Use 7-point finite difference stencil.


    for (int i=0; i<nx; ++i)
    {
        for (int j=0; j<ny; ++j)
        {
            for (int k=0; k<nz; ++k)
            {
                int l = k + j*nz + i*nz*ny;
                double ip, im, jp, jm, kp, km, ijk;

                ijk = 2.0/dx/dx + 2.0/dy/dy + 2.0/dz/dz;
                im = -1.0/dx/dx;
                ip = -1.0/dx/dx;
                jm = -1.0/dy/dy;
                jp = -1.0/dy/dy;
                km = -1.0/dz/dz;
                kp = -1.0/dz/dz;

                if (i==0)    { ijk -= 1.0/dx/dx; }
                if (i==nx-1) { ijk -= 1.0/dx/dx; }
                if (j==0)    { ijk -= 1.0/dy/dy; }
                if (j==ny-1) { ijk -= 1.0/dy/dy; }
                if (k==0)    { ijk -= 1.0/dz/dz; }
                if (k==nz-1) { ijk -= 1.0/dz/dz; }

                if (i!=0)    { idx.push_back(l-nx*ny);  val.push_back(im); }
                if (j!=0)    { idx.push_back(l-nx);     val.push_back(jm); }
                if (k!=0)    { idx.push_back(l-1);      val.push_back(km); }
                               idx.push_back(l);        val.push_back(ijk);
                if (k!=nz-1) { idx.push_back(l+1);      val.push_back(kp); }
                if (j!=ny-1) { idx.push_back(l+nx);     val.push_back(jp); }
                if (i!=nx-1) { idx.push_back(l+nx*ny);  val.push_back(ip); }
                ptr.push_back( idx.size());
            }
        }
    }


    return std::make_tuple(ptr, idx, val);

}



double dotProductVectors(Matrix<double, -1, 1>& X, Matrix<double, -1, 1>& Y)
{
    double result = 0.0;
    #pragma omp parallel for default(none) shared(X, Y) reduction(+:result)
    for (int h=0; h<ndim; ++h)
    {
        result += X(h)*Y(h);
    }
    return result;
}



double dotProductVectors(std::vector<double> & X,std::vector<double> & Y)
{
    double result = 0.0;
    #pragma omp parallel for default(none) shared(X, Y) reduction(+:result)
    for (int h=0; h<ndim; ++h)
    {
        result += X.at(h)*Y.at(h);
    }
    return result;
}


void multiplyMatrixVector(Matrix<double, -1, 1>& PRD,
    Matrix<double, -1, -1>& MAT, Matrix<double, -1, 1>& VEC)
{
    #pragma omp parallel for default(none) shared(PRD, MAT, VEC, cout)
    for (int r=0; r<ndim; r++)
    {
        double* vec = (VEC.data()+r);
        double* mat = (MAT.data()+r);
        double prd = 0.0;
        if (r-ny*nz >= 0)   { prd += *(mat+0*ndim) * (*(vec-nz*ny)); }
        if (r-nz >= 0)      { prd += *(mat+1*ndim) * (*(vec-nz));    }
        if (r-1 >= 0)       { prd += *(mat+2*ndim) * (*(vec-1));     }
                              prd += *(mat+3*ndim) * (*(vec));
        if (r+1 < ndim)     { prd += *(mat+4*ndim) * (*(vec+1));     }
        if (r+nz < ndim)    { prd += *(mat+5*ndim) * (*(vec+nz));    }
        if (r+ny*nz < ndim) { prd += *(mat+6*ndim) * (*(vec+nz*ny)); }
        PRD(r) = prd;
    }
}



void multiplyMatrixVector(std::vector<double>& PRD,
    Matrix<double, -1, -1>& MAT, std::vector<double> & VEC)
{
    #pragma omp parallel for default(none) shared(PRD, MAT, VEC, cout)
    for (int r=0; r<ndim; r++)
    {
        double* vec = (VEC.data()+r);
        double* mat = (MAT.data()+r);
        double prd = 0.0;
        if (r-ny*nz >= 0)   { prd += *(mat+0*ndim) * (*(vec-nz*ny)); }
        if (r-nz >= 0)      { prd += *(mat+1*ndim) * (*(vec-nz));    }
        if (r-1 >= 0)       { prd += *(mat+2*ndim) * (*(vec-1));     }
                              prd += *(mat+3*ndim) * (*(vec));
        if (r+1 < ndim)     { prd += *(mat+4*ndim) * (*(vec+1));     }
        if (r+nz < ndim)    { prd += *(mat+5*ndim) * (*(vec+nz));    }
        if (r+ny*nz < ndim) { prd += *(mat+6*ndim) * (*(vec+nz*ny)); }
        PRD.at(r) = prd;
    }
}

void bCGSTAB_vector(std::vector<double> &PB, std::vector<double> &PX){
    std::vector<double>
        R0(ndim), R1(ndim), P1(ndim), S1(ndim), AP(ndim), AS(ndim);

    double
        rr = 0, r0 = 0, a = 0, w = 0, b = 0, norm0 = normVector(PB);

    #pragma omp parallel for default(none) shared(PX, R0, PB, R1, P1)
    for (int r = 0; r < ndim; ++r)
    {
        PX.at(r) = 0.0;
        R0.at(r) = PB.at(r);
        R1.at(r) = R0.at(r);
        P1.at(r) = R0.at(r);
    }

    rr = dotProductVectors(R1,R0);
    pIter = 0;
    while (pIter < pIterMax)
    {
        multiplyMatrixVector(AP, PA, P1);
        a  = rr / dotProductVectors(AP, R0);

        #pragma omp parallel for default(none) shared(S1, R1, AP, a)
        for (int p=0; p<ndim; ++p) { S1.at(p) = R1.at(p) - a*AP.at(p); }  // 3.

        multiplyMatrixVector(AS, PA, S1);
        w  = dotProductVectors(AS, S1) / dotProductVectors(AS, AS);  //2. 

        #pragma omp parallel for default(none) shared(PX, P1, S1, R1, AS, a, w)
        for (int p=0; p<ndim; ++p)
        {
            PX.at(p) += a*P1.at(p) + w*S1.at(p);
            R1.at(p)    = S1.at(p) - w*AS.at(p);
        }

        if ((normVector(R1) / norm0) < pResidual)
        {
            break;
        }

        r0 = rr;
        rr = dotProductVectors(R1, R0);
        b  = (a / w) * (rr / r0);

        // Check rho for restart
        if (abs(rr) > pResidual*pRestartFactor)
        {
            #pragma omp parallel for default(none) shared(P1, R1, AP, b, w)
            for (int p=0; p<ndim; ++p) { P1.at(p) = R1.at(p) + b*(P1.at(p) - w*AP.at(p)); }
        }
        else
        {
            #pragma omp parallel for default(none) shared(R0, R1, P1)
            for (int r = 0; r < ndim; ++r)
            {
                R0.at(r) = R1.at(r);
                P1.at(r) = R1.at(r);
            }
            ++pRestarts;
        }
        ++pIter;
        ++pIterTotal;
    }
}




void bCGSTAB()
{

    Matrix<double, -1, 1>
        R0(ndim), R1(ndim), P1(ndim), S1(ndim), AP(ndim), AS(ndim);
    double
        rr = 0, r0 = 0, a = 0, w = 0, b = 0, norm0 = normVector(PB);

    #pragma omp parallel for default(none) shared(PX, R0, PB, R1, P1)
    for (int r = 0; r < ndim; ++r)
    {
        PX(r) = 0.0;
        R0(r) = PB(r);
        R1(r) = R0(r);
        P1(r) = R0(r);
    }
    rr = dotProductVectors(R1,R0);
    pIter = 0;
    while (pIter < pIterMax)
    {
        multiplyMatrixVector(AP, PA, P1);
        a  = rr / dotProductVectors(AP, R0);

        #pragma omp parallel for default(none) shared(S1, R1, AP, a)
        for (int p=0; p<ndim; ++p) { S1(p) = R1(p) - a*AP(p); }  // 3.

        multiplyMatrixVector(AS, PA, S1);
        w  = dotProductVectors(AS, S1) / dotProductVectors(AS, AS);  //2. 

        #pragma omp parallel for default(none) shared(PX, P1, S1, R1, AS, a, w)
        for (int p=0; p<ndim; ++p)
        {
            PX(p) += a*P1(p) + w*S1(p);
            R1(p) = S1(p) - w*AS(p);
        }

        if ((normVector(R1) / norm0) < pResidual)
        {
            break;
        }

        r0 = rr;
        rr = dotProductVectors(R1, R0);
        b  = (a / w) * (rr / r0);

        // Check rho for restart
        if (abs(rr) > pResidual*pRestartFactor)
        {
            #pragma omp parallel for default(none) shared(P1, R1, AP, b, w)
            for (int p=0; p<ndim; ++p) { P1(p) = R1(p) + b*(P1(p) - w*AP(p)); }
        }
        else
        {
            #pragma omp parallel for default(none) shared(R0, R1, P1)
            for (int r = 0; r < ndim; ++r)
            {
                R0(r) = R1(r);
                P1(r) = R1(r);
            }
            ++pRestarts;
        }
        ++pIter;
        ++pIterTotal;
    }
}

/*-------------------------------- Extra code --------------------------------*/
/* 
void pressureVectorConstructor_averaged()
{
    double bAvg = 0.0, ddt = 1.0/dt;
    #pragma omp parallel for default(none) shared(PB, US, VS, WS, ddt)\
            reduction(max:mChangeMax) reduction(+:bAvg)
    for (int i=0; i<nx; ++i)
    {
        for (int j=0; j<ny; ++j)
        {
            for (int k=0; k<nz; ++k)
            {
                double ig = i+gc, jg = j+gc, kg = k+gc;
                double mChange = (  (US(ig,jg,kg) - US(ig-1,jg,kg))*ddx
                                  + (VS(ig,jg,kg) - VS(ig,jg-1,kg))*ddy
                                  + (WS(ig,jg,kg) - WS(ig,jg,kg-1))*ddz );

                if (abs(mChange) > mChangeMax
                    && i!=nx-1 && j!=ny-1 && k!=nz-1 && i!= 0 && j!=0 && k!=0)
                {
                    mChangeMax = abs(mChange);
                }

                int l = k + j*nz + i*ny*nz;
                PB(l) = -ddt*mChange;
                bAvg += PB(l);
            }
        }
    }
    bAvg = bAvg/double(ndim);
    #pragma omp parallel for default(none) shared(PB, bAvg)
    for (int l=0; l<ndim; ++l)
    {
        PB(l) = PB(l) - bAvg;        
    }
}

void bCGSTAB_eigen()
{
    Matrix<double, -1, 1>
        R0(ndim), R1(ndim), P1(ndim), AP(ndim), S1(ndim), AS(ndim);
    double rr = 0, r0 = 0, a = 0, w = 0, b = 0, norm0 = PB.norm();

    PX.setZero();
    R0 = PB;
    R1 = R0;
    P1 = R0;
    rr = R1.dot(R0);

    pIter = 0;
    while (pIter < pIterMax)
    {
        multiplyMatrixVector(AP,PA,P1);
        a  = rr / AP.dot(R0);

        S1 = R1 - a*AP;

        multiplyMatrixVector(AS,PA,S1);
        w  = AS.dot(S1) / AS.dot(AS);

        PX += a*P1 + w*S1;
        R1 = S1 - w*AS;

        if ((R1.norm() / norm0) < pResidual)
        {
            break;
        }

        r0 = rr;
        rr = R1.dot(R0);
        b  = (a / w) * (rr / r0);

        // Check rho for restart
        if (abs(rr) > pResidual*pRestartFactor)
        {
            P1 = R1 + b*(P1 - w*AP);
        }
        else
        {
            R0 = R1;
            P1 = R1;
            ++pRestarts;
        }
        ++pIter;
        ++pIterTotal;
    }
}
 */
/* 
void multiplyMatrixVector2D(Matrix<double, -1, 1>& PRD,
    Matrix<double, -1, -1>& MAT,  Matrix<double, -1, 1>& VEC)
{
    for (int r=0; r<ndim; ++r)
    {
        int d0 = (r-ny >= 0);
        int d1 = (r-1 >= 0);
        int d3 = (r+1 < ndim);
        int d4 = (r+ny < ndim);
        PRD(r)  = d0*MAT(r,0)*VEC(d0*(r-ny));
        PRD(r) += d1*MAT(r,1)*VEC(d1*(r-1));
        PRD(r) +=    MAT(r,2)*VEC(r);
        PRD(r) += d3*MAT(r,3)*VEC(d3*(r+1));
        PRD(r) += d4*MAT(r,4)*VEC(d4*(r+ny));
    }
}
*/