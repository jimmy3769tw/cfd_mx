/**
 * @file        conditions.h
 * 
 * @project     3D N-S solver rebuild
 * @version     1.6
 * 
 * @author      Syed Ahmad Raza (sahmadrz@gmail.com)
 * @date        2019-11-05
 * 
 * @brief       Generate the initial and boundary conditions for the Navier-
 *              Stokes solver.
 * 
 */

#ifndef CONDITIONS_H
#define CONDITIONS_H

void initialConditions();
void boundaryConditions();

#endif