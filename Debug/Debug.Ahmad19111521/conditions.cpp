/**
 * @file        conditions.cpp
 *
 * @project     3D N-S solver rebuild
 * @version     1.6
 *
 * @author      Syed Ahmad Raza (sahmadrz@gmail.com)
 * @date        2019-11-05
 *
 * @brief       Generate the initial and boundary conditions for the Navier-
 *              Stokes solver.
 *
 */

#include "conditions.h"
#include "parameters.h"
#include <iostream>
void initialConditions()
{
    #pragma omp parallel for default(none) shared(P, U, US, V, VS, W, WS)
    for (int i=0; i<nxt; ++i)
    {
        for (int j=0; j<nyt; ++j)
        {
            for (int k=0; k<nzt; ++k)
            {
                P(i,j,k)  = 0.0;
                U(i,j,k)  = u0;
                V(i,j,k)  = 0.0;
                W(i,j,k)  = 0.0;

                US(i,j,k) = U(i,j,k);
                VS(i,j,k) = V(i,j,k);
                WS(i,j,k) = W(i,j,k);
            }
        }
    }
}

/**
 * Cavity flow
 * -----------------------------------------------------------------------------
 *      Velocities: All Dirichlet (all 0 except u = u0 only at North).
 *      Pressure:   All Dirichlet.
 * -----------------------------------------------------------------------------
void boundaryConditions()
{



    // std::vector<double> num = 


    #pragma omp parallel for default(none) shared(P, U, US, V, VS, W, WS)
    for (int j=gc; j<nyt-gc; ++j)
    {
        for (int k=gc; k<nzt-gc; ++k)
        {
            // West ----------------------------------------------------------//0
            P(0,j,k) = P(gc,j,k); // num
            P(1,j,k) = P(gc,j,k); // num

            U(0,j,k) = 0.0;                //  no-slip
            U(1,j,k) = 0.0;                //  no-slip
            V(0,j,k) = -V(gc,j,k) + 2*0.0; //  no-slip
            V(1,j,k) = -V(gc,j,k) + 2*0.0; //  no-slip
            W(0,j,k) = -W(gc,j,k) + 2*0.0; //  no-slip
            W(1,j,k) = -W(gc,j,k) + 2*0.0; //  no-slip

            US(0,j,k) = U(0,j,k);    
            US(1,j,k) = U(1,j,k);   
            VS(0,j,k) = V(0,j,k);   
            VS(1,j,k) = V(1,j,k);   
            WS(0,j,k) = W(0,j,k);   
            WS(1,j,k) = W(1,j,k);   

    }}
    for (int j=gc; j<nyt-gc; ++j)
    {
        for (int k=gc; k<nzt-gc; ++k)
        {
            // East ----------------------------------------------------------//1
            P(nxt-1,j,k) = P(nxt-1-gc,j,k);
            P(nxt-2,j,k) = P(nxt-1-gc,j,k);

            U(nxt-2,j,k) = 0.0;                      // no-slip
            U(nxt-3,j,k) = 0.0;                      // no-slip

            V(nxt-1,j,k) = -V(nxt-1-gc,j,k) + 2*0.0; // no-slip
            V(nxt-2,j,k) = -V(nxt-1-gc,j,k) + 2*0.0; // no-slip
            W(nxt-1,j,k) = -W(nxt-1-gc,j,k) + 2*0.0; // no-slip
            W(nxt-2,j,k) = -W(nxt-1-gc,j,k) + 2*0.0; // no-slip

            US(nxt-2,j,k) = U(nxt-2,j,k);
            US(nxt-3,j,k) = U(nxt-3,j,k);
            VS(nxt-1,j,k) = V(nxt-1,j,k);
            VS(nxt-2,j,k) = V(nxt-2,j,k);
            WS(nxt-1,j,k) = W(nxt-1,j,k);
            WS(nxt-2,j,k) = W(nxt-2,j,k);
        }
    }

    #pragma omp parallel for default(none) shared(P, U, US, V, VS, W, WS)
    for (int i=gc; i<nxt-gc; ++i)
    {
        for (int k=gc; k<nzt-gc; ++k)
        {
            // South ---------------------------------------------------------//2
            P(i,0,k) = P(i,gc,k);
            P(i,1,k) = P(i,gc,k);

            U(i,0,k) = -U(i,gc,k) + 2.0*0.0;    //  no-slip
            U(i,1,k) = -U(i,gc,k) + 2.0*0.0;    //  no-slip
            V(i,0,k) = 0.0;                     //  no-slip
            V(i,1,k) = 0.0;                     //  no-slip
            W(i,0,k) = -W(i,gc,k) + 2.0*0.0;    //  no-slip
            W(i,1,k) = -W(i,gc,k) + 2.0*0.0;    //  no-slip

            US(i,0,k) = U(i,0,k);
            US(i,1,k) = U(i,1,k);
            VS(i,0,k) = V(i,0,k);
            VS(i,1,k) = V(i,1,k);
            WS(i,0,k) = W(i,0,k);
            WS(i,1,k) = W(i,1,k);
        }
    }

    #pragma omp parallel for default(none) shared(P, U, US, V, VS, W, WS)
    for (int i=gc; i<nxt-gc; ++i)
    {
        for (int k=gc; k<nzt-gc; ++k)
        {
            // North ---------------------------------------------------------//3
            P(i,nyt-1,k) = P(i,nyt-1-gc,k);
            P(i,nyt-2,k) = P(i,nyt-1-gc,k);

            U(i,nyt-1,k) = -U(i,nyt-1-gc,k) + 2.0*u0;  // dir
            U(i,nyt-2,k) = -U(i,nyt-1-gc,k) + 2.0*u0;  // dir
            V(i,nyt-2,k) = 0.0;                        // no-slip
            V(i,nyt-3,k) = 0.0;                        // no-slip
            W(i,nyt-1,k) = -W(i,nyt-1-gc,k) + 2.0*0.0; //  no-slip
            W(i,nyt-2,k) = -W(i,nyt-1-gc,k) + 2.0*0.0; //  no-slip

            US(i,nyt-1,k) = U(i,nyt-1,k);
            US(i,nyt-2,k) = U(i,nyt-2,k);
            VS(i,nyt-2,k) = V(i,nyt-2,k);
            VS(i,nyt-3,k) = V(i,nyt-3,k);
            WS(i,nyt-1,k) = W(i,nyt-1,k);
            WS(i,nyt-2,k) = W(i,nyt-2,k);
        }
    }


    #pragma omp parallel for default(none) shared(P, U, US, V, VS, W, WS)
    for (int i=gc; i<nxt-gc; ++i)
    {
        for (int j=gc; j<nyt-gc; ++j)
        {
            // Back ----------------------------------------------------------//4
            P(i,j,0) = P(i,j,gc);
            P(i,j,1) = P(i,j,gc);

            U(i,j,0) = -U(i,j,gc) + 2*0.0;
            U(i,j,1) = -U(i,j,gc) + 2*0.0;
            V(i,j,0) = -V(i,j,gc) + 2*0.0;
            V(i,j,1) = -V(i,j,gc) + 2*0.0;
            W(i,j,0) = 0.0;
            W(i,j,1) = 0.0;

            US(i,j,0) = U(i,j,0);
            US(i,j,1) = U(i,j,1);
            VS(i,j,0) = V(i,j,0);
            VS(i,j,1) = V(i,j,1);
            WS(i,j,0) = W(i,j,0);
            WS(i,j,1) = W(i,j,1);


    }}

    #pragma omp parallel for default(none) shared(P, U, US, V, VS, W, WS)
    for (int i=gc; i<nxt-gc; ++i)
    {
        for (int j=gc; j<nyt-gc; ++j)
        {
            // Front ---------------------------------------------------------//5
            P(i,j,nzt-1) = P(i,j,nzt-1-gc);
            P(i,j,nzt-2) = P(i,j,nzt-1-gc);

            U(i,j,nzt-1) = -U(i,j,nzt-1-gc) + 2*0.0;
            U(i,j,nzt-2) = -U(i,j,nzt-1-gc) + 2*0.0;
            V(i,j,nzt-1) = -V(i,j,nzt-1-gc) + 2*0.0;
            V(i,j,nzt-2) = -V(i,j,nzt-1-gc) + 2*0.0;
            W(i,j,nzt-2) = 0.0;
            W(i,j,nzt-3) = 0.0;

            US(i,j,nzt-1) = U(i,j,nzt-1);
            US(i,j,nzt-2) = U(i,j,nzt-2);
            VS(i,j,nzt-1) = V(i,j,nzt-1);
            VS(i,j,nzt-2) = V(i,j,nzt-2);
            WS(i,j,nzt-2) = W(i,j,nzt-2);
            WS(i,j,nzt-3) = W(i,j,nzt-3);

        }
    }
}
 */

/**
 * Investigation domain
 * -----------------------------------------------------------------------------
 *      Velocities: All Dirichlet (all 0 except u = u0 at all boundaries) except
 *                  outlet (East).
 *      Pressure:   All Dirichlet except outlet (East).
 * -----------------------------------------------------------------------------
 */
/*
void boundaryConditions()
{
    #pragma omp parallel for default(none) shared(P, U, US, V, VS, W, WS)
    for (int k=gc; k<nzt-gc; ++k)
    {
        for (int j=gc; j<nyt-gc; ++j)
        {
            // West ------------------------------------------------------------
            P(0,j,k) = P(gc,j,k);
            P(1,j,k) = P(gc,j,k);

            U(0,j,k) = u0;                      // dir
            U(1,j,k) = u0;                      // dir
            V(0,j,k) = -V(gc,j,k) + 2*0.0;      //  no-slip
            V(1,j,k) = -V(gc,j,k) + 2*0.0;      //  no-slip
            W(0,j,k) = -W(gc,j,k) + 2*0.0;      //  no-slip
            W(1,j,k) = -W(gc,j,k) + 2*0.0;      //  no-slip

            US(0,j,k) = U(0,j,k);           US(1,j,k) = U(1,j,k);
            VS(0,j,k) = V(0,j,k);           VS(1,j,k) = V(1,j,k);
            WS(0,j,k) = W(0,j,k);           WS(1,j,k) = W(1,j,k);

            // East ------------------------------------------------------------
            P(nxt-1,j,k) = P(nxt-1-gc,j,k);
            P(nxt-2,j,k) = P(nxt-1-gc,j,k);

            U(nxt-2,j,k) = U(nxt-2-gc,j,k);
            U(nxt-3,j,k) = U(nxt-2-gc,j,k);
            V(nxt-1,j,k) = V(nxt-1-gc,j,k);
            V(nxt-2,j,k) = V(nxt-1-gc,j,k);
            W(nxt-1,j,k) = W(nxt-1-gc,j,k);
            W(nxt-2,j,k) = W(nxt-1-gc,j,k);

            US(nxt-2,j,k) = U(nxt-2,j,k);   US(nxt-3,j,k) = U(nxt-3,j,k);
            VS(nxt-1,j,k) = V(nxt-1,j,k);   VS(nxt-2,j,k) = V(nxt-2,j,k);
            WS(nxt-1,j,k) = W(nxt-1,j,k);   WS(nxt-2,j,k) = W(nxt-2,j,k);
        }
        for (int i=gc; i<nxt-gc; ++i)
        {
            // South -----------------------------------------------------------
            P(i,0,k) = P(i,gc,k);
            P(i,1,k) = P(i,gc,k);

            U(i,0,k) = -U(i,gc,k) + 2*0.0;
            U(i,1,k) = -U(i,gc,k) + 2*0.0;
            V(i,0,k) = 0.0;
            V(i,1,k) = 0.0;
            W(i,0,k) = -W(i,gc,k) + 2*0.0;
            W(i,1,k) = -W(i,gc,k) + 2*0.0;

            US(i,0,k) = U(i,0,k);           US(i,1,k) = U(i,1,k);
            VS(i,0,k) = V(i,0,k);           VS(i,1,k) = V(i,1,k);
            WS(i,0,k) = W(i,0,k);           WS(i,1,k) = W(i,1,k);

            // North -----------------------------------------------------------
            P(i,nyt-1,k) = P(i,nyt-1-gc,k);
            P(i,nyt-2,k) = P(i,nyt-1-gc,k);

            U(i,nyt-1,k) = -U(i,nyt-1-gc,k) + 2*0.0;
            U(i,nyt-2,k) = -U(i,nyt-1-gc,k) + 2*0.0;
            V(i,nyt-2,k) = 0.0;
            V(i,nyt-3,k) = 0.0;
            W(i,nyt-1,k) = -W(i,nyt-1-gc,k) + 2*0.0;;
            W(i,nyt-2,k) = -W(i,nyt-1-gc,k) + 2*0.0;;

            US(i,nyt-1,k) = U(i,nyt-1,k);   US(i,nyt-2,k) = U(i,nyt-2,k);
            VS(i,nyt-2,k) = V(i,nyt-2,k);   VS(i,nyt-3,k) = V(i,nyt-3,k);
            WS(i,nyt-1,k) = W(i,nyt-1,k);   WS(i,nyt-2,k) = W(i,nyt-2,k);
        }
    }
    #pragma omp parallel for default(none) shared(P, U, US, V, VS, W, WS)
    for (int j=gc; j<nyt-gc; ++j)
    {
        for (int i=gc; i<nxt-gc; ++i)
        {
            // Back ------------------------------------------------------------
            P(i,j,0) = P(i,j,gc);
            P(i,j,1) = P(i,j,gc);

            U(i,j,0) = -U(i,j,gc) + 2*0.0;
            U(i,j,1) = -U(i,j,gc) + 2*0.0;
            V(i,j,0) = -V(i,j,gc) + 2*0.0;
            V(i,j,1) = -V(i,j,gc) + 2*0.0;
            W(i,j,0) = 0.0;
            W(i,j,1) = 0.0;

            US(i,j,0) = U(i,j,0);           US(i,j,1) = U(i,j,1);
            VS(i,j,0) = V(i,j,0);           VS(i,j,1) = V(i,j,1);
            WS(i,j,0) = W(i,j,0);           WS(i,j,1) = W(i,j,1);

            // Front -----------------------------------------------------------
            P(i,j,nzt-1) = P(i,j,nzt-1-gc);
            P(i,j,nzt-2) = P(i,j,nzt-1-gc);

            U(i,j,nzt-1) = -U(i,j,nzt-1-gc) + 2*0.0;
            U(i,j,nzt-2) = -U(i,j,nzt-1-gc) + 2*0.0;
            V(i,j,nzt-1) = -V(i,j,nzt-1-gc) + 2*0.0;
            V(i,j,nzt-2) = -V(i,j,nzt-1-gc) + 2*0.0;
            W(i,j,nzt-2) = 0.0;
            W(i,j,nzt-3) = 0.0;

            US(i,j,nzt-1) = U(i,j,nzt-1);   US(i,j,nzt-2) = U(i,j,nzt-2);
            VS(i,j,nzt-1) = V(i,j,nzt-1);   VS(i,j,nzt-2) = V(i,j,nzt-2);
            WS(i,j,nzt-2) = W(i,j,nzt-2);   WS(i,j,nzt-3) = W(i,j,nzt-3);
        }
    }
}
*/

/**
 * Channel flow
 * -----------------------------------------------------------------------------
 *      Velocities: All Dirichlet (all 0 except u = u0 only at West) except
 *                  outlet (East).
 *      Pressure:   All Neumann.
 * -----------------------------------------------------------------------------
 */

// void boundaryConditions()
// {
//     #pragma omp parallel for default(none) shared(P, U, US, V, VS, W, WS)
//     for (int k=gc; k<nzt-gc; ++k)
//     {
//         for (int j=gc; j<nyt-gc; ++j)
//         {
//             // West ------------------------------------------------------------
//             P(0,j,k) = P(gc,j,k);
//             P(1,j,k) = P(gc,j,k);

//             U(0,j,k) = u0;
//             U(1,j,k) = u0;
//             V(0,j,k) = -V(gc,j,k) + 2*0.0;
//             V(1,j,k) = -V(gc,j,k) + 2*0.0;
//             W(0,j,k) = -W(gc,j,k) + 2*0.0;
//             W(1,j,k) = -W(gc,j,k) + 2*0.0;

//             US(0,j,k) = U(0,j,k);           US(1,j,k) = U(1,j,k);
//             VS(0,j,k) = V(0,j,k);           VS(1,j,k) = V(1,j,k);
//             WS(0,j,k) = W(0,j,k);           WS(1,j,k) = W(1,j,k);

//             // East ------------------------------------------------------------
//             P(nxt-1,j,k) = P(nxt-1-gc,j,k);
//             P(nxt-2,j,k) = P(nxt-1-gc,j,k);

//             U(nxt-2,j,k) = U(nxt-2-gc,j,k);  //num
//             U(nxt-3,j,k) = U(nxt-2-gc,j,k);  //num
//             V(nxt-1,j,k) = V(nxt-1-gc,j,k);  //num
//             V(nxt-2,j,k) = V(nxt-1-gc,j,k);  //num
//             W(nxt-1,j,k) = W(nxt-1-gc,j,k);  //num
//             W(nxt-2,j,k) = W(nxt-1-gc,j,k);  //num

//             US(nxt-2,j,k) = U(nxt-2,j,k);   US(nxt-3,j,k) = U(nxt-3,j,k);
//             VS(nxt-1,j,k) = V(nxt-1,j,k);   VS(nxt-2,j,k) = V(nxt-2,j,k);
//             WS(nxt-1,j,k) = W(nxt-1,j,k);   WS(nxt-2,j,k) = W(nxt-2,j,k);
//         }
//         for (int i=gc; i<nxt-gc; ++i)
//         {
//             // South -----------------------------------------------------------
//             P(i,0,k) = P(i,gc,k);
//             P(i,1,k) = P(i,gc,k);

//             U(i,0,k) = -U(i,gc,k) + 2*0.0; // no-slip
//             U(i,1,k) = -U(i,gc,k) + 2*0.0; // no-slip
//             V(i,0,k) = 0.0;                // no-slip
//             V(i,1,k) = 0.0;                // no-slip
//             W(i,0,k) = -W(i,gc,k) + 2*0.0; // no-slip
//             W(i,1,k) = -W(i,gc,k) + 2*0.0; // no-slip

//             US(i,0,k) = U(i,0,k);           US(i,1,k) = U(i,1,k);
//             VS(i,0,k) = V(i,0,k);           VS(i,1,k) = V(i,1,k);
//             WS(i,0,k) = W(i,0,k);           WS(i,1,k) = W(i,1,k);

//             // North -----------------------------------------------------------
//             P(i,nyt-1,k) = P(i,nyt-1-gc,k);
//             P(i,nyt-2,k) = P(i,nyt-1-gc,k);

//             U(i,nyt-1,k) = -U(i,nyt-1-gc,k) + 2*0.0;  //  no-slip
//             U(i,nyt-2,k) = -U(i,nyt-1-gc,k) + 2*0.0;  //  no-slip
//             V(i,nyt-2,k) = 0.0;                       //  no-slip
//             V(i,nyt-3,k) = 0.0;                       //  no-slip
//             W(i,nyt-1,k) = -W(i,nyt-1-gc,k) + 2*0.0;; //  no-slip
//             W(i,nyt-2,k) = -W(i,nyt-1-gc,k) + 2*0.0;; //  no-slip

//             US(i,nyt-1,k) = U(i,nyt-1,k);   US(i,nyt-2,k) = U(i,nyt-2,k);
//             VS(i,nyt-2,k) = V(i,nyt-2,k);   VS(i,nyt-3,k) = V(i,nyt-3,k);
//             WS(i,nyt-1,k) = W(i,nyt-1,k);   WS(i,nyt-2,k) = W(i,nyt-2,k);
//         }
//     }
//     #pragma omp parallel for default(none) shared(P, U, US, V, VS, W, WS)
//     for (int j=gc; j<nyt-gc; ++j)
//     {
//         for (int i=gc; i<nxt-gc; ++i)
//         {
//             // Back ------------------------------------------------------------
//             P(i,j,0) = P(i,j,gc);
//             P(i,j,1) = P(i,j,gc);

//             U(i,j,0) = -U(i,j,gc) + 2*0.0;  // no-slip
//             U(i,j,1) = -U(i,j,gc) + 2*0.0;  // no-slip
//             V(i,j,0) = -V(i,j,gc) + 2*0.0;  // no-slip
//             V(i,j,1) = -V(i,j,gc) + 2*0.0;  // no-slip
//             W(i,j,0) = 0.0;
//             W(i,j,1) = 0.0;

//             US(i,j,0) = U(i,j,0);           US(i,j,1) = U(i,j,1);
//             VS(i,j,0) = V(i,j,0);           VS(i,j,1) = V(i,j,1);
//             WS(i,j,0) = W(i,j,0);           WS(i,j,1) = W(i,j,1);

//             // Front -----------------------------------------------------------
//             P(i,j,nzt-1) = P(i,j,nzt-1-gc);
//             P(i,j,nzt-2) = P(i,j,nzt-1-gc);

//             U(i,j,nzt-1) = -U(i,j,nzt-1-gc) + 2*0.0;   // non-slip
//             U(i,j,nzt-2) = -U(i,j,nzt-1-gc) + 2*0.0;   // non-slip
//             V(i,j,nzt-1) = -V(i,j,nzt-1-gc) + 2*0.0;   // non-slip
//             V(i,j,nzt-2) = -V(i,j,nzt-1-gc) + 2*0.0;   // non-slip
//             W(i,j,nzt-2) = 0.0;
//             W(i,j,nzt-3) = 0.0;

//             US(i,j,nzt-1) = U(i,j,nzt-1);   US(i,j,nzt-2) = U(i,j,nzt-2);
//             VS(i,j,nzt-1) = V(i,j,nzt-1);   VS(i,j,nzt-2) = V(i,j,nzt-2);
//             WS(i,j,nzt-2) = W(i,j,nzt-2);   WS(i,j,nzt-3) = W(i,j,nzt-3);
//         }
//     }
// }



/**
 * Channel2 flow
 * -----------------------------------------------------------------------------
 *      Velocities: All Dirichlet (all 0 except u = u0 only at West) except
 *                  outlet (East).XXXXXXXXXXXXx
 *      Pressure:   All Neumann.
 * -----------------------------------------------------------------------------
 */


/*----------------------------------------------------------------


void boundaryConditions()
{
    #pragma omp parallel for default(none) shared(P, U, US, V, VS, W, WS)
    for (int k=gc; k<nzt-gc; ++k)
    {
        for (int j=gc; j<nyt-gc; ++j)
        {
            // West ------------------------------------------------------------
            P(0,j,k) = P(gc,j,k);
            P(1,j,k) = P(gc,j,k);

            U(0,j,k) = u0;
            U(1,j,k) = u0;
            V(0,j,k) = -V(gc,j,k) + 2*0.0;
            V(1,j,k) = -V(gc,j,k) + 2*0.0;
            W(0,j,k) = -W(gc,j,k) + 2*0.0;
            W(1,j,k) = -W(gc,j,k) + 2*0.0;

            US(0,j,k) = U(0,j,k);           US(1,j,k) = U(1,j,k);
            VS(0,j,k) = V(0,j,k);           VS(1,j,k) = V(1,j,k);
            WS(0,j,k) = W(0,j,k);           WS(1,j,k) = W(1,j,k);

            // East ------------------------------------------------------------
            P(nxt-1,j,k) = P(nxt-1-gc,j,k);
            P(nxt-2,j,k) = P(nxt-1-gc,j,k);

            U(nxt-2,j,k) = U(nxt-2-gc,j,k);  //num
            U(nxt-3,j,k) = U(nxt-2-gc,j,k);  //num
            V(nxt-1,j,k) = V(nxt-1-gc,j,k);  //num
            V(nxt-2,j,k) = V(nxt-1-gc,j,k);  //num
            W(nxt-1,j,k) = W(nxt-1-gc,j,k);  //num
            W(nxt-2,j,k) = W(nxt-1-gc,j,k);  //num

            US(nxt-2,j,k) = U(nxt-2,j,k);   US(nxt-3,j,k) = U(nxt-3,j,k);
            VS(nxt-1,j,k) = V(nxt-1,j,k);   VS(nxt-2,j,k) = V(nxt-2,j,k);
            WS(nxt-1,j,k) = W(nxt-1,j,k);   WS(nxt-2,j,k) = W(nxt-2,j,k);
        }
        for (int i=gc; i<nxt-gc; ++i)
        {
            // South -----------------------------------------------------------
            P(i,0,k) = P(i,gc,k);
            P(i,1,k) = P(i,gc,k);

            U(i,0,k) =  U(i,gc,k) ; // num
            U(i,1,k) =  U(i,gc,k) ; // num
            V(i,0,k) =  V(i,gc,k) ; // num
            V(i,1,k) =  V(i,gc,k) ; // num
            W(i,0,k) =  W(i,gc,k) ; // num
            W(i,1,k) =  W(i,gc,k) ; // num


            US(i,0,k) = U(i,0,k);           US(i,1,k) = U(i,1,k);
            VS(i,0,k) = V(i,0,k);           VS(i,1,k) = V(i,1,k);
            WS(i,0,k) = W(i,0,k);           WS(i,1,k) = W(i,1,k);

            // North -----------------------------------------------------------
            P(i,nyt-1,k) = P(i,nyt-1-gc,k);
            P(i,nyt-2,k) = P(i,nyt-1-gc,k);

            U(i,nyt-1,k) = U(i,nyt-1-gc,k);  // num
            U(i,nyt-2,k) = U(i,nyt-1-gc,k);  // num
            V(i,nyt-2,k) = V(i,nyt-2-gc,k);  // num
            V(i,nyt-3,k) = V(i,nyt-2-gc,k);  // num
            W(i,nyt-1,k) = W(i,nyt-1-gc,k);  // num
            W(i,nyt-2,k) = W(i,nyt-1-gc,k);  // num



            US(i,nyt-1,k) = U(i,nyt-1,k);   US(i,nyt-2,k) = U(i,nyt-2,k);
            VS(i,nyt-2,k) = V(i,nyt-2,k);   VS(i,nyt-3,k) = V(i,nyt-3,k);
            WS(i,nyt-1,k) = W(i,nyt-1,k);   WS(i,nyt-2,k) = W(i,nyt-2,k);
        }
    }
    #pragma omp parallel for default(none) shared(P, U, US, V, VS, W, WS)
    for (int j=gc; j<nyt-gc; ++j)
    {
        for (int i=gc; i<nxt-gc; ++i)
        {
            // Back ------------------------------------------------------------
            P(i,j,0) = P(i,j,gc);
            P(i,j,1) = P(i,j,gc);

            U(i,j,0) = -U(i,j,gc) + 2*0.0;  // no-slip
            U(i,j,1) = -U(i,j,gc) + 2*0.0;  // no-slip
            V(i,j,0) = -V(i,j,gc) + 2*0.0;  // no-slip
            V(i,j,1) = -V(i,j,gc) + 2*0.0;  // no-slip

            U(i,j,0) = -U(i,j,gc) + 2*0.0;  // no-slip
            U(i,j,1) = -U(i,j,gc) + 2*0.0;  // no-slip
            V(i,j,0) = -V(i,j,gc) + 2*0.0;  // no-slip
            V(i,j,1) = -V(i,j,gc) + 2*0.0;  // no-slip

            W(i,j,0) = 0.0;
            W(i,j,1) = 0.0;

            US(i,j,0) = U(i,j,0);           US(i,j,1) = U(i,j,1);
            VS(i,j,0) = V(i,j,0);           VS(i,j,1) = V(i,j,1);
            WS(i,j,0) = W(i,j,0);           WS(i,j,1) = W(i,j,1);

            // Front -----------------------------------------------------------
            P(i,j,nzt-1) = P(i,j,nzt-1-gc);
            P(i,j,nzt-2) = P(i,j,nzt-1-gc);

            U(i,j,nzt-1) = -U(i,j,nzt-1-gc) + 2*0.0;   // non-slip
            U(i,j,nzt-2) = -U(i,j,nzt-1-gc) + 2*0.0;   // non-slip
            V(i,j,nzt-1) = -V(i,j,nzt-1-gc) + 2*0.0;   // non-slip
            V(i,j,nzt-2) = -V(i,j,nzt-1-gc) + 2*0.0;   // non-slip
            W(i,j,nzt-2) = 0.0;
            W(i,j,nzt-3) = 0.0;

            US(i,j,nzt-1) = U(i,j,nzt-1);   US(i,j,nzt-2) = U(i,j,nzt-2);
            VS(i,j,nzt-1) = V(i,j,nzt-1);   VS(i,j,nzt-2) = V(i,j,nzt-2);
            WS(i,j,nzt-2) = W(i,j,nzt-2);   WS(i,j,nzt-3) = W(i,j,nzt-3);
        }
    }
}





*/





/**
 * Free flow
 * -----------------------------------------------------------------------------
 *      Velocities: All Dirichlet (all 0 except u = u0 only at West) except
 *                  outlet (East).XXXXXXXXXXXXx
 *      Pressure:   All Neumann.
 * -----------------------------------------------------------------------------
 */


// /*----------------------------------------------------------------


void boundaryConditions()
{
    #pragma omp parallel for default(none) shared(P, U, US, V, VS, W, WS)
    for (int k=gc; k<nzt-gc; ++k)
    {
        for (int j=gc; j<nyt-gc; ++j)
        {
            // West ------------------------------------------------------------
            P(0,j,k) = P(gc,j,k);
            P(1,j,k) = P(gc,j,k);

            U(0,j,k) = u0;
            U(1,j,k) = u0;
            V(0,j,k) = -V(gc,j,k) + 2*0.0;
            V(1,j,k) = -V(gc,j,k) + 2*0.0;
            W(0,j,k) = -W(gc,j,k) + 2*0.0;
            W(1,j,k) = -W(gc,j,k) + 2*0.0;

            US(0,j,k) = U(0,j,k);           US(1,j,k) = U(1,j,k);
            VS(0,j,k) = V(0,j,k);           VS(1,j,k) = V(1,j,k);
            WS(0,j,k) = W(0,j,k);           WS(1,j,k) = W(1,j,k);

            // East ------------------------------------------------------------
            P(nxt-1,j,k) = P(nxt-1-gc,j,k);
            P(nxt-2,j,k) = P(nxt-1-gc,j,k);

            U(nxt-2,j,k) = U(nxt-2-gc,j,k);  //num
            U(nxt-3,j,k) = U(nxt-2-gc,j,k);  //num
            V(nxt-1,j,k) = V(nxt-1-gc,j,k);  //num
            V(nxt-2,j,k) = V(nxt-1-gc,j,k);  //num
            W(nxt-1,j,k) = W(nxt-1-gc,j,k);  //num
            W(nxt-2,j,k) = W(nxt-1-gc,j,k);  //num

            US(nxt-2,j,k) = U(nxt-2,j,k);   US(nxt-3,j,k) = U(nxt-3,j,k);
            VS(nxt-1,j,k) = V(nxt-1,j,k);   VS(nxt-2,j,k) = V(nxt-2,j,k);
            WS(nxt-1,j,k) = W(nxt-1,j,k);   WS(nxt-2,j,k) = W(nxt-2,j,k);
        }
        for (int i=gc; i<nxt-gc; ++i)
        {
            // South -----------------------------------------------------------
            P(i,0,k) = P(i,gc,k);
            P(i,1,k) = P(i,gc,k);

            U(i,0,k) =  U(i,gc,k) ; // num
            U(i,1,k) =  U(i,gc,k) ; // num
            V(i,0,k) =  V(i,gc,k) ; // num
            V(i,1,k) =  V(i,gc,k) ; // num
            W(i,0,k) =  W(i,gc,k) ; // num
            W(i,1,k) =  W(i,gc,k) ; // num


            US(i,0,k) = U(i,0,k);           US(i,1,k) = U(i,1,k);
            VS(i,0,k) = V(i,0,k);           VS(i,1,k) = V(i,1,k);
            WS(i,0,k) = W(i,0,k);           WS(i,1,k) = W(i,1,k);

            // North -----------------------------------------------------------
            P(i,nyt-1,k) = P(i,nyt-1-gc,k);
            P(i,nyt-2,k) = P(i,nyt-1-gc,k);

            U(i,nyt-1,k) = U(i,nyt-1-gc,k);  // num
            U(i,nyt-2,k) = U(i,nyt-1-gc,k);  // num
            V(i,nyt-2,k) = V(i,nyt-2-gc,k);  // num
            V(i,nyt-3,k) = V(i,nyt-2-gc,k);  // num
            W(i,nyt-1,k) = W(i,nyt-1-gc,k);  // num
            W(i,nyt-2,k) = W(i,nyt-1-gc,k);  // num



            US(i,nyt-1,k) = U(i,nyt-1,k);   US(i,nyt-2,k) = U(i,nyt-2,k);
            VS(i,nyt-2,k) = V(i,nyt-2,k);   VS(i,nyt-3,k) = V(i,nyt-3,k);
            WS(i,nyt-1,k) = W(i,nyt-1,k);   WS(i,nyt-2,k) = W(i,nyt-2,k);
        }
    }
    #pragma omp parallel for default(none) shared(P, U, US, V, VS, W, WS)
    for (int j=gc; j<nyt-gc; ++j)
    {
        for (int i=gc; i<nxt-gc; ++i)
        {
            // Back ------------------------------------------------------------
            P(i,j,0) = P(i,j,gc);
            P(i,j,1) = P(i,j,gc);


            U(i,j,0) = U(i,j,gc);  // num
            U(i,j,1) = U(i,j,gc);  // num
            V(i,j,0) = V(i,j,gc);  // num
            V(i,j,1) = V(i,j,gc);  // num
            W(i,j,0) = W(i,j,gc);
            W(i,j,1) = W(i,j,gc);

            US(i,j,0) = U(i,j,0);           US(i,j,1) = U(i,j,1);
            VS(i,j,0) = V(i,j,0);           VS(i,j,1) = V(i,j,1);
            WS(i,j,0) = W(i,j,0);           WS(i,j,1) = W(i,j,1);

            // Front -----------------------------------------------------------
            P(i,j,nzt-1) = P(i,j,nzt-1-gc);
            P(i,j,nzt-2) = P(i,j,nzt-1-gc);

            U(i,j,nzt-1) = U(i,j,nzt-1-gc);   // num
            U(i,j,nzt-2) = U(i,j,nzt-1-gc);   // num
            V(i,j,nzt-1) = V(i,j,nzt-1-gc);   // num
            V(i,j,nzt-2) = V(i,j,nzt-1-gc);   // num
            W(i,j,nzt-2) = W(i,j,nzt-2-gc);   // num
            W(i,j,nzt-3) = W(i,j,nzt-2-gc);   // num

            US(i,j,nzt-1) = U(i,j,nzt-1);   US(i,j,nzt-2) = U(i,j,nzt-2);
            VS(i,j,nzt-1) = V(i,j,nzt-1);   VS(i,j,nzt-2) = V(i,j,nzt-2);
            WS(i,j,nzt-2) = W(i,j,nzt-2);   WS(i,j,nzt-3) = W(i,j,nzt-3);
        }
    }
}





// */


