#include<iostream>
#include<vector>
#include<algorithm>


using namespace std;


int main(void){

    std::vector<int> v = {2,4,6,8,10,12,14,16,18};
    
    vector<int>::iterator it = std::find(v.begin(), v.end(), 10);

    cout << "found " << *it  << endl;

    if (it != v.end()){
        cout << "found " << *it << ", " << std::distance(v.begin(), it)
            << endl;
    }
    else{
        cout << "not found \n";
    }


    return 0;
}