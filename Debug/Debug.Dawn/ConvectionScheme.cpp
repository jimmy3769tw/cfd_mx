#include <stdlib.h> 
#include <stdio.h>
#include <iostream>
#include <math.h>
#include <omp.h>
#include <cstring>

#include "Resolution.h"

using std::cout;
using std::endl;

void DiscretisationQUICK
    (
    // ======================================================== //
    double dt, double nu,

    double (*u)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],
    double (*v)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],
    double (*w)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],

    double (*u_star)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],
    double (*v_star)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],
    double (*w_star)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],

    double (*Dx) = new double[nx+4],
    double (*Dxs) = new double[nx+4],
    double (*Dy) = new double[ny+4],
    double (*Dys) = new double[ny+4],
    double (*Dz) = new double[nz+4],
    double (*Dzs) = new double[nz+4]

    
    // ======================================================== //
    )
{
    // ======================================================== //
    double u_tilde_x1, u_tilde_x2, u_tilde_y1, u_tilde_y2, u_tilde_z1, u_tilde_z2 ;

    double v_tilde_x1, v_tilde_x2, v_tilde_y1, v_tilde_y2, v_tilde_z1, v_tilde_z2;
    
    double w_tilde_x1, w_tilde_x2, w_tilde_y1, w_tilde_y2, w_tilde_z1, w_tilde_z2;

    double ue, uw, un, us, uf, ub, vnu, vsu, wfu, wbu;
    
    double ve, vw, vn, vs, vf, vb, uev, uwv, wfv, wbv;
    
    double we, ww, wn, ws, wf, wb, uew, uww, vnw, vsw;


    // ======================================================== //


    //u_star calculation (x component)
    for(size_t i = 2; i < nx+2; ++i )
    {
        for(size_t j = 2; j < ny+2; ++j )
        {
            for(size_t k = 2; k < nz+2; ++k)
            {

                u_tilde_x1 = 0.5 * (u[i+1][j][k]+u[i][j][k]);
                u_tilde_x2 = 0.5 * (u[i-1][j][k]+u[i][j][k]);
                v_tilde_x1 = 0.5 * (v[i][j][k]+v[i+1][j][k]);
                v_tilde_x2 = 0.5 * (v[i][j-1][k]+v[i+1][j-1][k]);
                w_tilde_x1 = 0.5 * (w[i+1][j][k]+w[i][j][k]);
                w_tilde_x2 = 0.5 * (w[i][j][k-1]+w[i+1][j][k-1] );



                if(u_tilde_x1 > 0.0)
                    ue  = 0.5*(u[i][j][k]     + u[i+1][j][k]  ) -0.125*Dx[i+1]*Dx[i+1]/Dxs[i] *  \
                        ( (u[i+1][j][k]   - u[i][j][k]    ) / Dx[i+1] \
                        - (u[i][j][k]     - u[i-1][j][k]  ) / Dx[i]   );
                else
                    ue  = 0.5*(u[i][j][k]     + u[i+1][j][k]  ) -0.125*Dx[i+1]*Dx[i+1]/Dxs[i+1]* \
                            ( (u[i+2][j][k]   - u[i+1][j][k]  ) / Dx[i+2] \
                            - (u[i+1][j][k]   - u[i][j][k]    ) / Dx[i+1] );


                if (u_tilde_x2 > 0.0)
                     uw  = 0.5*(u[i-1][j][k]   + u[i][j][k]    ) -0.125*Dx[i]*Dx[i]/Dxs[i-1]* \
                    (  (u[i][j][k]     - u[i-1][j][k]  ) / Dx[i]   \
                        - (u[i-1][j][k]   - u[i-2][j][k]  ) / Dx[i-1] ) ;
                else
                    uw  = 0.5*(u[i-1][j][k]   + u[i][j][k]    ) -0.125*Dx[i]*Dx[i]/Dxs[i]* \
                    (   (u[i+1][j][k]   - u[i][j][k]    ) / Dx[i+1] \
                        - (u[i][j][k]     - u[i-1][j][k]  ) / Dx[i]   )  ;



                if (v_tilde_x1 > 0.0)
                    un  = 0.5*(u[i][j][k]     + u[i][j+1][k]  ) -0.125*Dys[j]*Dys[j]/Dy[j]* \
                    (   (u[i][j+1][k]   - u[i][j][k]    ) / Dys[j]  \
                        - (u[i][j][k]     - u[i][j-1][k]  ) / Dys[j-1])  ;
                
                else
                    un  = 0.5*(u[i][j][k]     + u[i][j+1][k] )-0.125*Dys[j]*Dys[j]/Dy[j+1]* \
                    (   (u[i][j+2][k]   - u[i][j+1][k]  ) / Dys[j+1]\
                        - (u[i][j+1][k]   - u[i][j][k]    ) / Dys[j]  );


                if (v_tilde_x2 > 0.0)
                    us  = 0.5*(u[i][j-1][k]   + u[i][j][k]    ) -0.125*Dys[j-1]*Dys[j-1]/Dy[j-1]* \
                    (   (u[i][j][k]     - u[i][j-1][k]  ) / Dys[j-1]\
                        - (u[i][j-1][k]   - u[i][j-2][k]  ) / Dys[j-2]) ;
                        
                else
                    us  = 0.5*(u[i][j-1][k]   + u[i][j][k]    ) -0.125*Dys[j-1]*Dys[j-1]/Dy[j]* \
                    (   (u[i][j+1][k]   - u[i][j][k]    ) / Dys[j]  \
                        - (u[i][j][k]     - u[i][j-1][k]  ) / Dys[j-1])  ;


                if (w_tilde_x1 > 0.0) 
                    uf  = 0.5*(u[i][j][k]     + u[i][j][k+1]  ) -0.125*Dzs[k]*Dzs[k]/Dz[k]* \
                    (   (u[i][j][k+1]   - u[i][j][k]    ) / Dzs[k]  \
                        - (u[i][j][k]     - u[i][j][k-1]  ) / Dzs[k-1]);

                else
                    uf  = 0.5*(u[i][j][k]     + u[i][j][k+1]  ) -0.125*Dzs[k]*Dzs[k]/Dz[k+1]* \
                    (   (u[i][j][k+2]   - u[i][j][k+1]  ) / Dzs[k+1]\
                        - (u[i][j][k+1]   - u[i][j][k]    ) / Dzs[k]  );



                if (w_tilde_x2 > 0.0) 
                    ub  = 0.5*(u[i][j][k-1]   + u[i][j][k]    ) -0.125*Dzs[k-1]*Dzs[k-1]/Dz[k-1]* \
                    (   (u[i][j][k]     - u[i][j][k-1]  ) / Dzs[k-1]\
                        - (u[i][j][k-1]   - u[i][j][k-2]  ) / Dzs[k-2])  ;
                    
                else
                    ub  = 0.5*(u[i][j][k-1]   + u[i][j][k]    ) -0.125*Dzs[k-1]*Dzs[k-1]/Dz[k]* \
                    (   (u[i][j][k+1]   - u[i][j][k]    ) / Dzs[k]  \
                        - (u[i][j][k]     - u[i][j][k-1]  ) / Dzs[k-1]);


                /*if(i ==nx+1)
                {
                    for(size_t j=2;j<ny+2;++j)
                    {
                        for(size_t k=2;k<nz+2;++k)
                        {
                            cout <<   ue   << " " << j << "  " << k << endl;
                            cout <<   Dx[i+2]   << " " << j << "  " << k << endl;
                            


                        }
                    }
                }*/


                double convection = 0, difussion = 0;


                convection  =   -dt*(u_tilde_x1*ue-u_tilde_x2*uw) / Dxs[i] \
                                -dt*(v_tilde_x1*un-v_tilde_x2*us) / Dy[j] \
                                -dt*(w_tilde_x1*uf-w_tilde_x2*ub) / Dz[k] ;


                difussion =  (nu)*dt*( (u[i+1][j][k]-u[i][j][k]) / Dx[i+1] - (u[i][j][k]-u[i-1][j][k]) / Dx[i] ) / Dxs[i] \
                            +(nu)*dt*( (u[i][j+1][k]-u[i][j][k]) / Dys[j] - (u[i][j][k]-u[i][j-1][k]) / Dys[j-1] ) / Dy[j] \
                            +(nu)*dt*( (u[i][j][k+1]-u[i][j][k]) / Dzs[k] - (u[i][j][k]-u[i][j][k-1]) / Dzs[k-1] ) / Dz[k] ;

                // convection = 0.0
                //                + ue*u_tilde_x1//-u_tilde_x2*uw
                            // +v_tilde_x1*un-v_tilde_x2*us
                            // +w_tilde_x1*uf-w_tilde_x2*ub
                            // ;

                u_star[i][j][k] = u[i][j][k] + convection + difussion;

                                

      
            }
        }
    }

/*
    for(size_t i=2;i<nx+2;++i)
        for(size_t k=2;k<nz+2;++k)
            cout << u[i][ny+1][k] << "  " << i << "  " << k << endl;
*/ //nx+1 ue is problem
/*
for(size_t j=2;j<ny+2;++j)
    {
        for(size_t k=2;k<nz+2;++k)
        {
            cout <<   Dx[nx+1]  << " " << j << "  " << k << endl;
            


        }
    }*/
        


    //v_star calculation (y component) 
    for(size_t i = 2; i < nx+2; ++i )
    {
        for(size_t j = 2; j < ny+2; ++j )
        {
            for(size_t k = 2; k < nz+2; ++k)
            {


                u_tilde_y1 = 0.5 * (u[i][j][k]+u[i][j+1][k]);
                u_tilde_y2 = 0.5 * (u[i-1][j][k]+u[i-1][j+1][k]);
                v_tilde_y1 = 0.5 * (v[i][j][k]+v[i][j+1][k]);
                v_tilde_y2 = 0.5 * (v[i][j-1][k]+v[i][j][k]);
                w_tilde_y1 = 0.5 * (w[i][j][k]+w[i][j+1][k]);
                w_tilde_y2 = 0.5 * (w[i][j][k-1]+w[i][j+1][k-1]);

                if (u_tilde_y1 > 0.0)  
                    ve  = 0.5*(v[i][j][k]     + v[i+1][j][k]  ) -0.125*Dxs[i]*Dxs[i]/Dx[i]* \
                        (   (v[i+1][j][k]   - v[i][j][k]    ) / Dxs[i]  \
                            - (v[i][j][k]     - v[i-1][j][k]  ) / Dxs[i-1]);

                else
                    ve  = 0.5*(v[i][j][k]     + v[i+1][j][k]  ) -0.125*Dxs[i]*Dxs[i]/Dx[i+1]* \
                        (   (v[i+2][j][k]   - v[i+1][j][k]  ) / Dxs[i+1]\
                            - (v[i+1][j][k]   - v[i][j][k]    ) / Dxs[i]  );  


                if (u_tilde_y2 > 0.0)  
                    vw  = 0.5*(v[i-1][j][k]   + v[i][j][k]    ) -0.125*Dxs[i-1]*Dxs[i-1]/Dx[i-1]* \
                        (   (v[i][j][k]     - v[i-1][j][k]  ) / Dxs[i-1]\
                            - (v[i-1][j][k]   - v[i-2][j][k]  ) / Dxs[i-2]); 

                else 
                    vw  = 0.5*(v[i-1][j][k]   + v[i][j][k]    ) -0.125*Dxs[i-1]*Dxs[i-1]/Dx[i]* \
                        (   (v[i+1][j][k]   - v[i][j][k]    ) / Dxs[i]  \
                            - (v[i][j][k]     - v[i-1][j][k]  ) / Dxs[i-1]);  


                if (v_tilde_y1 > 0.0)  
                    vn  = 0.5*(v[i][j][k]     + v[i][j+1][k]  ) -0.125*Dy[j+1]*Dy[j+1]/Dys[j]* \
                        (   (v[i][j+1][k]   - v[i][j][k]    ) / Dy[j+1] \
                            - (v[i][j][k]     - v[i][j-1][k]  ) / Dy[j]   ); 
                    
                else
                    vn  = 0.5*(v[i][j][k]     + v[i][j+1][k]  ) -0.125*Dy[j+1]*Dy[j+1]/Dys[j+1]* \
                        (   (v[i][j+2][k]   - v[i][j+1][k]  ) / Dy[j+2] \
                            - (v[i][j+1][k]   - v[i][j][k]    ) / Dy[j+1] ); 


                if (v_tilde_y2 > 0.0)  
                    vs  = 0.5*(v[i][j-1][k]   + v[i][j][k]    ) -0.125*Dy[j]*Dy[j]/Dys[j-1]* \
                        (   (v[i][j][k]     - v[i][j-1][k]  ) / Dy[j]   \
                            - (v[i][j-1][k]   - v[i][j-2][k]  ) / Dy[j-1] ); 
                    
                else
                    vs  = 0.5*(v[i][j-1][k]   + v[i][j][k]    ) -0.125*Dy[j]*Dy[j]/Dys[j]* \
                        (   (v[i][j+1][k]   - v[i][j][k]    ) / Dy[j+1]\
                            - (v[i][j][k]     - v[i][j-1][k]  ) / Dy[j]  ); 



                if (w_tilde_y1 > 0.0)  
                    vf  = 0.5*(v[i][j][k]     + v[i][j][k+1]  ) -0.125*Dzs[k]*Dzs[k]/Dz[k]* \
                        (   (v[i][j][k+1]   - v[i][j][k]    ) / Dzs[k]  \
                            - (v[i][j][k]     - v[i][j][k-1]  ) / Dzs[k-1]);
                    
                else
                    vf  = 0.5*(v[i][j][k]     + v[i][j][k+1]  ) -0.125*Dzs[k]*Dzs[k]/Dz[k+1]* \
                        (   (v[i][j][k+2]   - v[i][j][k+1]  ) / Dzs[k+1]\
                            - (v[i][j][k+1]   - v[i][j][k]    ) / Dzs[k]  );

                if (w_tilde_y2 > 0.0)  
                    vb  = 0.5*(v[i][j][k-1]   + v[i][j][k]    ) -0.125*Dzs[k-1]*Dzs[k-1]/Dz[k-1]* \
                        (   (v[i][j][k]     - v[i][j][k-1]  ) / Dzs[k-1]\
                            - (v[i][j][k-1]   - v[i][j][k-2]  ) / Dzs[k-2]);
                    
                else
                    vb  = 0.5*(v[i][j][k-1]   + v[i][j][k]    ) -0.125*Dzs[k-1]*Dzs[k-1]/Dz[k]* \
                        (   (v[i][j][k+1]   - v[i][j][k]    ) / Dzs[k]  \
                            - (v[i][j][k]     - v[i][j][k-1]  ) / Dzs[k-1]);


                //nut = Viseff[i][j][k]

                v_star[i][j][k] = v[i][j][k]-dt*(u_tilde_y1*ve-u_tilde_y2*vw) / Dx[i] \
                                            -dt*(v_tilde_y1*vn-v_tilde_y2*vs) / Dys[j] \
                                            -dt*(w_tilde_y1*vf-w_tilde_y2*vb) / Dz[k] \

                                +(nu)*dt*( (v[i+1][j][k]-v[i][j][k]) / Dxs[i] - (v[i][j][k] - v[i-1][j][k]) / Dxs[i-1] ) / Dx[i] \
                                +(nu)*dt*( (v[i][j+1][k]-v[i][j][k]) / Dy[j+1] - (v[i][j][k] - v[i][j-1][k]) / Dy[j] ) / Dys[j] \
                                +(nu)*dt*( (v[i][j][k+1]-v[i][j][k]) / Dzs[k] - (v[i][j][k] - v[i][j][k-1]) / Dzs[k-1] ) / Dz[k] ;
    
            }
        }
    }


    //w_star calculation (z component)


    for(size_t i = 2; i < nx+2; ++i )
    {
            for(size_t j = 2; j < ny+2; ++j )
            {
                for(size_t k = 2; k < nz+2; ++k)
                {

                    u_tilde_z1 = 0.5 * (u[i][j][k+1]+u[i][j][k]);
                    u_tilde_z2 = 0.5 * (u[i-1][j][k+1]+u[i-1][j][k]);
                    v_tilde_z1 = 0.5 * (v[i][j][k]+v[i][j][k+1]);
                    v_tilde_z2 = 0.5 * (v[i][j-1][k+1]+v[i][j-1][k]);
                    w_tilde_z1 = 0.5 * (w[i][j][k+1]+w[i][j][k]);
                    w_tilde_z2 = 0.5 * (w[i][j][k]+w[i][j][k-1]);

                    if (u_tilde_z1 > 0.0)
                        we  = 0.5*(w[i][j][k]     + w[i+1][j][k]  ) -0.125*Dxs[i]*Dxs[i]/Dx[i]* \
                                (  (w[i+1][j][k]   - w[i][j][k]    ) / Dxs[i]  \
                                - (w[i][j][k]     - w[i-1][j][k]  ) / Dxs[i-1]) ;

                    else
                        we  = 0.5*(w[i][j][k]     + w[i+1][j][k]  ) -0.125*Dxs[i]*Dxs[i]/Dx[i+1]* \
                                (  (w[i+2][j][k]   - w[i+1][j][k]  ) / Dxs[i+1] \
                                - (w[i+1][j][k]   - w[i][j][k]    ) / Dxs[i]   );


                    if (u_tilde_z2 > 0.0)
                        ww  = 0.5*(w[i-1][j][k]   + w[i][j][k]    ) -0.125*Dxs[i-1]*Dxs[i-1]/Dx[i-1]* \
                                (  (w[i][j][k]     - w[i-1][j][k]  ) / Dxs[i-1] \
                                - (w[i-1][j][k]   - w[i-2][j][k]  ) / Dxs[i-2] );

                    else 
                        ww  = 0.5*(w[i-1][j][k]   + w[i][j][k]    ) -0.125*Dxs[i-1]*Dxs[i-1]/Dx[i]* \
                                (  (w[i+1][j][k]   - w[i][j][k]    ) / Dxs[i]  \
                                - (w[i][j][k]     - w[i-1][j][k]  ) / Dxs[i-1]) ;


                    if (v_tilde_z1 > 0.0)
                        wn  = 0.5*(w[i][j][k]     + w[i][j+1][k]  ) -0.125*Dys[j]*Dys[j]/Dy[j]* \
                                (   (w[i][j+1][k]   - w[i][j][k]    ) / Dys[j]  \
                                    - (w[i][j][k]     - w[i][j-1][k]  ) / Dys[j-1]);
                        
                    else
                        wn  = 0.5*(w[i][j][k]     + w[i][j+1][k]  ) -0.125*Dys[j]*Dys[j]/Dy[j+1]* \
                                (   (w[i][j+2][k]   - w[i][j+1][k]  ) / Dys[j+1]\
                                    - (w[i][j+1][k]   - w[i][j][k]    ) / Dys[j]  ) ;


                    if (v_tilde_z2 > 0.0)
                        ws  = 0.5*(w[i][j-1][k]   + w[i][j][k]    ) -0.125*Dys[j-1]*Dys[j-1]/Dy[j-1]* \
                                (   (w[i][j][k]     - w[i][j-1][k]  ) / Dys[j-1]\
                                    - (w[i][j-1][k]   - w[i][j-2][k]  ) / Dys[j-2]);
                        
                    else
                        ws  = 0.5*(w[i][j-1][k]   + w[i][j][k]    ) -0.125*Dys[j-1]*Dys[j-1]/Dy[j]* \
                                (   (w[i][j+1][k]   - w[i][j][k]    ) / Dys[j]  \
                                    - (w[i][j][k]     - w[i][j-1][k]  ) / Dys[j-1]);



                    if (w_tilde_z1 > 0.0)
                        wf  = 0.5*(w[i][j][k]     + w[i][j][k+1]  ) -0.125*Dz[k+1]*Dz[k+1]/Dzs[k]* \
                                (   (w[i][j][k+1]   - w[i][j][k]    ) / Dz[k+1]\
                                    - (w[i][j][k]     - w[i][j][k-1]  ) / Dz[k]  );
                        
                    else
                        wf  = 0.5*(w[i][j][k]     + w[i][j][k+1]  ) -0.125*Dz[k+1]*Dz[k+1]/Dzs[k+1]* \
                                (   (w[i][j][k+2]   - w[i][j][k+1]  ) / Dz[k+2]\
                                    - (w[i][j][k+1]   - w[i][j][k]    ) / Dz[k+1]);

                    if (w_tilde_z2 > 0.0)
                        wb  = 0.5*(w[i][j][k-1]   + w[i][j][k]    ) -0.125*Dz[k]*Dz[k]/Dzs[k-1]* \
                                (   (w[i][j][k]     - w[i][j][k-1]  ) / Dz[k]  \
                                    - (w[i][j][k-1]   - w[i][j][k-2]  ) / Dz[k-1]);
                        
                    else
                        wb  = 0.5*(w[i][j][k-1]   + w[i][j][k]    ) -0.125*Dz[k]*Dz[k]/Dzs[k]* \
                                    (  (w[i][j][k+1]   - w[i][j][k]    ) / Dz[k+1]\
                                - (   w[i][j][k]     - w[i][j][k-1]  ) / Dz[k]  );
                                            

                    //nut = Viseff[i][j][k]

                    w_star[i][j][k] = w[i][j][k]-dt*(u_tilde_z1*we-u_tilde_z2*ww) / Dx[i] \
                                                -dt*(v_tilde_z1*wn-v_tilde_z2*ws) / Dy[j] \
                                                -dt*(w_tilde_z1*wf-w_tilde_z2*wb) / Dzs[k] \
                                            
                                    +(nu)*dt*( ( w[i+1][j][k]-w[i][j][k] ) / Dxs[i] -  ( w[i][j][k]-w[i-1][j][k] ) / Dxs[i-1]  ) / Dx[i] \
                                    +(nu)*dt*( ( w[i][j+1][k]-w[i][j][k] ) / Dys[j] -  ( w[i][j][k]-w[i][j-1][k] ) / Dys[j-1]  ) / Dy[j] \
                                    +(nu)*dt*( ( w[i][j][k+1]-w[i][j][k] ) / Dz[k+1] - ( w[i][j][k]-w[i][j][k-1] ) / Dz[k]   ) / Dzs[k] ;

            }
        }
    }

}