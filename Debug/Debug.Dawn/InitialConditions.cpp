#include <stdlib.h> 
#include <stdio.h>
#include <math.h>
#include <omp.h>


#include "Resolution.h"

void InitialConditions
    (
    // ======================================================== //
    //vector<vector<vector<double> > > u( nx+4, vector<vector<double> >(ny+4,vector<double> (nz+4)) ),
    double (*u)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],
    double (*v)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],
    double (*w)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],
    
    double (*u1)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],
    double (*v1)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],
    double (*w1)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],

    double (*u2)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],
    double (*v2)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],
    double (*w2)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],

    double (*u_star)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],
    double (*v_star)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],
    double (*w_star)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],

    double (*ETA)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],


    double (*p)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4]
    // ======================================================== //
    )
{


    // ======================================================== //

    int i, j, k;

    // ======================================================== //

    for(i = 0; i < nx+4; ++i){
        for(j = 0; j < ny+4; ++j){
            for(k = 0; k < nz+4; ++k){
                u[i][j][k] = 0.0;
                v[i][j][k] = 0.0;
                w[i][j][k] = 0.0;
                u1[i][j][k] = 0.0;
                v1[i][j][k] = 0.0;
                w1[i][j][k] = 0.0;
                u2[i][j][k] = 0.0;
                v2[i][j][k] = 0.0;
                w2[i][j][k] = 0.0;
                u_star[i][j][k] = 0.0;
                v_star[i][j][k] = 0.0;
                w_star[i][j][k] = 0.0;
                p[i][j][k] = 0.0;
                ETA[i][j][k] = 0.0;
            }
        }
    }








}