#include <cmath>
#include <omp.h>
#include <iostream>

#include "Resolution.h"

using std::cout;
using std::endl;


void SuccessiveOverRelaxation
    (
    // ======================================================== //
    double zeta, int itmax, double dt, double omega,

    double (*pre)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],
    double (*p)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],
    double (*u_star)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],
    double (*v_star)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],
    double (*w_star)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],

    double (*Dx) = new double[nx+4],
    double (*Dxs) = new double[nx+4],
    double (*Dy) = new double[ny+4],
    double (*Dys) = new double[ny+4],
    double (*Dz) = new double[nz+4],
    double (*Dzs) = new double[nz+4]

    // ======================================================== //
    
    )
{
    // ======================================================== //
    int ik = 0;
    double pChangeMax = 1.0;
    double mChangeMax = 1.0;
    double mChange;
    double pNEW;
    double pChange;
    // ======================================================== //

    for(size_t i=2; i<nx+2; ++i)
    {
        for(size_t j=2; j<ny+2; ++j)
        {
            for(size_t k=2; k<nz+2; ++k)
            {
                
                pre[i][j][k] = p[i][j][k];
                
            }
        }    
    }


    while(pChangeMax>zeta && ik <itmax)
    {
        ik ++;
        
        pChangeMax = 0.0;
        mChangeMax = 0.0;


        for(size_t i=2; i<nx+2; ++i)
        {
            for(size_t j=2; j<ny+2; ++j)
            {
                for(size_t k=2; k<nz+2; ++k)
                {
                    mChange = ( u_star[i][j][k] - u_star[i-1][j][k] ) * Dy[j] * Dz[k] \
                            + ( v_star[i][j][k] - v_star[i][j-1][k] ) * Dx[i] * Dz[k] \
                            + ( w_star[i][j][k] - w_star[i][j][k-1] ) * Dx[i] * Dy[j];



                    pNEW = (- p[i+1][j][k] * Dy[j] * Dz[k] / Dxs[i] 
                            - p[i-1][j][k] * Dy[j] * Dz[k] / Dxs[i-1] 
                            - p[i][j+1][k] * Dx[i] * Dz[k] / Dys[j] 
                            - p[i][j-1][k] * Dx[i] * Dz[k] / Dys[j-1] 
                            - p[i][j][k+1] * Dx[i] * Dy[j] / Dzs[k] 
                            - p[i][j][k-1] * Dx[i] * Dy[j] / Dzs[k-1] 
                            + mChange / dt) /  (- Dy[j] * Dz[k] / Dxs[i] - Dy[j] * Dz[k] / Dxs[i-1] 
                                                - Dx[i] * Dz[k] / Dys[j] - Dx[i] * Dz[k] / Dys[j-1] 
                                                - Dx[i] * Dy[j] / Dzs[k] - Dx[i] * Dy[j] / Dzs[k-1] );

                    pChange = pNEW - p[i][j][k];

                    p[i][j][k] = p[i][j][k] + ( omega * pChange );
                    


                    if(std::abs(pChange) > pChangeMax)
                        pChangeMax = std::abs(pChange);

                    
                    
                }
            }    
        }
        
        


    }

    cout << "===================================================================="     << endl;
    cout << "Num Of Iter = " << ik << ", Residual = " << pChangeMax << endl;



    
    





}