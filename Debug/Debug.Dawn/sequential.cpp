#include "1_1_Sequential.hpp"
#include <random>
#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <ctime>       /* time */

#include"BoundaryCondition/Boundary_Condition.hpp"

// ! Dawn Yakutat_developing 

#include "gridder.h"

#include "readingData.h"

#include "InitialConditions.h"

#include "BoundaryConditions.h"

#include "filer.h"

#include "ConvectionScheme.h"

#include "PressureSolvers.h"

#include "calNewVelocity.h"




// ! io

#include"io/Plot3D/qfile.hpp"
#include"io/Plot3D/xfileWrite.hpp"


#include"analyses/GhiaProfile.hpp"

#include"profiling/chronograph.hpp"

#include"resize_and_simuSetting.hpp"



#include"grid/Generator.hpp"
#include"grid/Reader.hpp"

#include"source/engeryEquation.hpp"
#include "source/convection_and_difussion.hpp"




// ! Dawn Yakutat_developing 

bool debug_dwan_BC_and_quick(
    grid &gridA,
    clockstruct &timer,
    simuClass &simu,
    int argc, char **argv
)
{
  // ! Dwan --------------------------

    #include "Resolution.h"
    #include "Variables.h"

// ======================================================== // 

    Re = 100.;

    dt = simu.dt ;

    time = 0.0;

    nstep = 2000;

    isto = 100;

    zeta = 1.0e-5; 

    itmax = 3000;

    omega = 1.8;

    //non-uniform 0, uniform 1 
    char Gridder[20] = "uniform";            

    
// ======================================================== //

    #include "array.h"

    readingData(dx, dy, dz, lx, ly, lz, lxSml, lySml, lzSml, Gridder, 
                nx, ny, nz, nxSml, nySml, nzSml, dxSml, dySml, dzSml,
                Re, nu);



    gridder(GridderXc, GridderYc, GridderZc, lxSml, lySml, lzSml, dx, dy, dz, dxSml,
            dySml, dzSml, Gridder, X, Y, Z,Dx, Dxs, Dy, Dys, Dz, Dzs, Xa, Ya, Za, Xc, Yc, Zc);



    InitialConditions(u ,v ,w ,u1 ,v1 ,w1 ,u2 ,v2 ,w2, u_star ,v_star ,w_star ,ETA ,p);
    

  // ! Dwan --------------------------

  using value_type = double;

  //  * struct ---------------------------
  ImmersedBoundary Dfib;

  pressure t1;

  velocity T0 , T1, T3;

  SORcoefficient Sor;

  shareMenory ShareM;

  MxClass Mx;

  //  * struct ---------------------------


  // *         ============================  divid Domain ============================

  calDomain Lo;

  std::vector<int> grid_size{gridA.nx, gridA.ny, gridA.nz};

  std::vector<int> dims{1,1,1};

  Lo.initTable(grid_size, dims);

  Lo.initLocal(0,0,0);

  
  // *         ============================  divid Domain ============================

  // * -------------  Init the variables (First policy or data Locality) and Generate grids -------------

  resize_variable(gridA, simu, t1, T0, T1, T3, Dfib); // ! resize shared memory

  T0.iniU_omp(0.0, 0.0, 0.0);
  
  T1.iniU_omp(0.0, 0.0, 0.0);
  
  T3.iniU_omp(0.0, 0.0, 0.0);
  
  t1.init_p(0.0);

// !  ---------------------------- Dawn ----------------------------


  // ---------------------------- Rand ----------------- 
    std::random_device rd;
    auto randV = T0.u;

    for(auto & v :randV){
      v =  rd();
    }

    for(size_t i=0; i<nx+4; ++i)
    for(size_t j=0; j<ny+4; ++j)
    for(size_t k=0; k<nz+4; ++k)
    {
      u[i][j][k]  = rd();
      v[i][j][k]  = rd();
      w[i][j][k]  = rd();
      p[i][j][k]  = rd();
    }

    for(size_t i=0; i<nx+4; ++i)
    for(size_t j=0; j<ny+4; ++j)
    for(size_t k=0; k<nz+4; ++k)
    {
      T0.u[gridA.icel(i,j,k)] = u[i][j][k] ;
      T0.v[gridA.icel(i,j,k)] = v[i][j][k] ;
      T0.w[gridA.icel(i,j,k)] = w[i][j][k] ;
      t1.p[gridA.icel(i,j,k)] = p[i][j][k] ;
    }

  // ---------------------------- Rand ----------------- 
  
    BoundaryConditions(u, v, w, u1, v1, w1, u2, v2, w2, u_star, v_star, w_star, p);

    BoundaryCondtion(simu, Lo, T0, t1, gridA); // ! dependence on generateGride

    double 
           critical = 0.0001,
           sum =0;

    for(size_t i=0; i<nx+4; ++i)
    for(size_t j=0; j<ny+4; ++j)
    for(size_t k=0; k<nz+4; ++k)
    {
        auto a = u[i][j][k];
        auto b = T0.u[gridA.icel(i,j,k)];
        auto ab =  std::abs(a-b);
        sum +=ab;

        // if (ab > critical){
        //   cout << a << ", " << "b" << b << std::endl;
        // }
    }

    cout << "\nu " << sum;


    sum =0;
    for(size_t i=0; i<nx+4; ++i)
    for(size_t j=0; j<ny+4; ++j)
    for(size_t k=0; k<nz+4; ++k)
    {
        auto a = v[i][j][k];
        auto b = T0.v[gridA.icel(i,j,k)];
        auto ab =  std::abs(a-b);
        sum +=ab;

        // if (ab > critical){

        //   cout << a << ", " << "b" << b << std::endl;
        // }
    }
    cout << "\nv " << sum;

    sum =0;
    for(size_t i=0; i<nx+4; ++i)
    for(size_t j=0; j<ny+4; ++j)
    for(size_t k=0; k<nz+4; ++k)
    {
        auto a = w[i][j][k];
        auto b = T0.w[gridA.icel(i,j,k)];
        auto ab =  std::abs(a-b);
        sum +=ab;

        // if (ab > critical){

        //   cout << a << ", " << "b" << b << std::endl;
        // }
    }

    cout << "\nw " << sum;




    sum =0;
    for(size_t i=0; i<nx+4; ++i)
    for(size_t j=0; j<ny+4; ++j)
    for(size_t k=0; k<nz+4; ++k)
    {
        auto a = p[i][j][k];
        auto b = t1.p[gridA.icel(i,j,k)];
        auto ab =  std::abs(a-b);
        sum +=ab;

        // if (ab > critical){

        //   cout << a << ", " << "b" << b << std::endl;
        // }
    }
    cout << "\np " << sum;

    T1.u = T0.u;
    T1.v = T0.v;
    T1.w = T0.w;

    Output_Q_Cpp_PLOT3D(istep, p, u2, v2, w2, ETA);

    generateGride(simu, ShareM, Lo, gridA);


    for(size_t i=0; i<nx+4; ++i){
      Dx[i] = gridA.Dx[i];
      Dxs[i] = gridA.Dxs[i];
    }

    for(size_t j=0; j<ny+4; ++j){
      Dy[j] = gridA.Dy[j];
      Dys[j] = gridA.Dys[j];
    }

    for(size_t k=0; k<nz+4; ++k){
      Dz[k] = gridA.Dz[k];
      Dzs[k] = gridA.Dzs[k];
    }



  gridCheck_csv(simu, ShareM, Lo, gridA);

  OutputPlot3D_Xfile(simu, gridA);


  // * -------------  Init the variables (First policy or data Locality) and Generate grids -------------

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! yakutat

    dt = simu.dt;
    nu = simu.nu;


    DiscretisationQUICK(dt, nu, u, v, w, u_star, v_star, w_star, Dx, Dxs, Dy, Dys, Dz, Dzs);

    ConvectionDifussion(simu, T0, T1, Lo, gridA);


    sum =0;

    for(size_t i=0; i<nx+4; ++i)
    for(size_t j=0; j<ny+4; ++j)
    for(size_t k=0; k<nz+4; ++k)
    {
        auto a = u_star[i][j][k];
        auto b = T1.u[gridA.icel(i,j,k)];
        auto ab =  std::abs(a-b);
        sum +=ab;

        // if (ab > critical){

        //   cout << a << ", " << b << std::endl;
        // }
    }
    cout << "\nu_1 " << sum;



    sum =0;
    for(size_t i=0; i< nx+4; ++i)
    for(size_t j=0; j< ny+4; ++j)
    for(size_t k=0; k< nz+4; ++k)
    {
        auto a = v_star[i][j][k];
        auto b = T1.v[gridA.icel(i,j,k)];
        auto ab =  std::abs(a-b);
        sum +=ab;

        // if (ab > critical){

        //   cout << a << ", " << b << std::endl;
        // }
    }
    cout << "\nv_1 " << sum;


    sum =0;
    for(size_t i=0; i<nx+4; ++i)
    for(size_t j=0; j<ny+4; ++j)
    for(size_t k=0; k<nz+4; ++k)
    {
        auto a = w_star[i][j][k];
        auto b = T1.w[gridA.icel(i,j,k)];
        auto ab =  std::abs(a-b);
        sum += ab;

        // if (ab > critical){

        //   cout << a << ", " << b << " | " << ab << std::endl;
        // }
    }
    cout << "\nw_1 " << sum;

  return 0;


}
