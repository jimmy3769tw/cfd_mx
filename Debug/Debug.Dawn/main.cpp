#include <time.h>
#include <omp.h>
#include <iostream>
#include <vector>



#include "gridder.h"
#include "readingData.h"
#include "InitialConditions.h"
#include "BoundaryConditions.h"
#include "filer.h"
#include "ConvectionScheme.h"
#include "PressureSolvers.h"
#include "calNewVelocity.h"

using std::cout;
using std::endl;
using std::vector;


int main(int argc, char **argv)
{   

    #include "Resolution.h"
    #include "Variables.h"


// ======================================================== // 

    Re = 100;

    dt = 1.0e-3;

    time = 0.0;

    nstep = 2000;

    isto = 100;

    zeta = 1.0e-5; 

    itmax = 3000;

    omega = 1.8;

    //non-uniform 0, uniform 1 
    char Gridder[20] = "uniform";            

    
// ======================================================== //

    
    

    #include "array.h"


    readingData(dx, dy, dz, lx, ly, lz, lxSml, lySml, lzSml, Gridder, 
                nx, ny, nz, nxSml, nySml, nzSml, dxSml, dySml, dzSml,
                Re, nu);

                

    gridder(GridderXc, GridderYc, GridderZc, lxSml, lySml, lzSml, dx, dy, dz, dxSml,
            dySml, dzSml, Gridder, X, Y, Z,Dx, Dxs, Dy, Dys, Dz, Dzs, Xa, Ya, Za, Xc, Yc, Zc);


    InitialConditions(u ,v ,w ,u1 ,v1 ,w1 ,u2 ,v2 ,w2, u_star ,v_star ,w_star ,ETA ,p);
    


    BoundaryConditions(u, v, w, u1, v1, w1, u2, v2, w2, u_star, v_star, w_star, p);


    Output_Q_Cpp_PLOT3D(istep, p, u2, v2, w2, ETA);




    for(istep = 1; istep < nstep+1; ++istep)
    {

     

        SuccessiveOverRelaxation(zeta, itmax, dt, omega, pre, p, u_star, v_star, w_star, Dx, Dxs, Dy, Dys, Dz, Dzs);
        

        calNewVelocity(dt, p, u_star, v_star, w_star, u1, v1, w1, u2, v2, w2, ETA,
                       FX, FY, FZ, Dx, Dxs, Dy, Dys, Dz, Dzs);

        //virtualForceIntegrator


        updating(L2norm, pre, p, u, v, w, u2, v2, w2);
        cout << "    " << p[nx+1][ny+1][10] << ", " << u2[nx+1][ny+1][10] << endl;


        BoundaryConditions(u, v, w, u1, v1, w1, u2, v2, w2, u_star, v_star, w_star, p);




        time += dt;
        cout.setf(std::ios::scientific);
        cout << "u=" << L2norm[1]  \
             << ", v=" << L2norm[2]  \
             << ", w=" << L2norm[3] \
             << ", p=" << L2norm[4] << endl;
        cout.unsetf(std::ios::scientific);
        
        cout << "time = " << time << endl;
        cout << "====================================================================" << endl;
        cout << "                                  " << endl;

        if(istep%isto==0)
            Output_Q_Cpp_PLOT3D(istep, p, u2, v2, w2, ETA);



    }


















}
