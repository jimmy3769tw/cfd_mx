#include <cmath>
#include <omp.h>
#include <iostream>

#include "Resolution.h"


void calNewVelocity
    (
    // ======================================================== //
    double dt,

    double (*p)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],
    double (*u_star)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],
    double (*v_star)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],
    double (*w_star)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],

    double (*u1)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],
    double (*v1)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],
    double (*w1)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],
    
    double (*u2)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],
    double (*v2)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],
    double (*w2)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],

    double (*FX)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],
    double (*FY)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],
    double (*FZ)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],

    double (*ETA)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],

    
    double (*Dx) = new double[nx+4],
    double (*Dxs) = new double[nx+4],
    double (*Dy) = new double[ny+4],
    double (*Dys) = new double[ny+4],
    double (*Dz) = new double[nz+4],
    double (*Dzs) = new double[nz+4]
    // ======================================================== //

    )
{
    // ======================================================== //
    double u_solid; 
    double v_solid; 
    double w_solid; 
    

    // ======================================================== //

    


    /**************************************************************/
    /*        Calculation of velocity field at t = dt*n+1         */
    /**************************************************************/

    //In x direction
    for(size_t i=2; i<nx+2; ++i)
    {
        for(size_t j=2; j<ny+2; ++j)
        {
            for(size_t k=2; k<nz+2; ++k)
            {
                u1[i][j][k] = u_star[i][j][k] - dt*( p[i+1][j][k]-p[i][j][k] ) / Dxs[i];
            }
        }    
    }


    //In y direction
    for(size_t i=2; i<nx+2; ++i)
    {
        for(size_t j=2; j<ny+2; ++j)
        {
            for(size_t k=2; k<nz+2; ++k)
            {
                v1[i][j][k] = v_star[i][j][k] - dt*( p[i][j+1][k]-p[i][j][k] ) / Dys[j];
            }
        }    
    }

    //In z direction
    for(size_t i=2; i<nx+2; ++i)
    {
        for(size_t j=2; j<ny+2; ++j)
        {
            for(size_t k=2; k<nz+2; ++k)
            {
                w1[i][j][k] = w_star[i][j][k] - dt*( p[i][j][k+1]-p[i][j][k] ) / Dzs[k];
            }
        }    
    }


    /**************************************************************/
    /*         Calculation of velocity field for DFIB             */
    /**************************************************************/


    for(size_t i=2; i<nx+2; ++i)
    {
        for(size_t j=2; j<ny+2; ++j)
        {
            for(size_t k=2; k<nz+2; ++k)
            {
                u2[i][j][k] = ETA[i][j][k] * u_solid + (1-ETA[i][j][k]) * u1[i][j][k];
                FX[i][j][k] = (u2[i][j][k] - u1[i][j][k]) / dt;
            }
        }    
    }


    for(size_t i=2; i<nx+2; ++i)
    {
        for(size_t j=2; j<ny+2; ++j)
        {
            for(size_t k=2; k<nz+2; ++k)
            {
                v2[i][j][k] = ETA[i][j][k] * v_solid + (1-ETA[i][j][k]) * v1[i][j][k];
                FY[i][j][k] = (v2[i][j][k] - v1[i][j][k]) / dt;
            }
        }    
    }
    
    for(size_t i=2; i<nx+2; ++i)
    {
        for(size_t j=2; j<ny+2; ++j)
        {
            for(size_t k=2; k<nz+2; ++k)
            {
                w2[i][j][k] = ETA[i][j][k] * w_solid + (1-ETA[i][j][k]) * w1[i][j][k];
                FZ[i][j][k] = (w2[i][j][k] - w1[i][j][k]) / dt;
            }
        }    
    }


}

void updating
    (
    // ======================================================== //
    double (*L2norm) = new double[4],

    double (*pre)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],
    double (*p)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],

    double (*u)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],
    double (*v)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],
    double (*w)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],

    double (*u2)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],
    double (*v2)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],
    double (*w2)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4]


    // ======================================================== //

    )
{
    // ======================================================== //
    double (*MaxValue) = new double[4];
    
    // ======================================================== //

    L2norm[1] = 0;
    L2norm[2] = 0;
    L2norm[3] = 0;
    L2norm[4] = 0;

    for(size_t i=2; i<nx+2; ++i)
    {
        for(size_t j=2; j<ny+2; ++j)
        {
            for(size_t k=2; k<nz+2; ++k)
            {
                if(fabs(u2[i][j][k] - u[i][j][k]) > L2norm[1])
                    L2norm[1] = fabs(u2[i][j][k] - u[i][j][k]);

                if(fabs(v2[i][j][k] - v[i][j][k]) > L2norm[2])
                    L2norm[2] = fabs(v2[i][j][k] - v[i][j][k]);

                if(fabs(w2[i][j][k] - w[i][j][k]) > L2norm[3])
                    L2norm[3] = fabs(w2[i][j][k] - w[i][j][k]);

                if(fabs(p[i][j][k] - pre[i][j][k]) > L2norm[4])
                    L2norm[4] = fabs(p[i][j][k] - pre[i][j][k]);

                u[i][j][k] = u2[i][j][k];
                v[i][j][k] = v2[i][j][k];
                w[i][j][k] = w2[i][j][k];
                
                
            }
        }    
    }
}




























