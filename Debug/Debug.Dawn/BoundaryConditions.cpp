#include <stdlib.h> 
#include <stdio.h>
#include <iostream>
#include <cstring>
#include <math.h>
#include <omp.h>

#include "Resolution.h"

void BoundaryConditions (

    // ======================================================== //
    double (*u)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],
    double (*v)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],
    double (*w)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],

    double (*u1)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],
    double (*v1)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],
    double (*w1)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],

    double (*u2)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],
    double (*v2)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],
    double (*w2)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],

    double (*u_star)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],
    double (*v_star)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],
    double (*w_star)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],

    double (*p)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4]
    // ======================================================== //
)
{
    // ======================================================== //

    int i, j, k;

    // ======================================================== //



    //B.C
    // ======================================================== //

    //    y=1 ______________                                                                                 
    //       /             /|                                                     
    //      /       N     / |                                                          
    //     /____________ /  |                                
    //     |  |         |   |                                                        
    //     |  | B       |   |                                          
    //   W |  |         | E |                                           
    //     |  |_z=1_____|___|                                       
    //     |  /         |  /                                         
    //     | /     S    | /                                        
    //     |/___________|/  x=1                                        
    //  x=y=z=0   F     


    //   Neumann     du/dn = 0
    //   Dirichlet   u = 1
    //   no-slip     u = 0

    char WestWall_u[20]         = "no-slip";
    char WestWall_v[20]         = "no-slip";
    char WestWall_w[20]         = "no-slip";
    
    char EastWall_u[20]         = "no-slip";
    char EastWall_v[20]         = "no-slip";
    char EastWall_w[20]         = "no-slip";
    
    char SouthWall_u[20]        = "no-slip";
    char SouthWall_v[20]        = "no-slip";
    char SouthWall_w[20]        = "no-slip";
    
    char NorthWall_u[20]        = "no-slip";
    char NorthWall_v[20]        = "no-slip";
    char NorthWall_w[20]        = "no-slip";
    
    char BackWall_u[20]         = "no-slip";
    char BackWall_v[20]         = "no-slip";
    char BackWall_w[20]         = "no-slip";
    
    char FrontWall_u[20]        = "no-slip";
    char FrontWall_v[20]        = "no-slip";
    char FrontWall_w[20]        = "no-slip";

    // ======================================================== //


    // ======================================================== //
    //                                                          //
    //              Boundary conditions calculation             //
    //                                                          //
    // ======================================================== //




    for(j = 0; j < ny+4; ++j){
        for(k = 0; k < nz+4; ++k){

    // West vertical wall

            // =============================== //
            if(strcmp(WestWall_u, "no-slip") == 0)
                u[1][j][k] = 0.0;
            else if(strcmp(WestWall_u, "Neumann") == 0)
                u[1][j][k] = u[2][j][k];
            else if(strcmp(WestWall_u, "Dirichlet") == 0)
                u[1][j][k] = 1.0;


            u[0][j][k] = u[1][j][k];
            // =============================== //


            // =============================== //
            if(strcmp(WestWall_v, "no-slip") == 0)
                v[1][j][k] = 2.0*0.0-v[2][j][k];
            else if(strcmp(WestWall_v, "Neumann") == 0)
                v[1][j][k] = v[2][j][k];
            else if(strcmp(WestWall_v, "Dirichlet") == 0)
                v[1][j][k] = 2.0*1.0-v[2][j][k];


            v[0][j][k] = v[1][j][k];
            // =============================== //


            // =============================== //
            if(strcmp(WestWall_w, "no-slip") == 0)
                w[1][j][k] = 2.0*0.0-w[2][j][k];
            else if(strcmp(WestWall_w, "Neumann") == 0)
                w[1][j][k] = w[2][j][k];
            else if(strcmp(WestWall_w, "Dirichlet") == 0)
                w[1][j][k] = 2.0*1.0-w[2][j][k];


            w[0][j][k] = w[1][j][k];
            // =============================== //



            // East vertical wall

            // =============================== //
            if(strcmp(EastWall_u, "no-slip") == 0)
                u[nx+1][j][k] = 0.0;
            else if(strcmp(EastWall_u, "Neumann") == 0)
                u[nx+1][j][k] = u[nx][j][k];
            else if(strcmp(EastWall_u, "Dirichlet") == 0)
                u[nx+1][j][k] = 1.0;


            u[nx+2][j][k] = u[nx+1][j][k];
            // =============================== //


            // =============================== //
            if(strcmp(EastWall_v, "no-slip") == 0)
                v[nx+2][j][k] = 2.0*0.0-v[nx+1][j][k];
            else if(strcmp(EastWall_v, "Neumann") == 0)
                v[nx+2][j][k] = v[nx+1][j][k];
            else if(strcmp(EastWall_v, "Dirichlet") == 0)
                v[nx+2][j][k] = 2.0*1.0-v[nx+1][j][k];


            v[nx+3][j][k] = v[nx+2][j][k];
            // =============================== //


            // =============================== //
            if(strcmp(EastWall_w, "no-slip") == 0)
                w[nx+2][j][k] = 2.0*0.0-w[nx+1][j][k];
            else if(strcmp(EastWall_w, "Neumann") == 0)
                w[nx+2][j][k] = w[nx+1][j][k];
            else if(strcmp(EastWall_w, "Dirichlet") == 0)
                w[nx+2][j][k] = 2.0*1.0-w[nx+1][j][k];


            w[nx+3][j][k] = w[nx+2][j][k];
           // =============================== //

        }
    }








    for(i = 0; i < nx+4; ++i ){
        for(k = 0; k < nz+4; ++k ){

    // South horizontal wall

            // =============================== //
            if(strcmp(SouthWall_u, "no-slip") == 0)
                u[i][1][k] = 2.0*0.0-u[i][2][k];
            else if(strcmp(SouthWall_u, "Neumann") == 0)
                u[i][1][k] = u[i][2][k];
            else if(strcmp(SouthWall_u, "Dirichlet") == 0)
                u[i][1][k] = 2.0*1.0-u[i][2][k];


            u[i][0][k] = u[i][1][k];
            // =============================== //


            // =============================== //
            if(strcmp(SouthWall_v, "no-slip") == 0)
                v[i][1][k] = 0.0;
            else if(strcmp(SouthWall_v, "Neumann") == 0)
                v[i][1][k] = v[i][2][k];
            else if(strcmp(SouthWall_v, "Dirichlet") == 0)
                v[i][1][k] = 1.0;


            v[i][0][k] = v[i][1][k];
            // =============================== //

            // =============================== //
            if(strcmp(SouthWall_w, "no-slip") == 0)
                w[i][1][k] = 2.0*0.0-w[i][2][k];
            else if(strcmp(SouthWall_w, "Neumann") == 0)
                w[i][1][k] = w[i][2][k];
            else if(strcmp(SouthWall_w, "Dirichlet") == 0)
                w[i][1][k] = 2.0*1.0-w[i][2][k];


            w[i][0][k] = w[i][1][k];
            // =============================== //



    //North horizontal wall

            // =============================== //
            if(strcmp(NorthWall_u, "no-slip") == 0)
                u[i][ny+2][k] = 2.0*0.0-u[i][ny+1][k];
            else if(strcmp(NorthWall_u, "Neumann") == 0)
                u[i][ny+2][k] = u[i][ny+1][k];
            else if(strcmp(NorthWall_u, "Dirichlet") == 0){
                u[i][ny+2][k] = 2.0*1.0-u[i][ny+1][k];
            }
            u[i][ny+3][k] = u[i][ny+2][k];
            // =============================== //


            // =============================== //
            if(strcmp(NorthWall_v, "no-slip") == 0)
                v[i][ny+1][k] = 0.0;
            else if(strcmp(NorthWall_v, "Neumann") == 0)
                v[i][ny+1][k] = v[i][ny][k];
            else if(strcmp(NorthWall_v, "Dirichlet") == 0)
                v[i][ny+1][k] = 1.0;


            v[i][ny+2][k] = v[i][ny+1][k];
            // =============================== //

            // =============================== //
            if(strcmp(NorthWall_w, "no-slip") == 0)
                w[i][ny+2][k] = 2.0*0.0-w[i][ny+1][k];
            else if(strcmp(NorthWall_w, "Neumann") == 0)
                w[i][ny+2][k] = w[i][ny+1][k];
            else if(strcmp(NorthWall_w, "Dirichlet") == 0)
                w[i][ny+2][k] = 2.0*1.0-w[i][ny+1][k];


            w[i][ny+3][k] = w[i][ny+2][k];
            // =============================== //

        }
    }





    
    for(i = 0; i < nx+4; ++i ){
        for(j = 0; j < ny+4; ++j ){

    //Back horizontal wall
            // =============================== //
            if(strcmp(BackWall_u, "no-slip") == 0)
                u[i][j][1] = 2.0*0.0-u[i][j][2];
            else if(strcmp(BackWall_u, "Neumann") == 0)
                u[i][j][1] = u[i][j][2];
            else if(strcmp(BackWall_u, "Dirichlet") == 0)
                u[i][j][1] = 2.0*1.0-u[i][j][2];


            u[i][j][0] = u[i][j][1];
            // =============================== //


            // =============================== //
            if(strcmp(BackWall_v, "no-slip") == 0)
                v[i][j][1] = 2.0*0.0-v[i][j][2];
            else if(strcmp(BackWall_v, "Neumann") == 0)
                v[i][j][1] = v[i][j][2];
            else if(strcmp(BackWall_v, "Dirichlet") == 0)
                v[i][j][1] = 2.0*1.0-v[i][j][2];


            v[i][j][0] = v[i][j][1];
            // =============================== //


            // =============================== //
            if(strcmp(BackWall_w, "no-slip") == 0)
                w[i][j][1] = 0.0;
            else if(strcmp(BackWall_w, "Neumann") == 0)
                w[i][j][1] = w[i][j][2];
            else if(strcmp(BackWall_w, "Dirichlet") == 0)
                w[i][j][1] = 1.0;


            w[i][j][0] = w[i][j][1];
            // =============================== //



    //Front horizontal wall
            // =============================== //
            if(strcmp(FrontWall_u, "no-slip") == 0)
                u[i][j][nz+2] = 2.0*0.0-u[i][j][nz+1];
            else if(strcmp(FrontWall_u, "Neumann") == 0)
                u[i][j][nz+2] = u[i][j][nz+1];
            else if(strcmp(FrontWall_u, "Dirichlet") == 0)
                u[i][j][nz+2] = 2.0*1.0-u[i][j][nz+1];


            u[i][j][ny+3] = u[i][j][ny+2];
            // =============================== //


            // =============================== //
            if(strcmp(FrontWall_v, "no-slip") == 0)
                v[i][j][nz+2] = 2.0*0.0-v[i][j][nz+1];
            else if(strcmp(FrontWall_v, "Neumann") == 0)
                v[i][j][nz+2] = v[i][j][nz+1];
            else if(strcmp(FrontWall_v, "Dirichlet") == 0)
                v[i][j][nz+2] = 2.0*1.0-v[i][j][nz+1];


            v[i][j][nz+3] = v[i][j][nz+2];
            // =============================== //


            // =============================== //
            if(strcmp(FrontWall_w, "no-slip") == 0)
                w[i][j][nz+1] = 0.0;
            else if(strcmp(FrontWall_w, "Neumann") == 0)
                w[i][j][nz+1] = w[i][j][nz];    
            else if(strcmp(FrontWall_w, "Dirichlet") == 0)
                w[i][j][nz+1] = 1.0;      


            w[i][j][nz+2] = w[i][j][nz+1];
            // =============================== //

        }
    }



    for(i = 0; i < nx+4; ++i ){
        for(j = 0; j < ny+4; ++j ){
            for(k = 0; k < nz+4; ++k){

                u1[i][j][k] = u[i][j][k];
                v1[i][j][k] = v[i][j][k];
                w1[i][j][k] = w[i][j][k];

                u2[i][j][k] = u[i][j][k];
                v2[i][j][k] = v[i][j][k];
                w2[i][j][k] = w[i][j][k];

                u_star[i][j][k] = u[i][j][k];
                v_star[i][j][k] = v[i][j][k];
                w_star[i][j][k] = w[i][j][k];

            }
        }
    }



    for(j = 0; j < ny+4; ++j ){
        for(k = 0; k < nz+4; ++k ){

            //West vertical wall
            p[1][j][k] = p[2][j][k];
            p[0][j][k] = p[1][j][k];

            //East vertical wall
            p[nx+2][j][k] = p[nx+1][j][k];
            p[nx+3][j][k] = p[nx+2][j][k];
        }
    }


    for(i = 0; i < nx+4; ++i ){
        for(k = 0; k < nz+4; ++k ){

            //// South horizontal wall
            p[i][1][k] = p[i][2][k];
            p[i][0][k] = p[i][1][k];

            // //North horizontal wall
            p[i][ny+2][k] = p[i][ny+1][k];
            p[i][ny+3][k] = p[i][ny+2][k];
        }
    }

    for(i = 0; i < nx+4; ++i ){
        for(j = 0; j < ny+4; ++j ){

            // Back horizontal wall
            p[i][j][1] = p[i][j][2];
            p[i][j][0] = p[i][j][1];

            // Front horizontal wall
            p[i][j][nz+2] = p[i][j][nz+1];
            p[i][j][nz+3] = p[i][j][nz+2];
        }
    }



}