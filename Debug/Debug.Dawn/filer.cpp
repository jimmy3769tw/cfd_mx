#include <stdlib.h> 
#include <stdio.h>
#include <math.h>
#include <omp.h>
#include <cstring>
#include <iostream>

#include "Resolution.h"

void Output_Q_Cpp_PLOT3D
    (
    // ======================================================== //
    int istep,

    double (*p)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],

    double (*u2)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],
    double (*v2)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],
    double (*w2)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4],

	double (*ETA)[ny+4][nz+4] = new double[nx+4][ny+4][nz+4]
    
    // ======================================================== //
    )
{

    // ======================================================== //
    double (*uc)[ny][nz] = new double[nx][ny][nz];
    double (*vc)[ny][nz] = new double[nx][ny][nz];
    double (*wc)[ny][nz] = new double[nx][ny][nz];

	

    int Nblock = 1;
	int tempNX = nx;
	int tempNY = ny;
	int tempNZ = nz;
	int N_total = tempNX*tempNY*tempNZ;

    double temp = 1.0;    // mach, alpha, reyn, time //

    float(*QUout)[ny+2][nz+2] = new float[nx+2][ny+2][nz+2];


    // ======================================================== //



    for(size_t i = 0; i < nx; ++i )
	{
        for(size_t j = 0; j < ny; ++j )
		{
            for(size_t k = 0; k < nz; ++k)
			{
				
                uc[i][j][k] = 0.5 * (u2[i+2][j+2][k+2] + u2[i+1][j+2][k+2]);
                vc[i][j][k] = 0.5 * (v2[i+2][j+2][k+2] + v2[i+2][j+1][k+2]);
                wc[i][j][k] = 0.5 * (w2[i+2][j+2][k+2] + w2[i+2][j+2][k+1]);


            }
        }
    }

	static int io;
    char LESdata[100];
	FILE *fptrQ;
	sprintf(LESdata,"P3D""%0.5d"".q",io);
	fptrQ = fopen(LESdata,"wb");
	io ++;

	fwrite(&Nblock, sizeof(int), 1,fptrQ);

	fwrite(&tempNX, sizeof(int), 1,fptrQ);
	fwrite(&tempNY, sizeof(int), 1,fptrQ);
	fwrite(&tempNZ, sizeof(int), 1,fptrQ);

	fwrite(&temp, sizeof(float), 1,fptrQ);
	fwrite(&temp, sizeof(float), 1,fptrQ);
	fwrite(&temp, sizeof(float), 1,fptrQ);
	fwrite(&temp, sizeof(float), 1,fptrQ);




	for (size_t i = 0; i < nx; ++i) 
	{
		for (size_t j = 0; j < ny; ++j) 
		{ 
			for (size_t k = 0; k < nz; ++k) 
			{ 

				QUout[i][j][k] = p[i+2][j+2][k+2];

			}
		}
	}

	for (size_t k = 0; k < nz; ++k) 
	{
		for (size_t j = 0; j < ny; ++j) 
		{ 
			for (size_t i = 0; i < nx; ++i) 
			{

				fwrite(&QUout[i][j][k],sizeof(float),1,fptrQ);

			}
		}
	}


	for (size_t i = 0; i < nx; ++i) 
	{
		for (size_t j = 0; j < ny; ++j) 
		{ 
			for (size_t k = 0; k < nz; ++k) 
			{ 

				QUout[i][j][k] = uc[i][j][k];

			}
		}
	}

	for (size_t k = 0; k < nz; ++k) 
	{
		for (size_t j = 0; j < ny; ++j) 
		{ 
			for (size_t i = 0; i < nx; ++i) 
			{

				fwrite(&QUout[i][j][k],sizeof(float),1,fptrQ);

			}
		}
	}



	for (size_t i = 0; i < nx; ++i) 
	{
		for (size_t j = 0; j < ny; ++j) 
		{ 
			for (size_t k = 0; k < nz; ++k) 
			{ 

				QUout[i][j][k] = vc[i][j][k];

			}
		}
	}

	for (size_t k = 0; k < nz; ++k) 
	{
		for (size_t j = 0; j < ny; ++j) 
		{ 
			for (size_t i = 0; i < nx; ++i) 
			{

				fwrite(&QUout[i][j][k],sizeof(float),1,fptrQ);

			}
		}
	}



	for (size_t i = 0; i < nx; ++i) 
	{
		for (size_t j = 0; j < ny; ++j) 
		{ 
			for (size_t k = 0; k < nz; ++k) 
			{ 

				QUout[i][j][k] = wc[i][j][k];

			}
		}
	}

	for (size_t k = 0; k < nz; ++k) 
	{
		for (size_t j = 0; j < ny; ++j) 
		{ 
			for (size_t i = 0; i < nx; ++i) 
			{

				fwrite(&QUout[i][j][k],sizeof(float),1,fptrQ);

			}
		}
	}



    for (size_t i = 2; i < nx+2; ++i) 
	{
		for (size_t j = 2; j < ny+2; ++j) 
		{ 
			for (size_t k = 2; k < nz+2; ++k) 
			{ 

				QUout[i][j][k] = ETA[i][j][k];

			}
		}
	}

	for (size_t k = 0; k < nz; ++k) 
	{
		for (size_t j = 0; j < ny; ++j) 
		{ 
			for (size_t i = 0; i < nx; ++i) 
			{

				fwrite(&QUout[i][j][k],sizeof(float),1,fptrQ);

			}
		}
	}

    



	fclose(fptrQ);

	delete [] QUout;


}