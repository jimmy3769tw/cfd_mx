#include <stdlib.h> 
#include <stdio.h>
#include <math.h>
#include <omp.h>
#include <cstring>


#include "Resolution.h"

void gridder
    (
    // ======================================================== //
    double GridderXc, double GridderYc, double GridderZc, 
    double lxSml, double lySml, double lzSml, double dx,
    double dy, double dz, double dxSml, double dySml, double dzSml,
    char *Gridder,


    double (*X) = new double[nx+5],
    double (*Y) = new double[ny+5],
    double (*Z) = new double[nz+5],

    double (*Dx) = new double[nx+3],
    double (*Dxs) = new double[nx+3],
    double (*Dy) = new double[ny+3],
    double (*Dys) = new double[ny+3],
    double (*Dz) = new double[nz+3],
    double (*Dzs) = new double[nz+3],

    double (*Xa) = new double[nx],
    double (*Ya) = new double[ny],
    double (*Za) = new double[nz],

    double (*Xc) = new double[nx-1],
    double (*Yc) = new double[ny-1],
    double (*Zc) = new double[nz-1]
    // ======================================================== //
    )


{
    // ======================================================== //

    const double xSBgn = GridderXc - lxSml/2.0;

    const double xSEnd = GridderXc + lxSml/2.0;

    const double ySBgn = GridderYc - lySml/2.0;

    const double ySEnd = GridderYc + lySml/2.0;

    const double zSBgn = GridderZc - lzSml/2.0;

    const double zSEnd = GridderZc + lzSml/2.0;


    double xNextLrgValue;

    double xNextSmlValue;
    
    double yNextLrgValue;
    
    double yNextSmlValue;
    
    double zNextLrgValue;
    
    double zNextSmlValue;

    // ======================================================== //



    if(strcmp(Gridder, "non-uniform")  == 0)
    {

        //Unequal grid intervals
        // ======================================================== //

        for(size_t i = 2; i < nx+5; ++i)
        {
            xNextLrgValue =  X[i-1] + dx;   
            xNextSmlValue =  X[i-1] + dxSml;  

            if(i == 2){

                X[i] = 0.0;

            }
            else if(xNextLrgValue > xSBgn)
            {

                if(xNextSmlValue > xSEnd)
                {

                    X[i] = xNextLrgValue;


                }
                else{

                    X[i] = xNextSmlValue;

                }
            }
            else{
                X[i] = xNextLrgValue;
            }
            

        }

        for(size_t j = 2; j < ny+5; ++j)
        {
            yNextLrgValue =  Y[j-1] + dy;   
            yNextSmlValue =  Y[j-1] + dySml;  

            if(j == 2)
            {

                Y[j] = 0.0;

            }
            else if(yNextLrgValue > ySBgn)
            {

                if(yNextSmlValue > ySEnd)
                {

                    Y[j] = yNextLrgValue;

                }
                else{

                    Y[j] = yNextSmlValue;

                }

                

            }
            else{
                Y[j] = yNextLrgValue;
            }


        }


        for(size_t k = 2; k < nz+5; ++k)
        {
            zNextLrgValue =  Z[k-1] + dz;   
            zNextSmlValue =  Z[k-1] + dzSml;  

            if(k == 2)
            {

                Z[k] = 0.0;

            }
            else if(zNextLrgValue > zSBgn)
            {

                if(zNextSmlValue > zSEnd)
                {

                    Z[k] = zNextLrgValue;

                }
                else{

                    Z[k] = zNextSmlValue;

                }

                

            }
            else
            {
                Z[k] = zNextLrgValue;
            }


        }

        
        // ======================================================== //


    }
    else if(strcmp(Gridder, "uniform")  == 0)
    {



        //equal grid intervals
        // ======================================================== //
        for(size_t i = 2; i < nx+5; ++i)
        {
            if(i==2)
            {
                X[i] = 0.0;
                X[i-1] = X[i] - dx;
                X[i-2] = X[i-1] - dx;
            }
            else{
                X[i] = X[i-1] + dx;
            }
        }


        for(size_t j = 2; j < ny+5; ++j)
        {
            if(j==2)
            {
                Y[j] = 0.0;
                Y[j-1] = Y[j] - dy;
                Y[j-2] = Y[j-1] - dy;
            }
            else
            {
                Y[j] = Y[j-1] + dy;
            }
        }
        
        for(size_t k = 2; k < nz+5; ++k)
        {
            if(k==2)
            {
                Z[k] = 0.0;
                Z[k-1] = Z[k] - dz;
                Z[k-2] = Z[k-1] - dz;
            }
            else
            {
                Z[k] = Z[k-1] + dz;
            }
        }


        // ======================================================== //
    }

    



    





    // Define each of the directional grid lengths
    for (size_t i = 2; i < nx+2; ++i)
    {
        Dx[i] = ( X[i+1] - X[i] );
        Dxs[i] = ( X[i+2] - X[i] ) / 2.0;
    }

    for (size_t j = 2; j < ny+2; ++j)
    {
        Dy[j] = ( Y[j+1] - Y[j] );
        Dys[j] = ( Y[j+2] - Y[j] ) / 2.0;
    }


    for (size_t k = 2; k < nz+2; ++k)
    {
        Dz[k] = ( Y[k+1] - Z[k] );
        Dzs[k] = ( Y[k+2] - Z[k] ) / 2.0;
    }


    // Ghost boundary grid lengths
    Dx[1]    = Dx[2];
    Dx[0]    = Dx[2];
    Dx[nx+2] = Dx[nx+1];
    Dx[nx+3] = Dx[nx+1];

    Dxs[1]    = Dxs[2];
    Dxs[0]    = Dxs[2];
    Dxs[nx+2] = Dxs[nx+1];
    Dxs[nx+3] = Dxs[nx+1];


    Dy[1]    = Dy[2];
    Dy[0]    = Dy[2];
    Dy[ny+2] = Dy[ny+1];
    Dy[ny+3] = Dy[ny+1];

    Dys[1]    = Dys[2];
    Dys[0]    = Dys[2];
    Dys[ny+2] = Dys[ny+1];
    Dys[ny+3] = Dys[ny+1];


    Dz[1]    = Dz[2];
    Dz[0]    = Dz[2];
    Dz[nz+2] = Dz[nz+1];
    Dz[nz+3] = Dz[nz+2];

    Dzs[1]    = Dzs[2];
    Dzs[0]    = Dzs[2];
    Dzs[nz+2] = Dzs[nz+1];
    Dzs[nz+3] = Dzs[nz+1];



    // Modifying the index of X, Y and Z arrays to represent the actual grid
    for (size_t i = 0; i < nx+2; ++i)
    {
        Xa[i] = X[i+2];
    }

    for (size_t j = 0; j < ny+2; ++j)
    {
        Ya[j] = Y[j+2];
    }

    for (size_t k = 0; k < nz+2; ++k)
    {
        Za[k] = Z[k+2];
    }



    // Defining the midpoint values of the grids
    for (size_t i = 0; i < nx; ++i)
    {
        Xc[i] = ( Xa[i] + Xa[i+1] ) / 2.0;
    }

    for (size_t j = 0; j < ny; ++j)
    {
        Yc[j] = ( Ya[j] + Ya[j+1] ) / 2.0;
    }

    for (size_t k = 0; k < nz; ++k)
    {
        Zc[k] = ( Za[k] + Za[k+1] ) / 2.0;
    }

    


    int Nblock = 1;
	int tempNX = nx;
	int tempNY = ny;
	int tempNZ = nz;
	int N_total = tempNX*tempNY*tempNZ;


	float (*Xout)[ny][nx] = new float[nz][ny][nx];
	float (*Yout)[ny][nx] = new float[nz][ny][nx];
	float (*Zout)[ny][nx] = new float[nz][ny][nx];




	for (size_t  i = 0; i < nx; ++i) 
    {
		for (size_t  j = 0; j < ny; ++j) 
        { 
			for (size_t  k = 0; k < nz; ++k) 
            { 

				Xout[k][j][i] = Xc[i];
				Yout[k][j][i] = Yc[j];
				Zout[k][j][i] = Zc[k];

			}
		}
	}




	char LESdata[100];
	FILE *fptr;
	sprintf(LESdata,"P3D""%0.5d"".x",0);
	fptr = fopen(LESdata,"wb");

	fwrite(&Nblock, sizeof(int), 1,fptr);


	fwrite(&tempNX, sizeof(int), 1,fptr);
	fwrite(&tempNY, sizeof(int), 1,fptr);
	fwrite(&tempNZ, sizeof(int), 1,fptr);


	fwrite(Xout,sizeof(float),N_total,fptr);
	fwrite(Yout,sizeof(float),N_total,fptr);
	fwrite(Zout,sizeof(float),N_total,fptr);
    

	
    fclose(fptr);

    delete [] Xout;
	delete [] Yout;
	delete [] Zout;

    
}

    








