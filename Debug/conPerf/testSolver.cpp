#include <cmath>
#include <omp.h>

#include <iomanip>              // set output width
#include <fstream>              // file output
#include <limits>               // for "numeric_limits<double>::digits10"
#include <sys/types.h>
#include <dirent.h>

#include <yakutat/SparseMatrix/SparseMatrixELL.hpp>

#include <yakutat/SparseMatrix/SparseMatrixCSR.hpp>


#include <random>

                   
#include <grid/structureGrid.hpp>

#include "solver/math.hpp"

#include "grid/Generator.hpp"

#include "controlPanel.hpp"

#include "matrix/ELL_sparseMatrix.hpp"

#include "matrix/CSR_sparseMatrix.hpp"
#include "matrix/CSR_sparseMatrix0.hpp"

#include "matrix/SPE_sparseMatrix.hpp"
#include "matrix/SPE_sparseMatrix0.hpp"
#include "matrix/SPE_sparseMatrix1.hpp"
// ----------------`

// ----------------
#include "matrix/ELL_sparseMatrix0.hpp"

#include "profiling/STL_clock.hpp"


// --------------------------------
#include "solver/bicgstab.hpp"

#include "solver/bicgstab0.hpp"

#include "solver/bicgstabRe.hpp"

#include "solver/bicgstabRe2.hpp"
// --------------------------------



using namespace std;

class sol{

    public:

    auto ini(vector<double> &x, vector<double> &b){
        pre_x.resize(x.size());
        pre_x = x;
    }


    auto ini(vector<double> &x){
        pre_x.resize(x.size());
        pre_x = x;
    }



    auto checkX(vector<double> &x){
        auto sum = 0;
        for (size_t i = 0; i< x.size(); i++){
            sum += abs(pre_x.at(i)-x[i]);
        }
        pre_x = x;
        return sum;
    }

    auto checkX(vector<double> &x, vector<double> &b){
        auto sum = 0;
        for (size_t i = 0; i< x.size(); i++){
            sum += abs(pre_x.at(i)-x[i]);
        }
        pre_x = x;
        return sum;
    }
    private:
        std::vector<double> pre_x;

};


int main()
{

    int time  = 50;

    // -------------------------------------

    sol ch;
    grid gA;


    simuClass simu;

    shareMenory ShareM;


  // ! ============================  divid Domain ============================

  calDomain Lo;

  std::vector<int> grid_size{gA.nx, gA.ny, gA.nz};

  std::vector<int> dims{1, 1, 1};

  Lo.initTable(grid_size, dims);

  Lo.initLocal(0, 0, 0);


    // ! ---------------------------------------init 
    std::vector<stopWatch>  Timer(7);
    std::vector<stopWatch>  Timer_Seq(7);

    vector<string> variables, zone, A;
    string t = ", ";
    string tab = " ";
    string n = "\n";

    variables.push_back("processor");
    variables.push_back("Speed-up");
    variables.push_back("Speed-up(compare CSR0)");

    zone.push_back("CSR");
    zone.push_back("CSR0");
    zone.push_back("ELL");
    zone.push_back("ELL0");
    zone.push_back("SPE");
    zone.push_back("SPE0");
    zone.push_back("SPE1");
    // zone.push_back("mat_ELL_yakutat");


    A.resize(zone.size());

    for (size_t i = 0 ; i < zone.size(); i ++)
    {
        A.at(i)  += "TITLE     = \"\"\n";
        A.at(i)  += "VARIABLES = \"";
        A.at(i)  += variables.at(0);

        A.at(i)  += "\",\"";
        A.at(i)  += variables.at(1);

        A.at(i)  += "\"\n";
        A.at(i)  += "ZONE T=\"";
        A.at(i)  +=  zone.at(i);
        A.at(i)  += "\"\n";
    }
    // * ---------------------------------------init 


  // * ========================================================================
    generateGride(simu, ShareM, Lo, gA); 
    // -------------------------------------
    vector<double> x(gA.iceltotCal, 1.0), b(gA.iceltotCal, 1.0);

    mat::SPE_matrix<double> SPE(gA.nxCal, gA.nyCal, gA.nzCal);

    mat::SPE_matrix0<double> SPE0(gA.nxCal, gA.nyCal, gA.nzCal);

    mat::SPE_matrix1<double> SPE1(gA.nxCal, gA.nyCal, gA.nzCal);

    mat::ELL_matrix<double> ELL(gA.iceltotCal, 7);

    mat::ELL_matrix0<double> ELL0(gA.iceltotCal, 7);

    mat::CSR_matrix<double> CSR;
    mat::CSR_matrix0<double> CSR0;

    #if defined(YAKUTA)

    yakutat::SparseMatrixELL<double , 7> mat_ELL_yakutat(gA.iceltotCal);

    yakutat::SparseMatrixCSR<double> mat_CSR_yakutat(gA.iceltotCal);
    #endif 

    CSR.set(gA.createPressureMatrixCSR());

    CSR0.set(gA.createPressureMatrixCSR());

    // --------------------
    SPE.setupPressure( gA.gC, gA.Dx, gA.Dy, gA.Dz,gA.Dxs, gA.Dys, gA.Dzs);

    SPE0.setupPressure( gA.gC, gA.Dx, gA.Dy, gA.Dz,gA.Dxs, gA.Dys, gA.Dzs);

    SPE1.setupPressure( gA.gC, gA.Dx, gA.Dy, gA.Dz,gA.Dxs, gA.Dys, gA.Dzs);

    // auto jp = SPE1.set_Jp_pc();

    // --------------------

    // --------------------
    ELL.set(gA.createPressureMatrixCSR());

    ELL.sort_Idx();

    ELL0.set(gA.createPressureMatrixCSR());
    // --------------------


    std::random_device rd;
    std::default_random_engine gen = std::default_random_engine(rd());


    auto b_random = b;
    auto x_random = x;


    std::uniform_real_distribution<double> dis_real(1.0,1000.0);

    auto Randfunction = [&](auto &X){
        for (auto& x: X){
            x =  dis_real(gen);
        }
    };

    Randfunction(b_random);
    sol chr;

    SPE1.multiply(b_random, x_random);

    chr.ini(x_random);

    // --------------
    x_random = x;
    CSR.multiply_omp(b_random, x_random);

    cout << " CSR.multiply [" << chr.checkX(x_random) << "] " ;
    // --------------


    // --------------
    x_random = x;
    SPE0.multiply_omp(b_random, x_random);

    cout << " SPE.multiply [" << chr.checkX(x_random) << "] " ;
    // --------------


    // --------------
    x_random = x;
    CSR0.multiply_omp(b_random, x_random);

    cout << " CSR0.multiply [" << chr.checkX(x_random) << "] " ;
    // --------------


    // --------------
    x_random = x;
    ELL.multiply_omp(b_random, x_random);

    cout << " ELL.multiply [" << chr.checkX(x_random) << "] " ;
    // --------------


    // --------------
    x_random = x;
    ELL0.multiply_omp(b_random, x_random);

    cout << " ELL0.multiply [" << chr.checkX(x_random) << "] " ;
    // --------------




    cout << "\ncheck solver \n" ;



    solver::bicgstab<mat::SPE_matrix<double> > solverSPE(SPE);



    solver::bicgstab<mat::SPE_matrix0<double> > solverSPE0(SPE0);



    solver::bicgstab<mat::SPE_matrix1<double> > solverSPE1(SPE1);




    x_random = x;
    auto [iters, error] = solverSPE(b_random, x_random);
    chr.ini(x_random);


    x_random = x;
    auto [iters_0, error_0] = solverSPE0(b_random, x_random);
    cout << " solverSPE0 [" << chr.checkX(x_random) << "] " ;

    x_random = x;
    auto [iters_1, error_1] =  solverSPE1(b_random, x_random);
    cout << " solverSPE1 [" << chr.checkX(x_random) << "] " ;


    cout << " \niters "  << iters_0 << ", " << iters_1 << ", " << iters << endl; 
    cout << " \nerror "  << error << ", " << error_0 << ", " << error_1 << endl; 


    return 0;
}