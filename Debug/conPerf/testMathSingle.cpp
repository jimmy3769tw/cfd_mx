#include <cmath>
#include <omp.h>

#include <iomanip>              // set output width
#include <fstream>              // file output
#include <limits>               // for "numeric_limits<double>::digits10"
#include <sys/types.h>
#include <dirent.h>

#include <grid/structureGrid.hpp>
#include "solver/math.hpp"

#include "controlPanel.hpp"

#include "profiling/STL_clock.hpp"

using namespace std;


int main()
{
    int time  = 5000;
    // -------------------------------------
    grid gA;

    vector<double> x(gA.iceltotCal, 1.0), b(gA.iceltotCal, 1.0);

    double a;

    vector<double> x_aff(gA.iceltotCal), b_aff(gA.iceltotCal);

    cout <<"\n#threads :" << omp_get_max_threads() << "  [time] " << time << endl;


    static auto s = [&](auto a){ return std::to_string(a);};
    static auto ws = [&](auto a){ std::ostringstream s; s << a; return s.str();};

    std::vector<stopWatch> timeMath(6);

    int Nthreads{20};
    omp_set_num_threads(Nthreads);

    auto x_Naff = x;
    auto b_Naff = b;

    // timeMath[0].init_start();

    // for (int i = 0; i < time ;i ++)
    // a =  math::dotseq(b_Naff, x_Naff);

    // timeMath[0].stop();


    // -----------------------------
    cout << "A " << a;
    math::init(x_aff, x);
    math::init(b_aff, b);
    // -----------------------------



    timeMath[1].init_start();

    for (int i = 0; i < time ;i ++)
        a =  math::dot(b_aff, x_aff);

    timeMath[1].stop();


    // timeMath[1].init_start();

    // for (int i = 0; i < time ;i ++)
    //     a =  math::dot(b_Naff, x_Naff);

    // timeMath[1].stop();





    cout << "Timer[dot] "   << "\t"<< timeMath[0].elapsedTime() <<  ", " << timeMath[1].elapsedTime() << 
        "\tSpeedUP "<< (timeMath[0].elapsedTime() /  timeMath[1].elapsedTime()) << endl;


    return 0;
}