#include <cmath>
#include <omp.h>

#include <iomanip>              // set output width
#include <fstream>              // file output
#include <limits>               // for "numeric_limits<double>::digits10"
#include <sys/types.h>
#include <dirent.h>

#include <grid/structureGrid.hpp>
#include "solver/math.hpp"

#include "controlPanel.hpp"

#include "profiling/STL_clock.hpp"

using namespace std;


int main()
{
    int time  = 60;
    // -------------------------------------
    grid gA;

    vector<double> x(gA.iceltotCal, 1.0), b(gA.iceltotCal, 1.0);

    double a;

    vector<double> x_aff(gA.iceltotCal), b_aff(gA.iceltotCal);

    vector<string> variables, zone, A;
    string t = ", ";
    string tab = " ";
    string n = "\n";

    variables.push_back("Thread");
    variables.push_back("Speed Up");

    zone.push_back("point_SIMD");
    zone.push_back("point");
    zone.push_back("index_SIMD");
    zone.push_back("index");
    A.resize(zone.size());
    for (size_t i = 0 ; i < zone.size(); i ++)
    {
        A.at(i)  += "TITLE     = \"\"\n";
        A.at(i)  += "VARIABLES = \"";
        A.at(i)  += variables.at(0);

        A.at(i)  += "\",\"";
        A.at(i)  += variables.at(1);

        A.at(i)  += "\"\n";
        A.at(i)  += "ZONE T=\"";
        A.at(i)  +=  zone.at(i);
        A.at(i)  += "\"\n";
    }
    // * ---------------------------------------init 

    cout <<"\n#threads :" << omp_get_max_threads() << "  [time] " << time << endl;


    static auto s = [&](auto a){ return std::to_string(a);};
    static auto ws = [&](auto a){ std::ostringstream s; s << a; return s.str();};

    std::vector<stopWatch> timeMath(6);

    for (int Nthreads = 1; Nthreads <= 20; ++Nthreads)
    {

        omp_set_num_threads(Nthreads);

        auto x_Naff = x;
        auto b_Naff = b;

        timeMath[0].init_start();

        for (int i = 0; i < time ;i ++)
        a =  math::dotseq(b_Naff, x_Naff);

        timeMath[0].stop();



        // // -----------------------------
        // cout << "A " << a;
        // math::init(x_aff, x);
        // math::init(b_aff, b);
        // // -----------------------------



        // // -----------------------------
        cout << "A " << a;
        math::init(x_Naff, x);
        math::init(b_Naff, b);
        // // -----------------------------


        timeMath[1].init_start();

        for (int i = 0; i < time ;i ++)
            a =  math::dot(b_aff, x_aff);

        timeMath[1].stop();


        // -----------------------------
        cout << "A " << a;
        math::init(x_aff, x);
        math::init(b_aff, b);
        // -----------------------------


        timeMath[2].init_start();

        for (int i = 0; i < time ;i ++)
            a =  math::dotNsimd(b_aff, x_aff);

        timeMath[2].stop();


        // -----------------------------
        cout << "A " << a;
        math::init(x_aff, x);
        math::init(b_aff, b);
        // -----------------------------



        timeMath[3].init_start();

        for (int i = 0; i < time ;i ++)
            a =  math::dot_Npoint(b_aff, x_aff);

        timeMath[3].stop();




        // -----------------------------
        cout << "A " << a;
        math::init(x_aff, x);
        math::init(b_aff, b);
        // -----------------------------



        timeMath[4].init_start();

        for (int i = 0; i < time ;i ++)
            a =  math::dot_Npoint_Nsimd(b_aff, x_aff);

        timeMath[4].stop();


        // -----------------------------
        cout << "A " << a;
        math::init(x_aff, x);
        math::init(b_aff, b);
        // -----------------------------


        #pragma omp parallel
        {
            #pragma omp single
            cout << omp_get_num_threads() 
                 << tab  << timeMath[0].elapsedTime()  
                 << ", " << timeMath[1].elapsedTime()
                 << ", " << timeMath[0].elapsedTime() /  timeMath[1].elapsedTime()
                 << endl;


            for (size_t i = 0 ; i < zone.size(); i++)
            {

                #pragma omp single
                {
                    A.at(i) += s(omp_get_num_threads()) ;
                    A.at(i) += tab;
                    A.at(i) += s(timeMath[0].elapsedTime() /  timeMath[i+1].elapsedTime());
                    A.at(i) += "\n";
                }

            }
        }
    } // ----------------------------



    std::ofstream file;
    std::string fileN = "speeup.dat";
    file.open (fileN, std::ios::out); // |ios::app
    for (size_t i = 0 ; i < zone.size(); i++) { file << A.at(i); }  
    file.close();

    cout << "Timer[dot] "   << "\t"<< timeMath[0].elapsedTime() <<  ", " << timeMath[1].elapsedTime() << 
        "\tSpeedUP "<< (timeMath[0].elapsedTime() /  timeMath[1].elapsedTime()) << endl;


    return 0;
}