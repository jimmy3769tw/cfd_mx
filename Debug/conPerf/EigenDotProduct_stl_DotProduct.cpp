

#include <cmath>
#include <omp.h>
#include <grid/structureGrid.hpp>

#include "grid/Generator.hpp"
#include "controlPanel.hpp"

// ----------------`

// ----------------
#include "matrix/ELL_sparseMatrix0.hpp"

#include "profiling/STL_clock.hpp"

using namespace std;