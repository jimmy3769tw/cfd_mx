#include <cmath>
#include <omp.h>

#include <iomanip>              // set output width
#include <fstream>              // file output
#include <limits>               // for "numeric_limits<double>::digits10"
#include <sys/types.h>
#include <dirent.h>

#include <yakutat/SparseMatrix/SparseMatrixELL.hpp>

#include <yakutat/SparseMatrix/SparseMatrixCSR.hpp>


#include <random>

                   
#include <grid/structureGrid.hpp>

#include "solver/math.hpp"

#include "grid/Generator.hpp"

#include "controlPanel.hpp"

#include "matrix/ELL_sparseMatrix.hpp"

#include "matrix/CSR_sparseMatrix.hpp"
#include "matrix/CSR_sparseMatrix0.hpp"

#include "matrix/SPE_sparseMatrix.hpp"
#include "matrix/SPE_sparseMatrix0.hpp"
#include "matrix/SPE_sparseMatrix1.hpp"
// ----------------`

// ----------------
#include "matrix/ELL_sparseMatrix0.hpp"

#include "profiling/STL_clock.hpp"

using namespace std;

class sol{

    public:

    auto ini(vector<double> &x, vector<double> &b){
        pre_x.resize(x.size());
        pre_x = x;
    }




    auto ini(vector<double> &x){
        pre_x.resize(x.size());
        pre_x = x;
    }



    auto checkX(vector<double> &x){
        auto sum = 0;
        for (size_t i = 0; i< x.size(); i++){
            sum += abs(pre_x.at(i)-x[i]);
        }
        pre_x = x;
        return sum;
    }

    auto checkX(vector<double> &x, vector<double> &b){
        auto sum = 0;
        for (size_t i = 0; i< x.size(); i++){
            sum += abs(pre_x.at(i)-x[i]);
        }
        pre_x = x;
        return sum;
    }
    private:
        std::vector<double> pre_x;

};


int main()
{

    int time  = 50;

    // -------------------------------------

    sol ch;
    grid gA;


    simuClass simu;

    shareMenory ShareM;


  // ! ============================  divid Domain ============================

  calDomain Lo;

  std::vector<int> grid_size{gA.nx, gA.ny, gA.nz};

  std::vector<int> dims{1, 1, 1};

  Lo.initTable(grid_size, dims);

  Lo.initLocal(0, 0, 0);


    // ! ---------------------------------------init 
    std::vector<stopWatch>  Timer(7);
    std::vector<stopWatch>  Timer_Seq(7);

    vector<string> variables, zone, A;
    string t = ", ";
    string tab = " ";
    string n = "\n";

    variables.push_back("processor");
    variables.push_back("Speed-up");
    variables.push_back("Speed-up(compare CSR0)");

    zone.push_back("CSR");
    zone.push_back("CSR0");
    zone.push_back("ELL");
    zone.push_back("ELL0");
    zone.push_back("SPE");
    zone.push_back("SPE0");
    zone.push_back("SPE1");
    // zone.push_back("mat_ELL_yakutat");


    A.resize(zone.size());

    for (size_t i = 0 ; i < zone.size(); i ++)
    {
        A.at(i)  += "TITLE     = \"\"\n";
        A.at(i)  += "VARIABLES = \"";
        A.at(i)  += variables.at(0);

        A.at(i)  += "\",\"";
        A.at(i)  += variables.at(1);

        A.at(i)  += "\"\n";
        A.at(i)  += "ZONE T=\"";
        A.at(i)  +=  zone.at(i);
        A.at(i)  += "\"\n";
    }
    // * ---------------------------------------init 



  // * ========================================================================
    generateGride(simu, ShareM, Lo, gA); 
    // -------------------------------------
    vector<double> x(gA.iceltotCal, 1.0), b(gA.iceltotCal, 1.0);

    mat::SPE_matrix<double> SPE(gA.nxCal, gA.nyCal, gA.nzCal);

    mat::SPE_matrix0<double> SPE0(gA.nxCal, gA.nyCal, gA.nzCal);

    mat::SPE_matrix1<double> SPE1(gA.nxCal, gA.nyCal, gA.nzCal);

    mat::ELL_matrix<double> ELL(gA.iceltotCal, 7);

    mat::ELL_matrix0<double> ELL0(gA.iceltotCal, 7);

    mat::CSR_matrix<double> CSR;
    mat::CSR_matrix0<double> CSR0;

    #if defined(YAKUTA)

    yakutat::SparseMatrixELL<double , 7> mat_ELL_yakutat(gA.iceltotCal);

    yakutat::SparseMatrixCSR<double> mat_CSR_yakutat(gA.iceltotCal);
    #endif 

    CSR.set(gA.createPressureMatrixCSR());

    CSR0.set(gA.createPressureMatrixCSR());

    // --------------------
    SPE.setupPressure( gA.gC, gA.Dx, gA.Dy, gA.Dz,gA.Dxs, gA.Dys, gA.Dzs);

    SPE0.setupPressure( gA.gC, gA.Dx, gA.Dy, gA.Dz,gA.Dxs, gA.Dys, gA.Dzs);

    SPE1.setupPressure( gA.gC, gA.Dx, gA.Dy, gA.Dz,gA.Dxs, gA.Dys, gA.Dzs);

    // auto jp = SPE1.set_Jp_pc();

    // --------------------

    // --------------------
    ELL.set(gA.createPressureMatrixCSR());

    ELL.sort_Idx();

    ELL0.set(gA.createPressureMatrixCSR());
    // --------------------


    std::random_device rd;
    std::default_random_engine gen = std::default_random_engine(rd());


    auto b_random = b;
    auto x_random = x;


    std::uniform_real_distribution<double> dis_real(1.0,1000.0);

    auto Randfunction = [&](auto &X){
        for (auto& x: X){
            x =  dis_real(gen);
        }
    };

    Randfunction(b_random);
    sol chr;

    SPE1.multiply(b_random, x_random);

    chr.ini(x_random);

    // --------------
    x_random = x;
    CSR.multiply_omp(b_random, x_random);

    cout << " CSR.multiply [" << chr.checkX(x_random) << "] " ;
    // --------------


    // --------------
    x_random = x;
    SPE0.multiply_omp(b_random, x_random);

    cout << " SPE.multiply [" << chr.checkX(x_random) << "] " ;
    // --------------


    // --------------
    x_random = x;
    CSR0.multiply_omp(b_random, x_random);

    cout << " CSR0.multiply [" << chr.checkX(x_random) << "] " ;
    // --------------


    // --------------
    x_random = x;
    ELL.multiply_omp(b_random, x_random);

    cout << " ELL.multiply [" << chr.checkX(x_random) << "] " ;
    // --------------


    // --------------
    x_random = x;
    ELL0.multiply_omp(b_random, x_random);

    cout << " ELL0.multiply [" << chr.checkX(x_random) << "] " ;
    // --------------


    #if defined(YAKUTA)
    // set up yakutat
    // --------------
    auto [prt_gA, idx_gA, val_gA] = gA.createPressureMatrixCSR();

    // --------------
    auto row_gA = prt_gA.size()-1;
    // --------------


    cout << "set [Yakuta] ELL" << endl; 


    for (int row = 0; row < row_gA; ++row){
        for (int j = prt_gA[row]; j < prt_gA[row+1] ;++j){

            mat_ELL_yakutat.set(row, idx_gA[j], val_gA[j]);
        }
    }

    cout << "SET Yakuta CSR" << endl; 


    // for (int row = 0; row < row_gA; ++row){
    //     for (int j = prt_gA[row]; j < prt_gA[row+1] ;++j){

    //         mat_CSR_yakutat.set(row, idx_gA[j], val_gA[j]);
    //     }
    // }

    // cout << "FINI Yakuta matrix" << endl; 
    #endif 

    static auto s = [&](auto a){ return std::to_string(a);};

    static auto ws = [&](auto a){ std::ostringstream s; s << a; return s.str();};

    int j = 0;



    // -------------------------

    for (int i = 0; i < time ;i ++)
        SPE.multiply(b, x);


    ch.ini(x,b);
    // -------------------------



    // -------------------------
    Timer_Seq[j].init_start();

    for (int i = 0; i < time ;i ++)
        CSR.multiply(b, x);

    Timer_Seq[j++].stop();
    cout << "check CSR.multiply [" << ch.checkX(x,b) << "] " ;
    // -------------------------


    // -------------------------
    Timer_Seq[j].init_start();

    for (int i = 0; i < time ;i ++)
        CSR0.multiply(b, x);

    Timer_Seq[j++].stop();
    cout << ", check CSR0[" << ch.checkX(x,b) << "] " ;
    // -------------------------



    // -------------------------
    Timer_Seq[j].init_start();

    for (int i = 0; i < time ;i ++)
        ELL.multiply(b, x);

    Timer_Seq[j++].stop();
    cout << ", check ELL[" << ch.checkX(x,b) << "] " ;
    // -------------------------


    // -------------------------
    Timer_Seq[j].init_start();

    for (int i = 0; i < time ;i ++)
        ELL0.multiply(b, x);

    Timer_Seq[j++].stop();
    cout << ", check ELL0[" << ch.checkX(x,b) << "] " ;
    // -------------------------

    // -------------------------
    Timer_Seq[j].init_start();

    for (int i = 0; i < time ;i ++)
        SPE.multiply(b, x);


    Timer_Seq[j++].stop();
    cout << ", check SPE[" << ch.checkX(x,b) << "] " ;
    // -------------------------


    // -------------------------
    Timer_Seq[j].init_start();

    for (int i = 0; i < time ;i ++)
        SPE0.multiply(b, x);

    Timer_Seq[j++].stop();
    // -------------------------

    cout << ", check SPE0[" << ch.checkX(x,b)  ;
    // -------------------------



    // -------------------------
    Timer_Seq[j].init_start();

    for (int i = 0; i < time ;i ++)
        SPE1.multiply(b, x);

    Timer_Seq[j++].stop();
    // -------------------------

    cout << ", check SPE1[" << ch.checkX(x,b)  ;
    // -------------------------



    #if defined(YAKUTA)

    // -------------------------

    Timer_Seq[j].init_start();
    for (int i = 0; i < time ;i ++)
        x = mat_ELL_yakutat.multiply(b);
    Timer_Seq[j++].stop();

    // -------------------------

    cout << ", check ELL_yakutat[" << ch.checkX(x,b) ;
    // -------------------------
    #endif
        
    // Timer_Seq[j].init_start();
        
    // for (int i = 0; i < time ;i ++)
    //     x = mat_CSR_yakutat.multiply(b);

    // Timer_Seq[j++].stop();
    // // -------------------------

    // cout << ", check CSR_yakutat[" << ch.checkX(x,b) << "]  \nOMP\n" ;
    // // -------------------------


    cout <<"\n#threads(init) :" << omp_get_max_threads() << endl;

    vector<double> x_Naff(x.size());
    vector<double> b_Naff(x.size());

    math::init(x_Naff, x);
    math::init(b_Naff, b);



    for (int Nthreads = 1; Nthreads <= 20; ++Nthreads)
    {

        omp_set_num_threads(Nthreads);

        // -------------------------
        Timer[0].init_start();

        for (int i = 0; i < time ;i ++)
            CSR.multiply_omp(b_Naff, x_Naff);

        Timer[0].stop();

        cout << ", check CSR[" << ch.checkX(x,b) << "] " ;
        // -------------------------

        // -------------------------
        Timer[1].init_start();

        for (int i = 0; i < time ;i ++)
            CSR0.multiply_omp(b_Naff, x_Naff);

        Timer[1].stop();

        cout << ", check CSR0[" << ch.checkX(x,b) << "]" ;
        // -------------------------


        // -------------------------
        Timer[2].init_start();

        for (int i = 0; i < time ;i ++)
            ELL0.multiply_omp_Point(b_Naff, x_Naff);

        Timer[2].stop();

        cout << ", check ELL[" << ch.checkX(x,b) << "] " ;
        // -------------------------

        // -------------------------
        Timer[3].init_start();

        for (int i = 0; i < time ;i ++)
            ELL0.multiply_omp(b_Naff, x_Naff);

        Timer[3].stop();

        cout << ", check ELL0[" << ch.checkX(x,b) << "] " ;
        // -------------------------


        // -------------------------
        Timer[4].init_start();

        for (int i = 0; i < time ;i ++)
            SPE.multiply_omp(b_Naff, x_Naff);

        Timer[4].stop();

        cout << ", check SPE[" << ch.checkX(x,b) << "] " ;
        // -------------------------

        // -------------------------
        Timer[5].init_start();

        for (int i = 0; i < time ;i ++)
            SPE0.multiply_omp(b_Naff, x_Naff);

        Timer[5].stop();
        cout << ", check SPE0[" << ch.checkX(x,b) << "] " ;
        // -------------------------



        // -------------------------
        Timer[6].init_start();

        for (int i = 0; i < time ;i ++)
            SPE1.multiply_omp(b_Naff, x_Naff);

        Timer[6].stop();
        cout << ", check SPE1[" << ch.checkX(x,b) << "] " ;
        // ------------------


    #if defined( YAKUTA)
        // -------------------------

        Timer[7].init_start();
        for (int i = 0; i < time ;i ++)
            x = mat_ELL_yakutat.multiply(b);
        Timer[7].stop();

        // -------------------------

        cout << ", check ELL_yakutat[" << ch.checkX(x,b) ;
        // -------------------------
    #endif 
            
        // Timer[8].init_start();
            
        // for (int i = 0; i < time ;i ++)
        //     x = mat_CSR_yakutat.multiply(b);

        // Timer[8].stop();
        // // -------------------------

        // cout << ", check CSR_yakutat[" << ch.checkX(x,b);
        // // -------------------------


        cout <<"\n#threads :" << omp_get_max_threads() << endl;

        int i = 0;
        cout << "Timer[CSR] " << 
                "\tSpeedUP "   <<Timer_Seq[i].elapsedTime() /  Timer[i].elapsedTime() ;
        cout << "\tSpeedUP(0) "<<Timer_Seq[0].elapsedTime() /  Timer[i].elapsedTime() << endl;
        i++;

        cout << "Timer[CSR0] " << 
                "\tSpeedUP "   <<Timer_Seq[i].elapsedTime() /  Timer[i].elapsedTime() ;
        cout << "\tSpeedUP(0) "<<Timer_Seq[0].elapsedTime() /  Timer[i].elapsedTime() << endl;
        i++;

        cout << "Timer[ELL] "  << 
                "\tSpeedUP "   <<Timer_Seq[i].elapsedTime() /  Timer[i].elapsedTime() ;
        cout << "\tSpeedUP(0) "<<Timer_Seq[0].elapsedTime() /  Timer[i].elapsedTime() << endl;
        i++;

        cout << "Timer[ELL0] " << 
                "\tSpeedUP "   <<Timer_Seq[i].elapsedTime() /  Timer[i].elapsedTime() ;
        cout << "\tSpeedUP(0) "<<Timer_Seq[0].elapsedTime() /  Timer[i].elapsedTime() << endl;
        i++;

        cout << "Timer[SPE] "   << 
                "\tSpeedUP "   <<Timer_Seq[i].elapsedTime() /  Timer[i].elapsedTime() ;
        cout << "\tSpeedUP(0) "<<Timer_Seq[0].elapsedTime() /  Timer[i].elapsedTime() << endl;
        i++;

        cout << "Timer[SPE0] "   << 
                "\tSpeedUP "   <<Timer_Seq[i].elapsedTime() /  Timer[i].elapsedTime() ;
        cout << "\tSpeedUP(0) "<<Timer_Seq[0].elapsedTime() /  Timer[i].elapsedTime() << endl;
        i++;


        cout << "Timer[SPE1] "   << 
                "\tSpeedUP "   <<Timer_Seq[i].elapsedTime() /  Timer[i].elapsedTime() ;
        cout << "\tSpeedUP(0) "<<Timer_Seq[0].elapsedTime() /  Timer[i].elapsedTime() << endl;
        i++;

    #if defined( YAKUTA)

        cout << "Timer[yaELL] "   << 
                "\tSpeedUP "   <<Timer_Seq[i].elapsedTime() /  Timer[i].elapsedTime() ;
        cout << "\tSpeedUP(0) "<<Timer_Seq[0].elapsedTime() /  Timer[i].elapsedTime() << endl;
        i++;
    #endif 

        // cout << "Timer[yaCSR] "   << 
        //         "\tSpeedUP "   <<Timer_Seq[i].elapsedTime() /  Timer[i].elapsedTime() ;
        // cout << "\tSpeedUP(0) "<<Timer_Seq[0].elapsedTime() /  Timer[i].elapsedTime() << endl;
        // i++;


        #pragma omp parallel
        {

            for (size_t i = 0 ; i < zone.size(); i++)
            {
                #pragma omp single
                {
                    A.at(i) += s(omp_get_num_threads()) ;
                    A.at(i) += tab;
                    A.at(i) += s(Timer_Seq[i].elapsedTime() /  Timer[i].elapsedTime());
                    A.at(i) += tab;
                    A.at(i) += s(Timer_Seq[0].elapsedTime() /  Timer[i].elapsedTime());
                    A.at(i) += "\n";
                }

            }
        }

    }


    std::ofstream file;
    std::string fileN = "SPMV.dat";
    file.open (fileN, std::ios::out); // |ios::app
    for (size_t i = 0 ; i < zone.size(); i++) { file << A.at(i); }  
    file.close();



    return 0;
}