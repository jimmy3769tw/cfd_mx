#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <omp.h>
#include <mpi.h>

#include "eigen/Eigen/Eigen"
#include "stopWatch.hpp"

#include <amgcl/backend/eigen.hpp>
#include <amgcl/make_solver.hpp>
#include <amgcl/solver/bicgstab.hpp>
#include <amgcl/amg.hpp>
#include <amgcl/coarsening/smoothed_aggregation.hpp>
#include <amgcl/relaxation/spai0.hpp>

int main(void){

    using value_type = double;
    using size_type = size_t;

    typedef Eigen::Triplet<value_type> Triple_data;

    std::vector<Triple_data> tripletList;
    size_type n = 5;

    Eigen::SparseMatrix<value_type, Eigen::RowMajor> A(n,n); 
    Eigen::VectorXd x(n), b(n);


    std::cout << A << std::endl;

    for(size_t i=0; i<n; ++i)
    {
        for(size_t j=0; j<n; ++j)
        {
            if(i!=j)
                A.insert(i,j) = i+j + 2*i + j;
        }
        b(i) = (i+1)^2;
        A.insert(i,i) = (i+2) * 12;
    }

    std::cout << A << std::endl;
    A.makeCompressed();
    std::cout << A << std::endl;



 // Setup the solver:
    typedef amgcl::make_solver<
        amgcl::amg<
            amgcl::backend::eigen<double>,
            amgcl::coarsening::smoothed_aggregation,
            amgcl::relaxation::spai0
            >,
        amgcl::solver::bicgstab<amgcl::backend::eigen<double> >
        > Solver;


    
    Solver solve(A);

    std::cout << solve << std::endl;

    auto [iter, error] = solve(b, x);

    //Computation
    std::cout << "#iterations:     " << iter    << std::endl;
    std::cout << "estimated error: " << error   << std::endl;
    std::cout << "===solver1===" << std::endl;

    for(auto v:x){
        cout << v << ", ";
    }

    return 0;
}