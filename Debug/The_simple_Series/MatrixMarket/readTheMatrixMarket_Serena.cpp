// mpicxx -std=c++17 -O3 -I ../../../inc -I /home/Jimmy/installBoost/boost_1_76_0 \
	-fopenmp -o2 -o readTheMatrixMarket.Check.exe readTheMatrixMarket.Check.cpp

#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <omp.h>
#include <tuple>


#include <solver/bicgstab.hpp>
#include <solver/bicgstabRe2.hpp>
#include <solver/bicgstab0.hpp>

#include "matrix/ELL_sparseMatrix.hpp"
#include "matrix/CSR_sparseMatrix.hpp"

#include "matrix/ELL_sparseMatrix0.hpp"
#include "matrix/CSR_sparseMatrix0.hpp"


#include <amgcl/backend/builtin.hpp>
#include <amgcl/adapter/crs_tuple.hpp>
#include <amgcl/make_solver.hpp>
#include <amgcl/amg.hpp>
#include <amgcl/coarsening/smoothed_aggregation.hpp>
#include <amgcl/relaxation/spai0.hpp>
#include <amgcl/relaxation/gauss_seidel.hpp>
#include <amgcl/solver/bicgstab.hpp>
#include <amgcl/profiler.hpp>
#include <amgcl/io/mm.hpp>

#include "profiling/STL_clock.hpp"


int main(int argc, char **argv)
{   



    auto compare = [&](auto &A , auto &B ) { 
        if (A.size() != B.size() )
        {
            cout << "Fal";
            return false;
        }
        
        double sum{0};

        for ( size_t i = 0; i < A.size() ; i++){
           sum += abs( A[i]-B[i]);
        }

        cout << " [DIF]: \t" << sum << endl;

        return true;    
    };


    int length = 5;

    // -----------------------------------------
    std::vector<stopWatch>  Timer(length);
    std::vector<stopWatch>  Timer_seq(length);
    std::vector<int>        iter(length);
    std::vector<double>     error(length);
    // -----------------------------------------


    vector<string> variables, zone, fileCont;
    string t = ", ";
    string tab = " ";
    string n = "\n";

    variables.push_back("processor");
    variables.push_back("Speed-up");
    variables.push_back("Speed-up (compare CSR0)");

    zone.push_back("AMGCL");
    zone.push_back("CSR");
    zone.push_back("CSR0");
    zone.push_back("ELL");
    zone.push_back("ELL0");


    fileCont.resize(zone.size());

    for (size_t i = 0 ; i < zone.size(); i ++)
    {
        fileCont.at(i)  += "TITLE     = \"\"\n";
        fileCont.at(i)  += "VARIABLES = \"";
        fileCont.at(i)  += variables.at(0);

        fileCont.at(i)  += "\",\"";
        fileCont.at(i)  += variables.at(1);

        fileCont.at(i)  += "\",\"";
        fileCont.at(i)  += variables.at(2);

        fileCont.at(i)  += "\"\n";
        fileCont.at(i)  += "ZONE T=\"";
        fileCont.at(i)  +=  zone.at(i);
        fileCont.at(i)  += "\"\n";
    }
    // * ---------------------------------------init 



    // The matrix and the RHS file names should be in the command line options:
    // if (argc < 3) {
    //     std::cerr << "Usage: " << argv[0] << " <matrix.mtx> <rhs.mtx>" << std::endl;
    //     return 1;
    // }

    // The profiler:    
    amgcl::profiler<> prof("poisson3Db");

    // Read the system matrix and the RHS:
    int rows, cols;
    std::vector<int> ptr, col;
    std::vector<double> val, rhs;


    // ----------------------------
    prof.tic("read");
    std::tie(rows, cols) = amgcl::io::mm_reader("Serena/Serena.mtx")(ptr, col, val);
    std::cout << "Matrix " << "Serena/Serena.mtx" << ": " << rows << "x" << cols << std::endl;
    // ----------------------------
    rhs.resize(rows, 0.0);
    // std::tie(rows, cols) = amgcl::io::mm_reader("poisson3Db/poisson3Db_b.mtx")(rhs);
    // std::cout << "RHS " << "poisson3Db/poisson3Db_b.mtx" << ": " << rows << "x" << cols << std::endl;
    // prof.toc("read");

    // We use the tuple of CRS arrays to represent the system matrix.
    // Note that std::tie creates a tuple of references, so no data is actually
    // copied here:
    auto A = std::tie(rows, ptr, col, val);

    // ----------------------------
    // Compose the solver type
    //   the solver backend:
    typedef amgcl::backend::builtin<double> SBackend;
    //   the preconditioner backend:
    typedef amgcl::backend::builtin<double> PBackend;


    typedef amgcl::make_solver<
        amgcl::amg<
            PBackend,
            amgcl::coarsening::smoothed_aggregation,
            amgcl::relaxation::spai0
            >,
        amgcl::solver::bicgstab<SBackend>
        > Solver;
    // ----------------------------


    // ------------------------------
    std::tuple<std::vector<int>, std::vector<int>, std::vector<double> > csr = 
    std::tie(ptr, col, val);

    // Initialize the solver with the system matrix:
    prof.tic("setup");
    mat::CSR_matrix<double> matA_CSR(csr);
    mat::CSR_matrix0<double> matA_CSR0(csr);
    mat::ELL_matrix0<double> matA_ELL0(rows, 8);
    mat::ELL_matrix<double> matA_ELL(rows, 8);

    matA_ELL.set(csr);
    matA_ELL0.set(csr);
    // ------------------------------

    // ---------------------
    solver::bicgstabRe2<mat::CSR_matrix<double> > solverCSR(matA_CSR);
    solverCSR.setTolerance(1e-8);

    solver::bicgstabRe2<mat::ELL_matrix<double> >  solverELL(matA_ELL);
    solverELL.setTolerance(1e-8);
    // ---------------------

    // ---------------------
    solver::bicgstabRe2<mat::ELL_matrix0<double> >  solverELL0(matA_ELL0);
    solverELL0.setTolerance(1e-8);

    solver::bicgstabRe2<mat::CSR_matrix0<double> > solverCSR0(matA_CSR0);
    solverCSR0.setTolerance(1e-8);
    // ---------------------


    #if defined(AMG_CL_OFF)
    Solver solve(A);
    prof.toc("setup");
    // Show the mini-report on the constructed solver:
    std::cout << solve << std::endl;
    // Solve the system with the zero initial approximation:



    prof.tic("solve");
    // std::tie(iter[0], error[0]) = solve(A, rhs, x);
    #endif 

    // prof.toc("solve");

    std::vector<double> x(rows, 0.0);
    std::vector<double> x_csr(rows, 0.0);


    auto xPre = x;
    {
        // ------------------------------------------ FOR ELL
        // ----------------------------------------AMGCL
        #if defined(AMG_CL_OFF)
        std::fill(x.begin(),x.end(), 0.0);
        Timer_seq[0].init_start();
        std::tie(iter[0], error[0]) = solver(rhs, x);
        Timer_seq[0].stop();
        xPre = x;
        cout << "\nAMGCL " << "iter\t" << iter[0];
        file("ANGCL.dat",x);
        #endif

        omp_set_num_threads(1);
        // --------------------------------------- // 
        std::fill(x.begin(),x.end(), 0.0);
        // ---------------------------------------  
        Timer_seq[1].init_start();
        std::tie(iter[1], error[1]) = solverELL.solve(rhs, x);
        Timer_seq[1].stop();

        // ---------------------------------------  
        cout << "\nsolverELL " << "iter\t" << iter[1];
        compare(x, xPre);

        // --------------------------------------- // FOR CSR
        xPre = x;
        std::fill(x.begin(),x.end(), 0.0);
        // ---------------------------------------  
        Timer_seq[2].init_start();
        std::tie(iter[2], error[2]) = solverELL0.solve(rhs, x);
        Timer_seq[2].stop();
        // ---------------------------------------  
        cout << "\nsolverELL0 " << "iter\t" << iter[2];
        compare(x, xPre);

        // --------------------------------------- 
        xPre = x;
        std::fill(x.begin(),x.end(), 0.0);
        // ---------------------------------------  
        Timer_seq[3].init_start();
        std::tie(iter[3], error[3]) = solverCSR.solve(rhs, x);
        Timer_seq[3].stop();
        // ---------------------------------------  
        cout << "\nsolverCSR " << "iter\t" << iter[3];
        compare(x, xPre);

        // --------------------------------------- // FOR
        xPre = x;
        std::fill(x.begin(),x.end(), 0.0);
        // ---------------------------------------  
        Timer_seq[4].init_start();
        std::tie(iter[4], error[4]) = solverCSR0.solve(rhs, x);
        Timer_seq[4].stop();
        // ---------------------------------------  
        cout << "\nsolverCSR0 " << "iter\t" << iter[4];
        compare(x, xPre);
        xPre = x;
        // ---------------------------------------  
    }



    for (int Nthreads = 2; Nthreads <= 32; ++++Nthreads)
    {

        omp_set_num_threads(Nthreads);
        // ------------------------------------------ FOR ELL
        std::fill(x.begin(),x.end(), 0.0);
        // ---------------------------------------  
        Timer[1].init_start();
        std::tie(iter[1], error[1]) = solverELL.solve(rhs, x);
        Timer[1].stop();

        // ---------------------------------------  
        cout << "\nsolverELL " << "iter\t" << iter[1];
        compare(x, xPre);

        // --------------------------------------- // 
        xPre = x;
        std::fill(x.begin(),x.end(), 0.0);
        // ---------------------------------------  
        Timer[2].init_start();
        std::tie(iter[2], error[2]) = solverELL0.solve(rhs, x);
        Timer[2].stop();
        // ---------------------------------------  
        cout << "\nsolverELL0 " << "iter\t" << iter[2];
        compare(x, xPre);

        // --------------------------------------- 
        xPre = x;
        std::fill(x.begin(),x.end(), 0.0);
        // ---------------------------------------  
        Timer[3].init_start();
        std::tie(iter[3], error[3]) = solverCSR.solve(rhs, x);
        Timer[3].stop();
        // ---------------------------------------  
        cout << "\nsolverCSR " << "iter\t" << iter[3];
        compare(x, xPre);

        // --------------------------------------- // FOR
        xPre = x;
        std::fill(x.begin(),x.end(), 0.0);
        // ---------------------------------------  
        Timer[4].init_start();
        std::tie(iter[4], error[4]) = solverCSR0.solve(rhs, x);
        Timer[4].stop();
        // ---------------------------------------  
        cout << "\nsolverCSR0 " << "iter\t" << iter[4];
        compare(x, xPre);
        xPre = x;
        // ---------------------------------------  
        cout << "\n====================== # Thread"<< omp_get_max_threads() << ", " << Nthreads << "\n";
        cout << "ELL   " << "iter\t" << iter[1]<< "\tspeeUp \t" <<  Timer_seq[1].elapsedTime() / Timer[1].elapsedTime()  <<"\tspeeUp(CP) \t" <<  Timer_seq[1].elapsedTime() / Timer[1].elapsedTime()  <<  endl ;
        cout << "ELL0  " << "iter\t" << iter[2]<< "\tspeeUp \t" <<  Timer_seq[2].elapsedTime() / Timer[2].elapsedTime()  <<"\tspeeUp(CP) \t" <<  Timer_seq[1].elapsedTime() / Timer[2].elapsedTime()  <<  endl ;
        cout << "CSR   " << "iter\t" << iter[3]<< "\tspeeUp \t" <<  Timer_seq[3].elapsedTime() / Timer[3].elapsedTime()  <<"\tspeeUp(CP) \t" <<  Timer_seq[1].elapsedTime() / Timer[3].elapsedTime()  <<  endl ;
        cout << "CSR0  " << "iter\t" << iter[4]<< "\tspeeUp \t" <<  Timer_seq[4].elapsedTime() / Timer[4].elapsedTime()  <<"\tspeeUp(CP) \t" <<  Timer_seq[1].elapsedTime() / Timer[4].elapsedTime()  <<  endl ;
        cout << "\n====================== \n";


        static auto s = [&](auto a){ return std::to_string(a);};

        static auto ws = [&](auto a){ std::ostringstream s; s << a; return s.str();};


        #pragma omp parallel
        {

            for (size_t i = 1 ; i < zone.size(); i++)
            {
                #pragma omp single
                {
                    fileCont.at(i) += s(omp_get_num_threads()) ;
                    fileCont.at(i) += tab;
                    fileCont.at(i) += s(Timer_seq[i].elapsedTime() /  Timer[i].elapsedTime());
                    fileCont.at(i) += tab;
                    fileCont.at(i) += s(Timer_seq[1].elapsedTime() /  Timer[i].elapsedTime());
                    fileCont.at(i) += "\n";
                }

            }
        }

    }







    // for (int i = 0; i < x.size(); i++){
    //     // std::cout << x[i] << ", " << x_csr[i] << std::endl;
    //     std::cout << x[i] - x_csr[i] <<   std::endl;
    // }
    // Output the number of iterations, the relative error,
    // and the profiling data:
    // std::cout << "Iters: " << iter[0] << std::endl
    //           << "Error: " << error[0] << std::endl
    //           << prof << std::endl;

return 0;   
}