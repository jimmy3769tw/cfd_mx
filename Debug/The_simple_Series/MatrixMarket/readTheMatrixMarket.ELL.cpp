// mpicxx -std=c++17 -O3 -I ../../../inc -I /home/Jimmy/installBoost/boost_1_76_0 \
	-fopenmp -o2 -o readTheMatrixMarket.ELL.exe readTheMatrixMarket.ELL.cpp

#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <omp.h>
#include <mpi.h>

#include <amgcl/backend/builtin.hpp>
#include <amgcl/adapter/crs_tuple.hpp>
#include <amgcl/make_solver.hpp>
#include <amgcl/amg.hpp>
#include <amgcl/coarsening/smoothed_aggregation.hpp>
#include <amgcl/relaxation/spai0.hpp>
#include <amgcl/relaxation/gauss_seidel.hpp>
#include <amgcl/solver/bicgstab.hpp>
#include <amgcl/profiler.hpp>
#include <amgcl/io/mm.hpp>
#include "../inc/controlPanel.hpp"


int main(int argc, char **argv)
{   
    using size_type = size_t;
    using value_type = double;

    // // The matrix and the RHS file names should be in the command line options:
    // if (argc < 3) {
    //     std::cerr << "Usage: " << argv[0] << " <matrix.mtx> <rhs.mtx>" << std::endl;
    //     return 1;
    // }

    // The profiler:
    amgcl::profiler<> prof("poisson3Db");
    mat::CSR_matrix<double> matA_ell;


    // Read the system matrix and the RHS:
    int rows, cols;
    std::vector<int> ptr, col;
    std::vector<double> val, rhs;


    prof.tic("read");
    std::tie(rows, cols) = amgcl::io::mm_reader("poisson3Db/poisson3Db.mtx")(ptr, col, val);
    std::cout << "Matrix " << "poisson3Db/poisson3Db.mtx" << ": " << rows << "x" << cols << std::endl;

    std::tie(rows, cols) = amgcl::io::mm_reader("poisson3Db/poisson3Db_b.mtx")(rhs);
    std::cout << "RHS " << "poisson3Db/poisson3Db_b.mtx" << ": " << rows << "x" << cols << std::endl;
    prof.toc("read");



    // We use the tuple of CRS arrays to represent the system matrix.
    // Note that std::tie creates a tuple of references, so no data is actually
    // copied here:
    // auto A = std::tie(rows, ptr, col, val);

//     // Compose the solver type
//     //   the solver backend:
//     typedef amgcl::backend::builtin<double> SBackend;
//     //   the preconditioner backend:
// #ifdef MIXED_PRECISION
//     typedef amgcl::backend::builtin<float> PBackend;
// #else
//     typedef amgcl::backend::builtin<double> PBackend;
// #endif
    
//     typedef amgcl::make_solver<
//         amgcl::amg<
//             PBackend,
//             amgcl::coarsening::smoothed_aggregation,
//             amgcl::relaxation::spai0
//             >,
//         amgcl::solver::bicgstab<SBackend>
//         > Solver;

    // Initialize the solver with the system matrix:


    std::tuple< std::vector<int>,
                    std::vector<int>,
                    std::vector<double> > s = std::tie(ptr, col, val);

    prof.tic("setup");
    matA_ell.set(s);
        // Solver solve(A);
    prof.toc("setup");

    // Show the mini-report on the constructed solver:
    // std::cout << solve << std::endl;

    // Solve the system with the zero initial approximation:
    int iters;
    double error;
    std::vector<double> x(rows, 0.0);

    prof.tic("solve");

    std::tie(iters, error) = matA_ell.npc_bicgstab_omp(rhs, x);

    prof.toc("solve");

    // Output the number of iterations, the relative error,
    // and the profiling data:
    std::cout << "Iters: " << iters << std::endl
              << "Error: " << error << std::endl
              << prof << std::endl;

    return 0;
}