#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <omp.h>
#include <mpi.h>

#include "eigen/Eigen/Eigen"
#include "stopWatch.hpp"

int main(void){

    using value_type = double;
    using size_type = size_t;
    typedef Eigen::Triplet<value_type> Triple_data;
    std::vector<Triple_data> tripletList;
    size_type n = 5;

    Eigen::SparseMatrix<value_type, Eigen::RowMajor> A(n,n); 
    Eigen::VectorXd x(n), b(n);


    std::cout << A << std::endl;

    for(size_t i=0; i<n; ++i)
    {
        for(size_t j=0; j<n; ++j)
        {
            if(i!=j)
                A.insert(i,j) = i+j + 2*i + j;
        }
        b(i) = (i+1)^2;
        A.insert(i,i) = (i+2) * 12;
    }

    std::cout << A << std::endl;
    A.makeCompressed();
    std::cout << A << std::endl;


    //Computation
    std::cout << "\n===solver1===" << std::endl;
    Eigen::BiCGSTAB<Eigen::SparseMatrix<value_type> > solver1;
    solver1.compute(A);
    solver1.setTolerance(1e-15);
    x = solver1.solve(b);
    std::cout << "#iterations:     " << solver1.iterations() << std::endl;
    std::cout << "estimated error: " << solver1.error()      << std::endl;
    std::cout << "===solver1===" << std::endl;

    for(auto v:x){
        cout << v << ", ";
    }
    return 0;
}