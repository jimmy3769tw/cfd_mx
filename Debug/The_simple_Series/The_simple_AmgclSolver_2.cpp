//  mpicxx -std=c++11 -o2 -o  debug_hsuan.cpp
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <omp.h>
#include <mpi.h>


// #include "../../inc/matrix/ELL_sparseMatrix.hpp"
// #include <amgcl/backend/builtin.hpp>
// #include <amgcl/adapter/crs_tuple.hpp>
// #include <amgcl/make_solver.hpp>
// #include <amgcl/amg.hpp>
// #include <amgcl/coarsening/smoothed_aggregation.hpp>
// #include <amgcl/relaxation/spai0.hpp>
// #include <amgcl/relaxation/gauss_seidel.hpp>
// #include <amgcl/solver/bicgstab.hpp>
// #include <amgcl/profiler.hpp>

// #include "eigen/Eigen/Eigen"
// #include "stopWatch.hpp"

#include "../inc/controlPanel.hpp"

int main(int argc, char **argv)
{   

    // using size_type = size_t;
    // using value_type = double;

    // size_type n = 5;

    // //AMGCL
    // //================================================================================//
    // // * Initialize the timers for setup and solver phase
    // stopWatch setupTimer, solverTimer, Timer;


    // mat::ELL_matrix<value_type> lhs_mat(n, n);
    // std::vector<value_type> rhs(n, 0.0);
    
    // for(size_t i=0; i<n; ++i)
    // {
    //     for(size_t j=0; j<n; ++j)
    //     {
    //         if(i!=j)
    //         {
    //             lhs_mat.set(i,j, i+j + 2*i + j);
    //         }

    //     }
    //     lhs_mat.set(i,i, (i+2)*12);
    //     rhs[i] = (i+1)^2;

    // }
    
    // // 

    // auto data = lhs_mat.get_CSR();
    // size_t rows = lhs_mat.row();
    // auto ptr     =  std::get<0>(data);  // size_t
    // auto indices =  std::get<1>(data);  // szie_t
    // auto values  =  std::get<2>(data);  // double

    // auto amgcl_mat = std::tie(rows, ptr, indices, values);
    //     for (auto v : indices){
    //     cout << v << ", ";
    // }

    // //=========== Compose the solver type ===========//

    // //   the solver backend:
    // typedef amgcl::backend::builtin<value_type> SBackend;
    // //   the preconditioner backend:
    // typedef amgcl::backend::builtin<value_type> PBackend;

    // //=========== Compose the solver type ===========//

    // typedef amgcl::make_solver<
    //     amgcl::amg<
    //         PBackend,
    //         amgcl::coarsening::smoothed_aggregation,
    //         amgcl::relaxation::gauss_seidel
    //         >,
    //     amgcl::solver::bicgstab<SBackend>
    //     > Solver;

    

    // // * Initialize the values of iter, error, amgcl_x
    // size_t iters;
    // value_type error;
    // std::vector<value_type> amgcl_x(n, 0.0);
 

    // // * Initialize the solver with the system matrix:
    // setupTimer.start();
    // Solver solve(amgcl_mat);
    // setupTimer.stop();

    // solverTimer.start();
    // auto [iter, AAAA] = solve(amgcl_mat, rhs, amgcl_x); 
    // std::tie(iters, error) = solve(amgcl_mat, rhs, amgcl_x); 
    // solverTimer.stop();

    // //  * Ax = b
    
    // cout << "===============" << endl;
    // cout << "AMGCL" << endl;
    // for (size_t i = 0 ; i < n ;++i ){
    //     cout << amgcl_x[i] << "\n";
    // }



    // // * Output the number of iterations, the relative error,
    // // * and the profiling data:
    // std::cout << "Iters: " << iters << std::endl
    //           << "Error: " << error << std::endl;

    // std::cout << "setup cost time: "  << setupTimer.elapsedTime()   << std::endl;
    // std::cout << "solver cost time: " << solverTimer.elapsedTime() << std::endl;
    // std::cout << "other cost time: " << Timer.elapsedTime() << std::endl;

    // //================================================================================//
    // return 0;
}