//  conservativeResize()

// TODO :figuout


// The resize() method is a no-operation if the actual matrix size doesn't change; 
// otherwise it is destructive: the values of the coefficients may change.
//  If you want a conservative variant of resize()
// which does not change the coefficients, use conservativeResize(), see this page for more details.

#include <iostream>
#include <Eigen/Dense>
 
using namespace Eigen;
 
int main()
{
  MatrixXd m(2,5);
  m.conservativeResize(4,3);
  std::cout << "The matrix m is of size "
            << m.rows() << "x" << m.cols() << std::endl;

  m.resize(2,5);

  std::cout << "The matrix m is of size "
            << m.rows() << "x" << m.cols() << std::endl;


  m.conservativeResize(4,3);
  std::cout << "The matrix m is of size "
            << m.rows() << "x" << m.cols() << std::endl;

}
