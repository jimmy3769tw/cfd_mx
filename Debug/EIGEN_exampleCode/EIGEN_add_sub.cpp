#include <iostream>
#include <Eigen/Dense>
 



int main()
{

Eigen::Matrix2d a;

  a << 1, 2,
       3, 4;


// typedef Matrix<double, Dynamic, Dynamic> MatrixXd;
  Eigen::MatrixXd b(2,2);
  b << 2, 3,
       1, 4;
     
    
  std::cout << "a + b =\n" << a + b << std::endl;
  std::cout << "a - b =\n" << a - b << std::endl;
  std::cout << "Doing a += b;" << std::endl;
  a += b;


  std::cout << "Now a =\n" << a << std::endl;


//   in Eigen, vectors are just a special case of matrices, with either 1 row or 1 column. 

// ! typedef Matrix<float, 3, 1> Vector3f;

  Eigen::Vector3d v(1,2,3);
  Eigen::Vector3d w(1,0,0);

  std::cout << "-v + w - v =\n" << -v + w - v << std::endl;



// ! typedef Matrix<int, 1, 2> RowVector2i;


// Finally, we also offer some constructors to initialize the coefficients of small fixed-size vectors up to size 4:
// All Eigen matrices default to column-major storage order, but this can be changed to row-major, see Storage orders.

// ?Vector2d a(5.0, 6.0);
// ?Vector3d b(5.0, 6.0, 7.0);
// ?Vector4d c(5.0, 6.0, 7.0, 8.0);



}