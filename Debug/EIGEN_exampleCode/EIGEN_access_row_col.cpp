#include <Eigen/Dense>
#include <iostream>
 
using namespace std;
 
int main()
{
  Eigen::MatrixXf m(3,3);
  m << 1,2,3,
       4,5,6,
       7,8,9;
  cout << "\nHere is the matrix m:" << endl << m << endl;
  cout << "\n2nd Row: " << m.row(1) << endl;
  m.col(2) += 3 * m.col(0);
  cout << "\nAfter adding 3 times the first column into the third column, the matrix m is:\n";
  cout << m << endl;


  cout << m.col(2)<< endl;
}

