

// Column-major and row-major storage

// A= | 8 2 2 9 |
//    | 9 1 4 4 |
//    | 3 5 4 5 |

// * row major 8 2 2 9 9 1 4 4 3 5 4 5 


// * column-major  8 9 3 2 1 5 2 4 4 9 4 5 

#include <iostream>
#include <Eigen/Dense>


using namespace Eigen;
using namespace std;


//  ! Matrix<typename Scalar, int RowsAtCompileTime, int ColsAtCompileTime, ColMajor> // def

// If the storage order is not specified, then Eigen defaults to storing the entry in column-major. 
// This is also the case if one of the convenience typedefs (Matrix3f, ArrayXXd, etc.) is used.


//  ! Matrix<typename Scalar, int RowsAtCompileTime, int ColsAtCompileTime, RowMajor>

int main(void){


    Matrix<int, 3, 4, ColMajor> Acolmajor;
    // Comma-initialization
    Acolmajor << 8, 2, 2, 9,
                9, 1, 4, 4,
                3, 5, 4, 5;

    cout << "The matrix A:" << endl;
    cout << Acolmajor << endl << endl; 
    cout << "In memory (column-major):" << endl;
    for (int i = 0; i < Acolmajor.size(); i++)
    cout << *(Acolmajor.data() + i) << "  ";
    cout << endl << endl;






    Matrix<int, 3, 4, RowMajor> Arowmajor = Acolmajor;
    cout << "In memory (row-major):" << endl;
    for (int i = 0; i < Arowmajor.size(); i++)
    cout << *(Arowmajor.data() + i) << "  ";
    cout << endl;

}