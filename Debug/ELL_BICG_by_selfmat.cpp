
#include"../inc/matrix/ELL_sparseMatrix.hpp" //ELL_matrix
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <omp.h>

#include "eigen/Eigen/Eigen"
#include "stopWatch.hpp"

std::vector<double>  npc_bicgstab (
    std::vector <double> &B,
    mat::ELL_matrix <double> Amat
);

int main( int argc, char **argv)
{

    using value_type = double;
    using size_type = size_t;
    typedef Eigen::Triplet<value_type> Triple_data;
    std::vector<Triple_data> tripletList;
    size_type n = 10;

    Eigen::SparseMatrix<value_type, Eigen::RowMajor> A(n,n); 
    Eigen::VectorXd x(n), b(n);
    std::vector<value_type> b_v(n,0);

    // ! ----- my matA
    mat::ELL_matrix<double> Amat_ELL(n,n,n);

    // std::cout << A << std::endl;
    value_type val;
    value_type valB;


    //Computation

    for(size_t i=0; i<n; ++i)
    {
        for(size_t j=0; j<n; ++j)
        {
            if(i!=j){
                val = i+j + 2*i + j;
                Amat_ELL.set(i,j,val);
                A.insert(i,j) = val;
            }
        }
        valB = (i+1)^2;
        val = (i+2) * 12;
        b(i) = valB; b_v[i] = valB;
        A.insert(i,i) = val;
        Amat_ELL.set(i,i,val);
    }


    std::cout << A << std::endl;
    
    A.makeCompressed();
    
    std::cout << A << std::endl;


    std::cout << "\n===solver1===" << std::endl;
    
    Eigen::BiCGSTAB<Eigen::SparseMatrix<value_type> > solver1;


    // !-------------------- runtime -----------------------------

    solver1.compute(A);

    solver1.setTolerance(1e-15);
    

    x = solver1.solve(b);

    std::cout << "#iterations:     " << solver1.iterations() << std::endl;
    
    std::cout << "estimated error: " << solver1.error()      << std::endl;
    
    std::cout << "===solver1===" << std::endl;




    std::cout << "\n===solver2===" << std::endl;

    auto x_JM = npc_bicgstab(b_v, Amat_ELL);


    std::vector<double> x_JM_in(n);

    std::cout << "\n===RUN solver2===" << std::endl;
    
    auto [iter_seq, error_seq] = Amat_ELL.npc_bicgstab(b_v, x_JM_in);
    

    std::cout << "\n===RUN solver1===" << std::endl;
    std::vector<double> x_cg(n);
    auto [iter_seq_cg, error_seq_cg] = Amat_ELL.npc_cg(b_v, x_cg);

    std::vector<double> x_JM_in_omp(n);
    std::cout << "\n===RUN solver3 ===" << std::endl;
    auto [iter_omp, error_omp] = Amat_ELL.npc_bicgstab_omp(b_v, x_JM_in_omp);


    std::vector<double> x_pc_bicg(n);
    auto [iter_pc_bicg, error_pc_bicg] = Amat_ELL.pc_bicgstab(b_v, x_pc_bicg);



    // !-------------------- cout ------------------------------


    std::cout << "===ANS CG===" << std::endl;
    cout << iter_seq_cg << ", " << error_seq_cg << endl;
    for(auto v:x_cg){
        cout << v << ", ";
    }
    std::cout << "\n===ANS CG===" << std::endl;


    std::cout << "\n===Contanier seq===" << std::endl;
    cout << iter_seq << ", " << error_seq << endl;
    for(auto v:x_JM_in){
        cout << v << ", ";
    }
    std::cout << "\n===Contanier seq===" << std::endl;



    std::cout << "===ANS_1===" << std::endl;
    for(auto v:x_JM){
        cout << v << ", ";
    }
    std::cout << "\n===ANS_1===" << std::endl;


    std::cout << "\n===OUT SINE===" << std::endl;
    for(auto v:x){
        cout << v << ", ";
    }
    std::cout << "\n===OUT SINE===" << std::endl;


    std::cout << "\n===Contanier pc_bicg===" << std::endl;
    cout << iter_pc_bicg << ", " << error_pc_bicg << endl;
    for(auto v:x_pc_bicg){
        cout << v << ", ";
    }
    std::cout << "\n===Contanier pc_bicg===" << std::endl;



    std::cout << "\n===Contanier omp===" << std::endl;
    cout << iter_omp << ", " << error_omp << endl;
    for(auto v:x_JM_in_omp){
        cout << v << ", ";
    }
    std::cout << "\n===Contanier omp===" << std::endl;



    std::cout << "=== CP (2 results) ===" << std::endl;
    double dif =0;
    for(size_t i = 0; i < n ; i++){
        dif += std::abs( x_JM.at(i) - x(i) );
    }

    cout << "\n dif :" << dif<< endl;



    return 0;

}