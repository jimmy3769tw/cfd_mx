#include"../inc/matrix/ELL_sparseMatrix.hpp"
#include <iostream>
#include <vector>
#include <string>
#include <cmath>
#include <tuple> // cpp -11
#include <algorithm>
#include <iterator>
#include <numeric>
#include <functional>

#include <iostream>
#include <Eigen/Dense>

using namespace std;
// ref:: https://en.wikipedia.org/wiki/Biconjugate_gradient_stabilized_method

std::vector<double>  npc_bicgstab (
    std::vector <double> &B,
    mat::ELL_matrix <double> Amat
){

    // Eigen::Matrix2d mat;

    size_t ROW = B.size(); 

    vector<double> X(ROW,0.);
    vector<double> r(ROW,0.);
    vector<double> r_hat(ROW);

// ! npc BiCGSTAB

    //* 1.r0 = b − Ax0 // I.C. r @ i = 0
    auto TT = Amat * X;
    std::transform(B.begin(), B.end(), TT.begin(), r.begin(), std::minus<double>()); 

    //*2 r̂0 = r0 // r_hat[i] = const
    r_hat.assign(r.begin(),r.end());


    //*3 ρ0 = α = ω0 = 1
    double rho = 1, alpha = 1, omeaga = 1;

    // * 4. v0 = p0 = 0  //I.C.
    vector<double> v(ROW,0.);
    vector<double> p(ROW,0.);

    // * -----------------
    vector<double> s(ROW,0.);
    vector<double> t(ROW,0.);
    vector<double> h(ROW,0.);
    vector<double> rMin(ROW,0.);
    vector<double> XMin(ROW,0.);
    // * -----------------

    double beta = 0;
    double accurate = 1.0e-10;
    int which = 0;
//     //! 5. For i = 1, 2, 3, 
    size_t iter;
    for ( iter = 1 ; iter < 10 ;++iter){
        
        // for (size_t i = 0 ; i < ROW ;++i ){
        //     rMin[i] = r[i];
        //     vMin[i] = v[i];
        //     XMin[i] = X[i];
        // }
        rMin.assign(r.begin(), r.end());

        XMin.assign(X.begin(), X.end());

        // * 1.ρi = (r̂0, ri−1)
        double rhoMin = rho;
        // rho = 0;
        // for (size_t i = 0 ; i < ROW ;++i ){
        //     rho += r_hat[i] * r[i];
        // }
        rho = std::inner_product(r_hat.begin(), r_hat.end(),r.begin() ,0.0L);
        // * ---------------------



        // * 2.β = (ρi/ρi−1)(α/ωi−1)
        beta = (rho / rhoMin) * (alpha / omeaga);
        // * ---------------------


        // * 3.pi = r_{i−1} + β(p_{i−1} − ω_{i−1} * v_{i−1})
        // ! omeaga == omeagaMin;
        // ! p = pMin;
        // ! v = vMin;
        for (size_t i = 0 ; i < ROW ;++i ){
            p[i] = rMin[i] + beta * (p[i] - omeaga * v[i]);
        }
        // * ---------------------



        // * 4.vi = A p_i
        v = Amat * p;
        // * ---------------------



        // * 5.α = ρi/(r̂0, vi)
        // double temp = 0;
        // for (size_t i = 0 ; i < ROW ;++i ){
        //     temp += r_hat[i] * v[i];
        // }
        double temp = std::inner_product(r_hat.begin(), r_hat.end(),v.begin(),0.0L);
        alpha = rho / temp;
        // * ---------------------

        // * 6. h = x_{i−1} + α pi
        for (size_t i = 0 ; i < ROW ;++i ){
            h[i] =  XMin[i]+ alpha * p[i];
        }   
        // * ---------------------

        // * 7.If h is accurate enough, then set xi = h and quit
        temp = 0;
        for (size_t i = 0 ; i < ROW ;++i ){
            temp += std::abs(h[i] - X[i]);
        }

        if (temp < accurate){
            X.swap(h);
            break;
        }
        // * ---------------------

        // * 8.s = ri−1 − αvi
        for (size_t i = 0 ; i < ROW ;++i ){
            s[i] = rMin.at(i) - alpha * v.at(i);
        }
        // std::transform(rMin.begin(), rMin.end(), v.begin(), 
        //         std::back_inserter(s),[](double a, double b)
        //         {return a - alpha * b;});
        // * ---------------------

        // * 9. t = As
        t = Amat * s;
        // * ---------------------

        // * 10. ωi = (t, s)/(t, t)
        // ! befor 
        // temp = 0;
        // for (size_t i = 0 ; i < ROW ;++i ){
        //      temp += t[i] *  s[i];
        // }
        // omeaga = temp;
        // temp = 0;
        // for (size_t i = 0 ; i < ROW ;++i ){
        //     temp += t[i] *  t[i];
        // }
        // omeaga /= temp;

        // ! after 
        auto temp_10_1 = std::inner_product(t.begin(), t.end(),s.begin(),0.0);
        auto temp_10_2 = std::inner_product(t.begin(), t.end(),t.begin(),0.0);
        omeaga = temp_10_1/temp_10_2;
        // * ---------------------

        // * 11 xi = h + ωi*s
        for (size_t i = 0 ; i < ROW ;++i ){
            X[i] = h[i] + omeaga * s[i];
        }
        // * ---------------------
        
        // std::vector<double > TEMP_11(ROW,omeaga);
        // TODO:fix1
        // std::transform(TEMP_11.begin(), TEMP_11.end(), s.begin(), std::back_inserter(X), 
        //         [](double a, double b){return a*b;});
        // std::transform(h.begin(), h.end(),  s.begin(), std::back_inserter(X), 
        //         [](double a, double b){return a + b;});
        // TODO:fix2
        // std::transform(TEMP_11.begin(), TEMP_11.end(),  s.begin(), X.begin(), std::multiplies<double>());
        // std::transform(h.begin(), h.end(),  s.begin(), X.begin(), std::plus<double>());

        // * 12. If xi is accurate enough, then quit
        temp = 0;
        for (size_t i = 0 ; i < ROW ;++i ){
            temp += std::abs(XMin[i] - X[i]);
        }

        if (temp < accurate){
            break;
        }
        // * ---------------------

        // * 13 ri = s − ω_i t
        for (size_t i = 0 ; i < ROW ;++i ){
            r[i] = s[i] - omeaga * t[i];
        }
        // * ---------------------
    }
    cout << "[P_SOLVER_BICG_ELL] Iter: " << iter << endl;
    cout << "which:" << which<< endl;
    return X;
}
