
#include"../inc/matrix/ELL_sparseMatrix.hpp" //ELL_matrix
#include"../inc/matrix/CSR_sparseMatrix.hpp" //ELL_matrix
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <omp.h>

#include "eigen/Eigen/Eigen"
#include "stopWatch.hpp"
using namespace std;

int main( int argc, char **argv)
{

    using value_type = double;
    using size_type = size_t;
    size_type n = 10;
    std::vector<value_type> b_v(n,0);

    // ! ----- my matA
    mat::ELL_matrix<double> Amat_ELL(n,n,n);

    mat::CSR_matrix<double> Amat_CSR(n);

    value_type val;
    value_type valB;


    //Computation

    for(size_t i=0; i<n; ++i)
    {
        for(size_t j=0; j<n; ++j)
        {
            if(i!=j){
                val = i+j + 2*i + j;
                Amat_ELL.set(i,j,val);
            }
        }
        valB = (i+1)^2;
        val = (i+2) * 12;
        b_v[i] = valB;
        Amat_ELL.set(i,i,val);
    }

    auto j = Amat_ELL.get_CSR();
    Amat_CSR.set(j);
    
    std::vector<double> x_ELL(n, 0.0);
    std::vector<double> x_CSR(n, 0.0);
    Amat_ELL.npc_bicgstab(b_v, x_ELL);
    Amat_CSR.npc_bicgstab(b_v, x_CSR);

    cout << endl;
    for (auto v:x_ELL){
        cout << v << ", ";
    }


    cout << endl;
    for (auto v:x_CSR){
        cout << v << ", ";
    }



    return 0;
}