
#include <omp.h>
	// icpc -std=c++17 CSRcontanier_test.cpp -fopenmp -o CSR_Debug.exe
#include"../inc/matrix/ELL_sparseMatrix.hpp"
#include"../inc/matrix/CSR_sparseMatrix.hpp"

#include <iostream>
#include <vector>
#include <string>
#include <cmath>
#include <tuple> // cpp -11
#include <algorithm>
#include <iterator>
#include <numeric>
#include <functional>

using namespace std;


int main(void){


    int ROW = 20;
    size_t n = ROW;

    mat::ELL_matrix<double> my_ELL(ROW,ROW,ROW);
    mat::CSR_matrix<double> my_CSR(ROW,ROW);

    vector<double> B(ROW);

    
    for(size_t i=0; i<n; ++i)
    {
        for(size_t j=0; j<n; ++j)
        {
            if(i!=j){
                auto valA = i+j + 2*i + j;
                my_ELL.set(i, j, valA)  ;
            }

        }
        B.at(i) = (i+1)^2+3;

        auto valB = (i+2) * 12;

        my_ELL.set(i,i,valB)  ;
    }



    auto [prt_,idx_, val_] = my_ELL.get_CSR();

    auto j = my_ELL.get_CSR();
    my_CSR.set(j);
    cout << endl << "---------  B --------- \n";

    for (auto v:B){
        cout << v << ", ";
    }

    cout << endl << "---------  my_CSR --------- \n";

    auto d = my_CSR * B;
    for (auto v :d){
        cout << v << ", ";
    }

    cout << endl << "---------  my_ELL  --------- \n";

    auto c = my_ELL * B;

    for (auto v :c){
        cout << v << ", ";
    }





    return 0;
}