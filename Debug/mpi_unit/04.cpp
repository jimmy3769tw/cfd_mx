#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cmath>
#include <vector>
#include "mpi.h"
using namespace std;

// 
// This program normalizes a randomly genreated vector.
//
int main(int argc, char **argv) {
	int rank, size;
	int len = 0;

    // Vector
    std::vector<double> data_v;
    std::vector<int> displ_v;
    std::vector<int> count_v;
    std::vector<int> countOne_v;
    std::vector<double> myData_v;

	double mySum_v, length_v;


    // Array
	double *data=0;
	int *displ=0, *count=0, *countOne=0;
	double *myData=0;

	int myCount;
	double mySum, length;
	
	// Initialization
	MPI_Init(&argc, &argv);
	
	// Get environment information
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	// Generate some random data on the root
	if(rank == 0) {
		if( argc > 1 ) {
			len = atoi( argv[1] );
		}
		while( len <= 1 ) {
			cout << "\nPlease enter vector length: ";
			cin >> len;
		}


		data = new double[ len ];
        data_v.resize(len);


		srand(time(0));
		for(int i=0;i<len;++i) {
			data[i] = rand() / (double) RAND_MAX * 100.0;
		}


        for(size_t i = 0; i <len; ++i){
            data_v.at(i) =data[i]; 
        }

		displ = new int[ size ];
		count = new int[ size ];
		countOne = new int[ size ];

        displ_v.resize(size);
        count_v.resize(size);
        countOne_v.resize(size);

		// compute how many elements each process should handle
		for(int i=0;i<size;++i) {
			count[i] = len / size;
		}


		for(int i=0;i<size;++i) {
            count_v.at(i) = len/size;
		}

		count[size-1] = len - len / size * (size-1);
		count_v.at(size-1) = len - len / size * (size-1);

		// compute offsets
		displ[0] = 0;
		for(int i=0;i<size-1;++i) {
			displ[i+1] = displ[i] + count[i];
		}

		// compute offsets
        displ_v.at(0) = 0;
		for(int i=0;i<size-1;++i) {
            displ_v.at(i+1) = displ_v.at(i) + count_v.at(i);
		}
	}	

	double t1 = MPI_Wtime();

	// Scatter count onto all processes, and each process allocate just enough space to store data to be processed.
	MPI_Scatter(count_v.data(), 1, MPI_INT, &myCount, 1, MPI_INT, 0, MPI_COMM_WORLD);

	MPI_Scatter(count, 1, MPI_INT, &myCount, 1, MPI_INT, 0, MPI_COMM_WORLD);

	myData = new double[ myCount ];
    myData_v.resize(myCount);

	MPI_Scatterv( data, count, displ, MPI_DOUBLE, myData, myCount, MPI_DOUBLE, 0, MPI_COMM_WORLD );

	MPI_Scatterv( data_v.data(), count, displ_v.data(), MPI_DOUBLE, myData_v.data(), myCount, MPI_DOUBLE, 0, MPI_COMM_WORLD );

	// each process computes a local square sum
	mySum = 0;
	for(int i=0;i<myCount;++i) {
		mySum += myData[i] * myData[i];
	}

	mySum_v = 0;
	for(int i=0;i<myCount;++i) {
		mySum_v += myData_v[i] * myData_v[i];
	}


	// Add up all square-sums into length using MPI_Allreduce, so that all processes have the result
	MPI_Allreduce( &mySum, &length, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD );

	MPI_Allreduce( &mySum_v, &length_v, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD );



	length = sqrt( length );

    length_v = sqrt( length_v );



	// each process normalizes its own partial vector
	for(int i=0;i<myCount;++i) myData[i] /= length;

	for(int i=0;i<myCount;++i) myData_v[i] /= length_v;

	// gather all partial vectors onto root (rank==0)
	MPI_Gatherv( myData, myCount, MPI_DOUBLE, 
                data, count, displ, MPI_DOUBLE, 0, MPI_COMM_WORLD );

	MPI_Gatherv( myData_v.data(), myCount, MPI_DOUBLE, 
                data_v.data(), count, displ_v.data(), MPI_DOUBLE, 0, MPI_COMM_WORLD );


    for(size_t i = 0; i < myData_v.size() ; ++i ){

        auto abs = std::abs(myData_v.at(i) - myData[i]);
        if(abs>1.0-5){
            std::cout << "Rank i" << i << rank <<": " << abs << endl;
        }    
    }

    MPI_Barrier(MPI_COMM_WORLD);
	// at root
	if(rank==0) {
		if( argc>2 ) {	
			for(int i=0;i<len;++i) cout << data[i] << " "; 
			cout << endl;
		}
		t1 = MPI_Wtime() - t1;
		cout << "\nNormalization takes " << t1 << " seconds." << endl;
		delete []count;
		delete []displ;
		delete []countOne;
		delete []data;
	}

	delete []myData;

	// Finalize
	MPI_Finalize();
	
	return 0;
}