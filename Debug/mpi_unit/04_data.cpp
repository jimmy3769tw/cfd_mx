#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cmath>
#include <vector>
#include "mpi.h"
using namespace std;

// 

int main(int argc, char **argv){
    int rank, size;
    int len = 0;
    std::vector<double> data;
    std::vector<int> displ, count, countOne;


    int myCount;

    std::vector<double> myData;

    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    if (rank == 0){
        if (argc > 1){
            len = atoi(argv[1]);
        }
		while( len <= 1 ) {
			cout << "\nPlease enter vector length: ";
			cin >> len;
		}
        data.resize(len);
		srand(time(0));
		for(int i=0;i<len;++i) {
			data[i] = rand() / (double) RAND_MAX * 100.0;
		}

        displ.resize(size);
        count.resize(size);
        countOne.resize(size);

		// compute how many elements each process should handle
		for(int i=0;i<size;++i) {
			count[i] = len / size;
		}


		// compute offsets
		displ[0] = 0;
		for(int i=0;i<size-1;++i) {
			displ[i+1] = displ[i] + count[i];
		}
    }

	double t1 = MPI_Wtime();

	// Scatter count onto all processes, and each process allocate just enough space to store data to be processed.
	MPI_Scatter(count.data(), 1, MPI_INT, &myCount, 1, MPI_INT, 0, MPI_COMM_WORLD);
	myData.resize(myCount);
	MPI_Scatterv( data.data(), count.data(), displ.data(), MPI_DOUBLE, myData.data(), myCount, MPI_DOUBLE, 0, MPI_COMM_WORLD );

    double mySum, length;

	// each process computes a local square sum
	mySum = 0;
	for(int i=0;i<myCount;++i) {
		mySum += myData[i] * myData[i];
	}

	// Add up all square-sums into length using MPI_Allreduce, so that all processes have the result
	MPI_Allreduce( &mySum, &length, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD );
	length = sqrt( length );	

	// each process normalizes its own partial vector
	for(int i=0;i<myCount;++i) myData[i] /= length;

	// gather all partial vectors onto root (rank==0)
	MPI_Gatherv( myData.data(), myCount, MPI_DOUBLE, data.data(), count.data(), displ.data(), MPI_DOUBLE, 0, MPI_COMM_WORLD );

	// at root
	if(rank==0) {
		if( argc>2 ) {	
			for(int i=0;i<len;++i) cout << data[i] << " "; 
			cout << endl;
		}
		t1 = MPI_Wtime() - t1;
		cout << "\nNormalization takes " << t1 << " seconds." << endl;
	}

	// Finalize
	MPI_Finalize();
	
	return 0;
}