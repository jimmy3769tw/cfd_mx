#include <iostream>
#include <vector>
#include <chrono>
#include <cstdlib>
#include <algorithm>
#include <cmath>
#include <mpi.h>

using namespace std;

// This program simulates a rod with uniform unitial temperature of 25 degrees, and then heated at center to 100 degrees while keeping both ends fixed at 25 degrees.
// The program will 

// problem setup
const double alpha = 0.005;				 	// thermal diffusivity
const double criterion = 0.1; 				// maximum allowed change to be treated as steady state

// boundary conditions
const double heatX = 0.5;					// dimensionless x-coordinate
const double centerTemperature = 100.0;	 	// temperature at center
const double BCTemperature = 25.0;		  	// temperature at boundaries

// grid spacing
const auto dx = 0.01; 
const auto N = size_t(10.0 / dx + 1);		// total # of grid points

// time discretization
const auto dt = 0.25 * 0.5 * dx * dx / alpha;   // half of the upper limit of dt

// physical constant during calculation
const double C = alpha * dt / pow(dx,2);

int main(int argc, char **argv) {
	MPI_Init(&argc, &argv);

	int size, rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	// due to parallelization, each process now only have/need partial grid
	auto localN = (N + size - 1) / size;

	// compute where the center BC is
	auto center = int(heatX * N) ;
	auto [whoDoCenter, whoCenter] = div(center, localN);

	// adjust the last process to process the proper amount of grid points
	if( rank == size - 1 ) {
		localN = N - localN * (size-1);
	}

	// prepare ghost cells
	if( rank == 0 || rank == size-1 ) {
		localN+=1;
	}
	else {
		localN+=2;
	}
	if( whoDoCenter!=0 ) whoCenter++;		// adapt for ghost cell

	// storage for grid-points
	vector<double> T(localN, BCTemperature), T0(localN);	  

	auto t1 = chrono::steady_clock::now();

	auto it = 0;
	double residual;

	do {
		// apply BC
		if( rank == 0 ) {
			T[0] = BCTemperature;
		}
		else if( rank == size-1 ) {
			T[localN-1] = BCTemperature;
		}

		if( rank == whoDoCenter ) {
			T[ whoCenter ] = centerTemperature;			 // initial boundary condition
		}

		MPI_Status status[2];
		if( rank > 0 ) {
			// exchange ghost cells with the left.
			MPI_Send( T.data()+1, 1, MPI_DOUBLE, rank-1, 1, MPI_COMM_WORLD); 
			MPI_Recv( T.data()+0, 1, MPI_DOUBLE, rank-1, 1, MPI_COMM_WORLD, status ); 
		}
		if( rank < size - 1) {
			// Exchange ghost cells with the right.
			MPI_Send( T.data()+localN-2, 1, MPI_DOUBLE, rank+1, 1, MPI_COMM_WORLD); 
			MPI_Recv( T.data()+localN-1, 1, MPI_DOUBLE, rank+1, 1, MPI_COMM_WORLD, status+1 ); 
		} 
		T.swap(T0);

		residual = 0.0;
		auto tmp = 0.0; 
		for(size_t x=1;x<localN-1;++x) {
			double inc = C * (T0[x-1] + T0[x+1] - 2.0*T0[x]);
			T[x] = T0[x] + inc;
			tmp = std::max(tmp, abs(inc));
		}
		MPI_Allreduce(&tmp, &residual, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD); 

		++it;
	} while(residual>criterion); 
	auto t2 = chrono::steady_clock::now();

	if( rank == 0 ) 
		cout << "\n" << size << ":" << it << ":" << residual 
			<< ", " << chrono::duration_cast<chrono::nanoseconds>(t2-t1).count()/1.0e9 << " secs.";

	MPI_Finalize();

	return 0; 
}
