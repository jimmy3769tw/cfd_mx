
#include "controlPanel.hpp"

#if defined(PC_HYBRID_MPI_OMP)
#include "run/RUN_CPU_HYBRID_MPI.hpp"
#else
#include "run/RUN_CPU.hpp"
#include "run/RUN_CPU_PC_METHOD.hpp"
#endif

int main(int argc, char **argv) {
  clockstruct timer;
  simuClass simu;
  grid gA(40, 40, 40);

  // *   parameter
  gA.setLen(1, 1, 1);
  gA.setGridType("uniform");
  simu.set_time_max(40);
  simu.set_dt(0.001);
  simu.set_Re(10.0);
  simu.init_fileStep(1);
  // *   parameter  ------------
  // *  >>>>>>>>>>>>>----- RUNTIME SETTING
  // DFIB_Cylinder-X, DFIB_Cylinder-Z, "DFIB_Cylinder-Y", "OFF"
  simu.DfibMethod = "OFF";
  simu.Dfib_boolT = true;
  simu.Locality = 1;
  // *  >>>>>>>>>>>>>----- RUNTIME SETTING
  // ! RUN ============
  int mpi_word_rank = 0;
  if (mpi_word_rank == 0) {
    std::cout << "\n# MainLoop :" << simu.loop_max
              << "\n----------------------------------------------------\n";
  }
#if defined(PC_SEQ) || defined(PC_OMP)
  runSeqOmp(gA, timer, simu, argc, argv);
// runSeqOmp_predictionMethod(gA, timer, simu, argc, argv);
#elif defined(PC_HYBRID_MPI_OMP)
  Run_Hy_MPI_OpenMP(gA, timer, simu, argc, argv);
#endif
  // ! RUN RUN RUN ============
  std::cout << "Finish !\n";
  return 0;
}