#include "poisson.hpp"

#include "eigen/Eigen/Eigen"

// mpicxx -std=c++17 -O3 -fopenmp main.cpp -I ../inc/ -I /home/Jimmy/installBoost/boost_1_76_0 -fopenmp -I /mnt/c/Users/ -I /mnt/c/Users/user/includes/eigen/unsupported 


// icpc -std=c++17 -O3 -fopenmp main.cpp -I ../inc -I /mnt/c/Users/user/includes/eigen/unsupported -I /mnt/c/Users/user/includes
// ----------------------------------


Eigen::BiCGSTAB<Eigen::SparseMatrix<double> > solver_Eigen;
// ----------------------------------


//   the solver backend:
typedef amgcl::backend::builtin<double> SBackend;
//   the preconditioner backend:
typedef amgcl::backend::builtin<double> PBackend;

//=========== Compose the solver type ===========//

typedef amgcl::make_solver<
    amgcl::amg<
        PBackend,
        amgcl::coarsening::smoothed_aggregation,
        amgcl::relaxation::gauss_seidel
        >,
    amgcl::solver::bicgstab<SBackend>
    > Solver;

// #define AMG_CL_OFF

int main (){

    auto compare = [&](auto &A , auto &B ) { 
        if (A.size() != B.size() )
        {
            cout << "Fal";
            return false;
        }
        
        double sum{0};

        for ( size_t i = 0; i < A.size() ; i++){
           sum += abs( A[i]-B[i]);
        }

        cout << " [DIF]: \t" << sum << endl;

        return true;    
    };


    // !# set up 
    poisson();
    ELL_setUP();



    auto csr = std::tie(ptr, idx, val);

    size_t nnnn = nx*ny;

    auto csr_amgcl = std::tie(nnnn, ptr, idx, val);

    std::vector<Eigen::Triplet<double>> coefficients;

    Eigen::VectorXd matB_Eigen(nnnn);
    Eigen::SparseMatrix<double, Eigen::RowMajor> matA_Eigen(nnnn,nnnn);


    // buildProblem(coefficients, matB_Eigen, nnnn);
    auto estimation_of_entries  = 5*nnnn;
    coefficients.reserve(estimation_of_entries);

    for (int i = 0 ; i < nnnn; i++)
    for (int j = ptr[i]; j < ptr[i+1] ; j++){
        coefficients.push_back(Eigen::Triplet<double>( i, idx[j], val[j]));
    }

    matA_Eigen.setFromTriplets(coefficients.begin(), coefficients.end());


    Eigen::BiCGSTAB<Eigen::SparseMatrix<double, Eigen::RowMajor> > solver1(matA_Eigen);

    // Eigen::SimplicialCholesky<
    //     Eigen::SparseMatrix<double, Eigen::RowMajor> > 
    //         solver1(matA_Eigen);  // performs a Cholesky factorization of A

    Eigen::VectorXd x_Eigen = solver1.solve(matB_Eigen);
    std::cout << "#iterations:     " << solver1.iterations() << std::endl;
    std::cout << "estimated error: " << solver1.error()      << std::endl;
   

    // !## set  matA_CSR.set(csr);

    matA_CSR.set(csr);
    matA_CSR0.set(csr);
    matA_ELL0.set(csr);

    // ---------------------
    solver::bicgstabRe2<mat::CSR_matrix<double> > solverCSR(matA_CSR);
    solverCSR.setTolerance(1e-8);

    solver::bicgstabRe2<mat::ELL_matrix<double> >  solverELL(matA_ELL);
    solverELL.setTolerance(1e-8);
    // ---------------------

    // ---------------------
    solver::bicgstabRe2<mat::ELL_matrix0<double> >  solverELL0(matA_ELL0);
    solverELL0.setTolerance(1e-8);

    solver::bicgstabRe2<mat::CSR_matrix0<double> > solverCSR0(matA_CSR0);
    solverCSR0.setTolerance(1e-8);
    // ---------------------


    // !## set amgcl
    #if defined(AMG_CL_OFF)
    // -----------------------------------
    Solver solver(csr_amgcl);
    // -----------------------------------
    #endif
    // !# runtime 

    auto xPre = x;


    // ! ---------------------------------------init 
    int length = 5;

    std::vector<stopWatch>  Timer(length);
    std::vector<stopWatch>  Timer_seq(length);
    std::vector<int>        iter(length);
    std::vector<double>     error(length);

    vector<string> variables, zone, A;
    string t = ", ";
    string tab = " ";
    string n = "\n";

    variables.push_back("processor");
    variables.push_back("Speed-up");
    variables.push_back("Speed-up (compare CSR0)");

    zone.push_back("AMGCL");
    zone.push_back("CSR");
    zone.push_back("CSR0");
    zone.push_back("ELL");
    zone.push_back("ELL0");


    A.resize(zone.size());

    for (size_t i = 0 ; i < zone.size(); i ++)
    {
        A.at(i)  += "TITLE     = \"\"\n";
        A.at(i)  += "VARIABLES = \"";
        A.at(i)  += variables.at(0);

        A.at(i)  += "\",\"";
        A.at(i)  += variables.at(1);

        A.at(i)  += "\",\"";
        A.at(i)  += variables.at(2);

        A.at(i)  += "\"\n";
        A.at(i)  += "ZONE T=\"";
        A.at(i)  +=  zone.at(i);
        A.at(i)  += "\"\n";
    }
    // * ---------------------------------------init 



    {
        // ------------------------------------------ FOR ELL
        // ----------------------------------------AMGCL
        #if defined(AMG_CL_OFF)
        std::fill(x.begin(),x.end(), 0.0);
        Timer_seq[0].init_start();
        std::tie(iter[0], error[0]) = solver(rhs, x);
        Timer_seq[0].stop();
        xPre = x;
        cout << "\nAMGCL " << "iter\t" << iter[0];
        file("ANGCL.dat",x);
        #endif

        omp_set_num_threads(1);
        // --------------------------------------- // 
        std::fill(x.begin(),x.end(), 0.0);
        // ---------------------------------------  
        Timer_seq[1].init_start();
        std::tie(iter[1], error[1]) = solverELL.solve(rhs, x);
        Timer_seq[1].stop();
        file("solve.dat",x);

        // ---------------------------------------  
        cout << "\nsolverELL " << "iter\t" << iter[1];
        compare(x, xPre);

        // --------------------------------------- // FOR CSR
        xPre = x;
        std::fill(x.begin(),x.end(), 0.0);
        // ---------------------------------------  
        Timer_seq[2].init_start();
        std::tie(iter[2], error[2]) = solverELL0.solve(rhs, x);
        Timer_seq[2].stop();
        // ---------------------------------------  
        cout << "\nsolverELL0 " << "iter\t" << iter[2];
        compare(x, xPre);

        // --------------------------------------- 
        xPre = x;
        std::fill(x.begin(),x.end(), 0.0);
        // ---------------------------------------  
        Timer_seq[3].init_start();
        std::tie(iter[3], error[3]) = solverCSR.solve(rhs, x);
        Timer_seq[3].stop();
        // ---------------------------------------  
        cout << "\nsolverCSR " << "iter\t" << iter[3];
        compare(x, xPre);

        // --------------------------------------- // FOR
        xPre = x;
        std::fill(x.begin(),x.end(), 0.0);
        // ---------------------------------------  
        Timer_seq[4].init_start();
        std::tie(iter[4], error[4]) = solverCSR0.solve(rhs, x);
        Timer_seq[4].stop();
        // ---------------------------------------  
        cout << "\nsolverCSR0 " << "iter\t" << iter[4];
        compare(x, xPre);
        xPre = x;
        // ---------------------------------------  
    }




    for (int Nthreads = 2; Nthreads <= 32; ++++Nthreads)
    {

        omp_set_num_threads(Nthreads);
        // ------------------------------------------ FOR ELL
        std::fill(x.begin(),x.end(), 0.0);
        // ---------------------------------------  
        Timer[1].init_start();
        std::tie(iter[1], error[1]) = solverELL.solve(rhs, x);
        Timer[1].stop();

        // ---------------------------------------  
        cout << "\nsolverELL " << "iter\t" << iter[1];
        compare(x, xPre);

        // --------------------------------------- // 
        xPre = x;
        std::fill(x.begin(),x.end(), 0.0);
        // ---------------------------------------  
        Timer[2].init_start();
        std::tie(iter[2], error[2]) = solverELL0.solve(rhs, x);
        Timer[2].stop();
        // ---------------------------------------  
        cout << "\nsolverELL0 " << "iter\t" << iter[2];
        compare(x, xPre);

        // --------------------------------------- 
        xPre = x;
        std::fill(x.begin(),x.end(), 0.0);
        // ---------------------------------------  
        Timer[3].init_start();
        std::tie(iter[3], error[3]) = solverCSR.solve(rhs, x);
        Timer[3].stop();
        // ---------------------------------------  
        cout << "\nsolverCSR " << "iter\t" << iter[3];
        compare(x, xPre);

        // --------------------------------------- // FOR
        xPre = x;
        std::fill(x.begin(),x.end(), 0.0);
        // ---------------------------------------  
        Timer[4].init_start();
        std::tie(iter[4], error[4]) = solverCSR0.solve(rhs, x);
        Timer[4].stop();
        // ---------------------------------------  
        cout << "\nsolverCSR0 " << "iter\t" << iter[4];
        compare(x, xPre);
        xPre = x;
        // ---------------------------------------  
        cout << "\n====================== # Thread"<< omp_get_max_threads() << ", " << Nthreads << "\n";
        cout << "ELL   " << "iter\t" << iter[1]<< "\tspeeUp \t" <<  Timer_seq[1].elapsedTime() / Timer[1].elapsedTime()  <<"\tspeeUp(CP) \t" <<  Timer_seq[3].elapsedTime() / Timer[1].elapsedTime()  <<  endl ;
        cout << "ELL0  " << "iter\t" << iter[2]<< "\tspeeUp \t" <<  Timer_seq[2].elapsedTime() / Timer[2].elapsedTime()  <<"\tspeeUp(CP) \t" <<  Timer_seq[3].elapsedTime() / Timer[2].elapsedTime()  <<  endl ;
        cout << "CSR   " << "iter\t" << iter[3]<< "\tspeeUp \t" <<  Timer_seq[3].elapsedTime() / Timer[3].elapsedTime()  <<"\tspeeUp(CP) \t" <<  Timer_seq[3].elapsedTime() / Timer[3].elapsedTime()  <<  endl ;
        cout << "CSR0  " << "iter\t" << iter[4]<< "\tspeeUp \t" <<  Timer_seq[4].elapsedTime() / Timer[4].elapsedTime()  <<"\tspeeUp(CP) \t" <<  Timer_seq[3].elapsedTime() / Timer[4].elapsedTime()  <<  endl ;
        cout << "\n====================== \n";


        static auto s = [&](auto a){ return std::to_string(a);};

        static auto ws = [&](auto a){ std::ostringstream s; s << a; return s.str();};


        #pragma omp parallel
        {

            for (size_t i = 1 ; i < zone.size(); i++)
            {
                #pragma omp single
                {
                    A.at(i) += s(omp_get_num_threads()) ;
                    A.at(i) += tab;
                    A.at(i) += s(Timer_seq[i].elapsedTime() /  Timer[i].elapsedTime());
                    A.at(i) += tab;
                    A.at(i) += s(Timer_seq[3].elapsedTime() /  Timer[i].elapsedTime());
                    A.at(i) += "\n";
                }

            }
        }

    }

    std::ofstream file;
    std::string fileN = "poisson2DspeedUp.dat";
    file.open (fileN, std::ios::out); // |ios::app
    for (size_t i = 0 ; i < zone.size(); i++) { file << A.at(i); }  
    file.close();


    // file("ELl.dat",x);

    return 0;
}
