

#include <iostream>
#include <fstream>              // file output


#include <amgcl/backend/builtin.hpp>
#include <amgcl/adapter/crs_tuple.hpp>
#include <amgcl/make_solver.hpp>
#include <amgcl/amg.hpp>
#include <amgcl/coarsening/smoothed_aggregation.hpp>
#include <amgcl/relaxation/spai0.hpp>
#include <amgcl/relaxation/gauss_seidel.hpp>
#include <amgcl/solver/bicgstab.hpp>
#include <amgcl/profiler.hpp>


#include <solver/bicgstab.hpp>
#include <solver/bicgstabRe2.hpp>
#include <solver/bicgstab0.hpp>

#include "matrix/ELL_sparseMatrix.hpp"
#include "matrix/CSR_sparseMatrix.hpp"

#include "matrix/ELL_sparseMatrix0.hpp"
#include "matrix/CSR_sparseMatrix0.hpp"


// #include "profiling/STL_clock.hpp"

using namespace std;

int gC = 1;
int nxy = 600;

int nx = nxy, ny = nxy;

int nxt = nx+2*gC, 
    nyt = ny+2*gC;


double lx=1., ly = 1.;
double dx = lx/nx, dy = ly/ny;
int icel(int i, int j){
    return i*ny +j;
}



inline int icelShift(int i, int j){
    return (i-gC) + (j-gC)*nx ;
}

int ndim = nx*ny;

mat::CSR_matrix<double> matA_CSR(nx*ny, nx*ny);
mat::ELL_matrix<double> matA_ELL(nx*ny, nx*ny, 5);

mat::CSR_matrix0<double> matA_CSR0(nx*ny, nx*ny);
mat::ELL_matrix0<double> matA_ELL0(nx*ny, nx*ny, 5);


std::vector<double> rhs(ndim), x(ndim,0.0), val;

std::vector<int> ptr, idx;

// yakutat::SparseMatrixCSR<double> yakutatCSR(ndim);

auto poisson()
{

    int n2 = nx * ny;        // Number of points in the grid.

    ptr.clear(); ptr.reserve(n2 + 1); ptr.push_back(0);
    idx.clear(); idx.reserve(n2 * 5); // We use 5-point stencil, so the matrix
    val.clear(); val.reserve(n2 * 5); // will have at most n2 * 5 nonzero elements.

    rhs.resize(n2);

    for(int j = 0, k = 0; j < ny; ++j) {
        for(int i = 0; i < nx; ++i, ++k) {
            if (i == 0 || i == nx - 1 || j == 0 || j == ny - 1) {
                // Boundary point. Use Dirichlet condition.
                idx.push_back(k);
                val.push_back(1.0);

                rhs[k] = 0.0;
            } else {
                // Interior point. Use 5-point finite difference stencil.
                idx.push_back(k - ny);
                val.push_back(-1.0 / (dx*dx));

                idx.push_back(k - 1);
                val.push_back(-1.0 / (dx*dx));

                idx.push_back(k);
                val.push_back(2.0 / (dx*dx) + 2.0 / (dy*dy));

                idx.push_back(k + 1);
                val.push_back(-1.0 / (dy*dy));

                idx.push_back(k + ny);
                val.push_back(-1.0 / (dy*dy));

                rhs[k] = 1.0;
            }

            ptr.push_back(idx.size());
        }
    }
    return true;
}

// auto poisson_Yakuta()
// {
//     for (size_t row = 0; row < ptr.size() ; ++row)
//     for (size_t j = ptr[row]; j < ptr[row+1];++j)
//         yakutatCSR.set(row, idx[j], val[j]);
// }

auto ELL_setUP()
{
    matA_ELL.resize(ndim, 5);

    int k = 0;int count = 0;
    for (int j = gC; j < ny+gC; ++j)
    {
        for (int i = gC; i < nx+gC; ++i, k++ )
        {
            if (icelShift(i, j) != k){
                // if (count++ >10){break;}
                cout << icelShift(i,j) << " " << k << endl;
            }


            if (i == gC || j == gC || i== nx+gC-1 || j == ny+gC-1){
                matA_ELL.set(icelShift(i,j), icelShift(i, j), 1.0);
            } 
            else{
                matA_ELL.set(icelShift(i,j), icelShift(i-1,j), -1./(dx*dy));
                matA_ELL.set(icelShift(i,j), icelShift(i,j-1), -1./(dx*dy));
                matA_ELL.set(icelShift(i,j), icelShift(i,j),    4./(dx*dy));
                matA_ELL.set(icelShift(i,j), icelShift(i,j+1), -1./(dx*dy));
                matA_ELL.set(icelShift(i,j), icelShift(i+1,j), -1./(dx*dy));
            }
        }
    }

    // cout << "matA_ELL\n" ;
    // matA_ELL.Show_ELL();

    auto [ptr_ELL, idx_ELL, val_EKK] = matA_ELL.get_CSR();

    auto compare = [&](auto &A , auto &B ) { 
        if (A == B){
            cout << "\nT\n";
        }
        else {
            cout << "\nF\n";
        }
    };

    compare(ptr_ELL, ptr);
    compare(idx_ELL, idx);
    compare(val_EKK, val);


    return true;
}





void file(std::string fileN, std::vector<double> &Temp){

    std::vector<double> X(nx+1, 0), Y(ny+1, 0);

    for (int i = 1; i < nx+1; ++i)
        X[i] =X[i-1] + dx;

    for (int i = 1; i < ny+1; ++i)
        Y[i] = Y[i-1] + dy;

    ofstream fileQ;

    fileQ.precision(numeric_limits<double>::digits10 + 2);
    
    fileQ.open(fileN);

    fileQ << "TITLE= \"2D Navier-Stokes Solution\"" << "\n"
          << "VARIABLES= x, y, z, T" << "\n"
          << "ZONE T= \"Single Zone\""
          << ", I=" << to_string(nx)
          << ", J=" << to_string(ny)
          << ", K=1"
          << ", F=POINT" << "\n";

    for (int j = 0; j < ny; ++j)
    {
        for (int i = 0; i < nx; ++i)
        {
            
            fileQ << X.at(i) << "\t" << Y.at(j) << "\t" << 0.0 << "\t"
                  << Temp.at(i*ny +j) << "\n";
        }
    }

}
