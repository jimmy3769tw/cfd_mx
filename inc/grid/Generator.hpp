#pragma once

#include"controlPanel.hpp"


#include <chrono>
#include <vector>


bool firtTouch(
    grid& gA,
    calDomain& Lo
);

void generateGride(
    simuClass& simu,
    shareMenory& ShareM,
    calDomain& Lo,
    grid& gA
){

    auto [nx, ny, nz, gC] = gA.nxyzgC;

    gA.creatGrids();
    //  2 preparation for IO
    #pragma omp parallel for schedule(static)
    for (auto i = Lo.i_begin ; i < Lo.i_endof ; ++i )
    for (auto j = Lo.j_begin ; j < Lo.j_endof ; ++j )
    for (auto k = Lo.k_begin ; k < Lo.k_endof ; ++k )
    {
        gA.initNb(i,j,k);
    }

    // * 3 preparation 
    #pragma omp parallel for schedule(static)
    for (auto i = Lo.i_begin ; i < Lo.i_endof ; ++i )
    for (auto j = Lo.j_begin ; j < Lo.j_endof ; ++j )
    for (auto k = Lo.k_begin ; k < Lo.k_endof ; ++k )
    {
        gA.delta(i,j,k) = gA.Dx[i] * gA.Dy[j] * gA.Dz[k];
    }

}

void generateGride(
    simuClass& simu,
    calDomain& Lo,
    grid& gA
){
    gA.creatGrids();

    #pragma omp parallel for schedule(static)
    for (auto i = Lo.i_begin ; i < Lo.i_endof ; ++i )
    for (auto j = Lo.j_begin ; j < Lo.j_endof ; ++j )
    for (auto k = Lo.k_begin ; k < Lo.k_endof ; ++k )
    {
        gA.initNb(i,j,k);
    }

}